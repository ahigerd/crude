#include "appselector.h"

#include "wizard.h"

#include <QButtonGroup>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QRadioButton>
#include <QStandardPaths>
#include <QVBoxLayout>

AppSelector::AppSelector(
    QWidget* parent, Wizard* wizard, const QString& caption, const QString& type, const QStringList& options,
    const QString& value
)
: AppSelector(parent, wizard, caption, type, QString(), options, QString(), value)
{
  // forwarded constructor only
}

AppSelector::AppSelector(
    QWidget* parent, Wizard* wizard, const QString& caption, const QString& type, const QString& settingsKey,
    const QStringList& options, const QString& flagLabel, const QString& value
)
: QGroupBox(caption, parent), other(nullptr), otherFlags(nullptr), wizard(wizard), settingsKey(settingsKey)
{
  bool allowCustom = !settingsKey.isEmpty();

  QVBoxLayout* layout = new QVBoxLayout(this);
  layout->setSpacing(1);
  buttonGroup = new QButtonGroup(this);

  QRadioButton* comboRadio = nullptr;
  QString chooseMessage = QString("Choose a detected %1:").arg(type);
  if (allowCustom) {
    comboRadio = new QRadioButton(chooseMessage, this);
    buttonGroup->addButton(comboRadio, 0);
    layout->addWidget(comboRadio);
  } else {
    layout->addWidget(new QLabel(chooseMessage, this));
  }

  detected = new QComboBox(this);
  layout->addWidget(detected);

  layout->addSpacing(2);

  QRadioButton* otherRadio = nullptr;
  if (allowCustom) {
    otherRadio = new QRadioButton("Specify a command line:", this);
    buttonGroup->addButton(otherRadio, 1);
    layout->addWidget(otherRadio);

    other = new QLineEdit(this);
    other->setEnabled(false);
    layout->addWidget(other);
  }

  if (!flagLabel.isEmpty()) {
    layout->addSpacing(1);
    layout->addWidget(new QLabel(flagLabel));
    layout->addSpacing(1);
    otherFlags = new QLineEdit(this);
    layout->addWidget(otherFlags);
  } else {
    otherFlags = nullptr;
  }

  QString preload = value;
  if (value.isEmpty() && allowCustom && wizard->settings.contains(settingsKey)) {
    preload = wizard->settings.value(settingsKey).toString();
  }

  bool found = setOptions(options, preload);
  if (!allowCustom) {
    if (!found) {
      detected->setCurrentIndex(0);
    }
    return;
  }

  if (found) {
    // auto-detected
    comboRadio->setChecked(true);
    modeSwitch(0);
  } else if (wizard->settings.contains(settingsKey)) {
    // previously set but not auto-detected
    otherRadio->setChecked(true);
    QString t = wizard->settings.value(settingsKey).toString();
    if (otherFlags) {
      other->setText(t.section(' ', 0, 0));
      otherFlags->setText(t.section(' ', 1));
    } else {
      other->setText(t);
    }
    modeSwitch(1);
  } else {
    // not previously set and auto-detection failed
    detected->setCurrentIndex(0);
    comboRadio->setChecked(true);
    modeSwitch(0);
  }

  QObject::connect(buttonGroup, SIGNAL(buttonClicked(int)), this, SLOT(modeSwitch(int)));
  QObject::connect(detected, SIGNAL(currentIndexChanged(int)), this, SLOT(selectionChanged()));
  QObject::connect(other, SIGNAL(textEdited(QString)), this, SLOT(otherChanged()));
}

void AppSelector::selectionChanged()
{
  if (!settingsKey.isEmpty()) {
    wizard->settings.setValue(settingsKey, detected->currentData().toString());
  }
}

void AppSelector::otherChanged()
{
  QString cmd = other->text();
  if (otherFlags && !otherFlags->text().isEmpty()) {
    cmd += " " + otherFlags->text();
  }
  if (!QStandardPaths::findExecutable(cmd.section(' ', 0, 0)).isEmpty()) {
    wizard->settings.setValue(settingsKey, cmd);
  }
  emit validChanged();
}

void AppSelector::modeSwitch(int mode)
{
  if (mode == 0 && detected->count() == 0) {
    buttonGroup->button(0)->setChecked(false);
    buttonGroup->button(1)->setChecked(true);
    modeSwitch(1);
    return;
  }
  detected->setEnabled(!mode);
  other->setEnabled(mode);
  if (otherFlags) {
    otherFlags->setEnabled(mode);
  }
  if (mode) {
    otherChanged();
  } else {
    selectionChanged();
    emit validChanged();
  }
}

bool AppSelector::isValid() const
{
  if (detected->isEnabled()) {
    return true;
  }
  QString cmd = other->text();
  return !QStandardPaths::findExecutable(cmd).isEmpty();
}

bool AppSelector::setOptions(const QStringList& options, const QString& preload)
{
  QString current = preload.isEmpty() ? detected->currentData().toString() : preload;
  if (current.isEmpty() && detected->isEnabled() && !settingsKey.isEmpty()) {
    current = wizard->settings.value(settingsKey).toString();
  }
  detected->clear();
  for (const QString& option : options) {
    if (option == "builtin") {
      detected->addItem("Use CRUDE builtin", option);
    } else if (option.isEmpty()) {
      detected->addItem("Disabled", option);
    } else {
      detected->addItem(option.section(' ', 0, 0).replace("_", " "), option);
    }
  }
  if (!settingsKey.isEmpty()) {
    if (detected->count() == 0) {
      buttonGroup->button(0)->setEnabled(false);
      return false;
    }
    if (buttonGroup->button(0)->isChecked()) {
      buttonGroup->button(0)->setEnabled(true);
    }
  }
  int index = detected->findData(current);
  bool found = (index >= 0);
  if (!found) {
    index = detected->findData("builtin");
    if (index < 0) {
      index = 0;
    }
    // If there's no current setting, then that means the auto-detected option should be considered found.
    found = current.isEmpty();
  }
  detected->setCurrentIndex(index);
  if (detected->isEnabled()) {
    selectionChanged();
  }
  return found;
}
