#include "wizard.h"

#include "desktoppage.h"
#include "finishpage.h"
#include "libcrude/confignotifier.h"
#include "libcrude/environmentprefs.h"
#include "libcrude/platformintegration.h"
#include "terminalpage.h"
#include "welcomepage.h"

#include <QApplication>
#include <QLabel>
#include <QProcess>
#include <QScreen>
#include <QVBoxLayout>
#include <QtDebug>

#include <signal.h>

QLabel* Wizard::addLabel(QVBoxLayout* layout, const QString& text)
{
  QLabel* label = new QLabel(text);
  label->setWordWrap(true);
  layout->addWidget(label);
  return label;
}

Wizard::Wizard(QWidget* parent)
: QWizard(parent), settings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-environment")
{
  setWindowTitle("CRUDE Setup Wizard");
  addPage(new WelcomePage(this));
  TerminalPage* tp = new TerminalPage(this);
  addPage(tp);
  DesktopPage* dp = new DesktopPage(this);
  addPage(dp);
  addPage(new FinishPage(this));
  QObject::connect(this, SIGNAL(rescanFinished()), tp, SLOT(rescan()));
  QObject::connect(this, SIGNAL(rescanFinished()), dp, SLOT(rescan()));

  setOption(QWizard::HaveCustomButton1);
  setOption(QWizard::NoCancelButtonOnLastPage);
  setButtonText(QWizard::CustomButton1, "&Terminal");
  setButtonLayout(
      QList<QWizard::WizardButton>() << QWizard::CustomButton1 << QWizard::Stretch << QWizard::BackButton
                                     << QWizard::NextButton << QWizard::FinishButton << QWizard::CancelButton
  );
  QObject::connect(this, SIGNAL(customButtonClicked(int)), this, SLOT(openTerminal()));

  settings.setValue("incomplete", true);
  setAttribute(Qt::WA_DeleteOnClose, true);

  QObject::connect(this, SIGNAL(accepted()), this, SLOT(finalize()));
}

void Wizard::openTerminal()
{
  QStringList args;
  if (settings.contains("terminal")) {
    args = settings.value("terminal").toString().split(" ");
  } else {
    args = EnvironmentPrefs::terminalCommand();
  }
  QString cmd = args.takeFirst();
  QProcess* process = new QProcess(this);
  QObject::connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(rescan()));
  process->start(cmd);
  if (!process->waitForStarted()) {
    // TODO: error
  }
}

void Wizard::rescan()
{
  EnvironmentPrefs::rescanTerminals(this, SIGNAL(rescanFinished()));
}

void Wizard::finalize()
{
  settings.remove("incomplete");
  settings.setValue("version", CRUDE_VERSION);
  settings.sync();
  ConfigNotifier notifier;
  notifier.broadcast();
}
