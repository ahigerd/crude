#ifndef CRUDE_EMERGENCYWM_H
#define CRUDE_EMERGENCYWM_H

#include "emergencywindow.h"
#include "libcrude/qpointerset.h"
#include "libcrude/windowlist.h"
#include "libcrude/xcb/xcbintegration.h"

#include <QObject>

class EmergencyWM : public QObject, public XcbHandlerInterface
{
  Q_OBJECT
  friend class EmergencyWindow;

public:
  static EmergencyWM* instance();
  EmergencyWM(QObject* parent = nullptr);
  ~EmergencyWM();

  bool handleEvent(const CrudeXcb::EventType& event);

private slots:
  void windowShown(EmergencyWindow*);
  void windowHidden(EmergencyWindow*);
  void focusWindow(EmergencyWindow*);

private:
  EmergencyWindow* findWindow(WId id);

  QPointerSet<EmergencyWindow> windows;
};

#endif
