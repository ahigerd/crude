TEMPLATE = app
TARGET = crude-wizard
QT = core widgets x11extras
LIBS += -L.. -lcrude
include(../crude-common.pri)

SOURCES = main.cpp

HEADERS += wizard.h   appselector.h   welcomepage.h   terminalpage.h   desktoppage.h   finishpage.h
SOURCES += wizard.cpp appselector.cpp welcomepage.cpp terminalpage.cpp desktoppage.cpp finishpage.cpp

HEADERS += emergencywindow.h   emergencywm.h 
SOURCES += emergencywindow.cpp emergencywm.cpp

PRE_TARGETDEPS += ../libcrude.a

