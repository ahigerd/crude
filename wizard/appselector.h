#ifndef CRUDE_APPSELECTOR_H
#define CRUDE_APPSELECTOR_H

#include <QGroupBox>
class QComboBox;
class QLineEdit;
class QButtonGroup;
class Wizard;

class AppSelector : public QGroupBox
{
  Q_OBJECT

public:
  AppSelector(
      QWidget* parent, Wizard* wizard, const QString& caption, const QString& type, const QStringList& options,
      const QString& value
  );
  AppSelector(
      QWidget* parent, Wizard* wizard, const QString& caption, const QString& type, const QString& settingsKey,
      const QStringList& options, const QString& flagLabel = QString(), const QString& value = QString()
  );

  bool isValid() const;
  bool setOptions(const QStringList& options, const QString& preload = QString());

  QComboBox* detected;
  QLineEdit* other;
  QLineEdit* otherFlags;

private slots:
  void selectionChanged();
  void otherChanged();
  void modeSwitch(int);

signals:
  void validChanged();

private:
  Wizard* wizard;
  QString settingsKey;
  QButtonGroup* buttonGroup;
};

#endif
