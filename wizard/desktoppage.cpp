#include "desktoppage.h"

#include "appselector.h"
#include "libcrude/displaypower.h"
#include "libcrude/environmentprefs.h"
#include "libcrude/platformintegration.h"
#include "wizard.h"

#include <QLabel>
#include <QStandardPaths>
#include <QVBoxLayout>

static const QStringList knownDesktops{ "spacefm --desktop", "pcmanfm-qt --desktop", "pcmanfm --desktop", "nemo-desktop" };

static QStringList getScreenSavers()
{
  DisplayPower* dp = c_PI->displayPower();
  QStringList screenSaverOptions{ "" };
  QStringList lockers;
  auto allSavers = dp->scanScreenSavers();
  for (const auto& saver : allSavers) {
    if (saver.type == DisplayPower::Locker) {
      lockers << saver.name;
    }
  }
  for (const auto& saver : allSavers) {
    if (saver.type == DisplayPower::LockingScheduler) {
      screenSaverOptions << QString(saver.name).replace(" ", "_");
    } else if (saver.type == DisplayPower::Scheduler) {
      for (const QString& locker : lockers) {
        screenSaverOptions << QStringLiteral("%1 + %2").arg(saver.name).arg(locker).replace(" ", "_");
      }
    }
  }
  return screenSaverOptions;
}

DesktopPage::DesktopPage(Wizard* parent)
: QWizardPage(parent),
  wizard(parent),
  sessionSettings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-session")
{
  setTitle("Configure Desktop");
  setSubTitle("You may select a desktop manager and screen saver or locker.");

  QVBoxLayout* layout = new QVBoxLayout(this);
  layout->addStretch(1);

  dm = new AppSelector(this, wizard, "Desktop Manager", "desktop manager", "desktopmanager", scanDesktops());
  QObject::connect(dm, SIGNAL(validChanged()), this, SIGNAL(completeChanged()));
  layout->addWidget(dm);

  layout->addStretch(2);

  QString scheduler = sessionSettings.value("screensaver").toString();
  QString locker = sessionSettings.value("screenlocker").toString();
  QString selectedSaver;
  if (scheduler.isEmpty()) {
    selectedSaver = "Black_Screen";
  } else if (scheduler == "disabled") {
    selectedSaver = "";
  } else if (locker.isEmpty()) {
    selectedSaver = scheduler;
  } else {
    selectedSaver = QString("%1 + %2").arg(scheduler).arg(locker).replace(" ", "_");
  }

  ss = new AppSelector(this, wizard, "Screen Saver", "screen saver", getScreenSavers(), selectedSaver);
  QObject::connect(ss, SIGNAL(validChanged()), this, SIGNAL(completeChanged()));
  layout->addWidget(ss);

  layout->addStretch(1);
}

bool DesktopPage::isComplete() const
{
  return dm->isValid() && ss->isValid();
}

void DesktopPage::rescan()
{
  dm->setOptions(scanDesktops());
  ss->setOptions(getScreenSavers());
}

QStringList DesktopPage::scanDesktops()
{
  QStringList result;
  result << "builtin";
  for (const QString& cmd : knownDesktops) {
    QString name = cmd.section(' ', 0, 0, QString::SectionSkipEmpty);
    if (!QStandardPaths::findExecutable(name).isEmpty()) {
      result << cmd;
    }
  }
  return result;
}
