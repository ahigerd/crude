#include "emergencywindow.h"

#include "emergencywm.h"
#include "libcrude/xcb/xcbrequest_prop.h"

#include <QApplication>
#include <QColor>
#include <QMouseEvent>
#include <QPalette>
#include <QScreen>
#include <QStyleOptionTitleBar>
#include <QStylePainter>
#include <QWindow>
#include <QtDebug>

EmergencyWindow::EmergencyWindow(WId window, const QRect& geom)
: QWidget(nullptr, Qt::Window | Qt::BypassWindowManagerHint),
  child(QWindow::fromWinId(window)),
  focused(false),
  mapped(false)
{
  setAttribute(Qt::WA_NativeWindow, true);
  setAttribute(Qt::WA_TranslucentBackground, true);
  setContentsMargins(0, 0, 0, 0);

  callXcbChangeAttributes(
      window,
      XCB_CW_EVENT_MASK,
      { XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_RESIZE_REDIRECT | XCB_EVENT_MASK_PROPERTY_CHANGE |
        XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY }
  );
  callXcbChangeAttributes(
      this, XCB_CW_EVENT_MASK, { XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY }
  );

  wmName = callIcccmGetName(window)->takeResult();
  netWmName = callEwmhGetName(window)->takeResult();
  setWindowIcon(callEwmhGetIcon(window)->takeResult());
  setWindowTitle(netWmName.isEmpty() ? wmName : netWmName);
  childGeometry(geom);
}

void EmergencyWindow::paintEvent(QPaintEvent*)
{
  // This code is derived from QMdiSubWindow::paintEvent.

  QStylePainter painter(this);
  QStyleOptionFrame frameOptions;
  frameOptions.initFrom(this);
  frameOptions.state.setFlag(QStyle::State_Active, focused);

  painter.setFont(font());

  QStyleOptionTitleBar titleBarOptions;
  titleBarOptions.initFrom(this);
  if (windowIcon().isNull()) {
    titleBarOptions.icon = style()->standardIcon(QStyle::SP_TitleBarMaxButton);
  } else {
    titleBarOptions.icon = windowIcon();
  }
  titleBarOptions.subControls = QStyle::SC_All;
  titleBarOptions.text = windowTitle();
  titleBarOptions.state = frameOptions.state;
  titleBarOptions.titleBarFlags =
      Qt::Window | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint;
  titleBarOptions.titleBarState = focused ? QStyle::State_Active : QStyle::State_None;
  frameOptions.lineWidth = style()->pixelMetric(QStyle::PM_MdiSubWindowFrameWidth, 0, this);

  bool titleBarInsideBorder = !style()->styleHint(QStyle::SH_TitleBar_NoBorder, &titleBarOptions, this);
  int border = titleBarInsideBorder ? frameOptions.lineWidth : 0;
  int titleHeight = style()->pixelMetric(QStyle::PM_TitleBarHeight, &titleBarOptions, this);
  titleBarOptions.rect = QRect(border, border, width() - 2 * border, titleHeight - border);

  painter.fillRect(titleBarOptions.rect, QColor(32, 32, 64));
  painter.drawComplexControl(QStyle::CC_TitleBar, titleBarOptions);
  closeButton = style()->subControlRect(QStyle::CC_TitleBar, &titleBarOptions, QStyle::SC_TitleBarCloseButton, this);

  if (!titleBarInsideBorder) {
    painter.setClipRect(rect().adjusted(0, titleHeight, 0, 0));
  }
  painter.fillRect(frameOptions.rect, palette().color(QPalette::Window));
  painter.drawPrimitive(QStyle::PE_FrameWindow, frameOptions);
}

void EmergencyWindow::showEvent(QShowEvent*)
{
  if (!mapped) {
    QPoint offset = frameOffset();
    // temporarily suppress events so that the reparent doesn't cause confusion
    callXcbChangeAttributes(child, XCB_CW_EVENT_MASK, { 0 });
    callXcbMapWindow(child);
    callXcbReparentWindow(child, this, offset.x(), offset.y());
    // turn the events back on and more
    callXcbChangeAttributes(
        child,
        XCB_CW_EVENT_MASK,
        { XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_RESIZE_REDIRECT | XCB_EVENT_MASK_PROPERTY_CHANGE |
          XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY }
    );
    callXcbChangeAttributes(
        this,
        XCB_CW_EVENT_MASK,
        { XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_RESIZE_REDIRECT | XCB_EVENT_MASK_PROPERTY_CHANGE |
          XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY }
    );
    mapped = true;
  }
  emit shown(this);
}

void EmergencyWindow::hideEvent(QHideEvent*)
{
  emit hidden(this);
}

void EmergencyWindow::resizeEvent(QResizeEvent*)
{
  QStyleOptionTitleBar titleBarOptions;
  titleBarOptions.initFrom(this);
  int border = style()->pixelMetric(QStyle::PM_MdiSubWindowFrameWidth, 0, this);
  int titleHeight = style()->pixelMetric(QStyle::PM_TitleBarHeight, &titleBarOptions, this);
  callXcbSetGeometry(child, rect().adjusted(border, titleHeight, -border, -border));
}

void EmergencyWindow::mousePressEvent(QMouseEvent* event)
{
  if (!focused) {
    emit requestFocus(this);
  }
  raise();
  if (event->button() == Qt::LeftButton) {
    dragOffset = event->globalPos() - pos();

    if (closeButton.contains(event->pos())) {
      xcb_client_message_event_t event;
      event.response_type = XCB_CLIENT_MESSAGE;
      event.format = 32;
      event.window = child->winId();
      event.type = c_EWMH->WM_PROTOCOLS;
      event.data.data32[0] = callXcbInternAtom(false, "WM_DELETE_WINDOW")->takeResult();
      event.data.data32[1] = XCB_CURRENT_TIME;
      event.data.data32[2] = 0;
      event.data.data32[3] = 0;
      event.data.data32[4] = 0;
      callXcbSendEvent(true, child->winId(), XCB_EVENT_MASK_NO_EVENT, &event);
    }
  }
}

void EmergencyWindow::mouseMoveEvent(QMouseEvent* event)
{
  if (event->buttons() == Qt::LeftButton) {
    moveTo(event->globalPos() - dragOffset);
  }
}

void EmergencyWindow::setFocused(bool on)
{
  focused = on;
  update();
  if (focused) {
    xcb_ungrab_button(c_X11, XCB_BUTTON_INDEX_ANY, child->winId(), XCB_MOD_MASK_ANY);
  } else {
    xcb_grab_button(
        c_X11,
        1,
        child->winId(),
        XCB_EVENT_MASK_BUTTON_PRESS,
        XCB_GRAB_MODE_SYNC,
        XCB_GRAB_MODE_ASYNC,
        child->winId(),
        XCB_NONE,
        XCB_BUTTON_INDEX_ANY,
        XCB_MOD_MASK_ANY
    );
  }
}

void EmergencyWindow::childGeometry(xcb_get_geometry_reply_t* geom)
{
  childGeometry(QRect(geom->x, geom->y, geom->width, geom->height));
}

void EmergencyWindow::childGeometry(const QRect& geom)
{
  QPoint offset = frameOffset();

  resize(geom.width() + 2 * offset.x(), geom.height() + offset.x() + offset.y());
  moveTo(QPoint(geom.x() - offset.x(), geom.y() - offset.y()));
}

void EmergencyWindow::moveTo(const QPoint& pos)
{
  QRect screen = qApp->primaryScreen()->availableGeometry();
  QPoint newPos = pos;

  // Off the top or left side of the screen
  if (newPos.x() < 0) {
    newPos.setX(0);
  }
  if (newPos.y() < 0) {
    newPos.setY(0);
  }

  // New window in a bad position; cascade it
  if (!isVisible() && newPos.x() <= 0 && newPos.y() <= 0) {
    for (EmergencyWindow* win : EmergencyWM::instance()->windows) {
      if (!win->isVisible()) {
        continue;
      }
      if (win->x() > newPos.x()) {
        newPos.setX(win->x());
      }
      if (win->y() > newPos.y()) {
        newPos.setY(win->y());
      }
    }
    if (newPos.x() > 0 || newPos.y() > 0) {
      // Found a good position, now offset from that
      newPos.setX(newPos.x() + 32);
      newPos.setY(newPos.y() + 32);
    }
  }

  // Make sure we didn't go off the side of the screen
  if (newPos.x() + width() >= screen.right()) {
    newPos.setX(screen.right() - width());
  }
  if (newPos.y() + height() >= screen.bottom()) {
    newPos.setY(screen.bottom() - height());
  }

  move(newPos);
}

QPoint EmergencyWindow::frameOffset()
{
  QStyleOptionTitleBar titleBarOptions;
  titleBarOptions.initFrom(this);
  int border = style()->pixelMetric(QStyle::PM_MdiSubWindowFrameWidth, 0, this);
  int titleHeight = style()->pixelMetric(QStyle::PM_TitleBarHeight, &titleBarOptions, this);
  return QPoint(border, titleHeight);
}
