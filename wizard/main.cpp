#include "emergencywm.h"
#include "libcrude/platformintegration.h"
#include "wizard.h"

#include <QApplication>
#include <QMdiSubWindow>
#include <QScopedPointer>
#include <QScreen>

#include <cstdlib>

/* Planning notes:
 * - crude-environment.ini
 * - configured version setting, checked by crude-session to launch wizard
 *   - wizard checks to see if it should present upgrade or first-run options
 * - "incomplete" setting, set on wizard startup to indicate the need to resume if aborted
 * - choose terminal
 *   - add "open terminal" button
 * - choose window manager
 * - choose desktop manager or use builtin
 * - choose screensaver/locker or only use DPMS/ACPI
 * - choose notification daemon
 * - verify power/sound/network management options
 *   - save plugin selections to avoid needing to re-probe all the time
 * - optional advanced config with sane defaults:
 *   - choose Qt theme
 *     - session should assert using QT_STYLE_OVERRIDE after validating
 */

int main(int argc, char** argv)
{
  // Remove Qt overrides to try to failsafe the startup process
  ::setenv("QT_STYLE_OVERRIDE", "fusion", true);
  ::unsetenv("QT_QPA_PLATFORMTHEME");

  QApplication app(argc, argv);
  PlatformIntegrator platform(argc, argv);

  if (!c_PI->windowManagerExists()) {
    new EmergencyWM(qApp);
  }

  Wizard* wizard = new Wizard;
  QObject::connect(wizard, SIGNAL(finished(int)), qApp, SLOT(quit()));
  QObject::connect(wizard, SIGNAL(destroyed()), qApp, SLOT(quit()));
  QRect geom = wizard->geometry();
  QRect screen = qApp->primaryScreen()->geometry();
  geom.moveCenter(screen.center());
  wizard->setGeometry(geom);
  wizard->show();

  return app.exec();
}
