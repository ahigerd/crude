#ifndef CRUDE_DESKTOPPAGE_H
#define CRUDE_DESKTOPPAGE_H

#include <QSettings>
#include <QWizardPage>
class Wizard;
class AppSelector;

class DesktopPage : public QWizardPage
{
  Q_OBJECT

public:
  DesktopPage(Wizard* parent);

  bool isComplete() const;

public slots:
  void rescan();

private:
  Wizard* wizard;
  AppSelector* dm;
  AppSelector* ss;
  QSettings sessionSettings;

  QStringList scanDesktops();
};

#endif
