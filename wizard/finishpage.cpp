#include "finishpage.h"

#include "wizard.h"

#include <QApplication>
#include <QLabel>
#include <QVBoxLayout>

static QList<QPair<QString, QString>> lines = {
  { "Window Manager", "windowmanager" },
  { "Desktop Manager", "desktopmanager" },
  { "Screen Saver", "screensaver" },
  { "Terminal", "terminal" },
};

FinishPage::FinishPage(Wizard* parent) : QWizardPage(parent), wizard(parent)
{
  QVBoxLayout* layout = new QVBoxLayout(this);
  layout->setSpacing(16);

  setTitle("Setup Complete");
  setSubTitle("Your CRUDE configuration has been saved.");

  Wizard::addLabel(layout, "You have chosen the following settings:");

  choiceLabel = Wizard::addLabel(layout, "");

  Wizard::addLabel(
      layout,
      "To change your choices, you may run this wizard again using the \"crude-wizard\" "
      "command, or you can use the \"crude-config\" command to run the CRUDE Configurator to update these "
      "preferences as well as other available options."
  );

  Wizard::addLabel(layout, "Press the Finish button to start your CRUDE session.");
}

void FinishPage::initializePage()
{
  QStringList text;
  for (const auto& pair : lines) {
    QString cmd = wizard->settings.value(pair.second).toString().section(' ', 0, 0);
    text << QString("\t%1: %2").arg(pair.first).arg(cmd);
  }
  choiceLabel->setText(text.join('\n'));
}
