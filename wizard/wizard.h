#ifndef CRUDE_WIZARD_H
#define CRUDE_WIZARD_H

#include <QSettings>
#include <QWizard>
class QLabel;
class QVBoxLayout;

class Wizard : public QWizard
{
  Q_OBJECT

public:
  Wizard(QWidget* parent = nullptr);

  QSettings settings;

  static QLabel* addLabel(QVBoxLayout* layout, const QString& text);

signals:
  void rescanFinished();

public slots:
  void openTerminal();
  void rescan();
  void finalize();
};

#endif
