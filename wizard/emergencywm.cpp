#include "emergencywm.h"

#include "emergencywindow.h"
#include "libcrude/xcb/xcbrequest_prop.h"
#include "libcrude/xcb/xcbrequest_wm.h"

static EmergencyWM* EWM_instance = nullptr;

EmergencyWM* EmergencyWM::instance()
{
  return EWM_instance;
}

EmergencyWM::EmergencyWM(QObject* parent) : QObject(parent)
{
  EWM_instance = this;
  bool ok = XcbIntegration::instance()->enableWindowManagement();
  // TODO: handle failure
  XcbIntegration::instance()->addEventHandler(this);
}

EmergencyWM::~EmergencyWM()
{
  EWM_instance = nullptr;
}

bool EmergencyWM::handleEvent(const CrudeXcb::EventType& event)
{
  if (event.eventType == XCB_CLIENT_MESSAGE) {
    auto msg = event.cast<xcb_client_message_event_t>();
    if (msg->type == c_EWMH->_NET_ACTIVE_WINDOW) {
      EmergencyWindow* win = findWindow(msg->window);
      if (win) {
        focusWindow(win);
      }
    }
  } else if (event.eventType == XCB_BUTTON_PRESS) {
    auto be = event.cast<xcb_button_press_event_t>();
    xcb_allow_events(c_X11, XCB_ALLOW_REPLAY_POINTER, XCB_CURRENT_TIME);
    xcb_allow_events(c_X11, XCB_ALLOW_REPLAY_KEYBOARD, XCB_CURRENT_TIME);
    EmergencyWindow* win = findWindow(be->event);
    if (win) {
      focusWindow(win);
    }
  } else if (event.eventType == XCB_PROPERTY_NOTIFY) {
    auto pe = event.cast<xcb_property_notify_event_t>();
    EmergencyWindow* win = findWindow(pe->window);
    if (!win) {
      return false;
    }
    xcb_atom_t atom = pe->atom;
    if (atom == XCB_ATOM_WM_NAME) {
      win->wmName = callIcccmGetName(pe->window)->takeResult();
      if (win->netWmName.isEmpty()) {
        win->setWindowTitle(win->wmName);
        win->update();
      }
    } else if (atom == c_EWMH->_NET_WM_NAME) {
      win->netWmName = callEwmhGetName(pe->window)->takeResult();
      win->setWindowTitle(win->netWmName);
      win->update();
    } else if (atom == c_EWMH->_NET_WM_ICON) {
      win->setWindowIcon(callEwmhGetIcon(pe->window)->takeResult());
      win->update();
    }
  } else if (event.eventType == XCB_MAP_REQUEST) {
    auto me = event.cast<xcb_map_request_event_t>();
    EmergencyWindow* sub = findWindow(me->window);
    if (sub) {
      sub->show();
      focusWindow(sub);
    }
    return true;
  } else if (event.eventType == XCB_MAP_NOTIFY) {
    auto me = event.cast<xcb_map_notify_event_t>();
    EmergencyWindow* sub = findWindow(me->window);
    if (sub) {
      sub->show();
      focusWindow(sub);
    }
    return true;
  } else if (event.eventType == XCB_UNMAP_NOTIFY) {
    auto me = event.cast<xcb_unmap_notify_event_t>();
    EmergencyWindow* sub = findWindow(me->window);
    if (sub) {
      sub->hide();
    }
    return true;
  } else if (event.eventType == XCB_CONFIGURE_REQUEST) {
    auto ce = event.cast<xcb_configure_request_event_t>();
    EmergencyWindow* sub = findWindow(ce->window);
    if (!sub) {
      sub = findWindow(ce->parent);
    }
    if (sub) {
      sub->childGeometry(QRect(ce->x, ce->y, ce->width, ce->height));
    }
    return true;
  } else if (event.eventType == XCB_CREATE_NOTIFY) {
    auto ce = event.cast<xcb_create_notify_event_t>();
    if (ce->parent != QX11Info::appRootWindow() || ce->override_redirect) {
      return false;
    }
    EmergencyWindow* sub = new EmergencyWindow(ce->window, QRect(ce->x, ce->y, ce->width, ce->height));
    QObject::connect(sub, SIGNAL(shown(EmergencyWindow*)), this, SLOT(windowShown(EmergencyWindow*)));
    QObject::connect(sub, SIGNAL(hidden(EmergencyWindow*)), this, SLOT(windowHidden(EmergencyWindow*)));
    QObject::connect(sub, SIGNAL(requestFocus(EmergencyWindow*)), this, SLOT(focusWindow(EmergencyWindow*)));
    windows << sub;
  } else if (event.eventType == XCB_DESTROY_NOTIFY) {
    auto de = event.cast<xcb_destroy_notify_event_t>();
    EmergencyWindow* sub = findWindow(de->window);
    if (sub) {
      sub->deleteLater();
    }
  }
  // Don't consume events because other data structures need to know about it
  return false;
}

void EmergencyWM::windowShown(EmergencyWindow* w)
{
  windows << w;
  focusWindow(w);
}

void EmergencyWM::windowHidden(EmergencyWindow* w)
{
  if (!w->focused) {
    return;
  }
  for (EmergencyWindow* other : windows) {
    if (other != w && other->isVisible()) {
      focusWindow(other);
      break;
    }
  }
}

void EmergencyWM::focusWindow(EmergencyWindow* w)
{
  for (EmergencyWindow* other : windows) {
    if (other != w) {
      other->setFocused(false);
      other->update();
    }
  }
  callXcbSetFocus(w->child);
  callEwmhSetActiveWindow(w->child);
  w->setFocused(true);
  w->update();
  w->raise();
}

EmergencyWindow* EmergencyWM::findWindow(WId id)
{
  for (EmergencyWindow* win : windows) {
    if (win->child->winId() == id) {
      return win;
    }
  }
  return nullptr;
}
