#include "welcomepage.h"

#include "wizard.h"

#include <QApplication>
#include <QLabel>
#include <QVBoxLayout>

WelcomePage::WelcomePage(Wizard* parent) : QWizardPage(parent), wizard(parent)
{
  QVBoxLayout* layout = new QVBoxLayout(this);
  layout->setSpacing(16);

  QString oldVersion = wizard->settings.value("version").toString();
  if (qApp->arguments().contains("--bad-config")) {
    setTitle(QString("CRUDE version %1 - Setup Wizard").arg(CRUDE_VERSION));
    setSubTitle("An error was detected in your CRUDE configuration that prevents your session from starting.");
  } else if (oldVersion.isNull()) {
    setTitle(QString("CRUDE version %1 - First Time Setup").arg(CRUDE_VERSION));
    setSubTitle("This wizard will help you configure CRUDE for the first time.");
  } else if (oldVersion == CRUDE_VERSION) {
    setTitle(QString("CRUDE version %1 - Setup Wizard").arg(CRUDE_VERSION));
    setSubTitle("You have already configured CRUDE, but you have chosen to run this wizard again.");
  } else {
    setTitle(QString("Upgrading to CRUDE version %1").arg(CRUDE_VERSION));
    setSubTitle(QString("This wizard will help you update your configuration from CRUDE version %1.")
                    .arg(wizard->settings.value("version").toString()));
  }
  bool incomplete = wizard->settings.value("incomplete").toBool();

  if (incomplete) {
    Wizard::addLabel(
        layout,
        "Last time, this wizard was interrupted before completion. Any previous "
        "selections will be preserved here for confirmation."
    );
  }
  Wizard::addLabel(
      layout,
      "This wizard will analyze your system to make recommendations and find available "
      "options. You may cancel at any time to move ahead with the automatically detected defaults, and the "
      "wizard will run again next time you log in."
  );
  Wizard::addLabel(
      layout,
      "To change your choices, you may run this wizard again using the \"crude-wizard\" "
      "command, or you can use the \"crude-config\" command to run the CRUDE Configurator to update these "
      "preferences as well as other available options."
  );
  Wizard::addLabel(
      layout,
      "If you need to install additional packages during the setup process, you can open "
      "a terminal at any time using the \"Terminal\" button below. The wizard will rescan for available "
      "software when the terminal is closed."
  );
}
