#ifndef CRUDE_TERMINALPAGE_H
#define CRUDE_TERMINALPAGE_H

#include <QWizardPage>
class Wizard;
class AppSelector;

class TerminalPage : public QWizardPage
{
  Q_OBJECT

public:
  TerminalPage(Wizard* parent);

  bool isComplete() const;

public slots:
  void rescan();

private:
  Wizard* wizard;
  AppSelector* term;
  AppSelector* wm;

  QStringList scanWindowManagers();
};

#endif
