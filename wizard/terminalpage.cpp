#include "terminalpage.h"

#include "appselector.h"
#include "libcrude/environmentprefs.h"
#include "wizard.h"

#include <QLabel>
#include <QStandardPaths>
#include <QVBoxLayout>

static const QStringList knownWindowManagers{ "openbox", "fluxbox", "pekwm",   "cinnamon", "kwin",    "mutter", "marco",
                                              "compiz",  "i3wm",    "awesome", "xmonad",   "cwm",     "mwm",    "lwm",
                                              "icewm",   "fvwm",    "xfwm",    "jwm",      "metacity" };

TerminalPage::TerminalPage(Wizard* parent) : QWizardPage(parent), wizard(parent)
{
  setTitle("Select Required Components");
  setSubTitle("A terminal emulator and a window manager are required for a CRUDE session.");

  QVBoxLayout* layout = new QVBoxLayout(this);
  layout->addStretch(1);

  term = new AppSelector(
      this,
      wizard,
      "Terminal",
      "terminal",
      "terminal",
      EnvironmentPrefs::availableTerminals(),
      "Flags for running a shell command (often \"-e\"):"
  );
  QObject::connect(term, SIGNAL(validChanged()), this, SIGNAL(completeChanged()));
  layout->addWidget(term);

  layout->addStretch(1);

  wm = new AppSelector(this, wizard, "Window Manager", "window manager", "windowmanager", scanWindowManagers());
  QObject::connect(wm, SIGNAL(validChanged()), this, SIGNAL(completeChanged()));
  layout->addWidget(wm);

  layout->addStretch(1);
}

bool TerminalPage::isComplete() const
{
  return term->isValid() && wm->isValid();
}

void TerminalPage::rescan()
{
  term->setOptions(EnvironmentPrefs::availableTerminals());
  wm->setOptions(scanWindowManagers());
}

QStringList TerminalPage::scanWindowManagers()
{
  QStringList result;
  for (const QString& cmd : knownWindowManagers) {
    QString name = cmd.section(' ', 0, 0, QString::SectionSkipEmpty);
    if (!QStandardPaths::findExecutable(name).isEmpty()) {
      result << cmd;
    }
  }
  return result;
}
