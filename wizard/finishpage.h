#ifndef CRUDE_FINISHPAGE_H
#define CRUDE_FINISHPAGE_H

#include <QWizardPage>
class Wizard;
class AppSelector;
class QLabel;

class FinishPage : public QWizardPage
{
  Q_OBJECT

public:
  FinishPage(Wizard* parent);

  void initializePage();

private:
  Wizard* wizard;
  QLabel* choiceLabel;
};

#endif
