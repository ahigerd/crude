#ifndef CRUDE_EMERGENCYWINDOW_H
#define CRUDE_EMERGENCYWINDOW_H

#include "libcrude/xcb/xcbrequest_wm.h"

#include <QWidget>

class EmergencyWindow : public QWidget
{
  Q_OBJECT
  friend class EmergencyWM;

public:
  EmergencyWindow(WId window, const QRect& geom);

  void setFocused(bool on);

signals:
  void shown(EmergencyWindow*);
  void hidden(EmergencyWindow*);
  void requestFocus(EmergencyWindow*);

protected:
  void paintEvent(QPaintEvent*);
  void showEvent(QShowEvent*);
  void hideEvent(QHideEvent*);
  void resizeEvent(QResizeEvent*);
  void mousePressEvent(QMouseEvent*);
  void mouseMoveEvent(QMouseEvent*);

private slots:
  void childGeometry(xcb_get_geometry_reply_t* geom);
  void childGeometry(const QRect& geom);

private:
  QPoint frameOffset();
  void moveTo(const QPoint& pos);

  QWindow* child;
  bool focused : 1;
  bool mapped : 1;
  QPoint dragOffset;
  QRect closeButton;
  QString wmName, netWmName;
};

#endif
