#ifndef CRUDE_WELCOMEPAGE_H
#define CRUDE_WELCOMEPAGE_H

#include <QWizardPage>
class Wizard;

class WelcomePage : public QWizardPage
{
  Q_OBJECT

public:
  WelcomePage(Wizard* parent);

private:
  Wizard* wizard;
};

#endif
