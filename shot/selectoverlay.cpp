#include "selectoverlay.h"

#include "libcrude/xcb/xcbextension.h"
#include "libcrude/xcb/xcbhotkey_p.h"
#include "libcrude/xcb/xcbrequest_shape.h"
#include "libcrude/xcb/xcbrequest_wm.h"
#include "libcrudecomp/grabwindow.h"
#include "libcrudecomp/utility.h"
#include "libcrudecomp/windowtracker.h"
#include "shot_cursor.xbm"
#include "shot_cursor_mask.xbm"

#include <QApplication>
#include <QBitmap>
#include <QCursor>
#include <QDesktopWidget>
#include <QEventLoop>
#include <QExposeEvent>
#include <QKeyEvent>
#include <QPainter>
#include <QScreen>
#include <QStyle>
#include <QTimer>
#include <QX11Info>
#include <QtDebug>

#define DEBUG(ex) \
  if (c_PI->isDebugEnabled()) { \
    qDebug() << ex; \
  } else { \
    ex; \
  }

static Qt::WindowFlags overlayFlags = Qt::X11BypassWindowManagerHint | Qt::FramelessWindowHint |
    Qt::NoDropShadowWindowHint | Qt::WindowStaysOnTopHint | Qt::WindowDoesNotAcceptFocus;

SelectOverlay::SelectOverlay(bool forceOutline, WindowTracker* wt)
: QWidget(nullptr, overlayFlags), wt(wt), band(nullptr), loop(nullptr), isDragging(false)
{
  WindowTracker::instance()->ignoreWindow(winId());
  setMouseTracking(true);

  useComposite = !forceOutline && WindowTracker::hasCompositeManager();
  widget.reset(new QWidget(nullptr, overlayFlags));
  widget->setWindowFlag(Qt::WindowTransparentForInput);
  if (useComposite) {
    widget->setAttribute(Qt::WA_TranslucentBackground, true);
  } else {
    widget->setAttribute(Qt::WA_NoSystemBackground);
    widget->setAttribute(Qt::WA_OpaquePaintEvent);
  }
  widget->setMouseTracking(true);
  WindowTracker::instance()->ignoreWindow(widget->winId());
  widget->windowHandle()->installEventFilter(this);
}

bool SelectOverlay::getSelection(SelectOverlay::SelectionMode initialMode)
{
  if (this->loop) {
    // Don't allow this to be called recursively!
    qFatal("SelectOverlay::getSelection called recursively");
    return false;
  }

  mode = initialMode;
  QEventLoop loop;
  this->loop = &loop;
  if (c_PI->isDebugEnabled()) {
    QTimer::singleShot(5000, this->loop, SLOT(quit()));
  }
  if (band) {
    band->hide();
    band->deleteLater();
  }
  band = nullptr;
  selectedWindow = 0;
  selectedRect = QRect();

  // Save the current stacking order so we can restore it afterward if the user presses Tab
  stackingOrder = wt->visibleStackingOrder();
  originalStackingOrder = stackingOrder;

  handleMouseMove(QCursor::pos());
  QTimer::singleShot(100, this, SLOT(startGrab()));
  finished = false;
  int rv = loop.exec();

  this->loop = nullptr;

  //qApp->processEvents(QEventLoop::ExcludeUserInputEvents, 100);

  return rv && (selectedWindow || !selectedRect.isNull());
}

void SelectOverlay::startGrab()
{
  QCursor shotCursor(
      QBitmap::fromData(QSize(shot_cursor_width, shot_cursor_height), shot_cursor_bits, QImage::Format_MonoLSB),
      QBitmap::fromData(QSize(shot_cursor_width, shot_cursor_height), shot_cursor_mask_bits, QImage::Format_MonoLSB),
      shot_cursor_x_hot,
      shot_cursor_y_hot
  );

  setGeometry(qApp->primaryScreen()->virtualGeometry());
  callShapeSetRects(this, XCB_SHAPE_SK_BOUNDING, QVector<XcbRectAdaptor>())->force();
  show();
  grabMouse(shotCursor);
  grabKeyboard();
}

bool SelectOverlay::handleMouseMove(const QPoint& point)
{
  if (mode == RectangleMode) {
    if (!band || !band->isVisible()) {
      return false;
    }
    x1 = point.x();
    y1 = point.y();
    updateBand();
  } else {
    WindowTracker* wt = WindowTracker::instance();
    updateBand(wt->windowAt(point, mode == FrameMode));
  }
  return true;
}

void SelectOverlay::updateBand(GrabWindow* win)
{
  if (!band && (mode == RectangleMode || win)) {
    band = new QRubberBand(QRubberBand::Rectangle, widget.get());
    band->setCursor(Qt::CrossCursor);
    if (!useComposite) {
      band->installEventFilter(this);
    }
    band->move(0, 0);
    band->show();
    widget->show();
    WindowTracker::instance()->ignoreWindow(widget->winId());
  }
  if (mode == RectangleMode) {
    selectedRect = QRect(x0, y0, x1 - x0, y1 - y0).normalized();
    widget->setGeometry(selectedRect);
    selectedWindow = 0;
  } else if (win) {
    QRect screenGeom = qApp->primaryScreen()->geometry();
    if (mode == FrameMode) {
      widget->setGeometry(win->frameGeometry().intersected(screenGeom));
      selectedWindow = win->frameWindowId();
    } else {
      widget->setGeometry(win->childGlobalGeometry().intersected(screenGeom));
      selectedWindow = win->childWindowId();
    }
  } else {
    if (band) {
      delete band;
      band = nullptr;
    }
    selectedWindow = 0;
    selectedRect = QRect();
  }
  if (band) {
    band->resize(widget->size());
    if (!useComposite) {
      QRegion region;

      region += QRect(0, 0, 4, widget->height());
      region += QRect(0, widget->height() / 3, widget->width(), 1);
      region += QRect(0, widget->height() * 2 / 3, widget->width(), 1);
      region += QRect(0, widget->height() - 5, widget->width(), 5);

      region += QRect(0, 0, widget->width(), 4);
      region += QRect(widget->width() / 3, 0, 1, widget->height());
      region += QRect(widget->width() * 2 / 3, 0, 1, widget->height());
      region += QRect(widget->width() - 5, 0, 5, widget->height());

      widget->setMask(region);
      widget->raise();
    }
  }
}

void SelectOverlay::keyPressEvent(QKeyEvent* event)
{
  if (event->key() == Qt::Key_Tab) {
    quint32 topWindow;
    int failsafe = stackingOrder.size();
    do {
      if (event->modifiers()) {
        // modifier keys rotate backwards
        topWindow = stackingOrder.front();
        stackingOrder.erase(stackingOrder.begin());
        stackingOrder.insert(stackingOrder.end(), topWindow);
      } else {
        topWindow = stackingOrder.back();
        stackingOrder.pop_back();
        stackingOrder.insert(stackingOrder.begin(), topWindow);
      }
      if (WindowTracker::instance()->isWindowVisible(topWindow)) {
        callXcbConfigureWindow(topWindow, XCB_CONFIG_WINDOW_STACK_MODE, { XCB_STACK_MODE_ABOVE })->force();
        break;
      }
    } while (--failsafe > 0);
  } else if (event->key() == Qt::Key_Escape) {
    loop->exit(0);
  } else if (event->key() == Qt::Key_Space) {
    if (mode == RectangleMode) {
      if (event->modifiers() & Qt::ShiftModifier) {
        mode = WindowMode;
      } else {
        mode = FrameMode;
      }
    } else {
      mode = RectangleMode;
    }
    updateBand(WindowTracker::instance()->windowAt(QCursor::pos()));
  } else if (event->key() == Qt::Key_Shift) {
    if (mode == FrameMode) {
      mode = WindowMode;
      updateBand(WindowTracker::instance()->windowAt(QCursor::pos()));
    }
  }
}

void SelectOverlay::keyReleaseEvent(QKeyEvent* event)
{
  if (event->key() == Qt::Key_Shift) {
    if (mode == WindowMode) {
      mode = FrameMode;
      updateBand(WindowTracker::instance()->windowAt(QCursor::pos()));
    }
  }
}

void SelectOverlay::mousePressEvent(QMouseEvent* event)
{
  if (mode == RectangleMode) {
    x0 = x1 = event->globalPos().x();
    y0 = y1 = event->globalPos().y();
    isDragging = true;
    updateBand();
  } else {
    finishGrab();
  }
}

void SelectOverlay::mouseReleaseEvent(QMouseEvent*)
{
  if (isDragging) {
    finishGrab();
  }
}

void SelectOverlay::mouseMoveEvent(QMouseEvent* event)
{
  handleMouseMove(event->globalPos());
}

void SelectOverlay::exitLoop()
{
  loop->exit(finished ? 1 : 0);
}

bool SelectOverlay::eventFilter(QObject* obj, QEvent* event)
{
  if (event->type() == QEvent::Expose && obj == widget->windowHandle()) {
    QExposeEvent* ee = static_cast<QExposeEvent*>(event);
    if (ee->region().isNull() && loop && !isDragging) {
      QTimer::singleShot(16, this, SLOT(exitLoop()));
    }
  } else if (obj == band && event->type() == QEvent::Paint) {
    QPainter p(static_cast<QWidget*>(obj));
    p.setPen(Qt::black);
    p.setBrush(band->palette().highlight());
    p.drawRect(0, 0, widget->width() - 1, widget->height() - 1);
    p.setPen(Qt::white);
    p.setBrush(Qt::transparent);
    p.drawRect(3, 3, widget->width() - 7, widget->height() - 7);
    return true;
  }
  return false;
}

void SelectOverlay::restore()
{
  auto current = wt->visibleStackingOrder();
  if (wt->visibleStackingOrder() != originalStackingOrder) {
    WindowTracker::restoreStackingOrder(originalStackingOrder);
  }
}

void SelectOverlay::finishGrab()
{
  isDragging = false;
  finished = true;
  band->hide();
  widget->hide();
  releaseKeyboard();
  releaseMouse();
  hide();
}
