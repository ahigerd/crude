#include "libcrude/platformintegration.h"
#include "libcrude/qxtcommandoptions.h"
#include "libcrude/xcb/xcbextension.h"
#include "libcrude/xcb/xcbrequest_wm.h"
#include "libcrudecomp/grabwindow.h"
#include "libcrudecomp/windowtracker.h"
#include "previewwindow.h"
#include "selectoverlay.h"
#include "shotconfig.h"
#include "shotconfigwidget.h"

#include <QApplication>
#include <QClipboard>
#include <QSettings>

static int processImage(const QImage& image, const QxtCommandOptions& opt, SelectOverlay* overlay = nullptr)
{
  if (overlay) {
    overlay->restore();
  }
  if (image.isNull()) {
    return 1;
  }
  if (opt.count("clipboard")) {
    qApp->clipboard()->setImage(image);
    return 0;
  }
  QString filename =
      ShotConfigWidget::getSaveFileName(opt.count("interactive") && !opt.count("preview"), opt.value("output").toString());
  if (opt.count("preview")) {
    PreviewWindow preview(image, filename);
    int result = preview.exec();
    if (result == QDialog::Rejected) {
      // User cancelled
      return 0;
    }
    filename = preview.filename();
  }
  return image.save(filename) ? 0 : 1;
}

int main(int argc, char** argv)
{
  QApplication::setApplicationName("crude-shot");
  QApplication::setOrganizationName("Alkahest");
  QApplication::setApplicationVersion("0.0.1");
  QApplication app(argc, argv);
  PlatformIntegrator platform(argc, argv);
  WindowTracker::initExtensions();

  QxtCommandOptions opt;
  opt.addSection("Input options");
  opt.add("all", "capture the full screen (default)", QxtCommandOptions::NoValue, 1);
  opt.alias("all", "a");
  opt.add("window", "capture a window selected with the mouse", QxtCommandOptions::NoValue, 1);
  opt.alias("window", "w");
  opt.add("focus-window", "capture the currently focused window", QxtCommandOptions::NoValue, 1);
  opt.alias("focus-window", "W");
  opt.add("window-id", "capture a window specified by X11 window ID", QxtCommandOptions::ValueRequired, 1);
  opt.alias("window-id", "n");
  opt.add("region", "capture a rectangular region selected with the mouse", QxtCommandOptions::NoValue, 1);
  opt.alias("region", "g");
  opt.add("rectangle", "capture a rectangular region specified by geometry", QxtCommandOptions::ValueRequired, 1);
  opt.alias("rectangle", "r");
  opt.addSection("Output options");
  opt.add(
      "output", "filename for the saved screenshot (default: generate from timestamp)", QxtCommandOptions::ValueRequired
  );
  opt.alias("output", "o");
  opt.add("interactive", "prompt for a destination filename", QxtCommandOptions::NoValue, 2);
  opt.alias("interactive", "i");
  opt.add("preview", "preview the screenshot before saving", QxtCommandOptions::NoValue, 2);
  opt.alias("preview", "p");
  opt.add("clipboard", "save to the clipboard instead of a file", QxtCommandOptions::NoValue, 2);
  opt.alias("clipboard", "c");
  opt.addSection("Other options");
  opt.add("config", "open the configuration window");
  opt.add("help", "show this help text");
  opt.alias("help", "h");
  opt.parse(app.arguments());
  if (opt.count("help") || opt.showUnrecognizedWarning()) {
    opt.showUsage();
    return 1;
  }

  if (opt.count("config")) {
    ShotConfig config;
    config.exec();
    return 0;
  }

  if (opt.count("window-id")) {
    GrabWindow window(opt.value("window-id").toString().toInt(nullptr, 0));
    return processImage(window.grab(), opt);
  } else if (opt.count("focus-window")) {
    auto cookie = callEwmhGetActiveWindow(0);
    if (!cookie->force() || !cookie->result()) {
      qWarning("crude-shot: could not find current window");
      return 1;
    }
    quint32 winId = cookie->result();
    quint32 frameId = winId;
    quint32 parentId = winId;
    quint32 rootId = QX11Info::appRootWindow();
    while (parentId && parentId != rootId) {
      auto cParent = callXcbQueryTree(parentId)->uniquePtr();
      frameId = parentId;
      parentId = cParent->result()->parent;
    }
    GrabWindow window(frameId);
    return processImage(window.grab(), opt);
  } else if (opt.count("rectangle")) {
    QRegExp re("^(\\d+)x(\\d+)((?:\\+|-)\\d+)((?:\\+|-)\\d+)");
    bool ok = re.exactMatch(opt.value("rectangle").toString());
    if (!ok) {
      qWarning("crude-shot: unable to parse rectangle (expected: <width>x<height>+<left>+<top>)");
      return 1;
    }
    GrabWindow root(QX11Info::appRootWindow());
    return processImage(root.grab(QRect(re.cap(3).toInt(), re.cap(4).toInt(), re.cap(1).toInt(), re.cap(2).toInt())), opt);
  } else if (opt.count("window") || opt.count("region")) {
    QSettings settings;
    WindowTracker wt;
    bool forceOutline = settings.value("outline", false).toBool();
    SelectOverlay overlay(forceOutline, &wt);
    bool ok = overlay.getSelection(opt.count("region") ? SelectOverlay::RectangleMode : SelectOverlay::FrameMode);
    if (!ok) {
      return 0;
    }
    if (overlay.selectedWindow) {
      GrabWindow window(overlay.selectedWindow);
      return processImage(window.grab(), opt, &overlay);
    } else if (!overlay.selectedRect.isEmpty()) {
      return processImage(wt.grab(overlay.selectedRect), opt, &overlay);
    } else {
      return 1;
    }
  } else {
    GrabWindow root(QX11Info::appRootWindow());
    return processImage(root.grab(), opt);
  }
}
