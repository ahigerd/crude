TEMPLATE = app
TARGET = crude-shot
QT = core widgets x11extras
LIBS += -L.. -lcrude -lcrudecomp
include(../crude-common.pri)

HEADERS += selectoverlay.h   previewwindow.h   shotconfig.h   shotconfigwidget.h
SOURCES += selectoverlay.cpp previewwindow.cpp shotconfig.cpp shotconfigwidget.cpp main.cpp

PRE_TARGETDEPS += ../libcrude.a ../libcrudecomp.a
