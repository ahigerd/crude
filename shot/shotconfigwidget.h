#ifndef CRUDE_SHOTCONFIGWIDGET_H
#define CRUDE_SHOTCONFIGWIDGET_H

#include <QWidget>
class QCheckBox;
class QComboBox;
class QLineEdit;

class ShotConfigWidget : public QWidget
{
  Q_OBJECT

public:
  static QString getSaveFileName(bool interactive, const QString& defaultName = QString());

  ShotConfigWidget(QWidget* parent = nullptr);

signals:
  void fieldChanged();

public slots:
  void save();
  void browse();

private:
  QLineEdit* fPath;
  QComboBox* fImage;
  QLineEdit* fDate;
  QCheckBox* fOutline;
};

#endif
