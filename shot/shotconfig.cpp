#include "shotconfig.h"

#include <QCheckBox>
#include <QDialogButtonBox>
#include <QVBoxLayout>

ShotConfig::ShotConfig(QWidget* parent) : QDialog(parent)
{
  setWindowTitle("crude-shot Defaults");
  setWindowModality(Qt::ApplicationModal);

  QVBoxLayout* layout = new QVBoxLayout(this);

  widget = new ShotConfigWidget(this);
  layout->addWidget(widget, 1);

  QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);
  QObject::connect(buttons, SIGNAL(accepted()), this, SLOT(save()));
  QObject::connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
  layout->addWidget(buttons, 0);

  setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  setFixedSize(sizeHint().width() * 1.5, sizeHint().height());
}

void ShotConfig::save()
{
  widget->save();
  accept();
}
