#include "shotconfigwidget.h"

#include "previewwindow.h"

#include <QCheckBox>
#include <QComboBox>
#include <QDateTime>
#include <QFileDialog>
#include <QGridLayout>
#include <QImageWriter>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSettings>

QString ShotConfigWidget::getSaveFileName(bool interactive, const QString& defaultName)
{
  QSettings settings("Alkahest", "crude-shot");
  QString filename = defaultName;
  if (filename.isEmpty()) {
    QString dtFormat = settings.value("dateFormat").toString();
    if (dtFormat.isEmpty()) {
      dtFormat = QLocale()
                     .dateTimeFormat(QLocale::ShortFormat)
                     .replace("h", "hh")
                     .replace("hhhh", "hh")
                     .replace("hhh", "hh")
                     .replace("H", "HH")
                     .replace("HHHH", "HH")
                     .replace("HHH", "HH")
                     .replace("m", "mm")
                     .replace("mmmm", "mm")
                     .replace("mmm", "mm")
                     .replace("s", "ss")
                     .replace("ssss", "ss")
                     .replace("sss", "ss")
                     .replace("m", "mm")
                     .replace("mmmm", "mm")
                     .replace("mmm", "mm")
                     .replace("M", "MM")
                     .replace("MMMM", "MM")
                     .replace("MMM", "MM")
                     .replace("dddd", "\1")
                     .replace("ddd", "\2")
                     .replace("dd", "\3")
                     .replace("d", "dd")
                     .replace("\3", "dd")
                     .replace("\2", "ddd")
                     .replace("\1", "dddd")
                     .replace("yy", "yyyy")
                     .replace("yyyyyyyy", "yyyy")
#ifdef Q_OS_WIN
                     .replace(":", "-")
#endif
                     .replace("/", "-")
                     .replace(",", "");
      settings.setValue("dateFormat", dtFormat);
    }
    QString basePath = settings.value("outputPath").toString();
    if (basePath.isEmpty()) {
      basePath = QDir::homePath();
      settings.setValue("outputPath", basePath);
    }
    QString ext = settings.value("imageFormat").toString();
    if (ext.isEmpty()) {
      ext = "png";
      settings.setValue("imageFormat", ext);
    }
    filename =
        QString("%1/Screenshot at %2.%3").arg(basePath).arg(QDateTime::currentDateTime().toString(dtFormat)).arg(ext);
  }
  if (interactive) {
    QList<QByteArray> formats = QImageWriter::supportedImageFormats();
    QStringList filters, allImages;
    QString selectedFilter, pngFilter;
    QString extension = filename.section('.', -1);
    for (const QByteArray& format : formats) {
      allImages << QString("*.%1").arg(QString::fromUtf8(format));
      if (format == "jpeg") {
        continue;
      } else if (format == "jpg") {
        filters << "JPEG image (*.jpg *.jpeg)";
      } else {
        QString formatName = QString::fromUtf8(format);
        QString formatLabel = QString("%1 image (*.%2)").arg(formatName.toUpper()).arg(formatName);
        filters << formatLabel;
        if (formatName == "png") {
          pngFilter = formatLabel;
        }
        if (formatName == extension) {
          selectedFilter = formatLabel;
        }
      }
    }
    if (selectedFilter.isEmpty()) {
      selectedFilter = pngFilter;
    }
    filters << QString("All images (%1)").arg(allImages.join(' ')) << "All files (*)";
    filename = QFileDialog::getSaveFileName(nullptr, "Save Screenshot", filename, filters.join(";;"), &selectedFilter);
  }
  return filename;
}

ShotConfigWidget::ShotConfigWidget(QWidget* parent) : QWidget(parent)
{
  // This will populate the default settings
  getSaveFileName(false, QString());
  QSettings settings("Alkahest", "crude-shot");

  QGridLayout* layout = new QGridLayout(this);

  QLabel* lPath = new QLabel("Output &path:", this);
  fPath = new QLineEdit(settings.value("outputPath").toString(), this);
  lPath->setBuddy(fPath);
  QPushButton* bPath = new QPushButton("&Browse...", this);
  QObject::connect(bPath, SIGNAL(clicked()), this, SLOT(browse()));
  layout->addWidget(lPath, 0, 0);
  layout->addWidget(fPath, 0, 1);
  layout->addWidget(bPath, 0, 2);

  QLabel* lImage = new QLabel("&Image format:", this);
  fImage = new QComboBox(this);
  lImage->setBuddy(fImage);
  QList<QByteArray> formats = QImageWriter::supportedImageFormats();
  for (const QByteArray& format : formats) {
    if (format != "jpeg") {
      fImage->addItem(QString::fromUtf8(format).toUpper(), QString::fromUtf8(format));
    }
  }
  fImage->setCurrentIndex(fImage->findData(settings.value("imageFormat").toString()));
  layout->addWidget(lImage, 1, 0);
  layout->addWidget(fImage, 1, 1, 1, 2);

  QLabel* lDate = new QLabel("&Date format:", this);
  fDate = new QLineEdit(settings.value("dateFormat").toString(), this);
  lDate->setBuddy(fDate);
  QLabel* bDate = new QLabel("<a href='https://doc.qt.io/qt-5/qdatetime.html#toString'>(Format Help)</a>", this);
  bDate->setOpenExternalLinks(true);
  layout->addWidget(lDate, 2, 0);
  layout->addWidget(fDate, 2, 1);
  layout->addWidget(bDate, 2, 2);

  fOutline = new QCheckBox("Use &outline overlay instead of transparency", this);
  fOutline->setChecked(settings.value("outline", false).toBool());
  layout->addWidget(fOutline, 3, 1, 1, 2);

  layout->setColumnStretch(0, 0);
  layout->setColumnStretch(1, 1);
  layout->setColumnStretch(2, 0);

  QObject::connect(fPath, SIGNAL(editingFinished()), this, SIGNAL(fieldChanged()));
  QObject::connect(fDate, SIGNAL(editingFinished()), this, SIGNAL(fieldChanged()));
  QObject::connect(fImage, SIGNAL(currentIndexChanged(int)), this, SIGNAL(fieldChanged()));
  QObject::connect(fOutline, SIGNAL(toggled(bool)), this, SIGNAL(fieldChanged()));
}

void ShotConfigWidget::save()
{
  // TODO: validate
  QSettings settings("Alkahest", "crude-shot");
  settings.setValue("outputPath", fPath->text());
  settings.setValue("imageFormat", fImage->currentData().toString());
  settings.setValue("dateFormat", fDate->text());
  settings.setValue("outline", fOutline->isChecked());
}

void ShotConfigWidget::browse()
{
  QString path = QFileDialog::getExistingDirectory(this, "Select default output path", fPath->text());
  if (!path.isEmpty()) {
    fPath->setText(path);
    emit fieldChanged();
  }
}
