#include "previewwindow.h"

#include "libcrude/platformintegration.h"
#include "shotconfig.h"
#include "shotconfigwidget.h"

#include <QApplication>
#include <QBoxLayout>
#include <QClipboard>
#include <QDateTime>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QGridLayout>
#include <QImageWriter>
#include <QLabel>
#include <QLineEdit>
#include <QLocale>
#include <QMouseEvent>
#include <QPushButton>
#include <QScrollArea>
#include <QScrollBar>
#include <QSettings>
#include <QToolButton>
#include <QtDebug>

PreviewWindow::PreviewWindow(const QImage& image, const QString& defaultFn, QWidget* parent)
: QDialog(parent), pixmap(QPixmap::fromImage(image))
{
  pWidth = image.width();
  pHeight = image.height();
  zoom = 0;
  dragButton = 0;

  QGridLayout* layout = new QGridLayout(this);
  QVBoxLayout* sLayout = new QVBoxLayout;
  layout->addLayout(sLayout, 0, 0);

  sa = new QScrollArea(this);
  sa->setAlignment(Qt::AlignCenter);
  preview = new QLabel(this);
  preview->setScaledContents(true);
  preview->setPixmap(pixmap);
  preview->setCursor(Qt::OpenHandCursor);
  preview->resize(1, 1); // this will get resized when the window is displayed
  preview->installEventFilter(this);
  sa->setWidget(preview);
  sLayout->addWidget(sa, 1);

  QHBoxLayout* zoomLayout = new QHBoxLayout;
  zoomLayout->addStretch(1);
  sLayout->addLayout(zoomLayout, 0);

  QToolButton* zoomOutButton = new QToolButton;
  zoomOutButton->setText("-");
  zoomOutButton->setIcon(c_PI->icon({ "zoom-out-symbolic", "zoom-out" }));
  zoomLayout->addWidget(zoomOutButton, 0);
  QObject::connect(zoomOutButton, SIGNAL(clicked()), this, SLOT(zoomOut()));

  QToolButton* zoomInButton = new QToolButton;
  zoomInButton->setText("+");
  zoomInButton->setIcon(c_PI->icon({ "zoom-in-symbolic", "zoom-in" }));
  zoomLayout->addWidget(zoomInButton, 0);
  QObject::connect(zoomInButton, SIGNAL(clicked()), this, SLOT(zoomIn()));

  QGridLayout* gLayout = new QGridLayout;
  gLayout->setColumnStretch(0, 0);
  gLayout->setColumnStretch(1, 1);
  gLayout->setColumnStretch(2, 0);
  gLayout->setRowStretch(3, 1);
  layout->addLayout(gLayout, 1, 0);

  QLabel* fnLabel = new QLabel("&Filename:", this);
  fnEdit = new QLineEdit(ShotConfigWidget::getSaveFileName(false, defaultFn), this);
  QPushButton* fnBrowse = new QPushButton("&Browse...", this);
  fnLabel->setBuddy(fnEdit);
  QObject::connect(fnBrowse, SIGNAL(clicked()), this, SLOT(browse()));
  gLayout->addWidget(fnLabel, 0, 0);
  gLayout->addWidget(fnEdit, 0, 1);
  gLayout->addWidget(fnBrowse, 0, 2);

  QHBoxLayout* bLayout = new QHBoxLayout;
  layout->addLayout(bLayout, 2, 0);

  QPushButton* copyButton = new QPushButton("&Copy", this);
  QPushButton* prefsButton = new QPushButton("&Settings...", this);
  bLayout->addWidget(copyButton, 0);
  bLayout->addWidget(prefsButton, 0);
  QObject::connect(copyButton, SIGNAL(clicked()), this, SLOT(copyToClipboard()));
  QObject::connect(prefsButton, SIGNAL(clicked()), this, SLOT(openPrefs()));

  QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Cancel, this);
  QObject::connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
  QObject::connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
  bLayout->addWidget(buttons, 1);

  layout->setRowStretch(0, 1);
  layout->setRowStretch(1, 0);
  layout->setRowStretch(2, 0);
  layout->setColumnStretch(0, 1);
}

void PreviewWindow::zoomIn()
{
  zoom = zoom + 0.1;
  if (zoom > 1) {
    zoom = 1;
  }
  setZoom(zoom);
}

void PreviewWindow::zoomOut()
{
  zoom = zoom - 0.1;
  if (zoom < minZoom) {
    zoom = minZoom;
  }
  setZoom(zoom);
}

void PreviewWindow::copyToClipboard()
{
  qApp->clipboard()->setPixmap(pixmap);
}

bool PreviewWindow::eventFilter(QObject* obj, QEvent* ev)
{
  if (obj != preview) {
    return false;
  }
  if (ev->type() == QEvent::MouseButtonPress) {
    QMouseEvent* me = static_cast<QMouseEvent*>(ev);
    dragButton = me->button();
    preview->setCursor(Qt::ClosedHandCursor);
    dragAnchor = me->globalPos();
    origX = sa->horizontalScrollBar()->value();
    origY = sa->verticalScrollBar()->value();
  } else if (ev->type() == QEvent::MouseButtonRelease) {
    int button = static_cast<QMouseEvent*>(ev)->button();
    if (button == dragButton) {
      preview->setCursor(Qt::OpenHandCursor);
      dragButton = 0;
    }
  } else if (ev->type() == QEvent::MouseMove && dragButton) {
    QMouseEvent* me = static_cast<QMouseEvent*>(ev);
    int xOffset = (dragAnchor.x() - me->globalPos().x());
    int yOffset = (dragAnchor.y() - me->globalPos().y());
    sa->horizontalScrollBar()->setValue(origX + xOffset);
    sa->verticalScrollBar()->setValue(origY + yOffset);
  }
  return false;
}

void PreviewWindow::showEvent(QShowEvent*)
{
  raise();
}

void PreviewWindow::resizeEvent(QResizeEvent*)
{
  QSize minSize = sa->maximumViewportSize();
  float wf = (float)pWidth / minSize.width();
  float hf = (float)pHeight / minSize.height();
  if (preview->width() < minSize.width() || preview->height() < minSize.height() || zoom == 0) {
    if (wf > hf) {
      minZoom = 1.0 / wf;
    } else {
      minZoom = 1.0 / hf;
    }
    if (zoom < minZoom) {
      setZoom(minZoom);
    }
  }
}

void PreviewWindow::setZoom(float level)
{
  zoom = level;
  preview->resize(zoom * pWidth, zoom * pHeight);
}

QSize PreviewWindow::sizeHint() const
{
  return QSize(600, 400);
}

void PreviewWindow::browse()
{
  QString fn = ShotConfigWidget::getSaveFileName(true, fnEdit->text());
  if (!fn.isEmpty()) {
    fnEdit->setText(fn);
  }
}

QString PreviewWindow::filename() const
{
  return fnEdit->text();
}

void PreviewWindow::openPrefs()
{
  ShotConfig* config = new ShotConfig(this);
  config->open();
}
