#ifndef CRUDE_PREVIEWWINDOW_H
#define CRUDE_PREVIEWWINDOW_H

#include <QDialog>
class QLabel;
class QLineEdit;
class QScrollArea;

class PreviewWindow : public QDialog
{
  Q_OBJECT

public:
  PreviewWindow(const QImage& image, const QString& defaultFn, QWidget* parent = nullptr);

  bool eventFilter(QObject* obj, QEvent* ev);
  QString filename() const;

  QSize sizeHint() const;

protected:
  void showEvent(QShowEvent*);
  void resizeEvent(QResizeEvent*);

private slots:
  void zoomIn();
  void zoomOut();
  void copyToClipboard();
  void browse();
  void openPrefs();

private:
  void setZoom(float level);

  QPixmap pixmap;
  QScrollArea* sa;
  QLabel* preview;
  QLineEdit* fnEdit;
  int pWidth, pHeight, dragButton;
  float zoom, minZoom;
  QPoint dragAnchor;
  int origX, origY;
};

#endif
