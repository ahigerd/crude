#ifndef CRUDE_SELECTOVERLAY_H
#define CRUDE_SELECTOVERLAY_H

#include "libcrude/xcb/xcbintegration.h"

#include <QRubberBand>
#include <QWidget>

#include <memory>
class QEventLoop;
class GrabWindow;
class WindowTracker;

class SelectOverlay : public QWidget
{
  Q_OBJECT

public:
  enum SelectionMode {
    RectangleMode,
    FrameMode,
    WindowMode,
  };

  SelectOverlay(bool forceOutline, WindowTracker* wt);

  bool eventFilter(QObject* obj, QEvent* event);

  bool getSelection(SelectionMode initialMode);
  QRect selectedRect;
  quint32 selectedWindow;

  void restore();

protected:
  void keyPressEvent(QKeyEvent* event);
  void keyReleaseEvent(QKeyEvent* event);
  void mousePressEvent(QMouseEvent* event);
  void mouseReleaseEvent(QMouseEvent* event);
  void mouseMoveEvent(QMouseEvent* event);

private slots:
  void startGrab();
  void exitLoop();

private:
  bool handleMouseMove(const QPoint& point);
  void updateBand(GrabWindow* win = nullptr);
  void finishGrab();

  std::unique_ptr<QWidget> widget;
  std::vector<quint32> stackingOrder, originalStackingOrder;
  WindowTracker* wt;
  QRubberBand* band;
  QEventLoop* loop;
  SelectionMode mode;
  bool useComposite, isDragging, finished;
  int x0, y0, x1, y1;
};

#endif
