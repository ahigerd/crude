#ifndef CRUDE_SHOTCONFIG_H
#define CRUDE_SHOTCONFIG_H

#include "shotconfigwidget.h"

#include <QDialog>

class ShotConfig : public QDialog
{
  Q_OBJECT

public:
  ShotConfig(QWidget* parent = nullptr);

private slots:
  void save();

private:
  ShotConfigWidget* widget;
};

#endif
