crude-panel
===========
A launcher and task switcher for the CRUDE desktop environment.

Command Line
------------
**Usage:** `crude-panel [CONFIG]`

* **`CONFIG`**: Load the panel configuration from this file. This can be used to run
multiple panels with different layouts simultaneously. If omitted, the
configuration is loaded from `$XDG_CONFIG_HOME/Alkahest/crude-panel.ini`. (By
default, `XDG_CONFIG_HOME` is `~/.config`.)

[Standard Qt command line options](
https://doc.qt.io/qt-5/qapplication.html#QApplication) are also accepted.

Configuration File
------------------
The configuration file is stored in standard `.ini` format. If it does not
exist, it will be created and populated with a default configuration on
startup.

### `[General]` section

This section contains configuration options that apply to the panel as a whole.

* **`components`**: A comma-separated list of component identifiers. The
  components named in this list will be added to the panel from left to right
  or from top to bottom, depending on the panel's orientation. See the
  "Component sections" documentation below for the definition of the components.
  A special identifier `spacer` can be used to insert a blank area that expands
  to fill unused space in the panel.
* **`edge`**: Defines which side of the screen the panel will be docked to.
  Available options are `BottomEdge` (default), `TopEdge`, `LeftEdge`, and
  `RightEdge`.
* **`size`**: The thickness, in pixels, of the panel. For `BottomEdge` and
  `TopEdge`, this is the height (default based on chosen UI theme); for
  `LeftEdge` and `RightEdge` this is the width (default 150).

### `[StartupCommands]` section

This section contains a set of commands that are executed after the panel and
all of its components have finished loading. The commands are run in the
sorted order of the property keys.

### Component sections

Each identifier named in the `components` list above must correspond to a
section in the configuration file with the same name. There is one required
property in these sections:

* **`type`**: One of `Launcher`, `Switcher`, `SysTray`, or `Clock`.
  Additional types may be added in the future.

Each component type accepts additional properties. See the corresponding
documentation below for more information.

### Component Type: `Launcher`

A `Launcher` section provides a menu of available applications.

No additional properties are currently defined.

### Component Type: `Switcher`

A `Switcher` section presents a list of open windows and allows the user to
switch between them.

* **`subtype`**: One of `Taskbar` (default) or `DropDown`. If set to `Taskbar`,
  the component will expand to fill available space in the panel.
* **`group`**: A `|`-separated list of flags that control how windows are
  grouped. If multiple grouping modes are enabled, groups will contain windows
  that match in any selected attribute.
    * `NoGrouping`: All open windows will be displayed individually. (Default)
    * `EnableGrouping`: Groups windows together according to the group leaders
      specified in their ICCCM window manager hints.
    * `GroupByProcess`: Groups windows together according to the process ID that
      created them. This also implies `EnableGrouping`.
    * `GroupByProgram`: Groups windows together according to the name of the
      program that created them. If the name of the program cannot be determined
      (for example, if the client is running on a remote machine) then the
      process ID is used. This also implies `EnableGrouping`.
    * `GroupByClass`: Groups windows together according to the class name
      specified in their WM_CLASS property. This also implies `EnableGrouping`.
    * `GroupByInstance`: Groups windows together according to the instance name
      specified in their WM_CLASS property. This also implies `EnableGrouping`.
    * `GroupAggressively`: Equivalent to enabling all of the grouping options above.
    * `NoSingleGroups`: If grouping is enabled, all windows will belong to a
      group by default. Specifying this flag will cause groups that only contain
      a single window to instead be presented as a single item. This is generally
      the expected behavior in `Taskbar` mode.
* **`urgent`**: A `|`-separated list of flags that determine how windows that
  are marked as "urgent" or "demands attention" are displayed.
    * `NoUrgency`: Urgent windows are not displayed differently than non-urgent
      windows.
    * `UrgentText`: Urgent windows are highlighted with an animated mark in the
      title of the window or group. (default)
    * `UrgentCheck`: Urgent windows are highlighted with a checkbox. In `Taskbar`
      mode, this also causes the button to flash.
* **`display`**: A `|`-separated list of flags that determine how the windows or
  window groups are rendered in the task switcher.
    * `WindowTitle`: Show the title of the window or the most recently focused
      window in the group. (default)
    * `ProgramName`: Show the name of the program as determined by the process
      list.
    * `NoText`: Don't display any text.
    * `NoIcons`: Don't display icons.
    * `Flat`: In `Taskbar` mode, don't render button borders.
* **`iconSize`**: The size, in pixels, of the icons displayed in the task
  switcher. (default 16)
* **`useHotkeys`**: If `true`, this task switcher will respond to the hotkeys
  for switching between apps/windows.  Only one process may reliably respond to
  window-switching hotkeys, including the window manager. (default `false`)
* **`linear`**: If `true` and `useHotkeys` is true, then the `nextapp` hotkey
  will focus the next window or group in the switcher. If `false`, then the
  first press of the `nextapp` hotkey will focus the previously focused window
  or group. If the `nextapp` hotkey does not include at least one modifier key,
  focus switching is always linear. (default `false`)
* **`overlay`**: If `true`, then the `nextapp` and `prevapp` hotkeys will
  display an overlay to select the window to be focused. This only works if
  `useHotkeys` is `true` and the hotkeys include at least one modifier key.
  If `false` or if the overlay cannot be used, the `nextapp` and `prevapp`
  hotkeys will immediately focus the next window in the focus order.
  (default `true`)

### Component Type: `SysTray`

A `SysTray` component contains system tray icons provided by running programs.

This component currently has no configuration options.

### Component Type: `Clock`

A `Clock` component displays the current time and/or date.

* **`format`**: A format string describing how the time and/or date are to be
  displayed. Available format options can be found in the
  [Qt documentation](https://doc.qt.io/qt-5/qdatetime.html#toString).
  (default `hh:mm AP`).

Default components
------------------
If the configuration file does not exist, or if the `components` list does not
contain any valid component names, the following components are added:

  * `launcher`: A `Launcher` component with default settings.
  * `taskbar`: A `Switcher` component in `Taskbar` mode.
  * `systray`: A `SysTray` component with default settings.
  * `clock`: A `Clock` component with default settings.


