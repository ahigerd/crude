#ifndef CRUDE_ZOOMOVERLAY_H
#define CRUDE_ZOOMOVERLAY_H

#include "libcrude/xcb/xcbintegration.h"

#include <QRegion>
#include <QTimer>
#include <QTransform>

#include <memory>
class GrabWindow;
class xcb_xfixes_get_cursor_image_reply_t;

class ZoomOverlay : public QObject, public XcbHandlerInterface
{
  Q_OBJECT

public:
  ZoomOverlay(QObject* parent = nullptr);
  ~ZoomOverlay();

  bool handleEvent(const CrudeXcb::EventType& event);

public slots:
  void setZoomLevel(float zoom);
  void zoomIn();
  void zoomOut();
  void pause();
  void resume();
  void updateMatrix();

private slots:
  void refresh();
  void addDirty(const QRegion& region);
  void pollMouse(bool force = false);
  void updateCursor(xcb_xfixes_get_cursor_image_reply_t* reply);

private:
  std::unique_ptr<GrabWindow> rootWindow;
  uint32_t backing, overlay, picture, lastCursor, cursorPixmap, cursorPicture, cursorGC, backingPicture;
  int xHot, yHot, cursorW, cursorH;
  float zoomLevel;
  QTransform zoomMatrix;
  QRect screenGeom, zoomGeom;
  QImage cursor;
  QTimer pollTimer;
  QPoint lastPos;
  QRegion dirtyRegion;
};

#endif
