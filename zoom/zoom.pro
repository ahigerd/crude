TEMPLATE = app
TARGET = crude-zoom
QT = core widgets x11extras
LIBS += -L.. -lcrude -lcrudecomp
include(../crude-common.pri)

HEADERS += zoomoverlay.h
SOURCES += zoomoverlay.cpp main.cpp

PRE_TARGETDEPS += ../libcrude.a ../libcrudecomp.a
