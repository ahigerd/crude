#include "zoomoverlay.h"

#include "libcrude/hotkey.h"
#include "libcrude/xcb/xcbrequest_fixes.h"
#include "libcrude/xcb/xcbrequest_render.h"
#include "libcrude/xcb/xcbrequest_shape.h"
#include "libcrude/xcb/xcbrequest_wm.h"
#include "libcrudecomp/grabwindow.h"
#include "libcrudecomp/utility.h"
#include "libcrudecomp/windowtracker.h"

#include <QApplication>
#include <QElapsedTimer>
#include <QScreen>
#include <QX11Info>

static const int KEY_MODS = XCB_MOD_MASK_4 | XCB_MOD_MASK_1 | XCB_MOD_MASK_SHIFT;

ZoomOverlay::ZoomOverlay(QObject* parent)
: QObject(parent),
  rootWindow(new GrabWindow(QX11Info::appRootWindow())),
  picture(0),
  cursorPixmap(0),
  cursorPicture(0),
  cursorGC(0)
{
  XcbIntegration::instance()->addEventHandler(this);

  if (!WindowTracker::supportsShape()) {
    qFatal("Unable to initialize shape extension");
  }

  auto cookie = callCompositeGetOverlay(rootWindow->windowId());
  if (!cookie->force()) {
    qFatal("Could not get composite overlay window");
  }
  overlay = cookie->result()->overlay_win;
  callXcbUnmapWindow(overlay);
  WindowTracker::instance()->ignoreWindow(overlay);

  screenGeom = qApp->primaryScreen()->geometry();
  QObject::connect(WindowTracker::instance(), SIGNAL(regionDirty(QRegion)), this, SLOT(addDirty(QRegion)));

  backing = xcb_generate_id(c_X11);
  backingPicture = xcb_generate_id(c_X11);
  callXcbCreatePixmap(24, backing, rootWindow->windowId(), screenGeom.width(), screenGeom.height());
  int format = getRenderFormatForVisual(rootWindow->visualId());
  xcb_render_create_picture_value_list_t picValue;
  callRenderCreatePicture(backingPicture, backing, format, 0, picValue)->force();

  pollTimer.setInterval(16);
  pollTimer.setSingleShot(false);
  QObject::connect(&pollTimer, SIGNAL(timeout()), this, SLOT(pollMouse()));

  callSelectCursorInput(overlay, XCB_XFIXES_CURSOR_NOTIFY_MASK_DISPLAY_CURSOR);

  setZoomLevel(1);

  callXcbGrabButton(QX11Info::appRootWindow(), KEY_MODS, XCB_BUTTON_INDEX_4, false);
  callXcbGrabButton(QX11Info::appRootWindow(), KEY_MODS, XCB_BUTTON_INDEX_5, false);

  Hotkey* h1 = new Hotkey(QString("Meta+Ctrl+0"), this);
  QObject::connect(h1, SIGNAL(activated(QVariant)), this, SLOT(zoomIn()));
  Hotkey* h2 = new Hotkey(QString("Meta+Ctrl+9"), this);
  QObject::connect(h2, SIGNAL(activated(QVariant)), this, SLOT(zoomOut()));

  callXcbAllowEvents(XCB_ALLOW_ASYNC_POINTER)->force();
  callXcbAllowEvents(XCB_ALLOW_ASYNC_KEYBOARD)->force();
  callXcbAllowEvents(XCB_ALLOW_SYNC_POINTER)->force();
  callXcbAllowEvents(XCB_ALLOW_SYNC_KEYBOARD)->force();
}

ZoomOverlay::~ZoomOverlay()
{
  callRenderFreePicture(backingPicture);
  if (cursorPixmap) {
    callRenderFreePicture(cursorPicture);
    callXcbFreePixmap(cursorPixmap);
    callXcbFreeGc(cursorGC);
  }
  if (picture) {
    callRenderFreePicture(picture);
  }
}

void ZoomOverlay::zoomIn()
{
  setZoomLevel(zoomLevel + .1);
}

void ZoomOverlay::zoomOut()
{
  setZoomLevel(zoomLevel - .1);
}

void ZoomOverlay::setZoomLevel(float zoom)
{
  if (zoom <= 1.09) {
    zoom = 1;
  } else if (zoom > 5.91) {
    zoom = 6;
  }
  zoomLevel = zoom;
  if (zoom <= 1) {
    pause();
  } else {
    resume();
  }
}

void ZoomOverlay::pause()
{
  if (pollTimer.isActive()) {
    WindowTracker::instance()->setDamageEnabled(false);
    if (picture) {
      callRenderFreePicture(picture);
      picture = 0;
    }
    callShowCursor(overlay);
    callXcbUnmapWindow(overlay);
    callCompositeUnredirectWindows(rootWindow->windowId(), XCB_COMPOSITE_REDIRECT_AUTOMATIC);
    pollTimer.stop();
  }
}

void ZoomOverlay::resume()
{
  if (!pollTimer.isActive()) {
    xcb_window_t focus = callEwmhGetActiveWindow(0)->result();
    updateCursor(callGetCursorImage()->result());
    callXcbMapWindow(overlay);
    callShapeSetRects(overlay, XCB_SHAPE_SK_INPUT, QVector<XcbRectAdaptor>());
    if (picture) {
      callRenderFreePicture(picture);
    }
    picture = xcb_generate_id(c_X11);
    int format = getRenderFormatForVisual(rootWindow->visualId());
    xcb_render_create_picture_value_list_t picValue;
    callRenderCreatePicture(picture, overlay, format, 0, picValue)->force();
    callCompositeRedirectSubwindows(rootWindow->windowId(), XCB_COMPOSITE_REDIRECT_AUTOMATIC);
    callHideCursor(overlay);
    if (focus) {
      callEwmhRequestChangeActiveWindow(focus);
    }
    WindowTracker::instance()->setDamageEnabled(true);
    pollTimer.start();
  }
  pollMouse(true);
}

void ZoomOverlay::refresh()
{
  bool composite = WindowTracker::hasCompositeManager();
  auto wt = WindowTracker::instance();
  QRect rect = dirtyRegion.boundingRect().intersected(zoomGeom);
  callGrabServer()->force();
  for (auto win : wt->visibleWindows(rect)) {
    QRect clipRect = zoomGeom.translated(-win->frameGeometry().topLeft());
    QRect sourceRect = win->extentsGeometry().intersected(clipRect);
    QRect destRect = sourceRect.translated(win->frameGeometry().topLeft());
    callRenderComposite(
        composite && win->isAlpha() ? XCB_RENDER_PICT_OP_OVER : XCB_RENDER_PICT_OP_SRC,
        win->renderId(),
        backingPicture,
        sourceRect.x(),
        sourceRect.y(),
        destRect.x(),
        destRect.y(),
        destRect.width(),
        destRect.height()
    );
  }
  QRect cursorRect(lastPos.x() - xHot, lastPos.y() - yHot, cursorW, cursorH);
  if (rect.intersects(cursorRect)) {
    callRenderComposite(
        XCB_RENDER_PICT_OP_OVER, cursorPicture, backingPicture, 0, 0, lastPos.x() - xHot, lastPos.y() - yHot, cursorW, cursorH
    );
  }
  callRenderComposite(
      XCB_RENDER_PICT_OP_SRC,
      backingPicture,
      picture,
      zoomGeom.x() * zoomLevel,
      zoomGeom.y() * zoomLevel,
      0,
      0,
      screenGeom.width(),
      screenGeom.height()
  );
  callUngrabServer()->force();
}

void ZoomOverlay::pollMouse(bool force)
{
  QPoint pos = QCursor::pos();
  if (force || pos != lastPos) {
    int screenWidth = screenGeom.width();
    int width = int(screenWidth / zoomLevel) & ~0x01;
    int halfWidth = width >> 1;
    int x = pos.x() - halfWidth;
    if (x < 0) {
      x = 0;
    } else if (x > screenWidth - width) {
      x = screenWidth - width;
    }

    int screenHeight = screenGeom.height();
    int height = int(screenHeight / zoomLevel) & ~0x01;
    int halfHeight = height >> 1;
    int y = pos.y() - halfHeight;
    if (y < 0) {
      y = 0;
    } else if (y > screenHeight - height) {
      y = screenHeight - height;
    }

    QRect newGeom = QRect(x, y, width, height);
    if (force || newGeom != zoomGeom) {
      zoomGeom = newGeom;
      lastPos = pos;
      updateMatrix();
      dirtyRegion = zoomGeom;
    } else {
      addDirty(QRect(lastPos.x() - xHot - 1, lastPos.y() - yHot - 1, cursorW + 2, cursorH + 2));
      lastPos = pos;
      addDirty(QRect(lastPos.x() - xHot - 1, lastPos.y() - yHot - 1, cursorW + 2, cursorH + 2));
    }
  }
  if (!dirtyRegion.isEmpty()) {
    refresh();
    dirtyRegion = QRegion();
  }
}

void ZoomOverlay::updateMatrix()
{
  // Recalculate the zoom level for integer pixels
  //int targetWidth = int(screenGeom.width() / zoomLevel + 0.5);
  //zoomLevel = (float)screenGeom.width() / (float)targetWidth;

  callRenderSetSourceTransform(backingPicture, QTransform::fromScale(1.0 / zoomLevel, 1.0 / zoomLevel));
}

void ZoomOverlay::addDirty(const QRegion& region)
{
  dirtyRegion += region;
  // Now that dirtyRegion is nonempty the next tick of pollTimer will render
}

void ZoomOverlay::updateCursor(xcb_xfixes_get_cursor_image_reply_t* reply)
{
  if (!cursorPixmap || cursorW != reply->width || cursorH != reply->height) {
    if (cursorPixmap) {
      callXcbFreeGc(cursorGC);
      callXcbFreePixmap(cursorPixmap);
      callRenderFreePicture(cursorPicture);
    }
    cursorPixmap = xcb_generate_id(c_X11);
    cursorPicture = xcb_generate_id(c_X11);
    cursorGC = xcb_generate_id(c_X11);

    callXcbCreatePixmap(32, cursorPixmap, rootWindow->windowId(), reply->width, reply->height)->force();
    xcb_create_gc_value_list_t value;
    callXcbCreateGc(cursorGC, cursorPixmap, 0, value);
    int format = getRenderFormatForARGB32();
    xcb_render_create_picture_value_list_t picValue;
    callRenderCreatePicture(cursorPicture, cursorPixmap, format, 0, picValue);
  }
  xcb_put_image(
      c_X11,
      XCB_IMAGE_FORMAT_Z_PIXMAP,
      cursorPixmap,
      cursorGC,
      reply->width,
      reply->height,
      0,
      0,
      0,
      32,
      xcb_xfixes_get_cursor_image_cursor_image_length(reply) * 4,
      reinterpret_cast<uint8_t*>(xcb_xfixes_get_cursor_image_cursor_image(reply))
  );
  // Mark both the old cursor rectangle and the new cursor rectangle as dirty
  addDirty(QRect(lastPos.x() - xHot - 1, lastPos.y() - yHot - 1, cursorW + 2, cursorH + 2));
  xHot = reply->xhot;
  yHot = reply->yhot;
  cursorW = reply->width;
  cursorH = reply->height;
  addDirty(QRect(lastPos.x() - xHot - 1, lastPos.y() - yHot - 1, cursorW + 2, cursorH + 2));
}

bool ZoomOverlay::handleEvent(const CrudeXcb::EventType& event)
{
  callXcbAllowEvents(XCB_ALLOW_ASYNC_POINTER)->force();
  callXcbAllowEvents(XCB_ALLOW_ASYNC_KEYBOARD)->force();
  callXcbAllowEvents(XCB_ALLOW_SYNC_POINTER)->force();
  callXcbAllowEvents(XCB_ALLOW_SYNC_KEYBOARD)->force();
  if (event.extension == CrudeXcb::core) {
    if (event.eventType == XCB_BUTTON_RELEASE) {
      // Consume button releases silently
      return true;
    } else if (event.eventType == XCB_BUTTON_PRESS) {
      auto mEvent = event.cast<xcb_button_press_event_t>();
      if (mEvent->detail == XCB_BUTTON_INDEX_4) {
        zoomIn();
        return true;
      } else if (mEvent->detail == XCB_BUTTON_INDEX_5) {
        zoomOut();
        return true;
      }
      return true;
    }
  } else if (event.extension == CrudeXcb::xfixes && event.eventType == XCB_XFIXES_CURSOR_NOTIFY) {
    auto cEvent = event.cast<xcb_xfixes_cursor_notify_event_t>();
    if (cEvent->cursor_serial != lastCursor) {
      updateCursor(callGetCursorImage()->result());
    }
    return true;
  }
  return false;
}
