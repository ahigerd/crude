#include "libcrude/platformintegration.h"
#include "libcrude/xcb/xcbextension.h"
#include "libcrudecomp/windowtracker.h"
#include "zoomoverlay.h"

#include <QApplication>
#include <QTimer>
#include <QtDebug>

int main(int argc, char** argv)
{
  QApplication::setApplicationName("crude-zoom");
  QApplication::setOrganizationName("Alkahest");
  QApplication::setApplicationVersion("0.0.1");
  QApplication app(argc, argv);
  PlatformIntegrator platform(argc, argv);
  WindowTracker::initExtensions();

  WindowTracker wt;
  ZoomOverlay overlay;

  if (argc > 1) {
    QString zoom = app.arguments()[1];
    if (zoom.contains("%")) {
      overlay.setZoomLevel(zoom.replace("%", "").toFloat() * 0.01);
    } else {
      overlay.setZoomLevel(zoom.toFloat());
    }
  }

  if (c_PI->isDebugEnabled()) {
    QTimer::singleShot(5000, &app, SLOT(quit()));
  }
  int rv = app.exec();
  return rv;
}
