#ifndef CRUDE_POWERPANEL_H
#define CRUDE_POWERPANEL_H

#include <QWidget>
class QComboBox;
class QLabel;

class PowerPanel : public QWidget
{
  Q_OBJECT

public:
  PowerPanel(QWidget* parent = nullptr);

private slots:
  void loadPluginSettings();
  void pluginChanged();
  void lidActionChanged();

private:
  QComboBox* powerPlugin;
  QComboBox* batteryPlugin;
  QComboBox* lidAction;
  QLabel* restartNotice;
  QString originalBattery, originalPower;
};

#endif
