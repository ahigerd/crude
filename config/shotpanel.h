#ifndef CRUDE_SHOTPANEL_H
#define CRUDE_SHOTPANEL_H

#include "../shot/shotconfigwidget.h"
class QComboBox;

class ShotPanel : public ShotConfigWidget
{
  Q_OBJECT

public:
  ShotPanel(QWidget* parent = nullptr);

private slots:
  void localFieldChanged();

private:
  QComboBox* fSelection;
  QComboBox* fAction;
};

#endif
