#include "configwindow.h"
#include "displaypanel.h"
#include "hotkeypanel.h"
#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"
#include "panelpanel.h"
#include "powerpanel.h"
#include "shotpanel.h"

#include <QApplication>

int main(int argc, char** argv)
{
  QApplication::setApplicationName("crude-config");
  QApplication::setOrganizationName("Alkahest");
  QApplication::setApplicationVersion("0.0.1");
  QApplication app(argc, argv);
  PlatformIntegrator platform(argc, argv);
  Settings settings;

  ConfigWindow cw;
  cw.addPanel(new HotkeyPanel);
  cw.addPanel(new DisplayPanel);
  cw.addPanel(new PowerPanel);
  // cw.addPanel(new DesktopPanel);
  cw.addPanel(new PanelPanel);
  // cw.addPanel(new SoundPanel);
  // cw.addPanel(new NetworkPanel);
  cw.addPanel(new ShotPanel);
  cw.show();

  return app.exec();
}
