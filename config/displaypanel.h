#ifndef CRUDE_DISPLAYPANEL_H
#define CRUDE_DISPLAYPANEL_H

#include "libcrude/displaypower.h"

#include <QHash>
#include <QWidget>
class QLabel;
class QSlider;
class QComboBox;
class QCheckBox;
class QTimeEdit;
class QPushButton;
class QGroupBox;

class DisplayPanel : public QWidget
{
  Q_OBJECT

public:
  DisplayPanel(QWidget* parent = nullptr);

private slots:
  void buildScheduleLayout();
  void backlightAvailableUpdated(bool available);
  void backlightUpdated(float level);
  void updateBacklight(int level);
  void saverChanged();
  void scheduleToggled();
  void scheduleChanged();
  void openSaverSettings();
  void testSaver();

private:
  QTimeEdit* makeTimeEdit(QCheckBox* toggle);
  void loadSetting(const QList<DisplayPower::Schedule>& schedule, int mode, QCheckBox* check, QTimeEdit* time);
  void blockScheduleSignals(bool block);

  QWidget* backlightLabel;
  QSlider* backlight;
  QComboBox* saver;
  QLabel* lblLocker;
  QComboBox* locker;
  QGroupBox* schedule;
  QPushButton* saverSettings;
  QPushButton* saverTest;
  QCheckBox* saverOn;
  QTimeEdit* saverTime;
  QCheckBox* standbyOn;
  QTimeEdit* standbyTime;
  QCheckBox* suspendOn;
  QTimeEdit* suspendTime;
  QCheckBox* offOn;
  QTimeEdit* offTime;
  QHash<QString, DisplayPower::ScreenSaverConfig> screenSavers;
};

#endif
