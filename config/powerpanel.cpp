#include "powerpanel.h"

#include "libcrude/acpimonitor.h"
#include "libcrude/platformintegration.h"
#include "libcrude/powermanagement.h"
#include "libcrude/sessionsettings.h"

#include <QComboBox>
#include <QFont>
#include <QGridLayout>
#include <QLabel>
#include <QMetaEnum>
#include <QStyle>

PowerPanel::PowerPanel(QWidget* parent) : QWidget(parent)
{
  setWindowIcon(c_PI->icon({ "ac-adapter", "utilities-system-monitor", "preferences-system" }, QStyle::SP_ComputerIcon));
  setWindowTitle("Power Management");

  PowerManagement* pm = PowerManagement::instance();
  AcpiMonitor* acpi = AcpiMonitor::instance();

  QGridLayout* layout = new QGridLayout(this);
  layout->setColumnStretch(0, 0);
  layout->setColumnStretch(1, 1);

  layout->addWidget(new QLabel("Power Status Plugin:"), 0, 0);
  powerPlugin = new QComboBox(this);
  for (const QString& plugin : pm->availablePlugins()) {
    powerPlugin->addItem(plugin);
  }
  QObject::connect(powerPlugin, SIGNAL(currentIndexChanged(int)), this, SLOT(pluginChanged()));
  layout->addWidget(powerPlugin, 0, 1);

  layout->addWidget(new QLabel("Battery Plugin:"), 1, 0);
  batteryPlugin = new QComboBox(this);
  for (const QString& plugin : acpi->availablePlugins()) {
    batteryPlugin->addItem(plugin);
  }
  QObject::connect(batteryPlugin, SIGNAL(currentIndexChanged(int)), this, SLOT(pluginChanged()));
  layout->addWidget(batteryPlugin, 1, 1);

  restartNotice = new QLabel("You must log out for changes to take effect.");
  QFont boldFont = restartNotice->font();
  boldFont.setBold(true);
  restartNotice->setFont(boldFont);
  restartNotice->setAlignment(Qt::AlignCenter);
  restartNotice->setVisible(false);
  layout->addWidget(restartNotice, 2, 0, 1, 2);
  layout->setRowStretch(2, 0);

  layout->addWidget(new QLabel("When Lid is Closed:"), 3, 0);
  lidAction = new QComboBox(this);
  for (const auto& action : pm->allActions()) {
    lidAction->addItem(pm->actionName(action), action);
  }
  layout->addWidget(lidAction, 3, 1);

  layout->setRowStretch(4, 1);

  // TODO: configure sleep timeout

  QTimer::singleShot(0, this, SLOT(loadPluginSettings()));
}

void PowerPanel::loadPluginSettings()
{
  PowerManagement* pm = PowerManagement::instance();
  powerPlugin->blockSignals(true);
  originalPower = pm->activePlugin();
  powerPlugin->setCurrentText(originalPower);
  powerPlugin->blockSignals(false);

  AcpiMonitor* acpi = AcpiMonitor::instance();
  batteryPlugin->blockSignals(true);
  originalBattery = acpi->activePlugin();
  batteryPlugin->setCurrentText(originalBattery);
  batteryPlugin->blockSignals(false);
}

void PowerPanel::pluginChanged()
{
  bool powerChanged = powerPlugin->currentText() != originalPower;
  bool batteryChanged = batteryPlugin->currentText() != originalBattery;
  restartNotice->setVisible(powerChanged || batteryChanged);

  PowerManagement* pm = PowerManagement::instance();
  pm->setActivePlugin(powerPlugin->currentText());

  AcpiMonitor* acpi = AcpiMonitor::instance();
  acpi->setActivePlugin(batteryPlugin->currentText());
}

void PowerPanel::lidActionChanged()
{
  Settings::instance()->setValue(
      "lidaction", QMetaEnum::fromType<PowerManagement::Action>().valueToKey(lidAction->currentData().toInt())
  );
}
