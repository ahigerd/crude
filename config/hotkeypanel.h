#ifndef CRUDE_HOTKEYPANEL_H
#define CRUDE_HOTKEYPANEL_H

#include "hotkeymodel.h"

#include <QDialog>
#include <QTableView>
class QLineEdit;
class QPushButton;
class QStackedWidget;

class HotkeyPanel : public QTableView
{
  Q_OBJECT

public:
  HotkeyPanel(QWidget* parent = nullptr);

  QSize sizeHint() const;

private slots:
  void itemClicked(const QModelIndex& index);
  void triggered(const QModelIndex& index);
  void savePos();
  void restorePos();

private:
  int savedRow, savedCol;
};

class HotkeyEditor : public QDialog
{
  Q_OBJECT

public:
  HotkeyEditor(const HotkeyData& data, QWidget* parent = nullptr);

protected:
  void keyPressEvent(QKeyEvent* event);

signals:
  void updateHotkey(const QKeySequence& seq, const QString& command, const QString& customCommand);
  void private_updateHotkey(const QKeySequence& seq);
  void private_cancelHotkey();

private slots:
  void editHotkey();
  void save();

private:
  HotkeyData data;
  QPushButton* shortcut;
  QLineEdit* command;
  QStackedWidget* stack;
};

#endif
