#include "displaypanel.h"

#include "libcrude/displaypower.h"
#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"

#include <QCheckBox>
#include <QComboBox>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QStyle>
#include <QTimeEdit>

DisplayPanel::DisplayPanel(QWidget* parent) : QWidget(parent)
{
  setWindowIcon(c_PI->icon(
      { "preferences-desktop-screensaver", "preferences-desktop-display", "video-display" }, QStyle::SP_ComputerIcon
  ));
  setWindowTitle("Display");

  QGridLayout* layout = new QGridLayout(this);
  layout->setColumnStretch(0, 0);
  layout->setColumnStretch(1, 1);
  int row = 0;

  // TODO: what if multiple devices have backlight controls?
  DisplayPower* dp = c_PI->displayPower();
  backlightLabel = new QLabel("Backlight:");
  backlight = new QSlider(Qt::Horizontal, this);
  backlight->setMinimum(0);
  backlight->setMaximum(100);
  backlight->setSingleStep(1);
  backlight->setPageStep(10);
  backlight->setTickInterval(10);
  backlight->setTickPosition(QSlider::TicksBelow);
  backlight->setValue(dp->backlightLevel() * 100);
  layout->addWidget(backlightLabel, row, 0);
  layout->addWidget(backlight, row, 1);
  QObject::connect(dp, SIGNAL(backlightAvailableChanged(bool)), this, SLOT(backlightAvailableUpdated(bool)));
  QObject::connect(dp, SIGNAL(backlightChanged(float)), this, SLOT(backlightUpdated(float)));
  QObject::connect(backlight, SIGNAL(valueChanged(int)), this, SLOT(updateBacklight(int)));
  backlightAvailableUpdated(dp->backlightAvailable());
  ++row;

  layout->addWidget(new QLabel("Screen Saver:", this), row, 0);
  saver = new QComboBox(this);
  saver->addItem("Disabled", -1);
  layout->addWidget(saver, row, 1);
  ++row;

  layout->addWidget(lblLocker = new QLabel("Screen Locker:", this), row, 0);
  locker = new QComboBox(this);
  layout->addWidget(locker, row, 1);
  ++row;

  QString currentSaver = Settings::instance()->value("screensaver").toString();
  QString currentLocker = Settings::instance()->value("screenlocker").toString();
  bool hasSaverConfig = false;
  screenSavers.clear();
  for (const DisplayPower::ScreenSaverConfig& ss : dp->scanScreenSavers()) {
    screenSavers[ss.name] = ss;
    hasSaverConfig = hasSaverConfig || ss.config.contains("configure");
    if (ss.type == DisplayPower::Locker) {
      locker->addItem(ss.name, int(ss.type));
      if (currentLocker == ss.name) {
        locker->setCurrentIndex(locker->count() - 1);
      }
    } else {
      saver->addItem(ss.name, int(ss.type));
      if (currentSaver == ss.name) {
        saver->setCurrentIndex(saver->count() - 1);
      }
    }
  }
  saver->setEnabled(saver->count() > 1);
  locker->setEnabled(saver->currentData().toInt() == DisplayPower::Scheduler);
  QObject::connect(saver, SIGNAL(currentIndexChanged(int)), this, SLOT(saverChanged()));
  QObject::connect(locker, SIGNAL(currentIndexChanged(int)), this, SLOT(saverChanged()));

  saverSettings = new QPushButton("Open Screen Saver Settings", this);
  QObject::connect(saverSettings, SIGNAL(clicked()), this, SLOT(openSaverSettings()));
  layout->addWidget(saverSettings, row, 1);
  ++row;

  saverTest = new QPushButton("Test Screen Saver", this);
  QObject::connect(saverTest, SIGNAL(clicked()), this, SLOT(testSaver()));
  layout->addWidget(saverTest, row, 1);
  ++row;

  schedule = new QGroupBox("Schedule:", this);
  new QGridLayout(schedule);
  layout->addWidget(schedule, row, 0, 1, 2);
  ++row;

  buildScheduleLayout();
  QObject::connect(dp, SIGNAL(displayCapabilitiesUpdated()), this, SLOT(buildScheduleLayout()));

  layout->setRowStretch(row, 1);

  saverSettings->setVisible(hasSaverConfig);
  saverOn->setEnabled(dp->standbyModeAvailable(DisplayPower::ScreenSaver));
  lblLocker->setVisible(saver->currentData().toInt() == DisplayPower::Scheduler);
  locker->setVisible(saver->currentData().toInt() == DisplayPower::Scheduler);
}

void DisplayPanel::loadSetting(const QList<DisplayPower::Schedule>& schedule, int mode, QCheckBox* check, QTimeEdit* time)
{
  if (!check) {
    return;
  }
  for (const DisplayPower::Schedule& sched : schedule) {
    if (sched.standbyMode != mode) {
      continue;
    }
    check->setChecked(true);
    time->setTime(sched.delay);
    return;
  }
  check->setChecked(false);
  time->setEnabled(false);
}

void DisplayPanel::buildScheduleLayout()
{
  DisplayPower* dp = c_PI->displayPower();
  QGridLayout* sLayout = static_cast<QGridLayout*>(schedule->layout());
  for (QObject* obj : schedule->children()) {
    if (qobject_cast<QWidget*>(obj)) {
      delete obj;
    }
  }

  int ssRow = 0;
  saverOn = new QCheckBox("Screen Saver", schedule);
  saverTime = makeTimeEdit(saverOn);
  sLayout->addWidget(saverOn, ssRow, 0);
  sLayout->addWidget(saverTime, ssRow, 1);
  ++ssRow;

  if (dp->standbyModeAvailable(DisplayPower::Standby)) {
    standbyOn = new QCheckBox("Standby", schedule);
    standbyTime = makeTimeEdit(standbyOn);
    sLayout->addWidget(standbyOn, ssRow, 0);
    sLayout->addWidget(standbyTime, ssRow, 1);
    ++ssRow;
  } else {
    standbyOn = nullptr;
    standbyTime = nullptr;
  }

  if (dp->standbyModeAvailable(DisplayPower::Suspend)) {
    suspendOn = new QCheckBox("Suspend", schedule);
    suspendTime = makeTimeEdit(suspendOn);
    sLayout->addWidget(suspendOn, ssRow, 0);
    sLayout->addWidget(suspendTime, ssRow, 1);
    ++ssRow;
  } else {
    suspendOn = nullptr;
    suspendTime = nullptr;
  }

  if (dp->standbyModeAvailable(DisplayPower::Off)) {
    offOn = new QCheckBox("Display Off", schedule);
    offTime = makeTimeEdit(offOn);
    sLayout->addWidget(offOn, ssRow, 0);
    sLayout->addWidget(offTime, ssRow, 1);
    ++ssRow;
  } else {
    offOn = nullptr;
    offTime = nullptr;
  }

  QList<DisplayPower::Schedule> schedList = dp->schedule();
  blockScheduleSignals(true);
  loadSetting(schedList, DisplayPower::ScreenSaver, saverOn, saverTime);
  loadSetting(schedList, DisplayPower::Standby, standbyOn, standbyTime);
  loadSetting(schedList, DisplayPower::Suspend, suspendOn, suspendTime);
  loadSetting(schedList, DisplayPower::Off, offOn, offTime);
  saverTest->setVisible(saverOn->isEnabled());
  saverTest->setEnabled(saverOn->isEnabled() && saverOn->isChecked());
  blockScheduleSignals(false);
}

void DisplayPanel::backlightAvailableUpdated(bool available)
{
  backlightLabel->setVisible(available);
  backlight->setVisible(available);
}

void DisplayPanel::backlightUpdated(float level)
{
  backlight->blockSignals(true);
  backlight->setValue(level * 100);
  backlight->blockSignals(false);
}

void DisplayPanel::updateBacklight(int level)
{
  DisplayPower* dp = c_PI->displayPower();
  dp->setBacklightLevel(level * 0.01f);
}

void DisplayPanel::saverChanged()
{
  DisplayPower* dp = c_PI->displayPower();
  Settings* s = Settings::instance();
  int type = saver->currentData().toInt();
  QString ss = (type < 0) ? "disabled" : saver->currentText();
  QString sl = locker->currentText();
  QString oldValue = s->value("screensaver").toString();
  QString oldLocker = s->value("screenlocker").toString();

  s->setValue("screensaver", ss);
  if (type == DisplayPower::Scheduler) {
    s->setValue("screenlocker", sl);
  } else {
    s->remove("screenlocker");
  }
  bool hasSettings = false;
  if (type >= 0 && screenSavers.contains(ss)) {
    if (screenSavers[ss].config.contains("configure")) {
      hasSettings = true;
    } else if (type == DisplayPower::Scheduler && screenSavers.contains(sl)) {
      hasSettings = screenSavers[sl].config.contains("configure");
    }
  }
  saverSettings->setVisible(hasSettings);
  lblLocker->setVisible(type == DisplayPower::Scheduler);
  locker->setVisible(type == DisplayPower::Scheduler);
  saverOn->setEnabled(dp->standbyModeAvailable(DisplayPower::ScreenSaver));
  scheduleToggled();
  saverTest->setVisible(saverOn->isEnabled());
  saverTest->setEnabled(saverOn->isEnabled() && saverOn->isChecked());
  if (ss != oldValue || sl != oldLocker) {
    s->broadcastReload();
  }
}

void DisplayPanel::scheduleToggled()
{
  blockScheduleSignals(true);
  if (saverTime) {
    saverTime->setEnabled(saverOn->isEnabled() && saverOn->isChecked());
  }
  if (standbyTime) {
    standbyTime->setEnabled(standbyOn->isChecked());
  }
  if (suspendTime) {
    suspendTime->setEnabled(suspendOn->isChecked());
  }
  if (offTime) {
    offTime->setEnabled(offOn->isChecked());
  }
  blockScheduleSignals(false);
  scheduleChanged();
}

void DisplayPanel::openSaverSettings()
{
  QString ss = saver->currentText();
  QString sl = locker->currentText();
  if (screenSavers.contains(ss) && screenSavers[ss].config.contains("configure")) {
    c_PI->runCommand(screenSavers[ss].config["configure"]);
  } else if (screenSavers.contains(sl) && screenSavers[sl].config.contains("configure")) {
    c_PI->runCommand(screenSavers[sl].config["configure"]);
  }
}

void DisplayPanel::testSaver()
{
  DisplayPower* dp = c_PI->displayPower();
  dp->setStandbyMode(DisplayPower::ScreenSaver);
}

void DisplayPanel::scheduleChanged()
{
  QList<DisplayPower::Schedule> schedule;
  blockScheduleSignals(true);

  if (saverOn && saverOn->isChecked()) {
    schedule << (DisplayPower::Schedule){ saverTime->time(), DisplayPower::ScreenSaver };
  }
  if (standbyOn && standbyOn->isChecked()) {
    schedule << (DisplayPower::Schedule){ standbyTime->time(), DisplayPower::Standby };
    if (suspendTime && suspendTime->time() < standbyTime->time()) {
      suspendTime->setTime(standbyTime->time());
    }
    if (offTime && offTime->time() < standbyTime->time()) {
      offTime->setTime(standbyTime->time());
    }
  }
  if (suspendOn && suspendOn->isChecked()) {
    schedule << (DisplayPower::Schedule){ suspendTime->time(), DisplayPower::Suspend };
    if (offTime && offTime->time() < suspendTime->time()) {
      offTime->setTime(suspendTime->time());
    }
  }
  if (offOn && offOn->isChecked()) {
    schedule << (DisplayPower::Schedule){ offTime->time(), DisplayPower::Off };
  }

  DisplayPower* dp = c_PI->displayPower();
  dp->setSchedule(schedule);

  blockScheduleSignals(false);
}

QTimeEdit* DisplayPanel::makeTimeEdit(QCheckBox* toggle)
{
  QTimeEdit* edit = new QTimeEdit();
  edit->setMinimumTime(QTime(0, 1, 0));
  edit->setMaximumTime(QTime(10, 0, 0));
  edit->setDisplayFormat("HH:mm:ss");
  QObject::connect(toggle, SIGNAL(toggled(bool)), this, SLOT(scheduleToggled()));
  QObject::connect(edit, SIGNAL(timeChanged(QTime)), this, SLOT(scheduleChanged()));
  return edit;
}

void DisplayPanel::blockScheduleSignals(bool block)
{
  QWidget* widgets[] = { saverOn, standbyOn, suspendOn, offOn, saverTime, standbyTime, suspendTime, offTime };
  for (QWidget* w : widgets) {
    if (w) {
      w->blockSignals(block);
    }
  }
}
