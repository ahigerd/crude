TEMPLATE = app
TARGET = crude-config
QT = core widgets
LIBS += -L.. -lcrude
include(../crude-common.pri)

HEADERS += ../panel/panelwidget.h
SOURCES += main.cpp

HEADERS += configwindow.h   hotkeymodel.h   hotkeypanel.h   displaypanel.h   powerpanel.h   panelpanel.h
SOURCES += configwindow.cpp hotkeymodel.cpp hotkeypanel.cpp displaypanel.cpp powerpanel.cpp panelpanel.cpp

HEADERS += shotpanel.h   ../shot/shotconfigwidget.h
SOURCES += shotpanel.cpp ../shot/shotconfigwidget.cpp

HEADERS += panel/shortcuteditor.h   panel/switchereditor.h
SOURCES += panel/shortcuteditor.cpp panel/switchereditor.cpp

PRE_TARGETDEPS += ../libcrude.a


