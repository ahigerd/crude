#include "panelpanel.h"

#include "../panel/panelwidget.h"
#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"

#include <QAction>
#include <QBoxLayout>
#include <QComboBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QGridLayout>
#include <QItemSelectionModel>
#include <QLabel>
#include <QListWidget>
#include <QMenu>
#include <QMetaEnum>
#include <QMetaObject>
#include <QPushButton>
#include <QScopedPointer>
#include <QSettings>
#include <QSlider>
#include <QSpinBox>
#include <QStyle>
#include <QToolButton>

QMap<int, const QMetaObject*>& PanelPanel::editorTypes()
{
  static QMap<int, const QMetaObject*> types;
  return types;
}

QSettings* PanelPanel::getSettings(const QString& path)
{
  if (path == "default") {
    return new QSettings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-panel");
  } else {
    return new QSettings(path, QSettings::IniFormat);
  }
}

bool PanelPanel::registerEditor(int type, const QMetaObject& mo)
{
  editorTypes()[type] = &mo;
  return true;
}

PanelPanel::PanelPanel(QWidget* parent) : QWidget(parent)
{
  setWindowIcon(c_PI->icon(
      { "application-x-executable", "focus-legacy-systray", "preferences-desktop-theme" }, QStyle::SP_FileDialogListView
  ));
  setWindowTitle("Panel");

  QGridLayout* layout = new QGridLayout(this);
  layout->setColumnStretch(1, 1);
  layout->setRowStretch(2, 1);

  QHBoxLayout* selLayout = new QHBoxLayout;
  layout->addWidget(new QLabel("Panel:", this), 0, 0);
  layout->addLayout(selLayout, 0, 1);

  selector = new QComboBox(this);
  selLayout->addWidget(selector, 1);

  addPanelButton = new QToolButton(this);
  addPanelButton->setIcon(c_PI->icon("list-add"));
  addPanelButton->setText("+");
  selLayout->addWidget(addPanelButton, 0);

  delPanelButton = new QToolButton(this);
  delPanelButton->setIcon(c_PI->icon("list-remove"));
  delPanelButton->setText("-");
  selLayout->addWidget(delPanelButton, 0);

  edge = new QComboBox(this);
  layout->addWidget(new QLabel("Position:", this), 1, 0);
  layout->addWidget(edge, 1, 1);

  QHBoxLayout* sizeLayout = new QHBoxLayout;

  sizeSlider = new QSlider(Qt::Horizontal, this);
  sizeLayout->addWidget(sizeSlider, 1);

  sizeSpin = new QSpinBox(this);
  sizeLayout->addWidget(sizeSpin, 0);

  layout->addWidget(new QLabel("Size:", this), 2, 0);
  layout->addLayout(sizeLayout, 2, 1);

  list = new QListWidget(this);
  layout->addWidget(list, 3, 0, 1, 2);

  QHBoxLayout* buttonLayout = new QHBoxLayout;
  layout->addLayout(buttonLayout, 4, 0, 1, 2);
  buttonLayout->addStretch(1);

  QToolButton* addButton = new QToolButton(this);
  addButton->setIcon(c_PI->icon("list-add"));
  addButton->setText("+");
  addButton->setPopupMode(QToolButton::InstantPopup);
  buttonLayout->addWidget(addButton, 0);

  delButton = new QToolButton(this);
  delButton->setIcon(c_PI->icon("list-remove"));
  delButton->setText("-");
  buttonLayout->addWidget(delButton, 0);

  upButton = new QToolButton(this);
  upButton->setIcon(c_PI->icon({ "pan-up", "go-up" }, QStyle::SP_ArrowUp));
  upButton->setText("Move Up");
  buttonLayout->addWidget(upButton, 0);

  downButton = new QToolButton(this);
  downButton->setIcon(c_PI->icon({ "pan-down", "go-down" }, QStyle::SP_ArrowDown));
  downButton->setText("Move Down");
  buttonLayout->addWidget(downButton, 0);

  editButton = new QToolButton(this);
  editButton->setIcon(c_PI->icon({ "emblem-system-symbolic", "emblem-system" }));
  editButton->setText("...");
  buttonLayout->addWidget(editButton, 0);

  QMenu* addMenu = new QMenu(addButton);
  QMetaEnum widgetTypes = QMetaEnum::fromType<PanelWidget::Type>();
  for (int i = 0; i < widgetTypes.keyCount(); i++) {
    addMenu->addAction(widgetTypes.key(i))->setData(widgetTypes.value(i));
  }
  addButton->setMenu(addMenu);

  Settings* ss = Settings::instance();
  for (const QString& key : ss->keys("panels")) {
    QString path = ss->value("panels/" + key).toString();
    QScopedPointer<QSettings> s(getSettings(path));

    usedEdges << s->value("edge", "BottomEdge").toString();

    QString name;
    if (path.contains('/')) {
      name = path.section('/', -1, -1).section('.', 0, 0);
    } else {
      name = "Primary";
    }
    selector->addItem(name, path);
  }

  loadPanel();

  QObject::connect(selector, SIGNAL(currentIndexChanged(int)), this, SLOT(loadPanel()));
  QObject::connect(addPanelButton, SIGNAL(clicked()), this, SLOT(addPanel()));
  QObject::connect(delPanelButton, SIGNAL(clicked()), this, SLOT(removePanel()));
  QObject::connect(edge, SIGNAL(currentIndexChanged(int)), this, SLOT(changeEdge()));
  QObject::connect(sizeSlider, SIGNAL(valueChanged(int)), sizeSpin, SLOT(setValue(int)));
  QObject::connect(sizeSlider, SIGNAL(valueChanged(int)), this, SLOT(savePanel()), Qt::QueuedConnection);
  QObject::connect(sizeSpin, SIGNAL(valueChanged(int)), sizeSlider, SLOT(setValue(int)));
  QObject::connect(sizeSpin, SIGNAL(valueChanged(int)), this, SLOT(savePanel()), Qt::QueuedConnection);
  QObject::connect(addMenu, SIGNAL(triggered(QAction*)), this, SLOT(addComponent(QAction*)));
  QObject::connect(delButton, SIGNAL(clicked()), this, SLOT(removeComponent()));
  QObject::connect(upButton, SIGNAL(clicked()), this, SLOT(moveUp()));
  QObject::connect(downButton, SIGNAL(clicked()), this, SLOT(moveDown()));
  QObject::connect(editButton, SIGNAL(clicked()), this, SLOT(editComponent()));
  QObject::connect(list, SIGNAL(activated(QModelIndex)), this, SLOT(editComponent()));
  QObject::connect(
      list->selectionModel(), SIGNAL(currentChanged(QModelIndex, QModelIndex)), this, SLOT(selectionChanged())
  );
  QObject::connect(&reloadTimer, SIGNAL(timeout()), Settings::instance(), SLOT(broadcastReload()));

  reloadTimer.setSingleShot(true);
}

void PanelPanel::loadPanel()
{
  QScopedPointer<QSettings> s(getSettings(selector->currentData().toString()));
  int size = s->value("size", sizeSlider->minimum()).toInt();

  QString currentEdge = s->value("edge", "BottomEdge").toString();
  edge->clear();
  for (const QString& e : QStringList{ "TopEdge", "BottomEdge", "LeftEdge", "RightEdge" }) {
    if (!usedEdges.contains(e) || currentEdge == e) {
      edge->addItem(QString(e).replace("Edge", ""), e);
    }
  }
  edge->setCurrentIndex(edge->findData(currentEdge));

  list->clear();
  QStringList widgets = s->value("components").toStringList();
  for (const QString& widget : widgets) {
    if (widget == "spacer") {
      list->addItem("-- Spacer --");
      list->item(list->count() - 1)->setData(Qt::UserRole + 1, PanelWidget::Spacer);
    } else {
      QString type = s->value(widget + "/type").toString();
      int typeEnum = QMetaEnum::fromType<PanelWidget::Type>().keyToValue(qPrintable(type));
      list->addItem(QString("%1: %2").arg(widget).arg(type));
      list->item(list->count() - 1)->setData(Qt::UserRole + 1, typeEnum);
    }
    list->item(list->count() - 1)->setData(Qt::UserRole, widget);
  }

  selectionChanged();
  changeEdge(true);
  sizeSlider->setValue(size);
  sizeSpin->setValue(size);
}

void PanelPanel::savePanel()
{
  QMetaEnum widgetTypes = QMetaEnum::fromType<PanelWidget::Type>();

  QScopedPointer<QSettings> s(getSettings(selector->currentData().toString()));
  s->setValue("edge", edge->currentData().toString());
  s->setValue("size", sizeSlider->value());

  QStringList components;
  for (int i = 0; i < list->count(); i++) {
    QListWidgetItem* item = list->item(i);
    QString name = item->data(Qt::UserRole).toString();
    components << name;
    if (name != "spacer") {
      s->setValue(name + "/type", widgetTypes.valueToKey(item->data(Qt::UserRole + 1).toInt()));
    }
  }
  QStringList oldComponents = s->value("components").toStringList();
  for (const QString& comp : oldComponents) {
    if (!components.contains(comp)) {
      s->remove(comp);
    }
  }
  s->setValue("components", QVariant::fromValue(components));
  s->sync();

  reloadTimer.start(500);
}

void PanelPanel::addPanel() {}

void PanelPanel::removePanel() {}

void PanelPanel::changeEdge(bool loadOnly)
{
  QScopedPointer<QSettings> s(getSettings(selector->currentData().toString()));

  QString currentEdge = edge->currentData().toString();
  usedEdges.remove(s->value("edge", "BottomEdge").toString());
  usedEdges << currentEdge;

  int defaultSize;
  if (currentEdge == "TopEdge" || currentEdge == "BottomEdge") {
    sizeSlider->setRange(20, 60);
    sizeSpin->setRange(20, 60);
    defaultSize = 28;
  } else {
    sizeSlider->setRange(100, 300);
    sizeSpin->setRange(100, 300);
    defaultSize = 150;
  }
  if (sizeSlider->value() < sizeSlider->minimum() || sizeSlider->value() > sizeSlider->maximum()) {
    sizeSlider->setValue(defaultSize);
    sizeSpin->setValue(defaultSize);
  }

  if (!loadOnly) {
    savePanel();
  }
}

void PanelPanel::selectionChanged()
{
  int index = list->currentRow();
  delButton->setEnabled(index >= 0);
  upButton->setEnabled(index > 0);
  downButton->setEnabled(index >= 0 && index < list->count() - 1);
  if (index >= 0) {
    int widgetType = list->currentItem()->data(Qt::UserRole + 1).toInt();
    editButton->setEnabled(editorTypes().contains(widgetType));
  } else {
    editButton->setEnabled(false);
  }
}

void PanelPanel::addComponent(QAction* action)
{
  int widgetType = action->data().toInt();
  QString widget = QMetaEnum::fromType<PanelWidget::Type>().valueToKey(widgetType);
  QString name = widget.toLower();
  if (widgetType == PanelWidget::Spacer) {
    list->addItem("-- Spacer --");
  } else {
    int retry = 0;
    bool ok;
    do {
      ok = true;
      for (int i = 0; i < list->count(); i++) {
        if (list->item(i)->data(Qt::UserRole).toString() == name) {
          name = widget.toLower() + QString::number(++retry);
          ok = false;
          break;
        }
      }
    } while (!ok);
    list->addItem(QString("%1: %2").arg(name).arg(widget));
  }
  QListWidgetItem* item = list->item(list->count() - 1);
  item->setData(Qt::UserRole, name);
  item->setData(Qt::UserRole + 1, widgetType);
  savePanel();
}

void PanelPanel::removeComponent()
{
  QScopedPointer<QSettings> s(getSettings(selector->currentData().toString()));

  QString widget = list->currentItem()->data(Qt::UserRole).toString();
  s->remove(widget);

  delete list->takeItem(list->currentRow());

  savePanel();
}

void PanelPanel::moveUp()
{
  int pos = list->currentRow();
  list->insertItem(pos - 1, list->takeItem(pos));
  list->setCurrentRow(pos - 1);
  savePanel();
}

void PanelPanel::moveDown()
{
  int pos = list->currentRow();
  list->insertItem(pos + 1, list->takeItem(pos));
  list->setCurrentRow(pos + 1);
  savePanel();
}

void PanelPanel::editComponent()
{
  int widgetType = list->currentItem()->data(Qt::UserRole + 1).toInt();
  const QMetaObject* mo = editorTypes().value(widgetType);
  if (!mo) {
    return;
  }

  QWidget* editor = static_cast<QWidget*>(mo->newInstance(
      Q_ARG(QString, selector->currentData().toString()),
      Q_ARG(QString, list->currentItem()->data(Qt::UserRole).toString()),
      Q_ARG(QWidget*, nullptr)
  ));
  if (!editor) {
    return;
  }

  QDialog* dlg = new QDialog(window());
  QString widgetName = QMetaEnum::fromType<PanelWidget::Type>().valueToKey(widgetType);
  dlg->setWindowTitle(QString("Configure %1").arg(widgetName));
  dlg->setAttribute(Qt::WA_DeleteOnClose);

  QVBoxLayout* layout = new QVBoxLayout(dlg);
  layout->addWidget(editor, 1);

  QDialogButtonBox* buttons =
      new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel | QDialogButtonBox::Apply, dlg);
  layout->addWidget(buttons, 0);

  QObject::connect(buttons, SIGNAL(accepted()), dlg, SLOT(accept()));
  QObject::connect(buttons, SIGNAL(rejected()), dlg, SLOT(reject()));
  QObject::connect(buttons, SIGNAL(accepted()), editor, SLOT(save()));
  QObject::connect(buttons->button(QDialogButtonBox::Apply), SIGNAL(clicked()), editor, SLOT(save()));

  dlg->show();
  dlg->open();
}
