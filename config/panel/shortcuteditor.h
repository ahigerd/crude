#ifndef CRUDE_SHORTCUTEDITOR_H
#define CRUDE_SHORTCUTEDITOR_H

#include <QScopedPointer>
#include <QSettings>
#include <QWidget>
class QListWidget;
class QListWidgetItem;
class QLineEdit;

class ShortcutEditor : public QWidget
{
  Q_OBJECT

public:
  Q_INVOKABLE ShortcutEditor(const QString& settingsPath, const QString& widgetName, QWidget* parent = nullptr);

public slots:
  void save();

private slots:
  void browseDesktop();
  void browseMenu();
  void browseIcon();
  void addShortcut();
  void removeShortcut();
  void itemSelected(QListWidgetItem* item);
  void fieldEdited();

private:
  QScopedPointer<QSettings> settings;
  QString widgetName;
  QListWidget* shortcutList;
  QLineEdit* commandLine;
  QLineEdit* customIcon;
};

#endif
