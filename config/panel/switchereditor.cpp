#include "switchereditor.h"

#include "../../panel/taskswitcher.h"
#include "../panelpanel.h"
#include "libcrude/sessionsettings.h"
#include "libcrude/windowlist.h"

#include <QCheckBox>
#include <QComboBox>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QMetaEnum>
#include <QSpinBox>
#include <QVBoxLayout>

static bool registered = PanelPanel::registerEditor(PanelWidget::Switcher, SwitcherEditor::staticMetaObject);
static QMetaEnum urgentFlags = QMetaEnum::fromType<WindowList::UrgentOption>();

SwitcherEditor::SwitcherEditor(const QString& settingsPath, const QString& widgetName, QWidget* parent)
: QWidget(parent), settings(PanelPanel::getSettings(settingsPath)), widgetName(widgetName)
{
  settings->beginGroup(widgetName);

  QGridLayout* layout = new QGridLayout(this);
  layout->setColumnStretch(1, 1);

  TaskSwitcher::Type switcherType =
      settings->value("subtype", "Taskbar").toString() == "DropDown" ? TaskSwitcher::DropDown : TaskSwitcher::Taskbar;
  QLabel* lblSubtype = new QLabel(tr("Switcher &Type:"), this);
  subtype = new QComboBox(this);
  subtype->addItem(tr("Taskbar"), "Taskbar");
  subtype->addItem(tr("Dropdown"), "DropDown");
  subtype->setCurrentIndex(switcherType == TaskSwitcher::Taskbar ? 0 : 1);
  lblSubtype->setBuddy(subtype);
  layout->addWidget(lblSubtype, 0, 0);
  layout->addWidget(subtype, 0, 1);

  QList<QByteArray> display = settings->value("display").toByteArray().split('|');
  QLabel* lblButtonType = new QLabel(tr("&Label Type:"), this);
  buttonType = new QComboBox(this);
  buttonType->addItem(tr("Window Title"), "WindowTitle");
  buttonType->addItem(tr("Program Name"), "ProgramName");
  buttonType->addItem(tr("No Text"), "NoText");
  if (display.contains("NoText")) {
    buttonType->setCurrentIndex(2);
  } else if (display.contains("ProgramName")) {
    buttonType->setCurrentIndex(1);
  } else {
    buttonType->setCurrentIndex(0);
  }
  lblButtonType->setBuddy(buttonType);
  layout->addWidget(lblButtonType, 1, 0);
  layout->addWidget(buttonType, 1, 1);

  showIcons = new QCheckBox(tr("&Show Icons"), this);
  showIcons->setChecked(!display.contains("NoIcons"));
  layout->addWidget(showIcons, 2, 1);

  showFlat = new QCheckBox(tr("&Flat Buttons"), this);
  showFlat->setChecked(display.contains("Flat"));
  layout->addWidget(showFlat, 3, 1);

  QLabel* lblIconSize = new QLabel(tr("Icon Si&ze:"), this);
  iconSize = new QSpinBox(this);
  iconSize->setRange(16, 64);
  iconSize->setValue(settings->value("iconSize", 16).toInt());
  lblIconSize->setBuddy(iconSize);
  layout->addWidget(lblIconSize, 4, 0);
  layout->addWidget(iconSize, 4, 1);

  WindowList::UrgentFlags urgent(urgentFlags.keysToValue(settings->value("urgent", "UrgentText").toByteArray()));
  QLabel* lblUrgentMode = new QLabel(tr("&Urgent Windows:"), this);
  urgentMode = new QComboBox(this);
  urgentMode->addItem(tr("Do Not Highlight"), "NoUrgency");
  urgentMode->addItem(tr("Mark Title"), "UrgentText");
  urgentMode->addItem(tr("Flash"), "UrgentCheck");
  urgentMode->addItem(tr("Flash and Mark Title"), "UrgentCheck|UrgentText");
  urgentMode->setCurrentIndex(int(urgent));
  lblUrgentMode->setBuddy(buttonType);
  layout->addWidget(lblUrgentMode, 5, 0);
  layout->addWidget(urgentMode, 5, 1);

  WindowList::GroupingMode groupingMode = WindowList::stringToGroupingMode(settings->value("group").toByteArray());
  grpGrouping = new QGroupBox(tr("&Grouping"), this);
  QVBoxLayout* lGrouping = new QVBoxLayout(grpGrouping);
  grpGrouping->setCheckable(true);
  grpGrouping->setChecked(groupingMode & WindowList::EnableGrouping);
  layout->addWidget(grpGrouping, 6, 0, 1, 2);

  groupProcess = new QCheckBox(tr("Group by &Process ID"), grpGrouping);
  groupProcess->setChecked(groupingMode & WindowList::GroupByProcess);
  lGrouping->addWidget(groupProcess);

  groupProgram = new QCheckBox(tr("Group by Program &Name"), grpGrouping);
  groupProgram->setChecked(groupingMode & WindowList::GroupByProgram);
  lGrouping->addWidget(groupProgram);

  groupClass = new QCheckBox(tr("Group by &Class Name"), grpGrouping);
  groupClass->setChecked(groupingMode & WindowList::GroupByClass);
  lGrouping->addWidget(groupClass);

  groupInstance = new QCheckBox(tr("Group by &Instance Name"), grpGrouping);
  groupInstance->setChecked(groupingMode & WindowList::GroupByInstance);
  lGrouping->addWidget(groupInstance);

  noSingleGroups = new QCheckBox(tr("No Single G&roups"), grpGrouping);
  noSingleGroups->setChecked(groupingMode & WindowList::NoSingleGroups);
  lGrouping->addWidget(noSingleGroups);

  grpHotkey = new QGroupBox(tr("Use Focus &Hotkeys"), this);
  QVBoxLayout* lHotkey = new QVBoxLayout(grpHotkey);
  grpHotkey->setCheckable(true);
  grpHotkey->setChecked(settings->value("useHotkeys", false).toBool());
  layout->addWidget(grpHotkey, 7, 0, 1, 2);

  linearApp = new QCheckBox(tr("Lin&ear Focus Chain"), grpHotkey);
  linearApp->setChecked(settings->value("linear", false).toBool());
  lHotkey->addWidget(linearApp);

  useOverlay = new QCheckBox(tr("Use O&verlay"), grpHotkey);
  useOverlay->setChecked(settings->value("overlay", true).toBool());
  lHotkey->addWidget(useOverlay);

  QObject::connect(subtype, SIGNAL(currentIndexChanged(int)), this, SLOT(enableFlat(int)));
  QObject::connect(grpGrouping, SIGNAL(toggled(bool)), this, SLOT(enableGrouping(bool)));
  QObject::connect(grpHotkey, SIGNAL(toggled(bool)), this, SLOT(enableHotkey(bool)));
}

void SwitcherEditor::save()
{
  for (const QString& key : settings->childKeys()) {
    if (key == "type") {
      continue;
    }
    settings->remove(key);
  }

  settings->setValue("subtype", (subtype->currentIndex() == 0) ? "Taskbar" : "DropDown");
  settings->setValue("iconSize", iconSize->value());
  settings->setValue("urgent", urgentMode->currentData().toString());

  QString display = buttonType->currentData().toString();
  if (!showIcons->isChecked()) {
    display += "|NoIcons";
  }
  if (showFlat->isChecked()) {
    display += "|Flat";
  }
  settings->setValue("display", display);

  WindowList::GroupingMode group(0);
  if (grpGrouping->isChecked()) {
    group = WindowList::EnableGrouping;
    if (groupProcess->isChecked()) {
      group |= WindowList::GroupByProcess;
    }
    if (groupProgram->isChecked()) {
      group |= WindowList::GroupByProgram;
    }
    if (groupClass->isChecked()) {
      group |= WindowList::GroupByClass;
    }
    if (groupInstance->isChecked()) {
      group |= WindowList::GroupByInstance;
    }
    if (noSingleGroups->isChecked()) {
      group |= WindowList::NoSingleGroups;
    }
  }
  settings->setValue("group", QString::fromUtf8(WindowList::groupingModeToString(group)));

  if (grpHotkey->isChecked()) {
    settings->setValue("useHotkeys", true);
    settings->setValue("linear", linearApp->isChecked());
    settings->setValue("overlay", useOverlay->isChecked());
  }

  settings->sync();
  Settings::instance()->broadcastReload();
}

void SwitcherEditor::enableFlat(int subtype)
{
  showFlat->setEnabled(subtype == 0);
}

void SwitcherEditor::enableGrouping(bool on)
{
  groupProcess->setEnabled(on);
  groupProgram->setEnabled(on);
  groupClass->setEnabled(on);
  groupInstance->setEnabled(on);
  noSingleGroups->setEnabled(on);
}

void SwitcherEditor::enableHotkey(bool on)
{
  linearApp->setEnabled(on);
  useOverlay->setEnabled(on);
}
