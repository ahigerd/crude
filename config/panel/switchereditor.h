#ifndef CRUDE_SWITCHEREDITOR_H
#define CRUDE_SWITCHEREDITOR_H

#include <QScopedPointer>
#include <QSettings>
#include <QWidget>
class QGroupBox;
class QCheckBox;
class QComboBox;
class QSpinBox;

class SwitcherEditor : public QWidget
{
  Q_OBJECT

public:
  Q_INVOKABLE SwitcherEditor(const QString& settingsPath, const QString& widgetName, QWidget* parent = nullptr);

public slots:
  void save();

private slots:
  void enableFlat(int subtype);
  void enableGrouping(bool on);
  void enableHotkey(bool on);

private:
  QScopedPointer<QSettings> settings;
  QString widgetName;
  QComboBox* subtype;
  QComboBox* buttonType;
  QCheckBox* showIcons;
  QCheckBox* showFlat;
  QComboBox* urgentMode;
  QSpinBox* iconSize;
  QGroupBox* grpGrouping;
  QCheckBox* groupProcess;
  QCheckBox* groupProgram;
  QCheckBox* groupClass;
  QCheckBox* groupInstance;
  QCheckBox* noSingleGroups;
  QGroupBox* grpHotkey;
  QCheckBox* linearApp;
  QCheckBox* useOverlay;
};

#endif
