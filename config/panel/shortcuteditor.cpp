#include "shortcuteditor.h"

#include "../../panel/panelwidget.h"
#include "../panelpanel.h"
#include "libcrude/environmentprefs.h"
#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"
#include "libcrude/xdglauncher.h"
#include "libcrude/xdgmenumodel.h"

#include <QDialogButtonBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QImageReader>
#include <QItemSelectionModel>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QPushButton>
#include <QToolButton>
#include <QTreeView>

static bool registered = PanelPanel::registerEditor(PanelWidget::Shortcuts, ShortcutEditor::staticMetaObject);

static QString findDataDir(const QString& name)
{
  for (QString dir : EnvironmentPrefs::dataDirs()) {
    dir += "/" + name;
    if (QFileInfo(dir).isDir()) {
      return dir;
    }
  }
  return QString();
}

static void fillItem(QListWidgetItem* item, const QString& command, const QString& iconName, QString* updateIconName = nullptr)
{
  item->setData(Qt::UserRole, command);
  item->setData(Qt::UserRole + 1, iconName);

  QString iconLabel = command;
  QString displayIconName = iconName;
  QString programName = command;
  if (command.endsWith(".desktop")) {
    XdgLauncher launcher(command);
    programName = launcher.execPath();
    iconLabel = launcher.name();
    if (iconName.isEmpty()) {
      QString launcherIcon = launcher.iconPath();
      QIcon checkIcon = c_PI->icon(launcherIcon);
      if (!checkIcon.isNull()) {
        displayIconName = launcherIcon;
      }
    }
  }
  if (displayIconName.isEmpty() && updateIconName) {
    programName = QFileInfo(programName).fileName();
    int spacePos = programName.indexOf(' ');
    int quotePos = programName.indexOf('"');
    if (spacePos > 0 && (quotePos < 0 || quotePos > spacePos)) {
      programName = programName.left(spacePos - 1);
    } else if (quotePos > 0) {
      programName = programName.left(quotePos - 1);
    }
    programName = programName.section(".desktop", 0, 0);
    QIcon programIcon = c_PI->icon(programName);
    if (!programIcon.isNull()) {
      *updateIconName = displayIconName = programName;
    }
  }
  if (displayIconName.isEmpty()) {
    displayIconName = "image-missing";
  }
  item->setData(Qt::DisplayRole, iconLabel);
  item->setData(Qt::DecorationRole, c_PI->icon(displayIconName, QStyle::SP_MessageBoxQuestion));
}

ShortcutEditor::ShortcutEditor(const QString& settingsPath, const QString& widgetName, QWidget* parent)
: QWidget(parent), settings(PanelPanel::getSettings(settingsPath)), widgetName(widgetName)
{
  QGridLayout* layout = new QGridLayout(this);
  layout->setColumnStretch(0, 0);
  layout->setColumnStretch(1, 1);
  layout->setRowStretch(2, 1);
  layout->setRowStretch(7, 1);

  shortcutList = new QListWidget(this);
  int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
  int scrollWidth = style()->pixelMetric(QStyle::PM_ScrollBarExtent);
  shortcutList->setFixedHeight(64 + 2 * frameWidth + scrollWidth);
  shortcutList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  shortcutList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  shortcutList->setViewMode(QListView::IconMode);
  shortcutList->setResizeMode(QListView::Adjust);
  shortcutList->setIconSize(QSize(32, 32));
  shortcutList->setGridSize(QSize(48, 64));
  shortcutList->setSelectionMode(QListView::SingleSelection);
  shortcutList->setWrapping(false);
  shortcutList->setMovement(QListView::Static);
  shortcutList->setFlow(QListView::LeftToRight);
  shortcutList->setWordWrap(true);
  layout->addWidget(shortcutList, 0, 0, 1, 2);

  QHBoxLayout* hbox = new QHBoxLayout;
  hbox->addStretch(1);
  layout->addLayout(hbox, 1, 0, 1, 2);

  QToolButton* addButton = new QToolButton(this);
  addButton->setIcon(c_PI->icon("list-add"));
  addButton->setText("+");
  hbox->addWidget(addButton, 0);

  QToolButton* delButton = new QToolButton(this);
  delButton->setIcon(c_PI->icon("list-remove"));
  delButton->setText("-");
  hbox->addWidget(delButton, 0);

  layout->addWidget(new QLabel("Command:", this), 3, 0);
  commandLine = new QLineEdit(this);
  layout->addWidget(commandLine, 3, 1);

  hbox = new QHBoxLayout;
  layout->addLayout(hbox, 4, 0, 1, 2);
  hbox->addStretch(1);

  QPushButton* fromMenu = new QPushButton("Choose from Menu...", this);
  hbox->addWidget(fromMenu);

  QPushButton* browse = new QPushButton("Browse for .desktop...", this);
  hbox->addWidget(browse);

  layout->addWidget(new QLabel("Custom Icon:", this), 5, 0);
  customIcon = new QLineEdit(this);
  layout->addWidget(customIcon, 5, 1);

  hbox = new QHBoxLayout;
  hbox->addStretch(1);
  layout->addLayout(hbox, 6, 0, 1, 2);

  QPushButton* iconBrowse = new QPushButton("Browse for icon...", this);
  hbox->addWidget(iconBrowse, 0);

  settings->beginGroup(widgetName);
  for (const QString& key : settings->childKeys()) {
    if (key.contains('_') || key == "type") {
      continue;
    }
    QListWidgetItem* item = new QListWidgetItem(shortcutList);
    QString command = settings->value(key).toString();
    QString iconName = settings->value(key + "_icon").toString();
    fillItem(item, command, iconName);
  }

  QObject::connect(
      shortcutList,
      SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
      this,
      SLOT(itemSelected(QListWidgetItem*))
  );
  QObject::connect(addButton, SIGNAL(clicked()), this, SLOT(addShortcut()));
  QObject::connect(delButton, SIGNAL(clicked()), this, SLOT(removeShortcut()));
  QObject::connect(browse, SIGNAL(clicked()), this, SLOT(browseDesktop()));
  QObject::connect(fromMenu, SIGNAL(clicked()), this, SLOT(browseMenu()));
  QObject::connect(iconBrowse, SIGNAL(clicked()), this, SLOT(browseIcon()));
  QObject::connect(commandLine, SIGNAL(editingFinished()), this, SLOT(fieldEdited()));
  QObject::connect(customIcon, SIGNAL(editingFinished()), this, SLOT(fieldEdited()));
}

void ShortcutEditor::save()
{
  for (const QString& key : settings->childKeys()) {
    if (key == "type") {
      continue;
    }
    settings->remove(key);
  }
  for (int i = 0; i < shortcutList->count(); i++) {
    QListWidgetItem* item = shortcutList->item(i);
    settings->setValue(QString::number(i), item->data(Qt::UserRole));
    QString icon = item->data(Qt::UserRole + 1).toString();
    if (!icon.isEmpty()) {
      settings->setValue(QString::number(i) + "_icon", icon);
    }
  }

  settings->sync();
  Settings::instance()->broadcastReload();
}

void ShortcutEditor::itemSelected(QListWidgetItem* item)
{
  if (!item) {
    commandLine->setText("");
    commandLine->setEnabled(false);
    customIcon->setText("");
    customIcon->setEnabled(false);
  } else {
    commandLine->setText(item->data(Qt::UserRole).toString());
    commandLine->setEnabled(true);
    customIcon->setText(item->data(Qt::UserRole + 1).toString());
    customIcon->setEnabled(true);
  }
}

void ShortcutEditor::browseDesktop()
{
  QString currentPath = commandLine->text();
  if (!currentPath.contains('/')) {
    currentPath = findDataDir("applications");
  }

  QString newPath = QFileDialog::getOpenFileName(this, "Select .desktop File", currentPath, ".desktop files (*.desktop)");
  if (newPath.isEmpty()) {
    return;
  }

  commandLine->setText(newPath);
  fieldEdited();
}

void ShortcutEditor::browseMenu()
{
  QDialog dlg(this);
  QVBoxLayout* layout = new QVBoxLayout(&dlg);

  QTreeView* view = new QTreeView(&dlg);
  XdgMenuModel* model = new XdgMenuModel(view);
  view->setModel(model);
  layout->addWidget(view, 1);

  QDialogButtonBox* bb = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal, &dlg);
  QAbstractButton* okButton = bb->button(QDialogButtonBox::Ok);
  QObject::connect(bb, SIGNAL(accepted()), &dlg, SLOT(accept()));
  QObject::connect(bb, SIGNAL(rejected()), &dlg, SLOT(reject()));
  layout->addWidget(bb, 0);

  QObject::connect(
      view->selectionModel(),
      &QItemSelectionModel::currentChanged,
      [okButton, model](const QModelIndex& current, const QModelIndex&) {
        XdgMenuItem item = model->menuItem(current);
        okButton->setEnabled(item.item);
      }
  );
  QObject::connect(view, &QAbstractItemView::activated, [&dlg, model](const QModelIndex& idx) {
    XdgMenuItem item = model->menuItem(idx);
    if (item.item) {
      dlg.accept();
    }
  });

  bool ok = dlg.exec();
  if (!ok) {
    return;
  }

  XdgMenuItem item = model->menuItem(view->currentIndex());
  if (!item.item) {
    return;
  }

  commandLine->setText(item.item->path());
  fieldEdited();
}

void ShortcutEditor::browseIcon()
{
  bool svgEnabled = QImageReader::supportedImageFormats().contains("svg");
  QString filters = "PNG images (*.png)";
  if (svgEnabled) {
    filters += ";;SVG images (*.svg)";
  }

  QString currentPath = customIcon->text();
  if (!currentPath.contains('/')) {
    currentPath = findDataDir("pixmaps");
    if (!currentPath.contains('/')) {
      currentPath = findDataDir("icons");
    }
  }

  QString newPath = QFileDialog::getOpenFileName(this, "Select Icon", currentPath, filters);
  if (newPath.isEmpty()) {
    return;
  }

  customIcon->setText(newPath);
  fieldEdited();
}

void ShortcutEditor::addShortcut()
{
  QListWidgetItem* item = new QListWidgetItem(shortcutList);
  item->setData(Qt::DecorationRole, c_PI->icon("image-missing", QStyle::SP_MessageBoxQuestion));
  shortcutList->setCurrentItem(item);
}

void ShortcutEditor::removeShortcut()
{
  if (shortcutList->currentItem()) {
    delete shortcutList->takeItem(shortcutList->currentRow());
  }
}

void ShortcutEditor::fieldEdited()
{
  QListWidgetItem* item = shortcutList->currentItem();
  if (!item) {
    addShortcut();
    item = shortcutList->currentItem();
  }
  QString command = commandLine->text();
  QString iconName = customIcon->text();
  QString updateIconName;
  fillItem(item, command, iconName, &updateIconName);
  if (!updateIconName.isEmpty()) {
    customIcon->setText(updateIconName);
  }
}
