#include "configwindow.h"

#include "libcrude/platformintegration.h"

#include <QGroupBox>
#include <QHBoxLayout>
#include <QListWidget>
#include <QStackedWidget>
#include <QStyle>

ConfigWindow::ConfigWindow(QWidget* parent) : QWidget(parent, Qt::Window)
{
  setWindowTitle("CRUDE Configurator");
  setWindowIcon(c_PI->icon({ "configuration-editor", "preferences-system" }, QStyle::SP_DesktopIcon));

  QHBoxLayout* layout = new QHBoxLayout(this);

  panelList = new QListWidget(this);
  int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
  int scrollWidth = style()->pixelMetric(QStyle::PM_ScrollBarExtent);
  panelList->setFixedWidth(80 + 2 * frameWidth + scrollWidth);
  panelList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  panelList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  panelList->setViewMode(QListView::IconMode);
  panelList->setResizeMode(QListView::Adjust);
  panelList->setIconSize(QSize(32, 32));
  panelList->setGridSize(QSize(80, 72));
  panelList->setSelectionMode(QListView::SingleSelection);
  panelList->setWrapping(false);
  panelList->setMovement(QListView::Static);
  panelList->setFlow(QListView::TopToBottom);
  panelList->setWordWrap(true);
  layout->addWidget(panelList, 0);

  panelFrame = new QGroupBox(this);
  layout->addWidget(panelFrame, 1);

  QHBoxLayout* frameLayout = new QHBoxLayout(panelFrame);
  panelStack = new QStackedWidget(panelFrame);
  frameLayout->addWidget(panelStack, 1);

  QObject::connect(panelList, SIGNAL(currentRowChanged(int)), this, SLOT(selectionChanged(int)));
}

void ConfigWindow::selectionChanged(int item)
{
  panelStack->setCurrentIndex(item);
  panelFrame->setTitle(panelStack->currentWidget()->windowTitle());
}

void ConfigWindow::addPanel(QWidget* panel)
{
  QListWidgetItem* item = new QListWidgetItem(panel->windowIcon(), panel->windowTitle(), panelList);
  item->setSizeHint(QSize(80, 64));
  panelStack->addWidget(panel);
  if (panelList->currentRow() < 0) {
    panelList->setCurrentRow(0);
  }
}
