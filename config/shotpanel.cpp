#include "shotpanel.h"

#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"

#include <QComboBox>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QStyle>
#include <QtDebug>

ShotPanel::ShotPanel(QWidget* parent) : ShotConfigWidget(parent)
{
  setWindowIcon(c_PI->icon({ "camera-photo", "folder-pictures" }, QStyle::SP_FileDialogInfoView));
  setWindowTitle("Screenshot");

  QGridLayout* layout = static_cast<QGridLayout*>(this->layout());
  QGroupBox* gHotkey = new QGroupBox("When launched with a hotkey:", this);
  QGridLayout* gLayout = new QGridLayout(gHotkey);

  QLabel* lSelection = new QLabel("Capture region:", gHotkey);
  fSelection = new QComboBox(gHotkey);
  lSelection->setBuddy(fSelection);
  fSelection->addItem("Select a rectangle with the mouse", "-g");
  fSelection->addItem("Select a window with the mouse", "-w");
  fSelection->addItem("Capture the currently-focused window", "-W");
  int selection = fSelection->findData(Settings::instance()->value("environment/shotRegion"));
  fSelection->setCurrentIndex(selection < 0 ? 0 : selection);
  gLayout->addWidget(lSelection, 0, 0);
  gLayout->addWidget(fSelection, 0, 1);

  QLabel* lAction = new QLabel("Output screenshot to:", gHotkey);
  fAction = new QComboBox(gHotkey);
  lAction->setBuddy(fAction);
  fAction->addItem("Open a preview window", "-p");
  fAction->addItem("Save to the default path", "-o");
  fAction->addItem("Prompt for a save path", "-i");
  fAction->addItem("Capture to the clipboard (requires clipboard manager)", "-c");
  selection = fAction->findData(Settings::instance()->value("environment/shotOutput"));
  fAction->setCurrentIndex(selection < 0 ? 0 : selection);
  gLayout->addWidget(lAction, 1, 0);
  gLayout->addWidget(fAction, 1, 1);

  QObject::connect(this, SIGNAL(fieldChanged()), this, SLOT(save()));
  QObject::connect(fSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(localFieldChanged()));
  QObject::connect(fAction, SIGNAL(currentIndexChanged(int)), this, SLOT(localFieldChanged()));

  layout->setRowMinimumHeight(3, style()->pixelMetric(QStyle::PM_LayoutTopMargin) * 2);
  layout->addWidget(gHotkey, 4, 0, 1, 3);
  layout->setRowStretch(5, 1);
}

void ShotPanel::localFieldChanged()
{
  Settings::instance()->setValue("environment/shotRegion", fSelection->currentData());
  Settings::instance()->setValue("environment/shotOutput", fAction->currentData());
}
