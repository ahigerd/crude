#ifndef CRUDE_PANELPANEL_H
#define CRUDE_PANELPANEL_H

#include <QMap>
#include <QSet>
#include <QTimer>
#include <QWidget>
class QSettings;
class QComboBox;
class QListWidget;
class QToolButton;
class QSlider;
class QSpinBox;

class PanelPanel : public QWidget
{
  Q_OBJECT

public:
  static bool registerEditor(int type, const QMetaObject& mo);
  static QSettings* getSettings(const QString& path);

  PanelPanel(QWidget* parent = nullptr);

private slots:
  void loadPanel();
  void savePanel();
  void addPanel();
  void removePanel();
  void changeEdge(bool loadOnly = false);

  void selectionChanged();
  void addComponent(QAction* item);
  void removeComponent();
  void moveUp();
  void moveDown();
  void editComponent();

private:
  static QMap<int, const QMetaObject*>& editorTypes();

  QComboBox* selector;
  QToolButton* addPanelButton;
  QToolButton* delPanelButton;
  QComboBox* edge;
  QSlider* sizeSlider;
  QSpinBox* sizeSpin;

  QListWidget* list;
  QToolButton* delButton;
  QToolButton* upButton;
  QToolButton* downButton;
  QToolButton* editButton;

  QSet<QString> usedEdges;

  QTimer reloadTimer;
};

#endif
