#ifndef CRUDE_CONFIGWINDOW_H
#define CRUDE_CONFIGWINDOW_H

#include <QWidget>
class QListWidget;
class QGroupBox;
class QStackedWidget;

class ConfigWindow : public QWidget
{
  Q_OBJECT

public:
  ConfigWindow(QWidget* parent = nullptr);

  void addPanel(QWidget* panel);

private slots:
  void selectionChanged(int item);

private:
  QListWidget* panelList;
  QGroupBox* panelFrame;
  QStackedWidget* panelStack;
};

#endif
