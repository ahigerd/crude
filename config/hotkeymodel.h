#ifndef CRUDE_HOTKEYMODEL_H
#define CRUDE_HOTKEYMODEL_H

#include "libcrude/throttle.h"

#include <QAbstractTableModel>
#include <QKeySequence>

struct HotkeyData
{
  QKeySequence keySequence;
  QString action;
  QString customAction;
  QString displayAction;
};

class HotkeyModel : public QAbstractTableModel
{
  Q_OBJECT

public:
  HotkeyModel(QObject* parent = nullptr);

  int rowCount(const QModelIndex& parent = QModelIndex()) const;
  int columnCount(const QModelIndex& parent = QModelIndex()) const;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
  Qt::ItemFlags flags(const QModelIndex& index) const;

  HotkeyData data(int row) const;

public slots:
  void deleteItem(int row);
  void reload();
  void setHotkey(const QKeySequence& seq, const QString& command, const QString& customCommand = QString());

private slots:
  void save();

private:
  static QString displayAction(const QString& action, const QString& customAction);
  QList<HotkeyData> hotkeys;
  Throttle saveThrottle;
};

#endif
