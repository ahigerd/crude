#include "hotkeypanel.h"

#include "hotkeymodel.h"
#include "libcrude/platformintegration.h"

#include <QDialog>
#include <QDialogButtonBox>
#include <QEventLoop>
#include <QGridLayout>
#include <QHeaderView>
#include <QKeyEvent>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QStackedWidget>
#include <QStyle>
#include <QVBoxLayout>
#include <QtDebug>

HotkeyPanel::HotkeyPanel(QWidget* parent) : QTableView(parent)
{
  setModel(new HotkeyModel(this));
  setSortingEnabled(false);
  setSelectionMode(SingleSelection);
  setSelectionBehavior(SelectRows);
  horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
  horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
  horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);
  horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);
  horizontalHeader()->resizeSection(2, 32);
  horizontalHeader()->setSectionsMovable(false);
  verticalHeader()->hide();

  setWindowIcon(c_PI->icon(
      { "preferences-desktop-keyboard-shortcuts", "preferences-desktop-keyboard", "input-keyboard" },
      QStyle::SP_FileDialogListView
  ));
  setWindowTitle("Keyboard Shortcuts");

  QObject::connect(this, SIGNAL(clicked(QModelIndex)), this, SLOT(itemClicked(QModelIndex)));
  QObject::connect(this, SIGNAL(activated(QModelIndex)), this, SLOT(triggered(QModelIndex)));
  QObject::connect(model(), SIGNAL(modelAboutToBeReset()), this, SLOT(savePos()));
  QObject::connect(model(), SIGNAL(modelReset()), this, SLOT(restorePos()));
}

QSize HotkeyPanel::sizeHint() const
{
  int frame = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
  int scroll = style()->pixelMetric(QStyle::PM_ScrollBarExtent);
  QSize hint = viewportSizeHint() + QSize(frame * 2 + scroll, frame * 2);
  if (hint.width() < 300) {
    hint.setWidth(300);
  }
  if (hint.height() > 400) {
    hint.setHeight(400);
  }
  return hint;
}

void HotkeyPanel::itemClicked(const QModelIndex& index)
{
  if (index.column() != 2) {
    return;
  }
  if (index.row() == model()->rowCount() - 1) {
    triggered(model()->index(index.row(), 0));
  } else {
    static_cast<HotkeyModel*>(model())->deleteItem(index.row());
  }
  setCurrentIndex(QModelIndex());
  clearSelection();
}

void HotkeyPanel::triggered(const QModelIndex& index)
{
  if (index.column() == 2) {
    itemClicked(index);
    return;
  }

  HotkeyEditor* hk = new HotkeyEditor(static_cast<HotkeyModel*>(model())->data(index.row()), this);
  QObject::connect(
      hk, SIGNAL(updateHotkey(QKeySequence, QString, QString)), model(), SLOT(setHotkey(QKeySequence, QString, QString))
  );
  hk->setAttribute(Qt::WA_DeleteOnClose);
  hk->setModal(true);
  hk->show();
}

void HotkeyPanel::savePos()
{
  if (!currentIndex().isValid()) {
    savedRow = -1;
    savedCol = -1;
  } else {
    savedRow = currentIndex().row();
    savedCol = currentIndex().column();
  }
}

void HotkeyPanel::restorePos()
{
  if (savedRow >= model()->rowCount()) {
    savedRow = model()->rowCount() - 1;
  }
  if (savedCol >= model()->columnCount()) {
    savedCol = model()->columnCount() - 1;
  }
  setCurrentIndex(model()->index(savedRow, savedCol));
}

HotkeyEditor::HotkeyEditor(const HotkeyData& data, QWidget* parent) : QDialog(parent), data(data)
{
  QVBoxLayout* mainLayout = new QVBoxLayout(this);
  mainLayout->setContentsMargins(0, 0, 0, 0);
  stack = new QStackedWidget(this);
  mainLayout->addWidget(stack, 0, 0);

  QWidget* main = new QWidget(stack);
  stack->addWidget(main);
  QGridLayout* layout = new QGridLayout(main);

  layout->addWidget(new QLabel("Shortcut:"), 0, 0);
  QString shortcutText = "(unassigned)";
  if (!data.keySequence.isEmpty()) {
    shortcutText = data.keySequence.toString(QKeySequence::NativeText);
  }
  shortcut = new QPushButton(shortcutText, main);
  QObject::connect(shortcut, SIGNAL(clicked()), this, SLOT(editHotkey()));
  layout->addWidget(shortcut, 0, 1);

  layout->addWidget(new QLabel("Command:"), 1, 0);
  if (data.action.startsWith("custom")) {
    command = new QLineEdit(data.customAction, main);
    layout->addWidget(command, 1, 1);
  } else {
    command = nullptr;
    layout->addWidget(new QLabel(data.displayAction, main), 1, 1);
  }

  QDialogButtonBox* bb = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, main);
  layout->addWidget(bb, 2, 0, 1, 2);
  QObject::connect(bb, SIGNAL(rejected()), this, SLOT(reject()));
  QObject::connect(bb, SIGNAL(accepted()), this, SLOT(save()));

  layout->setColumnStretch(0, 0);
  layout->setColumnStretch(1, 1);

  QWidget* setKey = new QWidget(stack);
  QVBoxLayout* setLayout = new QVBoxLayout(setKey);
  setLayout->setSpacing(4);
  setLayout->addStretch(1);
  QLabel* line1 = new QLabel("Press the key sequence for the shortcut.", setKey);
  line1->setAlignment(Qt::AlignCenter);
  setLayout->addWidget(line1, 0);
  QLabel* line2 = new QLabel("Press Escape to cancel.", setKey);
  line2->setAlignment(Qt::AlignCenter);
  setLayout->addWidget(line2, 0);
  setLayout->addStretch(1);
  stack->addWidget(setKey);
}

void HotkeyEditor::save()
{
  if (command) {
    data.customAction = command->text();
  }
  emit updateHotkey(data.keySequence, data.action, data.customAction);
  accept();
}

void HotkeyEditor::editHotkey()
{
  stack->setCurrentIndex(1);
  grabKeyboard();

  QEventLoop loop;
  QObject::connect(this, SIGNAL(private_updateHotkey(QKeySequence)), &loop, SLOT(quit()));
  QObject::connect(this, SIGNAL(private_cancelHotkey()), &loop, SLOT(quit()));
  loop.exec();

  releaseKeyboard();
  stack->setCurrentIndex(0);
}

void HotkeyEditor::keyPressEvent(QKeyEvent* event)
{
  int key = event->key();
  if (stack->currentIndex() != 1 || key == Qt::Key_unknown) {
    return QDialog::keyPressEvent(event);
  }

  if (event->modifiers() == Qt::NoModifier && key == Qt::Key_Escape) {
    emit private_cancelHotkey();
    return;
  }

  if (key == Qt::Key_Alt || key == Qt::Key_AltGr || key == Qt::Key_Shift || key == Qt::Key_Control ||
      key == Qt::Key_Meta) {
    // Bare modifiers can't be shortcuts, because of ambiguity
    return;
  }

  QKeySequence seq(key | event->modifiers());
  data.keySequence = seq;
  shortcut->setText(seq.toString(QKeySequence::NativeText));
  emit private_updateHotkey(key | event->modifiers());
}
