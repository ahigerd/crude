#include "hotkeymodel.h"

#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"

#include <QStyle>
#include <QtDebug>

#include <algorithm>
#include <signal.h>

static QMap<QString, QString> builtinActions = { { "backlight+", "Backlight +" },
                                                 { "backlight-", "Backlight -" },
                                                 { "keybacklight+", "Keyboard Backlight +" },
                                                 { "keybacklight-", "Keyboard Backlight -" },
                                                 { "volume+", "Volume +" },
                                                 { "volume-", "Volume -" },
                                                 { "mute", "Toggle Mute" },
                                                 { "launchmenu", "Open Launch Menu" },
                                                 { "quickrun", "Open Quick Run" },
                                                 { "suspend", "Suspend Computer" },
                                                 { "zoom+", "Accessibility: Zoom In" },
                                                 { "zoom-", "Accessibility: Zoom Out" },
                                                 { "nextapp", "Switch to Next App" },
                                                 { "prevapp", "Switch to Previous App" },
                                                 { "nextappwin", "Switch to App's Next Window" },
                                                 { "prevappwin", "Switch to App's Previous Window" },
                                                 { "screenshot", "Capture Screenshot" },
                                                 { "screenshotrect", "Capture Screenshot Region" } };

HotkeyModel::HotkeyModel(QObject* parent)
: QAbstractTableModel(parent), saveThrottle(Throttle::connectedTo(this, SLOT(save())))
{
  QObject::connect(Settings::instance(), SIGNAL(reloadSettings()), this, SLOT(reload()));
  reload();
}

int HotkeyModel::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid()) {
    return 0;
  }
  return hotkeys.length() + 1;
}

int HotkeyModel::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid()) {
    return 0;
  }
  return 3;
}

QVariant HotkeyModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid() || index.parent().isValid()) {
    return QVariant();
  }
  if (role == Qt::TextAlignmentRole && index.column() != 1) {
    return Qt::AlignCenter;
  }
  if (role == Qt::DecorationRole && index.column() == 2) {
    if (index.row() == hotkeys.length()) {
      return c_PI->icon("list-add", QStyle::SP_FileDialogNewFolder);
    }
    if (hotkeys[index.row()].keySequence.isEmpty()) {
      return QVariant();
    }
    return c_PI->icon("edit-delete", QStyle::SP_TitleBarCloseButton);
  }
  if (index.row() == hotkeys.length() || (role != Qt::DisplayRole && role != Qt::ToolTipRole)) {
    return QVariant();
  }
  const HotkeyData& data = hotkeys[index.row()];
  switch (index.column()) {
  case 0:
    if (data.keySequence.isEmpty()) {
      return "(unassigned)";
    }
    return data.keySequence.toString(QKeySequence::NativeText);
  case 1: return data.displayAction;
  default: return QVariant();
  }
}

QVariant HotkeyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (orientation == Qt::Vertical || role != Qt::DisplayRole) {
    return QVariant();
  }
  switch (section) {
  case 0: return "Shortcut";
  case 1: return "Action";
  default: return QVariant();
  }
}

void HotkeyModel::reload()
{
  beginResetModel();
  QStringList order = builtinActions.keys();
  for (const HotkeyData& hk : hotkeys) {
    if (!order.contains(hk.action)) {
      order << hk.action;
    }
  }
  hotkeys.clear();

  auto s = Settings::instance();
  for (const QString& key : builtinActions.keys()) {
    HotkeyData data;
    data.keySequence = QKeySequence(s->value("hotkeys/" + key).toString(), QKeySequence::PortableText);
    data.action = key;
    data.displayAction = builtinActions[key];
    hotkeys << data;
  }
  for (const QString& key : s->keys("hotkeys")) {
    if (builtinActions.contains(key)) {
      // already done
      continue;
    }
    HotkeyData data;
    data.keySequence = QKeySequence(s->value("hotkeys/" + key).toString(), QKeySequence::PortableText);
    data.action = key;
    if (key.startsWith("custom")) {
      data.customAction = s->value("customActions/" + key).toString();
    }
    data.displayAction = displayAction(data.action, data.customAction);
    hotkeys << data;
  }

  std::sort(hotkeys.begin(), hotkeys.end(), [&order](const HotkeyData& lhs, const HotkeyData& rhs) -> bool {
    int lpos = order.indexOf(lhs.action);
    int rpos = order.indexOf(rhs.action);
    if (lpos < 0 && rpos < 0) {
      return lhs.action < rhs.action;
    } else if (lpos < 0 || rpos < 0) {
      return rpos < 0;
    } else {
      return lpos < rpos;
    }
  });
  endResetModel();
}

Qt::ItemFlags HotkeyModel::flags(const QModelIndex& index) const
{
  if (index.column() == 2 && index.row() < hotkeys.length()) {
    if (hotkeys[index.row()].keySequence.isEmpty()) {
      return Qt::NoItemFlags;
    }
  }
  return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

void HotkeyModel::deleteItem(int row)
{
  if (row >= hotkeys.length()) {
    return;
  }
  HotkeyData& data = hotkeys[row];
  Settings* s = Settings::instance();
  s->remove("hotkeys/" + data.action);
  if (data.action.startsWith("custom")) {
    s->remove("customActions/" + data.action);
    beginRemoveRows(QModelIndex(), row, row);
    hotkeys.removeAt(row);
    endRemoveRows();
  } else {
    data.keySequence = QKeySequence();
    emit dataChanged(index(row, 0), index(row, 2), { Qt::DisplayRole, Qt::DecorationRole });
  }
  saveThrottle.trigger();
}

void HotkeyModel::setHotkey(const QKeySequence& seq, const QString& command, const QString& customCommand)
{
  Settings* s = Settings::instance();
  s->setValue("hotkeys/" + command, seq.toString(QKeySequence::PortableText));
  if (command.startsWith("custom")) {
    s->setValue("customActions/" + command, customCommand);
  }
  saveThrottle.trigger();
  for (int i = 0; i < hotkeys.length(); i++) {
    HotkeyData& data = hotkeys[i];
    if (data.action == command) {
      data.keySequence = seq;
      data.customAction = customCommand;
      data.displayAction = displayAction(command, customCommand);
      emit dataChanged(index(i, 0), index(i, 2), { Qt::DisplayRole, Qt::DecorationRole });
      return;
    }
  }
  beginInsertRows(QModelIndex(), hotkeys.length(), hotkeys.length());
  hotkeys << (HotkeyData){ seq, command, customCommand, displayAction(command, customCommand) };
  endInsertRows();
}

void HotkeyModel::save()
{
  Settings::instance()->broadcastReload();
}

HotkeyData HotkeyModel::data(int row) const
{
  if (row >= 0 && row < hotkeys.length()) {
    return hotkeys[row];
  }
  int nextId = 0;
  for (int i = 0; i < hotkeys.length(); i++) {
    if (hotkeys[i].action.startsWith("custom")) {
      int id = hotkeys[i].action.mid(6).toInt();
      if (id > nextId) {
        nextId = id + 1;
      }
    }
  }
  return (HotkeyData){ QKeySequence(), QString("custom%1").arg(nextId), QString(), QString() };
}

QString HotkeyModel::displayAction(const QString& action, const QString& customAction)
{
  if (builtinActions.contains(action)) {
    return builtinActions.value(action);
  } else if (customAction.isEmpty()) {
    return action;
  } else {
    return customAction;
  }
}
