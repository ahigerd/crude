/* seedentropy - use a urandom seed as an entropy source
 *
 * Written in the USA in 2019 by Adam Higerd <chighland@gmail.com>.
 *
 * To the extent possible under law, the author has voluntarily waived
 * all copyright and related or neighboring rights to this software to
 * the public domain worldwide. This software is distributed without
 * any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication
 * along with this software as CC0.txt. If not, see 
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 *
 * This software is included in the CRUDE source code repository, but
 * it is not a part of the CRUDE desktop environment. It is meant as a
 * tool to support certain minimalist systems that may not have enough
 * entropy provided by the environment during the boot process to
 * support the getentropy() call used by Qt5.
 *
 * This script must be run as root, ideally near the time that your
 * distribution uses the same seed file to initialize /dev/urandom.
 *
 * If CRUDE starts up without delays, you don't need this.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/random.h>

int main(int argc, char** argv) {
  if (argc != 2) {
    printf("Usage: %s <seed-file>\n", argv[0]);
    return 1;
  }

  int e;

  int seedFD = open(argv[1], O_RDONLY);
  if (seedFD < 0) {
    e = errno;
    printf("Error opening seed file %s, aborting.\n", argv[1]);
    return e;
  }

  struct rand_pool_info* poolinfo = malloc(sizeof(struct rand_pool_info) + 1024);
  int bytes = read(seedFD, poolinfo->buf, 1024); 
  e = errno;
  close(seedFD);
  if (bytes < 1) {
    printf("Error reading seed file %s, aborting.\n", argv[1]);
    return e;
  }

  int rndFD = open("/dev/random", O_WRONLY);
  if (rndFD < 0) {
    e = errno;
    printf("Error opening /dev/random, aborting.\n");
    free(poolinfo);
    return e;
  }

  poolinfo->entropy_count = bytes * 8; // We assume that 100% of the seed entropy is good
  poolinfo->buf_size = bytes;
  int status = ioctl(rndFD, RNDADDENTROPY, poolinfo);
  e = errno;
  close(rndFD);
  free(poolinfo);
  if (status < 0) {
    printf("Error adding entropy, aborting.\n");
    return e;
  }

  return 0;
}
