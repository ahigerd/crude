TEMPLATE = lib
TARGET = QtXkbCommonSupport
DESTDIR = ../..
CONFIG += staticlib create_prl debug
QT = core gui
OBJECTS_DIR = .tmp
MOC_DIR = .tmp
LIBS += -lxkbcommon -lxkb

DEFINES += QT_NO_CAST_FROM_ASCII

HEADERS += qxkbcommon_p.h
SOURCES += qxkbcommon.cpp qxkbcommon_3rdparty.cpp

# qxkbcommon.cpp::KeyTbl has more than 256 levels of expansion and older
# Clang uses that as a limit (it's 1024 in current versions).
clang:!intel_icc: QMAKE_CXXFLAGS += -ftemplate-depth=1024
