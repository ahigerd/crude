/****************************************************************************
**
** Copyright (C) 2019 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtGui module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or (at your option) the GNU General
** Public license version 3 or any later version approved by the KDE Free
** Qt Foundation. The licenses are as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html and
** https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qmakearray_p.h"
#include "qxkbcommon_p.h"

#include <QtCore/QMetaMethod>
#include <QtDebug>
#include <QtGui/QKeyEvent>

#include <algorithm>
#include <vector>

QT_BEGIN_NAMESPACE

static int keysymToQtKey_internal(
    xkb_keysym_t keysym, Qt::KeyboardModifiers modifiers, xkb_state* state, xkb_keycode_t code, bool superAsMeta,
    bool hyperAsMeta
);

typedef struct xkb2qt
{
  unsigned int xkb;
  unsigned int qt;

  constexpr bool operator<=(const xkb2qt& that) const noexcept { return xkb <= that.xkb; }

  constexpr bool operator<(const xkb2qt& that) const noexcept { return xkb < that.xkb; }
} xkb2qt_t;

template <std::size_t Xkb, std::size_t Qt>
struct Xkb2Qt
{
  using Type = xkb2qt_t;

  static constexpr Type data() noexcept { return Type{ Xkb, Qt }; }
};

/* clang-format off */
static std::vector<xkb2qt_t> makeKeyTbl() {
  std::vector<xkb2qt_t> table = {
    // misc keys

    Xkb2Qt<XKB_KEY_Escape,                  Qt::Key_Escape>().data(),
    Xkb2Qt<XKB_KEY_Tab,                     Qt::Key_Tab>().data(),
    Xkb2Qt<XKB_KEY_ISO_Left_Tab,            Qt::Key_Backtab>().data(),
    Xkb2Qt<XKB_KEY_BackSpace,               Qt::Key_Backspace>().data(),
    Xkb2Qt<XKB_KEY_Return,                  Qt::Key_Return>().data(),
    Xkb2Qt<XKB_KEY_Insert,                  Qt::Key_Insert>().data(),
    Xkb2Qt<XKB_KEY_Delete,                  Qt::Key_Delete>().data(),
    Xkb2Qt<XKB_KEY_Clear,                   Qt::Key_Delete>().data(),
    Xkb2Qt<XKB_KEY_Pause,                   Qt::Key_Pause>().data(),
    Xkb2Qt<XKB_KEY_Print,                   Qt::Key_Print>().data(),
    Xkb2Qt<0x1005FF60,                      Qt::Key_SysReq>().data(),         // hardcoded Sun SysReq
    Xkb2Qt<0x1007ff00,                      Qt::Key_SysReq>().data(),         // hardcoded X386 SysReq

    // cursor movement

    Xkb2Qt<XKB_KEY_Home,                    Qt::Key_Home>().data(),
    Xkb2Qt<XKB_KEY_End,                     Qt::Key_End>().data(),
    Xkb2Qt<XKB_KEY_Left,                    Qt::Key_Left>().data(),
    Xkb2Qt<XKB_KEY_Up,                      Qt::Key_Up>().data(),
    Xkb2Qt<XKB_KEY_Right,                   Qt::Key_Right>().data(),
    Xkb2Qt<XKB_KEY_Down,                    Qt::Key_Down>().data(),
    Xkb2Qt<XKB_KEY_Prior,                   Qt::Key_PageUp>().data(),
    Xkb2Qt<XKB_KEY_Next,                    Qt::Key_PageDown>().data(),

    // modifiers

    Xkb2Qt<XKB_KEY_Shift_L,                 Qt::Key_Shift>().data(),
    Xkb2Qt<XKB_KEY_Shift_R,                 Qt::Key_Shift>().data(),
    Xkb2Qt<XKB_KEY_Shift_Lock,              Qt::Key_Shift>().data(),
    Xkb2Qt<XKB_KEY_Control_L,               Qt::Key_Control>().data(),
    Xkb2Qt<XKB_KEY_Control_R,               Qt::Key_Control>().data(),
    Xkb2Qt<XKB_KEY_Meta_L,                  Qt::Key_Meta>().data(),
    Xkb2Qt<XKB_KEY_Meta_R,                  Qt::Key_Meta>().data(),
    Xkb2Qt<XKB_KEY_Alt_L,                   Qt::Key_Alt>().data(),
    Xkb2Qt<XKB_KEY_Alt_R,                   Qt::Key_Alt>().data(),
    Xkb2Qt<XKB_KEY_Caps_Lock,               Qt::Key_CapsLock>().data(),
    Xkb2Qt<XKB_KEY_Num_Lock,                Qt::Key_NumLock>().data(),
    Xkb2Qt<XKB_KEY_Scroll_Lock,             Qt::Key_ScrollLock>().data(),
    Xkb2Qt<XKB_KEY_Super_L,                 Qt::Key_Super_L>().data(),
    Xkb2Qt<XKB_KEY_Super_R,                 Qt::Key_Super_R>().data(),
    Xkb2Qt<XKB_KEY_Menu,                    Qt::Key_Menu>().data(),
    Xkb2Qt<XKB_KEY_Hyper_L,                 Qt::Key_Hyper_L>().data(),
    Xkb2Qt<XKB_KEY_Hyper_R,                 Qt::Key_Hyper_R>().data(),
    Xkb2Qt<XKB_KEY_Help,                    Qt::Key_Help>().data(),
    Xkb2Qt<0x1000FF74,                      Qt::Key_Backtab>().data(),        // hardcoded HP backtab
    Xkb2Qt<0x1005FF10,                      Qt::Key_F11>().data(),            // hardcoded Sun F36 (labeled F11)
    Xkb2Qt<0x1005FF11,                      Qt::Key_F12>().data(),            // hardcoded Sun F37 (labeled F12)

    // numeric and function keypad keys

    Xkb2Qt<XKB_KEY_KP_Space,                Qt::Key_Space>().data(),
    Xkb2Qt<XKB_KEY_KP_Tab,                  Qt::Key_Tab>().data(),
    Xkb2Qt<XKB_KEY_KP_Enter,                Qt::Key_Enter>().data(),
    Xkb2Qt<XKB_KEY_KP_Home,                 Qt::Key_Home>().data(),
    Xkb2Qt<XKB_KEY_KP_Left,                 Qt::Key_Left>().data(),
    Xkb2Qt<XKB_KEY_KP_Up,                   Qt::Key_Up>().data(),
    Xkb2Qt<XKB_KEY_KP_Right,                Qt::Key_Right>().data(),
    Xkb2Qt<XKB_KEY_KP_Down,                 Qt::Key_Down>().data(),
    Xkb2Qt<XKB_KEY_KP_Prior,                Qt::Key_PageUp>().data(),
    Xkb2Qt<XKB_KEY_KP_Next,                 Qt::Key_PageDown>().data(),
    Xkb2Qt<XKB_KEY_KP_End,                  Qt::Key_End>().data(),
    Xkb2Qt<XKB_KEY_KP_Begin,                Qt::Key_Clear>().data(),
    Xkb2Qt<XKB_KEY_KP_Insert,               Qt::Key_Insert>().data(),
    Xkb2Qt<XKB_KEY_KP_Delete,               Qt::Key_Delete>().data(),
    Xkb2Qt<XKB_KEY_KP_Equal,                Qt::Key_Equal>().data(),
    Xkb2Qt<XKB_KEY_KP_Multiply,             Qt::Key_Asterisk>().data(),
    Xkb2Qt<XKB_KEY_KP_Add,                  Qt::Key_Plus>().data(),
    Xkb2Qt<XKB_KEY_KP_Separator,            Qt::Key_Comma>().data(),
    Xkb2Qt<XKB_KEY_KP_Subtract,             Qt::Key_Minus>().data(),
    Xkb2Qt<XKB_KEY_KP_Decimal,              Qt::Key_Period>().data(),
    Xkb2Qt<XKB_KEY_KP_Divide,               Qt::Key_Slash>().data(),

    // special non-XF86 function keys

    Xkb2Qt<XKB_KEY_Undo,                    Qt::Key_Undo>().data(),
    Xkb2Qt<XKB_KEY_Redo,                    Qt::Key_Redo>().data(),
    Xkb2Qt<XKB_KEY_Find,                    Qt::Key_Find>().data(),
    Xkb2Qt<XKB_KEY_Cancel,                  Qt::Key_Cancel>().data(),

    // International input method support keys

    // International & multi-key character composition
    Xkb2Qt<XKB_KEY_ISO_Level3_Shift,        Qt::Key_AltGr>().data(),
    Xkb2Qt<XKB_KEY_Multi_key,               Qt::Key_Multi_key>().data(),
    Xkb2Qt<XKB_KEY_Codeinput,               Qt::Key_Codeinput>().data(),
    Xkb2Qt<XKB_KEY_SingleCandidate,         Qt::Key_SingleCandidate>().data(),
    Xkb2Qt<XKB_KEY_MultipleCandidate,       Qt::Key_MultipleCandidate>().data(),
    Xkb2Qt<XKB_KEY_PreviousCandidate,       Qt::Key_PreviousCandidate>().data(),

    // Misc Functions
    Xkb2Qt<XKB_KEY_Mode_switch,             Qt::Key_Mode_switch>().data(),
    Xkb2Qt<XKB_KEY_script_switch,           Qt::Key_Mode_switch>().data(),

    // Japanese keyboard support
    Xkb2Qt<XKB_KEY_Kanji,                   Qt::Key_Kanji>().data(),
    Xkb2Qt<XKB_KEY_Muhenkan,                Qt::Key_Muhenkan>().data(),
    //Xkb2Qt<XKB_KEY_Henkan_Mode,           Qt::Key_Henkan_Mode>().data(),
    Xkb2Qt<XKB_KEY_Henkan_Mode,             Qt::Key_Henkan>().data(),
    Xkb2Qt<XKB_KEY_Henkan,                  Qt::Key_Henkan>().data(),
    Xkb2Qt<XKB_KEY_Romaji,                  Qt::Key_Romaji>().data(),
    Xkb2Qt<XKB_KEY_Hiragana,                Qt::Key_Hiragana>().data(),
    Xkb2Qt<XKB_KEY_Katakana,                Qt::Key_Katakana>().data(),
    Xkb2Qt<XKB_KEY_Hiragana_Katakana,       Qt::Key_Hiragana_Katakana>().data(),
    Xkb2Qt<XKB_KEY_Zenkaku,                 Qt::Key_Zenkaku>().data(),
    Xkb2Qt<XKB_KEY_Hankaku,                 Qt::Key_Hankaku>().data(),
    Xkb2Qt<XKB_KEY_Zenkaku_Hankaku,         Qt::Key_Zenkaku_Hankaku>().data(),
    Xkb2Qt<XKB_KEY_Touroku,                 Qt::Key_Touroku>().data(),
    Xkb2Qt<XKB_KEY_Massyo,                  Qt::Key_Massyo>().data(),
    Xkb2Qt<XKB_KEY_Kana_Lock,               Qt::Key_Kana_Lock>().data(),
    Xkb2Qt<XKB_KEY_Kana_Shift,              Qt::Key_Kana_Shift>().data(),
    Xkb2Qt<XKB_KEY_Eisu_Shift,              Qt::Key_Eisu_Shift>().data(),
    Xkb2Qt<XKB_KEY_Eisu_toggle,             Qt::Key_Eisu_toggle>().data(),
    //Xkb2Qt<XKB_KEY_Kanji_Bangou,          Qt::Key_Kanji_Bangou>().data(),
    //Xkb2Qt<XKB_KEY_Zen_Koho,              Qt::Key_Zen_Koho>().data(),
    //Xkb2Qt<XKB_KEY_Mae_Koho,              Qt::Key_Mae_Koho>().data(),
    Xkb2Qt<XKB_KEY_Kanji_Bangou,            Qt::Key_Codeinput>().data(),
    Xkb2Qt<XKB_KEY_Zen_Koho,                Qt::Key_MultipleCandidate>().data(),
    Xkb2Qt<XKB_KEY_Mae_Koho,                Qt::Key_PreviousCandidate>().data(),

    // Korean keyboard support
    Xkb2Qt<XKB_KEY_Hangul,                  Qt::Key_Hangul>().data(),
    Xkb2Qt<XKB_KEY_Hangul_Start,            Qt::Key_Hangul_Start>().data(),
    Xkb2Qt<XKB_KEY_Hangul_End,              Qt::Key_Hangul_End>().data(),
    Xkb2Qt<XKB_KEY_Hangul_Hanja,            Qt::Key_Hangul_Hanja>().data(),
    Xkb2Qt<XKB_KEY_Hangul_Jamo,             Qt::Key_Hangul_Jamo>().data(),
    Xkb2Qt<XKB_KEY_Hangul_Romaja,           Qt::Key_Hangul_Romaja>().data(),
    //Xkb2Qt<XKB_KEY_Hangul_Codeinput,      Qt::Key_Hangul_Codeinput>().data(),
    Xkb2Qt<XKB_KEY_Hangul_Codeinput,        Qt::Key_Codeinput>().data(),
    Xkb2Qt<XKB_KEY_Hangul_Jeonja,           Qt::Key_Hangul_Jeonja>().data(),
    Xkb2Qt<XKB_KEY_Hangul_Banja,            Qt::Key_Hangul_Banja>().data(),
    Xkb2Qt<XKB_KEY_Hangul_PreHanja,         Qt::Key_Hangul_PreHanja>().data(),
    Xkb2Qt<XKB_KEY_Hangul_PostHanja,        Qt::Key_Hangul_PostHanja>().data(),
    //Xkb2Qt<XKB_KEY_Hangul_SingleCandidate,Qt::Key_Hangul_SingleCandidate>().data(),
    //Xkb2Qt<XKB_KEY_Hangul_MultipleCandidate,Qt::Key_Hangul_MultipleCandidate>().data(),
    //Xkb2Qt<XKB_KEY_Hangul_PreviousCandidate,Qt::Key_Hangul_PreviousCandidate>().data(),
    Xkb2Qt<XKB_KEY_Hangul_SingleCandidate,  Qt::Key_SingleCandidate>().data(),
    Xkb2Qt<XKB_KEY_Hangul_MultipleCandidate,Qt::Key_MultipleCandidate>().data(),
    Xkb2Qt<XKB_KEY_Hangul_PreviousCandidate,Qt::Key_PreviousCandidate>().data(),
    Xkb2Qt<XKB_KEY_Hangul_Special,          Qt::Key_Hangul_Special>().data(),
    //Xkb2Qt<XKB_KEY_Hangul_switch,         Qt::Key_Hangul_switch>().data(),
    Xkb2Qt<XKB_KEY_Hangul_switch,           Qt::Key_Mode_switch>().data(),

    // dead keys
    Xkb2Qt<XKB_KEY_dead_grave,              Qt::Key_Dead_Grave>().data(),
    Xkb2Qt<XKB_KEY_dead_acute,              Qt::Key_Dead_Acute>().data(),
    Xkb2Qt<XKB_KEY_dead_circumflex,         Qt::Key_Dead_Circumflex>().data(),
    Xkb2Qt<XKB_KEY_dead_tilde,              Qt::Key_Dead_Tilde>().data(),
    Xkb2Qt<XKB_KEY_dead_macron,             Qt::Key_Dead_Macron>().data(),
    Xkb2Qt<XKB_KEY_dead_breve,              Qt::Key_Dead_Breve>().data(),
    Xkb2Qt<XKB_KEY_dead_abovedot,           Qt::Key_Dead_Abovedot>().data(),
    Xkb2Qt<XKB_KEY_dead_diaeresis,          Qt::Key_Dead_Diaeresis>().data(),
    Xkb2Qt<XKB_KEY_dead_abovering,          Qt::Key_Dead_Abovering>().data(),
    Xkb2Qt<XKB_KEY_dead_doubleacute,        Qt::Key_Dead_Doubleacute>().data(),
    Xkb2Qt<XKB_KEY_dead_caron,              Qt::Key_Dead_Caron>().data(),
    Xkb2Qt<XKB_KEY_dead_cedilla,            Qt::Key_Dead_Cedilla>().data(),
    Xkb2Qt<XKB_KEY_dead_ogonek,             Qt::Key_Dead_Ogonek>().data(),
    Xkb2Qt<XKB_KEY_dead_iota,               Qt::Key_Dead_Iota>().data(),
    Xkb2Qt<XKB_KEY_dead_voiced_sound,       Qt::Key_Dead_Voiced_Sound>().data(),
    Xkb2Qt<XKB_KEY_dead_semivoiced_sound,   Qt::Key_Dead_Semivoiced_Sound>().data(),
    Xkb2Qt<XKB_KEY_dead_belowdot,           Qt::Key_Dead_Belowdot>().data(),
    Xkb2Qt<XKB_KEY_dead_hook,               Qt::Key_Dead_Hook>().data(),
    Xkb2Qt<XKB_KEY_dead_horn,               Qt::Key_Dead_Horn>().data(),
    Xkb2Qt<XKB_KEY_dead_stroke,             Qt::Key_Dead_Stroke>().data(),
    Xkb2Qt<XKB_KEY_dead_abovecomma,         Qt::Key_Dead_Abovecomma>().data(),
    Xkb2Qt<XKB_KEY_dead_abovereversedcomma, Qt::Key_Dead_Abovereversedcomma>().data(),
    Xkb2Qt<XKB_KEY_dead_doublegrave,        Qt::Key_Dead_Doublegrave>().data(),
    Xkb2Qt<XKB_KEY_dead_belowring,          Qt::Key_Dead_Belowring>().data(),
    Xkb2Qt<XKB_KEY_dead_belowmacron,        Qt::Key_Dead_Belowmacron>().data(),
    Xkb2Qt<XKB_KEY_dead_belowcircumflex,    Qt::Key_Dead_Belowcircumflex>().data(),
    Xkb2Qt<XKB_KEY_dead_belowtilde,         Qt::Key_Dead_Belowtilde>().data(),
    Xkb2Qt<XKB_KEY_dead_belowbreve,         Qt::Key_Dead_Belowbreve>().data(),
    Xkb2Qt<XKB_KEY_dead_belowdiaeresis,     Qt::Key_Dead_Belowdiaeresis>().data(),
    Xkb2Qt<XKB_KEY_dead_invertedbreve,      Qt::Key_Dead_Invertedbreve>().data(),
    Xkb2Qt<XKB_KEY_dead_belowcomma,         Qt::Key_Dead_Belowcomma>().data(),
    Xkb2Qt<XKB_KEY_dead_currency,           Qt::Key_Dead_Currency>().data(),
    Xkb2Qt<XKB_KEY_dead_a,                  Qt::Key_Dead_a>().data(),
    Xkb2Qt<XKB_KEY_dead_A,                  Qt::Key_Dead_A>().data(),
    Xkb2Qt<XKB_KEY_dead_e,                  Qt::Key_Dead_e>().data(),
    Xkb2Qt<XKB_KEY_dead_E,                  Qt::Key_Dead_E>().data(),
    Xkb2Qt<XKB_KEY_dead_i,                  Qt::Key_Dead_i>().data(),
    Xkb2Qt<XKB_KEY_dead_I,                  Qt::Key_Dead_I>().data(),
    Xkb2Qt<XKB_KEY_dead_o,                  Qt::Key_Dead_o>().data(),
    Xkb2Qt<XKB_KEY_dead_O,                  Qt::Key_Dead_O>().data(),
    Xkb2Qt<XKB_KEY_dead_u,                  Qt::Key_Dead_u>().data(),
    Xkb2Qt<XKB_KEY_dead_U,                  Qt::Key_Dead_U>().data(),
    Xkb2Qt<XKB_KEY_dead_small_schwa,        Qt::Key_Dead_Small_Schwa>().data(),
    Xkb2Qt<XKB_KEY_dead_capital_schwa,      Qt::Key_Dead_Capital_Schwa>().data(),
    Xkb2Qt<XKB_KEY_dead_greek,              Qt::Key_Dead_Greek>().data(),
#ifdef XKB_KEY_dead_lowline
    Xkb2Qt<XKB_KEY_dead_lowline,            Qt::Key_Dead_Lowline>().data(),
    Xkb2Qt<XKB_KEY_dead_aboveverticalline,  Qt::Key_Dead_Aboveverticalline>().data(),
    Xkb2Qt<XKB_KEY_dead_belowverticalline,  Qt::Key_Dead_Belowverticalline>().data(),
    Xkb2Qt<XKB_KEY_dead_longsolidusoverlay, Qt::Key_Dead_Longsolidusoverlay>().data(),
#endif

    // Special keys from X.org - This include multimedia keys,
    // wireless/bluetooth/uwb keys, special launcher keys, etc.
    Xkb2Qt<XKB_KEY_XF86Back,                Qt::Key_Back>().data(),
    Xkb2Qt<XKB_KEY_XF86Forward,             Qt::Key_Forward>().data(),
    Xkb2Qt<XKB_KEY_XF86Stop,                Qt::Key_Stop>().data(),
    Xkb2Qt<XKB_KEY_XF86Refresh,             Qt::Key_Refresh>().data(),
    Xkb2Qt<XKB_KEY_XF86Favorites,           Qt::Key_Favorites>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioMedia,          Qt::Key_LaunchMedia>().data(),
    Xkb2Qt<XKB_KEY_XF86OpenURL,             Qt::Key_OpenUrl>().data(),
    Xkb2Qt<XKB_KEY_XF86HomePage,            Qt::Key_HomePage>().data(),
    Xkb2Qt<XKB_KEY_XF86Search,              Qt::Key_Search>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioLowerVolume,    Qt::Key_VolumeDown>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioMute,           Qt::Key_VolumeMute>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioRaiseVolume,    Qt::Key_VolumeUp>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioPlay,           Qt::Key_MediaPlay>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioStop,           Qt::Key_MediaStop>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioPrev,           Qt::Key_MediaPrevious>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioNext,           Qt::Key_MediaNext>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioRecord,         Qt::Key_MediaRecord>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioPause,          Qt::Key_MediaPause>().data(),
    Xkb2Qt<XKB_KEY_XF86Mail,                Qt::Key_LaunchMail>().data(),
    Xkb2Qt<XKB_KEY_XF86MyComputer,          Qt::Key_Launch0>().data(),  // ### Qt 6: remap properly
    Xkb2Qt<XKB_KEY_XF86Calculator,          Qt::Key_Launch1>().data(),
    Xkb2Qt<XKB_KEY_XF86Memo,                Qt::Key_Memo>().data(),
    Xkb2Qt<XKB_KEY_XF86ToDoList,            Qt::Key_ToDoList>().data(),
    Xkb2Qt<XKB_KEY_XF86Calendar,            Qt::Key_Calendar>().data(),
    Xkb2Qt<XKB_KEY_XF86PowerDown,           Qt::Key_PowerDown>().data(),
    Xkb2Qt<XKB_KEY_XF86ContrastAdjust,      Qt::Key_ContrastAdjust>().data(),
    Xkb2Qt<XKB_KEY_XF86Standby,             Qt::Key_Standby>().data(),
    Xkb2Qt<XKB_KEY_XF86MonBrightnessUp,     Qt::Key_MonBrightnessUp>().data(),
    Xkb2Qt<XKB_KEY_XF86MonBrightnessDown,   Qt::Key_MonBrightnessDown>().data(),
    Xkb2Qt<XKB_KEY_XF86KbdLightOnOff,       Qt::Key_KeyboardLightOnOff>().data(),
    Xkb2Qt<XKB_KEY_XF86KbdBrightnessUp,     Qt::Key_KeyboardBrightnessUp>().data(),
    Xkb2Qt<XKB_KEY_XF86KbdBrightnessDown,   Qt::Key_KeyboardBrightnessDown>().data(),
    Xkb2Qt<XKB_KEY_XF86PowerOff,            Qt::Key_PowerOff>().data(),
    Xkb2Qt<XKB_KEY_XF86WakeUp,              Qt::Key_WakeUp>().data(),
    Xkb2Qt<XKB_KEY_XF86Eject,               Qt::Key_Eject>().data(),
    Xkb2Qt<XKB_KEY_XF86ScreenSaver,         Qt::Key_ScreenSaver>().data(),
    Xkb2Qt<XKB_KEY_XF86WWW,                 Qt::Key_WWW>().data(),
    Xkb2Qt<XKB_KEY_XF86Sleep,               Qt::Key_Sleep>().data(),
    Xkb2Qt<XKB_KEY_XF86LightBulb,           Qt::Key_LightBulb>().data(),
    Xkb2Qt<XKB_KEY_XF86Shop,                Qt::Key_Shop>().data(),
    Xkb2Qt<XKB_KEY_XF86History,             Qt::Key_History>().data(),
    Xkb2Qt<XKB_KEY_XF86AddFavorite,         Qt::Key_AddFavorite>().data(),
    Xkb2Qt<XKB_KEY_XF86HotLinks,            Qt::Key_HotLinks>().data(),
    Xkb2Qt<XKB_KEY_XF86BrightnessAdjust,    Qt::Key_BrightnessAdjust>().data(),
    Xkb2Qt<XKB_KEY_XF86Finance,             Qt::Key_Finance>().data(),
    Xkb2Qt<XKB_KEY_XF86Community,           Qt::Key_Community>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioRewind,         Qt::Key_AudioRewind>().data(),
    Xkb2Qt<XKB_KEY_XF86BackForward,         Qt::Key_BackForward>().data(),
    Xkb2Qt<XKB_KEY_XF86ApplicationLeft,     Qt::Key_ApplicationLeft>().data(),
    Xkb2Qt<XKB_KEY_XF86ApplicationRight,    Qt::Key_ApplicationRight>().data(),
    Xkb2Qt<XKB_KEY_XF86Book,                Qt::Key_Book>().data(),
    Xkb2Qt<XKB_KEY_XF86CD,                  Qt::Key_CD>().data(),
    Xkb2Qt<XKB_KEY_XF86Calculater,          Qt::Key_Calculator>().data(),
    Xkb2Qt<XKB_KEY_XF86Clear,               Qt::Key_Clear>().data(),
    Xkb2Qt<XKB_KEY_XF86ClearGrab,           Qt::Key_ClearGrab>().data(),
    Xkb2Qt<XKB_KEY_XF86Close,               Qt::Key_Close>().data(),
    Xkb2Qt<XKB_KEY_XF86Copy,                Qt::Key_Copy>().data(),
    Xkb2Qt<XKB_KEY_XF86Cut,                 Qt::Key_Cut>().data(),
    Xkb2Qt<XKB_KEY_XF86Display,             Qt::Key_Display>().data(),
    Xkb2Qt<XKB_KEY_XF86DOS,                 Qt::Key_DOS>().data(),
    Xkb2Qt<XKB_KEY_XF86Documents,           Qt::Key_Documents>().data(),
    Xkb2Qt<XKB_KEY_XF86Excel,               Qt::Key_Excel>().data(),
    Xkb2Qt<XKB_KEY_XF86Explorer,            Qt::Key_Explorer>().data(),
    Xkb2Qt<XKB_KEY_XF86Game,                Qt::Key_Game>().data(),
    Xkb2Qt<XKB_KEY_XF86Go,                  Qt::Key_Go>().data(),
    Xkb2Qt<XKB_KEY_XF86iTouch,              Qt::Key_iTouch>().data(),
    Xkb2Qt<XKB_KEY_XF86LogOff,              Qt::Key_LogOff>().data(),
    Xkb2Qt<XKB_KEY_XF86Market,              Qt::Key_Market>().data(),
    Xkb2Qt<XKB_KEY_XF86Meeting,             Qt::Key_Meeting>().data(),
    Xkb2Qt<XKB_KEY_XF86MenuKB,              Qt::Key_MenuKB>().data(),
    Xkb2Qt<XKB_KEY_XF86MenuPB,              Qt::Key_MenuPB>().data(),
    Xkb2Qt<XKB_KEY_XF86MySites,             Qt::Key_MySites>().data(),
    Xkb2Qt<XKB_KEY_XF86New,                 Qt::Key_New>().data(),
    Xkb2Qt<XKB_KEY_XF86News,                Qt::Key_News>().data(),
    Xkb2Qt<XKB_KEY_XF86OfficeHome,          Qt::Key_OfficeHome>().data(),
    Xkb2Qt<XKB_KEY_XF86Open,                Qt::Key_Open>().data(),
    Xkb2Qt<XKB_KEY_XF86Option,              Qt::Key_Option>().data(),
    Xkb2Qt<XKB_KEY_XF86Paste,               Qt::Key_Paste>().data(),
    Xkb2Qt<XKB_KEY_XF86Phone,               Qt::Key_Phone>().data(),
    Xkb2Qt<XKB_KEY_XF86Reply,               Qt::Key_Reply>().data(),
    Xkb2Qt<XKB_KEY_XF86Reload,              Qt::Key_Reload>().data(),
    Xkb2Qt<XKB_KEY_XF86RotateWindows,       Qt::Key_RotateWindows>().data(),
    Xkb2Qt<XKB_KEY_XF86RotationPB,          Qt::Key_RotationPB>().data(),
    Xkb2Qt<XKB_KEY_XF86RotationKB,          Qt::Key_RotationKB>().data(),
    Xkb2Qt<XKB_KEY_XF86Save,                Qt::Key_Save>().data(),
    Xkb2Qt<XKB_KEY_XF86Send,                Qt::Key_Send>().data(),
    Xkb2Qt<XKB_KEY_XF86Spell,               Qt::Key_Spell>().data(),
    Xkb2Qt<XKB_KEY_XF86SplitScreen,         Qt::Key_SplitScreen>().data(),
    Xkb2Qt<XKB_KEY_XF86Support,             Qt::Key_Support>().data(),
    Xkb2Qt<XKB_KEY_XF86TaskPane,            Qt::Key_TaskPane>().data(),
    Xkb2Qt<XKB_KEY_XF86Terminal,            Qt::Key_Terminal>().data(),
    Xkb2Qt<XKB_KEY_XF86Tools,               Qt::Key_Tools>().data(),
    Xkb2Qt<XKB_KEY_XF86Travel,              Qt::Key_Travel>().data(),
    Xkb2Qt<XKB_KEY_XF86Video,               Qt::Key_Video>().data(),
    Xkb2Qt<XKB_KEY_XF86Word,                Qt::Key_Word>().data(),
    Xkb2Qt<XKB_KEY_XF86Xfer,                Qt::Key_Xfer>().data(),
    Xkb2Qt<XKB_KEY_XF86ZoomIn,              Qt::Key_ZoomIn>().data(),
    Xkb2Qt<XKB_KEY_XF86ZoomOut,             Qt::Key_ZoomOut>().data(),
    Xkb2Qt<XKB_KEY_XF86Away,                Qt::Key_Away>().data(),
    Xkb2Qt<XKB_KEY_XF86Messenger,           Qt::Key_Messenger>().data(),
    Xkb2Qt<XKB_KEY_XF86WebCam,              Qt::Key_WebCam>().data(),
    Xkb2Qt<XKB_KEY_XF86MailForward,         Qt::Key_MailForward>().data(),
    Xkb2Qt<XKB_KEY_XF86Pictures,            Qt::Key_Pictures>().data(),
    Xkb2Qt<XKB_KEY_XF86Music,               Qt::Key_Music>().data(),
    Xkb2Qt<XKB_KEY_XF86Battery,             Qt::Key_Battery>().data(),
    Xkb2Qt<XKB_KEY_XF86Bluetooth,           Qt::Key_Bluetooth>().data(),
    Xkb2Qt<XKB_KEY_XF86WLAN,                Qt::Key_WLAN>().data(),
    Xkb2Qt<XKB_KEY_XF86UWB,                 Qt::Key_UWB>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioForward,        Qt::Key_AudioForward>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioRepeat,         Qt::Key_AudioRepeat>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioRandomPlay,     Qt::Key_AudioRandomPlay>().data(),
    Xkb2Qt<XKB_KEY_XF86Subtitle,            Qt::Key_Subtitle>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioCycleTrack,     Qt::Key_AudioCycleTrack>().data(),
    Xkb2Qt<XKB_KEY_XF86Time,                Qt::Key_Time>().data(),
    Xkb2Qt<XKB_KEY_XF86Select,              Qt::Key_Select>().data(),
    Xkb2Qt<XKB_KEY_XF86View,                Qt::Key_View>().data(),
    Xkb2Qt<XKB_KEY_XF86TopMenu,             Qt::Key_TopMenu>().data(),
    Xkb2Qt<XKB_KEY_XF86Red,                 Qt::Key_Red>().data(),
    Xkb2Qt<XKB_KEY_XF86Green,               Qt::Key_Green>().data(),
    Xkb2Qt<XKB_KEY_XF86Yellow,              Qt::Key_Yellow>().data(),
    Xkb2Qt<XKB_KEY_XF86Blue,                Qt::Key_Blue>().data(),
    Xkb2Qt<XKB_KEY_XF86Bluetooth,           Qt::Key_Bluetooth>().data(),
    Xkb2Qt<XKB_KEY_XF86Suspend,             Qt::Key_Suspend>().data(),
    Xkb2Qt<XKB_KEY_XF86Hibernate,           Qt::Key_Hibernate>().data(),
    Xkb2Qt<XKB_KEY_XF86TouchpadToggle,      Qt::Key_TouchpadToggle>().data(),
    Xkb2Qt<XKB_KEY_XF86TouchpadOn,          Qt::Key_TouchpadOn>().data(),
    Xkb2Qt<XKB_KEY_XF86TouchpadOff,         Qt::Key_TouchpadOff>().data(),
    Xkb2Qt<XKB_KEY_XF86AudioMicMute,        Qt::Key_MicMute>().data(),
    Xkb2Qt<XKB_KEY_XF86Launch0,             Qt::Key_Launch2>().data(), // ### Qt 6: remap properly
    Xkb2Qt<XKB_KEY_XF86Launch1,             Qt::Key_Launch3>().data(),
    Xkb2Qt<XKB_KEY_XF86Launch2,             Qt::Key_Launch4>().data(),
    Xkb2Qt<XKB_KEY_XF86Launch3,             Qt::Key_Launch5>().data(),
    Xkb2Qt<XKB_KEY_XF86Launch4,             Qt::Key_Launch6>().data(),
    Xkb2Qt<XKB_KEY_XF86Launch5,             Qt::Key_Launch7>().data(),
    Xkb2Qt<XKB_KEY_XF86Launch6,             Qt::Key_Launch8>().data(),
    Xkb2Qt<XKB_KEY_XF86Launch7,             Qt::Key_Launch9>().data(),
    Xkb2Qt<XKB_KEY_XF86Launch8,             Qt::Key_LaunchA>().data(),
    Xkb2Qt<XKB_KEY_XF86Launch9,             Qt::Key_LaunchB>().data(),
    Xkb2Qt<XKB_KEY_XF86LaunchA,             Qt::Key_LaunchC>().data(),
    Xkb2Qt<XKB_KEY_XF86LaunchB,             Qt::Key_LaunchD>().data(),
    Xkb2Qt<XKB_KEY_XF86LaunchC,             Qt::Key_LaunchE>().data(),
    Xkb2Qt<XKB_KEY_XF86LaunchD,             Qt::Key_LaunchF>().data(),
    Xkb2Qt<XKB_KEY_XF86LaunchE,             Qt::Key_LaunchG>().data(),
    Xkb2Qt<XKB_KEY_XF86LaunchF,             Qt::Key_LaunchH>().data()
  };
  std::sort(table.begin(), table.end());
  return table;
}

/* clang-format on */

static const std::vector<xkb2qt_t> KeyTbl = makeKeyTbl();

xkb_keysym_t QXkbCommon::qxkbcommon_xkb_keysym_to_upper(xkb_keysym_t ks)
{
  xkb_keysym_t lower, upper;

  xkbcommon_XConvertCase(ks, &lower, &upper);

  return upper;
}

QString QXkbCommon::lookupString(struct xkb_state* state, xkb_keycode_t code)
{
  QVarLengthArray<char, 32> chars(32);
  const int size = xkb_state_key_get_utf8(state, code, chars.data(), chars.size());
  if (Q_UNLIKELY(size + 1 > chars.size())) { // +1 for NUL
    chars.resize(size + 1);
    xkb_state_key_get_utf8(state, code, chars.data(), chars.size());
  }
  return QString::fromUtf8(chars.constData(), size);
}

QString QXkbCommon::lookupStringNoKeysymTransformations(xkb_keysym_t keysym)
{
  QVarLengthArray<char, 32> chars(32);
  const int size = xkb_keysym_to_utf8(keysym, chars.data(), chars.size());
  if (size == 0) {
    return QString(); // the keysym does not have a Unicode representation
  }

  if (Q_UNLIKELY(size > chars.size())) {
    chars.resize(size);
    xkb_keysym_to_utf8(keysym, chars.data(), chars.size());
  }
  return QString::fromUtf8(chars.constData(), size - 1);
}

QVector<xkb_keysym_t> QXkbCommon::toKeysym(QKeyEvent* event)
{
  return QXkbCommon::toKeysym(event->key(), event->modifiers(), event->text());
}

QVector<xkb_keysym_t> QXkbCommon::toKeysym(int qtKey, Qt::KeyboardModifiers modifiers, const QString& text)
{
  QVector<xkb_keysym_t> keysyms;

  if (qtKey >= Qt::Key_F1 && qtKey <= Qt::Key_F35) {
    keysyms.append(XKB_KEY_F1 + (qtKey - Qt::Key_F1));
  } else if (modifiers & Qt::KeypadModifier) {
    if (qtKey >= Qt::Key_0 && qtKey <= Qt::Key_9) {
      keysyms.append(XKB_KEY_KP_0 + (qtKey - Qt::Key_0));
    }
  } else if (isLatin(qtKey) && text.length() > 0 && text[0].isUpper()) {
    keysyms.append(qtKey);
  }

  if (!keysyms.isEmpty()) {
    return keysyms;
  }

  // check if we have a direct mapping
  auto it = std::find_if(KeyTbl.cbegin(), KeyTbl.cend(), [&qtKey](xkb2qt_t elem) {
    return elem.qt == static_cast<uint>(qtKey);
  });
  if (it != KeyTbl.end()) {
    keysyms.append(it->xkb);
    return keysyms;
  }

  QVector<uint> ucs4;
  if (text.isEmpty()) {
    ucs4.append(qtKey);
  } else {
    ucs4 = text.toUcs4();
  }

  // From libxkbcommon keysym-utf.c:
  // "We allow to represent any UCS character in the range U-00000000 to
  // U-00FFFFFF by a keysym value in the range 0x01000000 to 0x01ffffff."
  for (uint utf32 : qAsConst(ucs4)) {
    keysyms.append(utf32 | 0x01000000);
  }

  return keysyms;
}

int QXkbCommon::keysymToQtKey(xkb_keysym_t keysym, Qt::KeyboardModifiers modifiers)
{
  return keysymToQtKey(keysym, modifiers, nullptr, 0);
}

int QXkbCommon::keysymToQtKey(
    xkb_keysym_t keysym, Qt::KeyboardModifiers modifiers, xkb_state* state, xkb_keycode_t code, bool superAsMeta,
    bool hyperAsMeta
)
{
  // Note 1: All standard key sequences on linux (as defined in platform theme)
  // that use a latin character also contain a control modifier, which is why
  // checking for Qt::ControlModifier is sufficient here. It is possible to
  // override QPlatformTheme::keyBindings() and provide custom sequences for
  // QKeySequence::StandardKey. Custom sequences probably should respect this
  // convention (alternatively, we could test against other modifiers here).
  // Note 2: The possibleKeys() shorcut mechanism is not affected by this value
  // adjustment and does its own thing.
  if (modifiers & Qt::ControlModifier) {
    // With standard shortcuts we should prefer a latin character, this is
    // for checks like "some qkeyevent == QKeySequence::Copy" to work even
    // when using for example 'russian' keyboard layout.
    if (!QXkbCommon::isLatin(keysym)) {
      xkb_keysym_t latinKeysym = QXkbCommon::lookupLatinKeysym(state, code);
      if (latinKeysym != XKB_KEY_NoSymbol) {
        keysym = latinKeysym;
      }
    }
  }

  return keysymToQtKey_internal(keysym, modifiers, state, code, superAsMeta, hyperAsMeta);
}

static int keysymToQtKey_internal(
    xkb_keysym_t keysym, Qt::KeyboardModifiers modifiers, xkb_state* state, xkb_keycode_t code, bool superAsMeta,
    bool hyperAsMeta
)
{
  int qtKey = 0;

  // lookup from direct mapping
  if (keysym >= XKB_KEY_F1 && keysym <= XKB_KEY_F35) {
    // function keys
    qtKey = Qt::Key_F1 + (keysym - XKB_KEY_F1);
  } else if (keysym >= XKB_KEY_KP_0 && keysym <= XKB_KEY_KP_9) {
    // numeric keypad keys
    qtKey = Qt::Key_0 + (keysym - XKB_KEY_KP_0);
  } else if (QXkbCommon::isLatin(keysym)) {
    qtKey = QXkbCommon::qxkbcommon_xkb_keysym_to_upper(keysym);
  } else {
    // check if we have a direct mapping
    xkb2qt_t searchKey{ keysym, 0 };
    auto it = std::lower_bound(KeyTbl.cbegin(), KeyTbl.cend(), searchKey);
    if (it != KeyTbl.end() && !(searchKey < *it)) {
      qtKey = it->qt;
    }
  }

  if (qtKey) {
    return qtKey;
  }

  // lookup from unicode
  QString text;
  if (!state || modifiers & Qt::ControlModifier) {
    // Control modifier changes the text to ASCII control character, therefore we
    // can't use this text to map keysym to a qt key. We can use the same keysym
    // (it is not affectd by transformation) to obtain untransformed text. For details
    // see "Appendix A. Default Symbol Transformations" in the XKB specification.
    text = QXkbCommon::lookupStringNoKeysymTransformations(keysym);
  } else {
    text = QXkbCommon::lookupString(state, code);
  }
  if (!text.isEmpty()) {
    if (text.unicode()->isDigit()) {
      // Ensures that also non-latin digits are mapped to corresponding qt keys,
      // e.g CTRL + ۲ (arabic two), is mapped to CTRL + Qt::Key_2.
      qtKey = Qt::Key_0 + text.unicode()->digitValue();
    } else {
      qtKey = text.unicode()->toUpper().unicode();
    }
  }

  // translate Super/Hyper keys to Meta if we're using them as the MetaModifier
  if (superAsMeta && (qtKey == Qt::Key_Super_L || qtKey == Qt::Key_Super_R)) {
    qtKey = Qt::Key_Meta;
  }
  if (hyperAsMeta && (qtKey == Qt::Key_Hyper_L || qtKey == Qt::Key_Hyper_R)) {
    qtKey = Qt::Key_Meta;
  }

  return qtKey;
}

Qt::KeyboardModifiers QXkbCommon::modifiers(struct xkb_state* state)
{
  Qt::KeyboardModifiers modifiers = Qt::NoModifier;

  if (xkb_state_mod_name_is_active(state, XKB_MOD_NAME_CTRL, XKB_STATE_MODS_EFFECTIVE) > 0) {
    modifiers |= Qt::ControlModifier;
  }
  if (xkb_state_mod_name_is_active(state, XKB_MOD_NAME_ALT, XKB_STATE_MODS_EFFECTIVE) > 0) {
    modifiers |= Qt::AltModifier;
  }
  if (xkb_state_mod_name_is_active(state, XKB_MOD_NAME_SHIFT, XKB_STATE_MODS_EFFECTIVE) > 0) {
    modifiers |= Qt::ShiftModifier;
  }
  if (xkb_state_mod_name_is_active(state, XKB_MOD_NAME_LOGO, XKB_STATE_MODS_EFFECTIVE) > 0) {
    modifiers |= Qt::MetaModifier;
  }

  return modifiers;
}

// Possible modifier states.
static const Qt::KeyboardModifiers ModsTbl[] = {
  Qt::NoModifier,                                            // 0
  Qt::ShiftModifier,                                         // 1
  Qt::ControlModifier,                                       // 2
  Qt::ControlModifier | Qt::ShiftModifier,                   // 3
  Qt::AltModifier,                                           // 4
  Qt::AltModifier | Qt::ShiftModifier,                       // 5
  Qt::AltModifier | Qt::ControlModifier,                     // 6
  Qt::AltModifier | Qt::ShiftModifier | Qt::ControlModifier, // 7
  Qt::NoModifier                                             // Fall-back to raw Key_*, for non-latin1 kb layouts
};

QList<int> QXkbCommon::possibleKeys(xkb_state* state, const QKeyEvent* event, bool superAsMeta, bool hyperAsMeta)
{
  QList<int> result;
  quint32 keycode = event->nativeScanCode();
  Qt::KeyboardModifiers modifiers = event->modifiers();
  xkb_keymap* keymap = xkb_state_get_keymap(state);
  // turn off the modifier bits which doesn't participate in shortcuts
  Qt::KeyboardModifiers notNeeded = Qt::KeypadModifier | Qt::GroupSwitchModifier;
  modifiers &= ~notNeeded;
  // create a fresh kb state and test against the relevant modifier combinations
  ScopedXKBState scopedXkbQueryState(xkb_state_new(keymap));
  xkb_state* queryState = scopedXkbQueryState.get();
  if (!queryState) {
    qWarning() << Q_FUNC_INFO << "failed to compile xkb keymap";
    return result;
  }
  // get kb state from the master state and update the temporary state
  xkb_layout_index_t lockedLayout = xkb_state_serialize_layout(state, XKB_STATE_LAYOUT_LOCKED);
  xkb_mod_mask_t latchedMods = xkb_state_serialize_mods(state, XKB_STATE_MODS_LATCHED);
  xkb_mod_mask_t lockedMods = xkb_state_serialize_mods(state, XKB_STATE_MODS_LOCKED);
  xkb_mod_mask_t depressedMods = xkb_state_serialize_mods(state, XKB_STATE_MODS_DEPRESSED);
  xkb_state_update_mask(queryState, depressedMods, latchedMods, lockedMods, 0, 0, lockedLayout);
  // handle shortcuts for level three and above
  xkb_layout_index_t layoutIndex = xkb_state_key_get_layout(queryState, keycode);
  xkb_level_index_t levelIndex = 0;
  if (layoutIndex != XKB_LAYOUT_INVALID) {
    levelIndex = xkb_state_key_get_level(queryState, keycode, layoutIndex);
    if (levelIndex == XKB_LEVEL_INVALID) {
      levelIndex = 0;
    }
  }
  if (levelIndex <= 1) {
    xkb_state_update_mask(queryState, 0, latchedMods, lockedMods, 0, 0, lockedLayout);
  }

  xkb_keysym_t sym = xkb_state_key_get_one_sym(queryState, keycode);
  if (sym == XKB_KEY_NoSymbol) {
    return result;
  }

  int baseQtKey = keysymToQtKey_internal(sym, modifiers, queryState, keycode, superAsMeta, hyperAsMeta);
  if (baseQtKey) {
    result += (baseQtKey + modifiers);
  }

  xkb_mod_index_t shiftMod = xkb_keymap_mod_get_index(keymap, "Shift");
  xkb_mod_index_t altMod = xkb_keymap_mod_get_index(keymap, "Alt");
  xkb_mod_index_t controlMod = xkb_keymap_mod_get_index(keymap, "Control");
  xkb_mod_index_t metaMod = xkb_keymap_mod_get_index(keymap, "Meta");

  Q_ASSERT(shiftMod < 32);
  Q_ASSERT(altMod < 32);
  Q_ASSERT(controlMod < 32);

  xkb_mod_mask_t depressed;
  int qtKey = 0;
  // obtain a list of possible shortcuts for the given key event
  for (uint i = 1; i < sizeof(ModsTbl) / sizeof(*ModsTbl); ++i) {
    Qt::KeyboardModifiers neededMods = ModsTbl[i];
    if ((modifiers & neededMods) == neededMods) {
      if (i == 8) {
        if (isLatin(baseQtKey)) {
          continue;
        }
        // add a latin key as a fall back key
        sym = lookupLatinKeysym(state, keycode);
      } else {
        depressed = 0;
        if (neededMods & Qt::AltModifier) {
          depressed |= (1 << altMod);
        }
        if (neededMods & Qt::ShiftModifier) {
          depressed |= (1 << shiftMod);
        }
        if (neededMods & Qt::ControlModifier) {
          depressed |= (1 << controlMod);
        }
        if (metaMod < 32 && neededMods & Qt::MetaModifier) {
          depressed |= (1 << metaMod);
        }
        xkb_state_update_mask(queryState, depressed, latchedMods, lockedMods, 0, 0, lockedLayout);
        sym = xkb_state_key_get_one_sym(queryState, keycode);
      }
      if (sym == XKB_KEY_NoSymbol) {
        continue;
      }

      Qt::KeyboardModifiers mods = modifiers & ~neededMods;
      qtKey = keysymToQtKey_internal(sym, mods, queryState, keycode, superAsMeta, hyperAsMeta);
      if (!qtKey || qtKey == baseQtKey) {
        continue;
      }

      // catch only more specific shortcuts, i.e. Ctrl+Shift+= also generates Ctrl++ and +,
      // but Ctrl++ is more specific than +, so we should skip the last one
      bool ambiguous = false;
      for (int shortcut : qAsConst(result)) {
        if (int(shortcut & ~Qt::KeyboardModifierMask) == qtKey && (shortcut & mods) == mods) {
          ambiguous = true;
          break;
        }
      }
      if (ambiguous) {
        continue;
      }

      result += (qtKey + mods);
    }
  }

  return result;
}

void QXkbCommon::verifyHasLatinLayout(xkb_keymap* keymap)
{
  const xkb_layout_index_t layoutCount = xkb_keymap_num_layouts(keymap);
  const xkb_keycode_t minKeycode = xkb_keymap_min_keycode(keymap);
  const xkb_keycode_t maxKeycode = xkb_keymap_max_keycode(keymap);

  const xkb_keysym_t* keysyms = nullptr;
  int nrLatinKeys = 0;
  for (xkb_layout_index_t layout = 0; layout < layoutCount; ++layout) {
    for (xkb_keycode_t code = minKeycode; code < maxKeycode; ++code) {
      xkb_keymap_key_get_syms_by_level(keymap, code, layout, 0, &keysyms);
      if (keysyms && isLatin(keysyms[0])) {
        nrLatinKeys++;
      }
      if (nrLatinKeys > 10) { // arbitrarily chosen threshold
        return;
      }
    }
  }
  // This means that lookupLatinKeysym() will not find anything and latin
  // key shortcuts might not work. This is a bug in the affected desktop
  // environment. Usually can be solved via system settings by adding e.g. 'us'
  // layout to the list of seleced layouts, or by using command line, "setxkbmap
  // -layout rus,en". The position of latin key based layout in the list of the
  // selected layouts is irrelevant. Properly functioning desktop environments
  // handle this behind the scenes, even if no latin key based layout has been
  // explicitly listed in the selected layouts.
  qWarning() << "QXkbCommon: no keyboard layouts with latin keys present";
}

xkb_keysym_t QXkbCommon::lookupLatinKeysym(xkb_state* state, xkb_keycode_t keycode)
{
  xkb_layout_index_t layout;
  xkb_keysym_t sym = XKB_KEY_NoSymbol;
  xkb_keymap* keymap = xkb_state_get_keymap(state);
  const xkb_layout_index_t layoutCount = xkb_keymap_num_layouts_for_key(keymap, keycode);
  const xkb_layout_index_t currentLayout = xkb_state_key_get_layout(state, keycode);
  // Look at user layouts in the order in which they are defined in system
  // settings to find a latin keysym.
  for (layout = 0; layout < layoutCount; ++layout) {
    if (layout == currentLayout) {
      continue;
    }
    const xkb_keysym_t* syms = nullptr;
    xkb_level_index_t level = xkb_state_key_get_level(state, keycode, layout);
    if (xkb_keymap_key_get_syms_by_level(keymap, keycode, layout, level, &syms) != 1) {
      continue;
    }
    if (isLatin(syms[0])) {
      sym = syms[0];
      break;
    }
  }

  if (sym == XKB_KEY_NoSymbol) {
    return sym;
  }

  xkb_mod_mask_t latchedMods = xkb_state_serialize_mods(state, XKB_STATE_MODS_LATCHED);
  xkb_mod_mask_t lockedMods = xkb_state_serialize_mods(state, XKB_STATE_MODS_LOCKED);

  // Check for uniqueness, consider the following setup:
  // setxkbmap -layout us,ru,us -variant dvorak,, -option 'grp:ctrl_alt_toggle' (set 'ru' as active).
  // In this setup, the user would expect to trigger a ctrl+q shortcut by pressing ctrl+<physical x key>,
  // because "US dvorak" is higher up in the layout settings list. This check verifies that an obtained
  // 'sym' can not be acquired by any other layout higher up in the user's layout list. If it can be acquired
  // then the obtained key is not unique. This prevents ctrl+<physical q key> from generating a ctrl+q
  // shortcut in the above described setup. We don't want ctrl+<physical x key> and ctrl+<physical q key> to
  // generate the same shortcut event in this case.
  const xkb_keycode_t minKeycode = xkb_keymap_min_keycode(keymap);
  const xkb_keycode_t maxKeycode = xkb_keymap_max_keycode(keymap);
  ScopedXKBState queryState(xkb_state_new(keymap));
  for (xkb_layout_index_t prevLayout = 0; prevLayout < layout; ++prevLayout) {
    xkb_state_update_mask(queryState.get(), 0, latchedMods, lockedMods, 0, 0, prevLayout);
    for (xkb_keycode_t code = minKeycode; code < maxKeycode; ++code) {
      xkb_keysym_t prevSym = xkb_state_key_get_one_sym(queryState.get(), code);
      if (prevSym == sym) {
        sym = XKB_KEY_NoSymbol;
        break;
      }
    }
  }

  return sym;
}

QT_END_NAMESPACE
