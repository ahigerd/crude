#ifndef CRUDE_SMSERVER_H
#define CRUDE_SMSERVER_H

#include <QMap>
#include <QObject>
#include <QVariant>
class QSocketNotifier;
class SMServerPrivate;
class SMClient;

class SMServer : public QObject
{
  Q_OBJECT

public:
  SMServer(QObject* parent = nullptr);
  ~SMServer();

  QList<SMClient*> clients() const;

signals:
  void newClient(SMClient* client);

private slots:
  void newConnection(int fd);
  void socketActivity(int fd);

  void interactRequest(int type, SMClient* client);
  void interactDone(bool cancel, SMClient* client);
  void needsPhase2(SMClient* client);
  void saved(bool success, SMClient* client);
  void propertyChanged(const QString& key, SMClient* client);
  void disconnected(const QStringList& reasons, SMClient* client);
  void clientDestroyed(QObject* client);

  void shutdownTimeout();

private:
  friend class SMClient;
  friend class SMConnection;
  friend class SMServerPrivate;
  SMServerPrivate* d;

  void addClient(SMClient* client);
  void iceClosed(int fd);
  void broadcastPhase1(int saveType, bool shutdown, int style, bool fast);
  void checkPhases();
};

#endif
