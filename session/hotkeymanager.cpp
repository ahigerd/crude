#include "hotkeymanager.h"

#include "libcrude/displaypower.h"
#include "libcrude/hotkey.h"
#include "libcrude/platformintegration.h"
#include "libcrude/powermanagement.h"
#include "libcrude/sessionsettings.h"
#include "volumeicon.h"

#include <QApplication>
#include <QTimer>

static QStringList builtinActions = { "backlight+", "backlight-", "keybacklight+", "keybacklight-",  "volume+",
                                      "volume-",    "mute",       "screenshot",    "screenshotrect", "suspend" };

HotkeyManager::HotkeyManager(QObject* parent) : QObject(parent)
{
  QObject::connect(Settings::instance(), SIGNAL(reloadSettings()), this, SLOT(reload()));
  reload();
}

void HotkeyManager::reload()
{
  savePending = true;
  qDeleteAll(hotkeys);
  hotkeys.clear();
  custom.clear();
  for (const QString& key : Settings::instance()->keys("hotkeys")) {
    QString customAction;
    if (key.startsWith("custom")) {
      customAction = Settings::instance()->value("customActions/" + key).toString();
      if (customAction.isEmpty()) {
        // Invalid configuration, skip
        continue;
      }
    }
    if (builtinActions.contains(key) || !customAction.isEmpty()) {
      QString seq = Settings::instance()->value("hotkeys/" + key).toString();
      if (!seq.isEmpty()) {
        addHotkey(QKeySequence(seq, QKeySequence::PortableText), key, customAction);
      }
    }
  }
  savePending = false;
}

void HotkeyManager::addHotkey(const QKeySequence& shortcut, const QString& action, const QString& customAction)
{
  Hotkey* hotkey = new Hotkey(shortcut, this);
  hotkey->setData(action);
  hotkeys << hotkey;
  if (action.startsWith("custom") && !customAction.isEmpty()) {
    custom[action] = customAction;
  }
  QObject::connect(hotkey, SIGNAL(activated(QVariant)), this, SLOT(triggered(QVariant)));
  queueSave();
}

void HotkeyManager::removeHotkey(const QKeySequence& seq)
{
  Hotkey* hotkey = findHotkey(seq);
  if (hotkey) {
    hotkeys.remove(hotkey);
    hotkey->deleteLater();
    queueSave();
  }
}

Hotkey* HotkeyManager::findHotkey(const QKeySequence& shortcut) const
{
  for (Hotkey* hotkey : hotkeys) {
    if (hotkey->keySequence() == shortcut) {
      return hotkey;
    }
  }
  return nullptr;
}

QString HotkeyManager::action(const QKeySequence& hotkey) const
{
  Hotkey* hk = findHotkey(hotkey);
  if (hk) {
    return hk->data().toString();
  }
  return QString();
}

QKeySequence HotkeyManager::findHotkey(const QString& action) const
{
  for (Hotkey* hotkey : hotkeys) {
    if (hotkey->data().toString() == action) {
      return hotkey->keySequence();
    }
  }
  return QKeySequence();
}

void HotkeyManager::triggered(const QVariant& data)
{
  QString action = data.toString();
  if (action.startsWith("custom")) {
    c_PI->runCommand(custom[action]);
  } else if (action == "backlight+") {
    c_PI->displayPower()->increaseBacklight();
  } else if (action == "backlight-") {
    c_PI->displayPower()->decreaseBacklight();
  } else if (action == "volume+") {
    VolumeIcon::modifyVolume(+0.05f);
  } else if (action == "volume-") {
    VolumeIcon::modifyVolume(-0.05f);
  } else if (action == "mute") {
    VolumeIcon::toggleMute();
  } else if (action == "suspend") {
    PowerManagement::instance()->perform(PowerPlugin::Suspend);
  } else if (action == "screenshot" || action == "screenshotrect") {
    QString command = qApp->applicationDirPath() + "/crude-shot ";
    QString output = Settings::instance()->value("environment/shotOutput").toString();
    if (output.length() == 2 && output[0] == '-') {
      command += output;
    } else {
      command += "-p";
    }
    if (action == "screenshotrect") {
      QString region = Settings::instance()->value("environment/shotRegion").toString();
      if (region.length() == 2 && region[0] == '-') {
        command += " " + region;
      } else {
        command += " -g";
      }
    }
    c_PI->runCommand(command);
  }
}

const QSet<Hotkey*>& HotkeyManager::allHotkeys() const
{
  return hotkeys;
}

QString HotkeyManager::customAction(const QString& name) const
{
  return custom.value(name);
}

bool HotkeyManager::setCustomAction(const QString& name, const QString& action)
{
  if (name.startsWith("custom")) {
    custom[name] = action;
    queueSave();
    return true;
  }
  return false;
}

void HotkeyManager::queueSave()
{
  if (!savePending) {
    savePending = true;
    QTimer::singleShot(0, this, SLOT(save()));
  }
}

void HotkeyManager::save()
{
  QStringList usedKeys;
  for (const Hotkey* hotkey : hotkeys) {
    usedKeys << hotkey->data().toString();
  }
  for (const QString& key : Settings::instance()->keys("customActions")) {
    if (!usedKeys.contains(key)) {
      Settings::instance()->remove("customActions/" + key);
    }
  }
  for (const QString& key : Settings::instance()->keys("hotkeys")) {
    if (!usedKeys.contains(key)) {
      Settings::instance()->remove("hotkeys/" + key);
    }
  }
  for (Hotkey* hotkey : hotkeys) {
    QString key = hotkey->data().toString();
    Settings::instance()->setValue("hotkeys/" + key, hotkey->keySequence().toString(QKeySequence::PortableText));
    if (key.startsWith("custom")) {
      Settings::instance()->setValue("customActions/" + key, custom[key]);
    }
  }
  savePending = false;
}
