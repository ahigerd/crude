TEMPLATE = app
TARGET = crude-session
QT = core widgets x11extras
LIBS += -L.. -lcrude -lICE -lSM
include(../crude-common.pri)

HEADERS = sm_p.h
SOURCES = main.cpp

HEADERS += respawner.h   appgroup.h   smserver.h   smclient.h   hotkeymanager.h
SOURCES += respawner.cpp appgroup.cpp smserver.cpp smclient.cpp hotkeymanager.cpp

HEADERS += systraybuiltin.h   builtinicon.h   batteryicon.h   saverbuiltin.h   volumeicon.h
SOURCES += systraybuiltin.cpp builtinicon.cpp batteryicon.cpp saverbuiltin.cpp volumeicon.cpp

RESOURCES = resources.qrc

PRE_TARGETDEPS += ../libcrude.a
