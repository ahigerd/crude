#ifndef CRUDE_SAVERBUILTIN_H
#define CRUDE_SAVERBUILTIN_H

#include "systraybuiltin.h"

#include <QHash>

#include <memory>

class SaverBuiltin : public SystrayBuiltin
{
  Q_OBJECT

public:
  SaverBuiltin(QObject* parent = nullptr);

  virtual bool isRunning() const;
  virtual void start();
  virtual void stop();

private slots:
  void applySettings();

private:
  QString fillFields(const QString& command) const;
  void updateTimeout();
  void stopScheduler();

  bool running;
  QHash<QString, QString> schedulerConfig, lockerConfig;
  Respawner* schedulerTask;
  int delaySeconds;
};

#endif
