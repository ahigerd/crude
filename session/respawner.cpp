#include "respawner.h"

#include <QCoreApplication>
#include <QTimer>
#include <QtDebug>

#include <signal.h>

Respawner::Respawner(const QString& command, QObject* parent) : Respawner(command, QString(), parent)
{
  // Forwarded ctor only
}

Respawner::Respawner(const QString& command, const QString& stopCommand, QObject* parent)
: QObject(parent),
  process(nullptr),
  cmd(command),
  stopCmd(stopCommand),
  cleanRespawn(false),
  crashRespawn(true),
  shuttingDown(false),
  crashExit(false)
{
  timer = new QTimer(this);
  timer->setInterval(2000); // TODO: does this need to be configurable?
  timer->setSingleShot(true);
  QObject::connect(timer, SIGNAL(timeout()), this, SLOT(start()));
  QObject::connect(QCoreApplication::instance(), SIGNAL(aboutToQuit()), this, SLOT(stop()));
}

QString Respawner::command() const
{
  return cmd;
}

QString Respawner::stopCommand() const
{
  return stopCmd;
}

bool Respawner::respawnOnCleanExit() const
{
  return cleanRespawn;
}

void Respawner::setRespawnOnCleanExit(bool on)
{
  cleanRespawn = on;
}

bool Respawner::respawnOnCrash() const
{
  return crashRespawn;
}

void Respawner::setRespawnOnCrash(bool on)
{
  crashRespawn = on;
}

bool Respawner::isRunning() const
{
  return process && process->state() != QProcess::NotRunning;
}

bool Respawner::isStopping() const
{
  return shuttingDown && isRunning();
}

bool Respawner::isCrashed() const
{
  return crashExit;
}

void Respawner::start()
{
  if (process) {
    qWarning("Respawner: cannot start '%s', already running", qPrintable(cmd));
    return;
  }
  shuttingDown = false;
  crashExit = false;
  process = new QProcess(this);
  process->setProcessChannelMode(QProcess::ForwardedChannels);
  QObject::connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(finished(int, QProcess::ExitStatus)));
  QObject::connect(process, SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(errored(QProcess::ProcessError)));
  qDebug() << cmd;
  process->start(cmd, QIODevice::ReadWrite);
  /*
  process->closeReadChannel(QProcess::StandardOutput);
  process->closeReadChannel(QProcess::StandardError);
  */
}

void Respawner::stop()
{
  shuttingDown = true;
  timer->stop();
  if (process) {
    if (!stopCmd.isEmpty()) {
      // We must assume that the program will eventually stop.
      // If it doesn't, we must not forcefully abort it, because if that program is
      // a locked instance of xscreensaver or xautolock, the X server becomes unusable.
      QProcess::startDetached(stopCmd);
      qWarning("ran %s", qPrintable(stopCmd));
      return;
    }
    QObject::connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), process, SLOT(deleteLater()));
    QTimer::singleShot(5000, process, SLOT(kill()));
    process->closeWriteChannel();
    process->terminate();
    process = nullptr;
  }
}

void Respawner::finished(int exitCode, QProcess::ExitStatus status)
{
  Q_UNUSED(exitCode);
  crashExit = (status == QProcess::CrashExit);
  process->deleteLater();
  process = nullptr;
  if (crashExit) {
    emit crashed();
    if (crashRespawn && !shuttingDown) {
      timer->start();
    }
  } else {
    emit exited();
    if (cleanRespawn && !shuttingDown) {
      timer->start();
    }
  }
}

void Respawner::errored(QProcess::ProcessError code)
{
  if (code == QProcess::FailedToStart) {
    emit failed();
    stop();
  }
}

void Respawner::sendUnixSignal(int sig)
{
  if (isRunning() && process) {
    ::kill(process->processId(), sig);
  }
}
