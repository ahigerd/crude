#include "batteryicon.h"

#include "libcrude/acpimonitor.h"
#include "libcrude/powermanagement.h"
#include "libcrude/throttle.h"

#include <QApplication>
#include <QImage>
#include <QImageReader>
#include <QPainter>
#include <QPalette>
#include <QPixmap>
#include <QTimer>

static bool useSvg = false;

BatteryIcon::BatteryIcon(QObject* parent) : BuiltinIcon(parent)
{
  AcpiMonitor* acpi = AcpiMonitor::instance();
  QObject::connect(acpi, SIGNAL(batteryLevelChanged(int)), this, SLOT(batteryLevelChanged(int)));
  QObject::connect(acpi, SIGNAL(plugChanged(bool)), this, SLOT(plugChanged(bool)));
  lastPlug = acpi->isPluggedIn();
  lastLevel = acpi->batteryLevel();

  useSvg = QImageReader::supportedImageFormats().contains("svg");
}

void BatteryIcon::plugChanged(bool on)
{
  lastPlug = on;
  emit dataUpdated();
}

void BatteryIcon::batteryLevelChanged(int level)
{
  lastLevel = level;
  emit dataUpdated();
}

QIcon BatteryIcon::render(const QSize& size)
{
  if (lastLevel < 0) {
    // No battery, assume AC power
    setToolTip(QString());
    if (useSvg) {
      return QIcon(":/resources/ac-power.svg");
    } else {
      return QIcon(":/resources/ac-power.png");
    }
  } else {
    int w = size.width();
    float fraction = lastLevel / 100.0;
    QImage img(size, QImage::Format_ARGB32);
    img.fill(Qt::transparent);
    QBrush fg = QApplication::palette("QFrame").buttonText();
    QBrush base = QApplication::palette("QFrame").base();
    QPainter p(&img);
    p.setBrush(fg);
    p.setPen(Qt::transparent);
    p.drawRect(0, 0, w - 3, w / 2);
    p.drawRect(w - 3, w / 8, 3, w / 2 - 2 * (w / 8));
    p.setBrush(base);
    p.drawRect(1, 1, w - 5, w / 2 - 2);
#define lerp(a, b, t) (a + t * double(b - a))
    if (fraction > .6) {
      float color = (fraction - .6) / .4;
      int red = lerp(224, 0, color);
      int green = lerp(224, 255, color);
      p.setBrush(QColor(red, green, 0));
    } else {
      float color = fraction / .6;
      int red = lerp(255, 224, color);
      int green = lerp(0, 224, color);
      p.setBrush(QColor(red, green, 0));
    }
    p.drawRect(2, 2, ((w - 7) * fraction), w / 2 - 4);
    if (lastPlug) {
      QTime ttf = AcpiMonitor::instance()->timeToFull();
      if (ttf.isNull()) {
        setToolTip(QString());
      } else {
        setToolTip("Estimated time to full: " + ttf.toString("h'h' m'm'"));
      }
      int step = (w / 2 - 5) / 2;
      p.setPen(QPen(fg, 3));
      p.drawLine(w / 2 - 2, w / 4 - step, w / 2 - 2, w / 4 + step);
      p.drawLine(w / 2 - 2 - step, w / 4, w / 2 - 2 + step, w / 4);
      p.setPen(QPen(base, 1));
      p.drawLine(w / 2 - 2, w / 4 - step, w / 2 - 2, w / 4 + step);
      p.drawLine(w / 2 - 2 - step, w / 4, w / 2 - 2 + step, w / 4);
    } else {
      QTime tte = AcpiMonitor::instance()->timeToEmpty();
      if (tte.isNull()) {
        setToolTip(QString());
      } else {
        setToolTip("Estimated time to empty: " + tte.toString("h'h' m'm'"));
      }
    }
    p.setBrush(fg);
    p.setPen(QPen(fg, 0));
    QFont font = p.font();
    font.setPixelSize(w * .45);
    p.setFont(font);
    p.drawText(QRect(0, w / 2, w, w / 2), Qt::AlignCenter, QString::number(lastLevel) + "%");
    return QIcon(QPixmap::fromImage(img));
  }
}
