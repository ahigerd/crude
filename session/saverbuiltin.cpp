#include "saverbuiltin.h"

#include "libcrude/displaypower.h"
#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"

#include <QTime>
#include <QFile>

SaverBuiltin::SaverBuiltin(QObject* parent)
: SystrayBuiltin(nullptr, parent), running(false), schedulerTask(nullptr), delaySeconds(0)
{
  QObject::connect(Settings::instance(), SIGNAL(reloadSettings()), this, SLOT(applySettings()));
}

bool SaverBuiltin::isRunning() const
{
  return running;
}

void SaverBuiltin::start()
{
  shuttingDown = false;
  running = true;
  applySettings();
}

void SaverBuiltin::applySettings()
{
  if (!running) {
    // Don't do anything if we're explicitly stopped
    return;
  }
  auto savers = DisplayPower::scanScreenSavers();
  Settings* s = Settings::instance();
  QString scheduler = s->value("screensaver").toString();
  QString locker = s->value("screenlocker").toString();
  DisplayPower::ScreenSaverConfig cfgSched, cfgLock;
  for (const DisplayPower::ScreenSaverConfig& cfg : savers) {
    if (cfg.name == scheduler && cfg.type != DisplayPower::Locker) {
      cfgSched = cfg;
      if (cfgLock.name == locker) {
        break;
      }
    } else if (cfg.name == locker && cfg.type == DisplayPower::Locker) {
      cfgLock = cfg;
      if (cfgSched.name == scheduler) {
        break;
      }
    }
  }

  QString delaySetting = Settings::instance()->value("dpms/screensaver").toString();
  int oldDelay = delaySeconds;
  delaySeconds = 0;
  if (!delaySetting.isEmpty() && delaySetting != "disabled") {
    delaySeconds = QTime::fromString(delaySetting).msecsSinceStartOfDay() / 1000;
  }

  if (delaySeconds == 0) {
    // screensaver is disabled
    stopScheduler();
    return;
  }

  if (cfgSched.config == schedulerConfig && cfgLock.config == lockerConfig && oldDelay == delaySeconds) {
    // No configuration changes
    return;
  }
  schedulerConfig = cfgSched.config;
  lockerConfig = cfgLock.config;

  QString schedulerStart = fillFields(schedulerConfig["enable"]);
  QString schedulerStop = fillFields(schedulerConfig["disable"]);
  bool isChanged =
      !schedulerTask || schedulerTask->command() != schedulerStart || schedulerTask->stopCommand() != schedulerStop;
  if (!isChanged) {
    isChanged = schedulerConfig.contains("configfile") && schedulerConfig.contains("timeout");
  }
  if (isChanged && schedulerTask) {
    if (schedulerTask->isRunning()) {
      schedulerTask->stop();
    }
    schedulerTask->deleteLater();
  }

  updateTimeout();

  if (isChanged) {
    schedulerTask = new Respawner(schedulerStart, schedulerStop, this);
  }
  if (isRunning() && schedulerTask && !schedulerTask->isRunning()) {
    schedulerTask->start();
  }
}

void SaverBuiltin::stop()
{
  shuttingDown = true;
  running = false;
  stopScheduler();
}

void SaverBuiltin::stopScheduler()
{
  if (schedulerTask && schedulerTask->isRunning()) {
    schedulerTask->stop();
  }
}

QString SaverBuiltin::fillFields(const QString& command) const
{
  return QString(command)
      .replace("{seconds}", QString::number(delaySeconds))
      .replace("{minutes}", QString::number(delaySeconds / 60))
      .replace("{h:m:s}", QTime::fromMSecsSinceStartOfDay(delaySeconds * 1000).toString())
      .replace("{locker}", lockerConfig["activate"]);
}

void SaverBuiltin::updateTimeout()
{
  if (delaySeconds == 0) {
    // Invalid configuration
    return;
  }

  QString timeoutCommand = schedulerConfig["timeout"];
  if (timeoutCommand.isEmpty()) {
    // There's no action to update the timeout.
    // applySettings() will restart the scheduler.
    return;
  }

  QString configFile = schedulerConfig["configfile"];
  if (configFile.isEmpty()) {
    c_PI->runCommand(fillFields(timeoutCommand));
  } else {
    QFile ssConfig(configFile);
    if (!ssConfig.open(QIODevice::ReadOnly)) {
      return;
    }
    QList<QByteArray> lines = ssConfig.readAll().split('\n');
    ssConfig.close();
    QByteArray find =
        timeoutCommand.replace("{h:m:s}", "").replace("{seconds}", "").replace("{minutes}", "").trimmed().toUtf8();
    if (ssConfig.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
      for (QByteArray& line : lines) {
        if (line.contains(find)) {
          ssConfig.write((QString(line).section(find, 0, -2) + fillFields(timeoutCommand)).toUtf8());
        } else {
          ssConfig.write(line);
        }
        ssConfig.write("\n");
      }
      ssConfig.close();
    }
  }
}
