#ifndef CRUDE_APPGROUP_H
#define CRUDE_APPGROUP_H

#include <QMap>
#include <QObject>
#include <QSet>
class Respawner;

class AppGroup : public QObject
{
  Q_OBJECT

public:
  AppGroup(const QString& group, QObject* parent = nullptr);

public slots:
  void systrayStateChanged(bool on);
  void stopAll();
  void reloadSettings();

signals:
  void fatalError();
  void logoutRequested();

private:
  QString group;
  QMap<QString, Respawner*> apps;
  QSet<Respawner*> systrayKill;
};

#endif
