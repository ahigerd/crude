#include "appgroup.h"

#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"
#include "respawner.h"
#include "systraybuiltin.h"

#include <QApplication>
#include <QSystemTrayIcon>

AppGroup::AppGroup(const QString& group, QObject* parent) : QObject(parent), group(group)
{
  QObject::connect(Settings::instance(), SIGNAL(reloadSettings()), this, SLOT(reloadSettings()));
  reloadSettings();
}

void AppGroup::reloadSettings()
{
  Settings* s = Settings::instance();
  for (const QString& key : s->keys(group)) {
    if (group == "environment" && (key == "terminal" || key == "version")) {
      continue;
    }
    Respawner* rs = nullptr;
    SessionService service = s->getService(group, key);
    if (service.commandLine.isEmpty() || service.commandLine == "disabled") {
      // Explicitly disabled. Leave rs as nullptr.
    } else if (group == "panels") {
      QString commandLine = QApplication::applicationDirPath() + "/crude-panel --in-session";
      if (service.commandLine != "default") {
        commandLine += " " + service.commandLine;
      }
      rs = new Respawner(commandLine, this);
      QObject::connect(c_PI, SIGNAL(unixSignal(int)), rs, SLOT(sendUnixSignal(int)));
    } else {
      if (service.commandLine == "builtin") {
        if (key == "zoom") {
          // Zoom builtin is actually an external program
          rs = new Respawner(QApplication::applicationDirPath() + "/crude-zoom", this);
        } else if (key != "desktopmanager") {
          // Desktop manager builtin is a no-op
          SystrayBuiltin* icon = SystrayBuiltin::create(key, this);
          if (icon) {
            rs = icon;
          } else {
            qWarning("Respawner: unknown builtin '%s'", qPrintable(key));
          }
        }
      } else {
        rs = new Respawner(service.commandLine, this);
      }
    }
    Respawner* existing = nullptr;
    if (apps.contains(key)) {
      existing = apps[key];
      if (rs && existing->command() == rs->command()) {
        // When reloading settings, the program was found to already exist.
        // Delete the new instance and skip to the next one.
        delete rs;
        continue;
      } else if (existing->isRunning()) {
        // When reloading settings, the program was found to already be running,
        // but the command changed. Stop the existing instance and prepare to
        // launch the new one when it exits.
        if (rs) {
          QObject::connect(existing, SIGNAL(exited()), rs, SLOT(start()));
          QObject::connect(existing, SIGNAL(crashed()), rs, SLOT(start()));
        }
        QObject::connect(existing, SIGNAL(exited()), existing, SLOT(deleteLater()));
        QObject::connect(existing, SIGNAL(crashed()), existing, SLOT(deleteLater()));
        existing->stop();
      } else {
        // When reloading settings, the command line was changed, but the existing
        // instance was not running. Mark it as stopped so it doesn't try to respawn
        // in case of a race condition, but go ahead and start this instance immediately.
        existing->stop();
        existing->deleteLater();
        existing = nullptr;
      }
    }
    if (!rs) {
      apps.remove(key);
      continue;
    }
    apps[key] = rs;
    rs->setRespawnOnCrash(service.respawnOnCrash);
    rs->setRespawnOnCleanExit(service.respawnOnCrash && !service.logoutOnExit);
    QObject::connect(rs, SIGNAL(failed()), this, SIGNAL(fatalError()));
    if (service.logoutOnExit) {
      QObject::connect(rs, SIGNAL(exited()), this, SIGNAL(logoutRequested()));
    }
    if (group == "systemtray") {
      if (service.killWithSystray) {
        systrayKill << rs;
      }
    } else if (!existing) {
      rs->start();
    }
  }
  if (group == "systemtray") {
    QObject::connect(c_PI, SIGNAL(systemTrayAvailabilityChanged(bool)), this, SLOT(systrayStateChanged(bool)));
    if (QSystemTrayIcon::isSystemTrayAvailable()) {
      systrayStateChanged(true);
    }
  }
}

void AppGroup::systrayStateChanged(bool on)
{
  if (!on) {
    for (Respawner* rs : systrayKill) {
      rs->stop();
    }
  } else {
    for (Respawner* rs : apps.values()) {
      // XXX: There might be a bug here, if a systray icon has a _stop command and the
      // old instance doesn't exit in a timely manner, If there was a settings change,
      // then both the old program and the new program may be running at the same time.
      // Alternatively, if the systray was restarted, the old instance may not have
      // completed its shutdown; if it isn't configured to respawn itself on exit, then
      // it won't ever be restarted.
      //
      // This is a difficult problem to resolve, especially considering the narrow scope
      // of the potential failure case, so the answer might need to be that this combination
      // of settings with a program that might linger after being asked to shut down should
      // be prohibited.
      if (!rs->isRunning()) {
        rs->start();
      }
    }
  }
}

void AppGroup::stopAll()
{
  for (Respawner* rs : apps.values()) {
    rs->stop();
  }
}
