#include "smclient.h"

#include "sm_p.h"
#include "smserver.h"

#include <cstdlib>
#include <memory>

#define RETURN_Status return
#define RETURN_void
#define _S(...) __VA_ARGS__
#define SM_METHOD(Return, Class, name, signature, ...) \
  static Return _##name(SmsConn conn, SmPointer d, signature) \
  { \
    Q_UNUSED(conn); \
    RETURN_##Return reinterpret_cast<Class*>(d)->name(__VA_ARGS__); \
  } \
  Return name(signature)
#define SM_METHOD_0(Return, Class, name) \
  static Return _##name(SmsConn conn, SmPointer d) \
  { \
    Q_UNUSED(conn); \
    RETURN_##Return reinterpret_cast<Class*>(d)->name(); \
  } \
  Return name()

struct SMConnection : public SMConnectionBase
{
  SMConnection(SMServer* server, SmsConn conn) : SMConnectionBase(conn), p(nullptr), server(server) {}

  SMClient* p;
  SMServer* server;
  QByteArray clientId;
  QMap<QString, QVariant> props;

  SM_METHOD(Status, SMConnection, registerClient, _S(char* _previousId), _previousId)
  {
    QByteArray previousId;
    if (_previousId) {
      previousId = _previousId;
      std::free(_previousId);
      // TODO: look up session
      return 0;
    }
    std::unique_ptr<char, decltype(&std::free)> uuid(SmsGenerateClientID(connection), std::free);
    clientId = uuid.get();
    int ok = SmsRegisterClientReply(connection, clientId.data());
    if (ok) {
      SmsSaveYourself(connection, SmSaveLocal, false, SmInteractStyleNone, false);
      p = new SMClient(this, server);
      server->addClient(p);
    }
    return ok;
  }

  SM_METHOD(void, SMConnection, interactRequest, _S(int dialogType), dialogType)
  {
    if (p) {
      emit p->interactRequest(dialogType, p);
    }
  }

  SM_METHOD(void, SMConnection, interactDone, _S(int cancel), cancel)
  {
    if (p) {
      emit p->interactDone(cancel, p);
    }
  }

  SM_METHOD(
      void, SMConnection, phase1Request, _S(int saveType, int shutdown, int style, int fast, int global), saveType,
      shutdown, style, fast, global
  )
  {
    if (global) {
      server->broadcastPhase1(saveType, shutdown, style, fast);
    } else {
      SmsSaveYourself(connection, saveType, shutdown, style, fast);
    }
  }

  SM_METHOD_0(void, SMConnection, phase2Request)
  {
    if (p) {
      emit p->needsPhase2(p);
    }
  }

  SM_METHOD(void, SMConnection, saveYourselfDone, _S(int success), success)
  {
    if (p) {
      emit p->saved(success, p);
    }
  }

  SM_METHOD(void, SMConnection, connectionClosed, _S(int count, char** msgs), count, msgs)
  {
    QStringList reasons;
    for (int i = 0; i < count; i++) {
      reasons << QString::fromUtf8(msgs[i]);
    }
    SmFreeReasons(count, msgs);
    emit p->disconnected(reasons, p);
    SmsCleanUp(connection);
    connection = nullptr;
  }

  SM_METHOD(void, SMConnection, setProperties, _S(int count, SmProp** newProps), count, newProps)
  {
    for (int i = 0; i < count; i++) {
      SmProp* prop = newProps[i];
      QString key = QString::fromUtf8(prop->name);
      QByteArray type = prop->type;
      QVariant value;
      if (type == SmCARD8) {
        value = (int)*(quint8*)(prop->vals[0].value);
      } else if (type == SmARRAY8) {
        value = QString::fromUtf8((char*)prop->vals[0].value, prop->vals[0].length);
      } else if (type == SmLISTofARRAY8) {
        QStringList values;
        for (int k = 0; k < prop->num_vals; k++) {
          values << QString::fromUtf8((char*)prop->vals[k].value, prop->vals[k].length);
        }
        value = values;
      } else {
        qWarning("SMConnection: unknown property type %s", type.constData());
      }
      p->setProperty(key, value);
      SmFreeProperty(prop);
    }
    std::free(newProps);
  }

  SM_METHOD(void, SMConnection, deleteProperties, _S(int count, char** names), count, names)
  {
    for (int i = 0; i < count; i++) {
      QString key = names[i];
      if (props.contains(key)) {
        props.remove(key);
        emit p->propertyChanged(key, p);
      }
    }
  }

  SM_METHOD_0(void, SMConnection, getProperties)
  {
    QStringList keys = props.keys();
    int numKeys = keys.length();
    std::unique_ptr<SmProp*[]> result(new SmProp*[numKeys]);
    std::unique_ptr<SmProp[]> propObjects(new SmProp[numKeys]);
    QList<SmPropValue*> freeValues;
    QList<QByteArray> freeStrings;
    for (int i = 0; i < numKeys; i++) {
      QString key = keys[i];
      result[i] = &propObjects[i];
      QVariant value = props[key];
      freeStrings << qPrintable(key);
      result[i]->name = freeStrings.last().data();
      if (value.type() == QVariant::Int) {
        result[i]->type = SmCARD8;
        result[i]->num_vals = 1;
        result[i]->vals = new SmPropValue[1]{ { 1, new char[1]{ (char)value.toInt() } } };
      } else if (value.type() == QVariant::String) {
        result[i]->type = SmARRAY8;
        result[i]->num_vals = 1;
        freeStrings << value.toString().toUtf8();
        result[i]->vals = new SmPropValue[1]{ { freeStrings.last().length(), freeStrings.last().data() } };
      } else if (value.type() == QVariant::StringList) {
        QStringList v = value.toStringList();
        result[i]->type = SmLISTofARRAY8;
        result[i]->num_vals = v.length();
        result[i]->vals = new SmPropValue[v.length()];
        for (int j = 0; j < result[i]->num_vals; j++) {
          freeStrings << v[j].toUtf8();
          result[i]->vals[j].length = freeStrings.last().length();
          result[i]->vals[j].value = freeStrings.last().data();
        }
      } else {
        qWarning("SMConnection: unknown property type for %s", qPrintable(key));
      }
      freeValues << result[i]->vals;
    }
    SmsReturnProperties(connection, numKeys, result.get());
    for (SmPropValue* pv : freeValues) {
      delete[] pv;
    }
  }
};

SMClient::SMClient(SMConnection* conn, QObject* parent) : QObject(parent), d(conn)
{
  // initializers only
}

SMClient::~SMClient()
{
  delete d;
}

QString SMClient::clientId() const
{
  return d->clientId;
}

QVariant SMClient::property(const QString& name) const
{
  return d->props.value(name);
}

void SMClient::setProperty(const QString& name, const QVariant& value)
{
  if (value.type() == QVariant::String || value.type() == QVariant::StringList || value.type() == QVariant::Int) {
    if (value != d->props.value(name)) {
      d->props[name] = value;
      emit propertyChanged(name, this);
    }
    if (name == "CrudeLogoutMethod") {
      d->server->d->shutdownMethod = value.toString();
    }
  } else {
    qWarning("SMClient: unknown property type for %s", qPrintable(name));
  }
}

SMConnectionBase* SMClient::dbase()
{
  return d;
}

Status SMServerPrivate::newClient(SmsConn conn, SmPointer _d, unsigned long* mask, SmsCallbacks* cb, char** err)
{
  Q_UNUSED(err);
  SMServerPrivate* d = reinterpret_cast<SMServerPrivate*>(_d);
  SMConnection* newConn = new SMConnection(d->p, conn);
  *mask = 0x2F;
#define SET_CALLBACK(ptr, method) \
  cb->ptr.callback = &SMConnection::_##method; \
  cb->ptr.manager_data = newConn;
  SET_CALLBACK(register_client, registerClient);
  SET_CALLBACK(interact_request, interactRequest);
  SET_CALLBACK(interact_done, interactDone);
  SET_CALLBACK(save_yourself_request, phase1Request);
  SET_CALLBACK(save_yourself_phase2_request, phase2Request);
  SET_CALLBACK(save_yourself_done, saveYourselfDone);
  SET_CALLBACK(close_connection, connectionClosed);
  SET_CALLBACK(set_properties, setProperties);
  SET_CALLBACK(delete_properties, deleteProperties);
  SET_CALLBACK(get_properties, getProperties);
#undef SET_CALLBACK
  return 1;
}
