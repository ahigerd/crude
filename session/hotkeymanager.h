#ifndef CRUDE_HOTKEYMANAGER_H
#define CRUDE_HOTKEYMANAGER_H

#include <QKeySequence>
#include <QMap>
#include <QObject>
#include <QSet>
class Hotkey;

class HotkeyManager : public QObject
{
  Q_OBJECT

public:
  HotkeyManager(QObject* parent = nullptr);

  void addHotkey(const QKeySequence& shortcut, const QString& action, const QString& customAction = QString());
  void removeHotkey(const QKeySequence& hotkey);

  QString action(const QKeySequence& hotkey) const;
  QKeySequence findHotkey(const QString& action) const;
  const QSet<Hotkey*>& allHotkeys() const;

  QString customAction(const QString& name) const;
  bool setCustomAction(const QString& name, const QString& action);

private slots:
  void triggered(const QVariant& data);
  void save();
  void reload();

private:
  Hotkey* findHotkey(const QKeySequence& shortcut) const;
  void queueSave();

  QSet<Hotkey*> hotkeys;
  QMap<QString, QString> custom;
  bool savePending;
};

#endif
