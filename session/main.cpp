#include "appgroup.h"
#include "hotkeymanager.h"
#include "libcrude/acpimonitor.h"
#include "libcrude/displaypower.h"
#include "libcrude/platformintegration.h"
#include "libcrude/powermanagement.h"
#include "libcrude/sessionsettings.h"
#include "respawner.h"
#include "smserver.h"
#include "systraybuiltin.h"

#include <QApplication>
#include <QMetaEnum>
#include <QProcess>
#include <QSettings>
#include <QStandardPaths>
#include <QVersionNumber>

#include <memory>
#include <vector>

static bool mustReconfigure = false;
static bool noSession = false;

int run(int argc, char** argv)
{
  QApplication app(argc, argv);
  qputenv("CRUDE_SESSION_PID", QByteArray::number(app.applicationPid()));

  // Testing mode for builtins that doesn't launch an actual session
  noSession = app.arguments().contains("--no-session");

  {
    QSettings envSettings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-environment");
    QVersionNumber current = QVersionNumber::fromString(CRUDE_VERSION);
    QVersionNumber previous = envSettings.contains("version")
        ? QVersionNumber::fromString(envSettings.value("version").toString())
        : QVersionNumber();
    if (previous.isNull() || QVersionNumber::compare(previous, current) < 0) {
      QString commandLine = QApplication::applicationDirPath() + "/crude-wizard";
      QProcess::execute(commandLine);
    }
  }

  PlatformIntegrator platform(argc, argv);
  Settings settings;
  {
    QString wm = settings.value("environment/windowmanager").toString();
    QString term = settings.value("environment/terminal").toString();
    if (wm.isEmpty() || term.isEmpty() || QStandardPaths::findExecutable(wm.section(' ', 0, 0)).isEmpty() ||
        QStandardPaths::findExecutable(term.section(' ', 0, 0)).isEmpty()) {
      mustReconfigure = true;
      return 1;
    }
  }

  {
    QList<QPair<QString, const char*>> themeKeys = {
      { "icon", "QT_QPA_SYSTEM_ICON_THEME" },
      { "qt", "QT_QPA_PLATFORMTHEME" },
      { "qtStyle", "QT_STYLE_OVERRIDE" },
      { "gtk", "GTK_THEME" },
    };
    for (const auto& key : themeKeys) {
      QString theme = settings.value("environment/theme/" + key.first).toString();
      if (!theme.isEmpty() && qgetenv(key.second).isEmpty()) {
        qputenv(key.second, theme.toUtf8());
      }
    }
  }

  std::unique_ptr<SMServer> smServer;
  std::unique_ptr<HotkeyManager> hotkeyManager;
  std::vector<std::unique_ptr<SystrayBuiltin>> nonSessionBuiltins;
  QList<AppGroup*> groups;
  if (!noSession) {
    smServer.reset(new SMServer());
    hotkeyManager.reset(new HotkeyManager());

    QString lidAction = settings.value("lidaction").toString();
    if (!lidAction.isEmpty()) {
      int action = QMetaEnum::fromType<PowerManagement::Action>().keyToValue(qPrintable(lidAction));
      if (action >= 0) {
        QObject::connect(AcpiMonitor::instance(), &AcpiMonitor::lidChanged, [action](bool open) {
          if (!open) {
            PowerManagement::instance()->perform((PowerManagement::Action)action);
          }
        });
      }
    }

    // TODO: XDG autostart
    for (const QString& group : settings.appGroups()) {
      AppGroup* g = new AppGroup(group, &app);
      QObject::connect(g, SIGNAL(logoutRequested()), &app, SLOT(quit()));
      QObject::connect(&app, SIGNAL(aboutToQuit()), g, SLOT(stopAll()));
      if (group == "environment") {
        QObject::connect(g, &AppGroup::fatalError, [&app] {
          mustReconfigure = true;
          app.quit();
        });
      }
      groups << g;
    }
    settings.savePending();
    c_PI->displayPower()->restoreSchedule();
  } else {
    // Only start the systray icons
    // TODO: Take over hotkey management for testing that too
    qDebug("Running in non-session mode.");
    nonSessionBuiltins.emplace_back(SystrayBuiltin::create("battery"));
    nonSessionBuiltins.emplace_back(SystrayBuiltin::create("volume"));
    nonSessionBuiltins.emplace_back(SystrayBuiltin::create("screensaver"));
    for (const auto& builtin : nonSessionBuiltins) {
      builtin->start();
    }
  }

  int rv = app.exec();
  for (AppGroup* g : groups) {
    g->stopAll();
  }
  return rv;
}

int main(int argc, char** argv)
{
  int rv = 0;
  do {
    if (mustReconfigure) {
      QApplication app(argc, argv);
      QString commandLine = QApplication::applicationDirPath() + "/crude-wizard --bad-config";
      QProcess::execute(commandLine);
      mustReconfigure = false;
    }
    rv = run(argc, argv);
  } while (mustReconfigure);
  return rv;
}
