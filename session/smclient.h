#ifndef CRUDE_SMCLIENT_H
#define CRUDE_SMCLIENT_H

#include <QObject>
class SMConnectionBase;
class SMConnection;

class SMClient : public QObject
{
  Q_OBJECT
  friend class SMServer;
  friend class SMServerPrivate;
  friend class SMConnection;

public:
  ~SMClient();

  QString clientId() const;

  QVariant property(const QString& name) const;
  void setProperty(const QString& name, const QVariant& value);

signals:
  void interactRequest(int type, SMClient* client);
  void interactDone(bool cancel, SMClient* client);
  void needsPhase2(SMClient* client);
  void saved(bool success, SMClient* client);
  void propertyChanged(const QString& key, SMClient* client);
  void disconnected(const QStringList& reasons, SMClient* client);

private:
  SMClient(SMConnection* conn, QObject* parent);
  SMConnection* d;
  SMConnectionBase* dbase();
};

#endif
