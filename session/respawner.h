#ifndef CRUDE_RESPAWNER_H
#define CRUDE_RESPAWNER_H

#include <QObject>
#include <QProcess>
class QTimer;

class Respawner : public QObject
{
  Q_OBJECT

public:
  Respawner(const QString& command, QObject* parent = nullptr);
  Respawner(const QString& command, const QString& stopCommand, QObject* parent = nullptr);

  QString command() const;
  QString stopCommand() const;

  bool respawnOnCleanExit() const;
  void setRespawnOnCleanExit(bool on);

  bool respawnOnCrash() const;
  void setRespawnOnCrash(bool on);

  virtual bool isRunning() const;
  bool isStopping() const;
  bool isCrashed() const;

public slots:
  virtual void start();
  virtual void stop();
  void sendUnixSignal(int sig);

signals:
  void failed();
  void exited();
  void crashed();

private slots:
  void finished(int exitCode, QProcess::ExitStatus status);
  void errored(QProcess::ProcessError code);

protected:
  QProcess* process;
  QTimer* timer;
  QString cmd, stopCmd;
  bool cleanRespawn, crashRespawn, shuttingDown, crashExit;
};

#endif
