#include "builtinicon.h"

#include <QApplication>
#include <QImage>
#include <QPainter>
#include <QStyle>

BuiltinIcon::BuiltinIcon(QObject* parent) : QSystemTrayIcon(parent)
{
  QObject::connect(this, SIGNAL(dataUpdated()), this, SLOT(update()), Qt::QueuedConnection);
  if (isSystemTrayAvailable()) {
    emit visibilityChanged();
  }
}

QSize BuiltinIcon::size() const
{
  return geometry().size();
}

void BuiltinIcon::visibilityChanged()
{
  if (isSystemTrayAvailable()) {
    update();
  }
}

void BuiltinIcon::update()
{
  QSize sz = size();
  if (sz.isEmpty()) {
    return;
  }
  // This is a workaround for a Qt5 bug.
  QImage img(sz, QImage::Format_RGB32);
  img.fill(qApp->style()->standardPalette().button().color());
  QPainter p(&img);
  QIcon icon = render(sz);
  icon.paint(&p, img.rect());
  setIcon(QPixmap::fromImage(img));
}
