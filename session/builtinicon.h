#ifndef CRUDE_BUILTINICON_H
#define CRUDE_BUILTINICON_H

#include <QSystemTrayIcon>

class BuiltinIcon : public QSystemTrayIcon
{
  Q_OBJECT

public:
  BuiltinIcon(QObject* parent = nullptr);

  QSize size() const;

public slots:
  virtual void visibilityChanged();

signals:
  void dataUpdated();

protected:
  virtual QIcon render(const QSize& size) = 0;

private slots:
  void update();
};

#endif
