#include "smserver.h"

#include "libcrude/powermanagement.h"
#include "sm_p.h"
#include "smclient.h"

#include <QCoreApplication>
#include <QSocketNotifier>

#include <cstdlib>

static int trivialAuth(char*)
{
  return true;
}

static void eatError(IceConn) {}

SMServerPrivate::SMServerPrivate(SMServer* p) : p(p), inPhase2(false), shuttingDown(false), shutdownCancelled(false)
{
  // initializers only
}

SMServer::SMServer(QObject* parent) : QObject(parent), d(new SMServerPrivate(this))
{
  char err[1024];
  // auth?
  bool ok = SmsInitialize(
      "crude-session",
      qPrintable(QCoreApplication::applicationVersion()),
      &SMServerPrivate::newClient,
      d,
      trivialAuth,
      sizeof(err),
      err
  );
  if (!ok) {
    qFatal("SMServer: Could not initialize libSM: %s", err);
  }
  IceSetIOErrorHandler(eatError);
  ok = IceListenForConnections(&d->numListeners, &d->listenersArray, sizeof(err), err);
  if (!ok) {
    qFatal("SMServer: Could not listen for connections: %s", err);
  }
  for (int i = 0; i < d->numListeners; i++) {
    IceSetHostBasedAuthProc(d->listenersArray[i], trivialAuth);
    int fd = IceGetListenConnectionNumber(d->listenersArray[i]);
    QSocketNotifier* notifier = new QSocketNotifier(fd, QSocketNotifier::Read, this);
    QObject::connect(notifier, SIGNAL(activated(int)), this, SLOT(newConnection(int)));
    d->listeners[fd] = d->listenersArray[i];
    d->notifiers[fd] = notifier;
  }
  qputenv("SESSION_MANAGER", IceComposeNetworkIdList(d->numListeners, d->listenersArray));

  QObject::connect(&d->forceShutdownTimer, SIGNAL(timeout()), this, SLOT(shutdownTimeout()));
}

SMServer::~SMServer()
{
  IceFreeListenObjs(d->numListeners, d->listenersArray);
  delete d;
}

void SMServer::newConnection(int fd)
{
  IceAcceptStatus status;
  IceConn conn = IceAcceptConnection(d->listeners[fd], &status);
  if (status != IceAcceptSuccess) {
    IceCloseConnection(conn);
    return;
  }

  int newFd = IceConnectionNumber(conn);
  QSocketNotifier* notifier = new QSocketNotifier(newFd, QSocketNotifier::Read, this);
  QObject::connect(notifier, SIGNAL(activated(int)), this, SLOT(socketActivity(int)));
  d->notifiers[newFd] = notifier;
  d->iceConnections[newFd] = conn;
  socketActivity(newFd);
}

void SMServer::socketActivity(int fd)
{
  IceConn conn = d->iceConnections[fd];
  IceConnectStatus status = IceConnectionStatus(conn);
  if (status == IceConnectRejected || status == IceConnectIOError) {
    iceClosed(fd);
    return;
  }
  IceProcessMessagesStatus mstatus = IceProcessMessages(conn, nullptr, nullptr);
  if (mstatus != IceProcessMessagesSuccess) {
    iceClosed(fd);
    return;
  }
  status = IceConnectionStatus(conn);
  if (status == IceConnectRejected || status == IceConnectIOError) {
    iceClosed(fd);
    return;
  }
}

void SMServer::iceClosed(int fd)
{
  IceConn conn = d->iceConnections.take(fd);
  delete d->notifiers.take(fd);
  for (SMClient* client : d->clients) {
    if (SmsGetIceConnection(client->dbase()->connection) == conn) {
      client->deleteLater();
    }
  }
  IceCloseConnection(conn);
}

QList<SMClient*> SMServer::clients() const
{
  QList<SMClient*> result;
  for (SMClient* c : d->clients) {
    result << c;
  }
  return result;
}

void SMServer::addClient(SMClient* client)
{
  QObject::connect(client, SIGNAL(interactRequest(int, SMClient*)), this, SLOT(interactRequest(int, SMClient*)));
  QObject::connect(client, SIGNAL(interactDone(bool, SMClient*)), this, SLOT(interactDone(bool, SMClient*)));
  QObject::connect(client, SIGNAL(needsPhase2(SMClient*)), this, SLOT(needsPhase2(SMClient*)));
  QObject::connect(client, SIGNAL(saved(bool, SMClient*)), this, SLOT(saved(bool, SMClient*)));
  QObject::connect(client, SIGNAL(propertyChanged(QString, SMClient*)), this, SLOT(propertyChanged(QString, SMClient*)));
  QObject::connect(client, SIGNAL(disconnected(QStringList, SMClient*)), this, SLOT(disconnected(QStringList, SMClient*)));
  QObject::connect(client, SIGNAL(destroyed(QObject*)), this, SLOT(clientDestroyed(QObject*)));
  d->clients << client;
  emit newClient(client);
}

void SMServer::broadcastPhase1(int saveType, bool shutdown, int style, bool fast)
{
  if (shutdown && !d->shuttingDown) {
    d->shuttingDown = true;
    d->shutdownCancelled = false;
    d->forceShutdownTimer.start(10000); // TODO: configurable?
  }
  for (SMClient* client : d->clients) {
    d->pendingPhase1 << client;
    SmsSaveYourself(client->dbase()->connection, saveType, shutdown, style, fast);
  }
  checkPhases();
}

void SMServer::interactRequest(int type, SMClient* client)
{
  Q_UNUSED(type);
  d->forceShutdownTimer.stop();

  if (d->interactQueue.isEmpty()) {
    SmsInteract(client->dbase()->connection);
  }
  d->interactQueue << client;
}

void SMServer::interactDone(bool cancel, SMClient* client)
{
  if (cancel) {
    if (!d->shutdownCancelled) {
      d->forceShutdownTimer.stop();
      d->shuttingDown = false;
      d->shutdownCancelled = true;
      for (SMClient* client : d->clients) {
        SmsShutdownCancelled(client->dbase()->connection);
      }
    }
    d->interactQueue.clear();
    return;
  }
  bool wasFirst = !d->interactQueue.isEmpty() && d->interactQueue.first() == client;
  d->interactQueue.removeAll(client);
  if (d->interactQueue.isEmpty()) {
    d->forceShutdownTimer.start(10000);
    checkPhases();
  } else if (wasFirst) {
    SmsInteract(d->interactQueue.first()->dbase()->connection);
  }
}

void SMServer::needsPhase2(SMClient* client)
{
  d->pendingPhase1.removeAll(client);
  d->pendingPhase2 << client;
  checkPhases();
}

void SMServer::saved(bool success, SMClient* client)
{
  Q_UNUSED(success);

  d->pendingPhase1.removeAll(client);
  d->pendingPhase2.removeAll(client);
  interactDone(false, client);
  checkPhases();
}

void SMServer::propertyChanged(const QString& key, SMClient* client)
{
  Q_UNUSED(key);
  Q_UNUSED(client);
}

void SMServer::disconnected(const QStringList& reasons, SMClient* client)
{
  Q_UNUSED(reasons);

  d->clients.remove(client);
  d->pendingPhase1.removeAll(client);
  d->pendingPhase2.removeAll(client);
  interactDone(false, client);
  client->deleteLater();
  checkPhases();
}

void SMServer::clientDestroyed(QObject* client)
{
  disconnected(QStringList(), (SMClient*)client);
}

void SMServer::checkPhases()
{
  if (!d->shutdownForced) {
    if (!d->interactQueue.isEmpty() || !d->pendingPhase1.isEmpty()) {
      return;
    }
    if (!d->pendingPhase2.isEmpty()) {
      if (!d->inPhase2) {
        // Restart the shutdown timer when entering phase 2
        d->forceShutdownTimer.start(10000);
        d->inPhase2 = true;
        for (SMClient* client : d->pendingPhase2) {
          SmsSaveYourselfPhase2(client->dbase()->connection);
        }
      }
      return;
    }
  }
  d->inPhase2 = false;
  if (d->shuttingDown && !d->shutdownCancelled) {
    // That's all, folks!
    if (d->shutdownMethod == "reboot") {
      PowerManagement::instance()->perform(PowerPlugin::Reboot);
    } else if (d->shutdownMethod == "shutdown") {
      PowerManagement::instance()->perform(PowerPlugin::Shutdown);
    }
    d->shutdownMethod = "done";
    QCoreApplication::instance()->quit();
  }
}

void SMServer::shutdownTimeout()
{
  d->shutdownForced = true;
  checkPhases();
}
