#ifndef CRUDE_SM_P_H
#define CRUDE_SM_P_H

// While this is unused, it must be included before SMlib
#include "libcrude/qpointerset.h"

#include <QJsonValue>
#include <QList>
#include <QMap>
#include <QTimer>
#include <QVariant>

#include <X11/ICE/ICElib.h>
#include <X11/ICE/ICEmsg.h>
#include <X11/SM/SMlib.h>
class SMServer;
class QSocketNotifier;

struct SMConnectionBase
{
  SMConnectionBase(SmsConn conn) : connection(conn) {}

  SmsConn connection;
};

struct SMServerPrivate
{
  SMServerPrivate(SMServer* p);
  SMServer* p;
  int numListeners;
  IceListenObj* listenersArray;
  QMap<int, IceListenObj> listeners;
  QMap<int, IceConn> iceConnections;
  // shared by both listeners and ICE connections
  QMap<int, QSocketNotifier*> notifiers;
  QPointerSet<SMClient> clients;
  QList<SMClient*> pendingPhase1, pendingPhase2, interactQueue;
  QString shutdownMethod;
  bool inPhase2 : 1;
  bool shuttingDown : 1;
  bool shutdownCancelled : 1;
  bool shutdownForced : 1;
  QTimer forceShutdownTimer;

  // Implementation is in smclient.cpp since this is basically a constructor
  static Status newClient(SmsConn conn, SmPointer _d, unsigned long* mask, SmsCallbacks* cb, char** err);
};

#endif
