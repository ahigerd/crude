#ifndef CRUDE_VOLUMEICON_H
#define CRUDE_VOLUMEICON_H

#include "builtinicon.h"
#include "libcrude/pluginshell.h"
#include "plugins/volumeplugin.h"
#include "libcrude/throttle.h"

#include <QProcess>
#include <QVariant>
class QFrame;
class QSlider;
class QCheckBox;
class QToolButton;

class VolumeIcon : public BuiltinIcon
{
  Q_OBJECT

public:
  VolumeIcon(QObject* parent = nullptr);
  ~VolumeIcon();

  static void modifyVolume(float delta);
  static void toggleMute();

private:
  void readMixer();

private slots:
  void iconActivated(QSystemTrayIcon::ActivationReason);
  void setVolume(int level);
  void setMute(bool mute);
  void volumeChanged(float level);
  void muteChanged(bool mute);
  void volumeError();
  void launchMixer();

protected:
  virtual QIcon render(const QSize& size);

private:
  QString mixerCommand() const;

  QFrame* popup;
  QSlider* slider;
  QCheckBox* muteCheck;
  QToolButton* mixerButton;
};

#endif
