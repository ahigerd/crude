#ifndef CRUDE_BATTERYICON_H
#define CRUDE_BATTERYICON_H

#include "builtinicon.h"

class BatteryIcon : public BuiltinIcon
{
  Q_OBJECT

public:
  BatteryIcon(QObject* parent = nullptr);

public slots:
  void plugChanged(bool on);
  void batteryLevelChanged(int level);

protected:
  virtual QIcon render(const QSize& size);

private:
  bool lastPlug;
  int lastLevel;
};

#endif
