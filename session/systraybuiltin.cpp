#include "systraybuiltin.h"

#include "batteryicon.h"
#include "libcrude/platformintegration.h"
#include "saverbuiltin.h"
#include "volumeicon.h"

#include <QSystemTrayIcon>

SystrayBuiltin* SystrayBuiltin::create(const QString& type, QObject* parent)
{
  if (type == "battery") {
    return new SystrayBuiltin(new BatteryIcon, parent);
  } else if (type == "volume") {
    return new SystrayBuiltin(new VolumeIcon, parent);
  } else if (type == "screensaver") {
    return new SaverBuiltin(parent);
  } else {
    return nullptr;
  }
}

SystrayBuiltin::SystrayBuiltin(QSystemTrayIcon* icon, QObject* parent)
: Respawner("builtin", parent), icon(icon), updateVisibility(Throttle::connectedTo(this, SIGNAL(visibilityUpdated())))
{
  if (icon) {
    icon->setParent(this);
    QObject::connect(c_PI, SIGNAL(systemTrayAvailabilityChanged(bool)), icon, SLOT(hide()), Qt::DirectConnection);
    QObject::connect(c_PI, SIGNAL(systemTrayAvailabilityChanged(bool)), icon, SLOT(setVisible(bool)), Qt::QueuedConnection);
    // Dynamically detect additional slots
    if (icon->metaObject()->indexOfSlot(QMetaObject::normalizedSignature("visibilityChanged()").constData()) >= 0) {
      updateVisibility.timeout(50);
      QObject::connect(c_PI, SIGNAL(systemTrayAvailabilityChanged(bool)), &updateVisibility, SLOT(trigger()));
      QObject::connect(this, SIGNAL(visibilityUpdated()), icon, SLOT(visibilityChanged()));
    }
  }
}

bool SystrayBuiltin::isRunning() const
{
  return icon && icon->isVisible();
}

void SystrayBuiltin::start()
{
  if (icon) {
    icon->show();
    updateVisibility.trigger();
  }
}

void SystrayBuiltin::stop()
{
  if (icon) {
    icon->hide();
    updateVisibility.trigger();
  }
}
