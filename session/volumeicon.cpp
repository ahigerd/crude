#include "volumeicon.h"

#include "libcrude/environmentprefs.h"
#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"
#include "libcrude/throttle.h"

#include <QApplication>
#include <QCheckBox>
#include <QDateTime>
#include <QFileInfo>
#include <QFrame>
#include <QRegExp>
#include <QScreen>
#include <QSlider>
#include <QStandardPaths>
#include <QTimer>
#include <QToolButton>
#include <QVBoxLayout>
#include <QtDebug>

class VolumePluginShell;
static VolumePluginShell* pluginShell = nullptr;

class VolumePluginShell : public PluginShell<VolumePlugin>
{
public:
  using PluginShell<VolumePlugin>::plugin;

  static VolumePluginShell* instance()
  {
    if (!pluginShell) {
      pluginShell = new VolumePluginShell();
      pluginShell->selectDefaultPlugin();
    }
    return pluginShell;
  }

  ~VolumePluginShell() {}

  QString pluginPrefix() const
  {
    return "volume";
  }

  QStringList pluginPriority() const
  {
    return QStringList() << "wireplumber" << "alsa";
  }

  QString noPluginName() const
  {
    return "No Volume Control";
  }

private:
  VolumePluginShell()
  : PluginShell<VolumePlugin>()
  {
    // initializers only
  }
};

static VolumePlugin* plugin()
{
  VolumePlugin* p = VolumePluginShell::instance()->plugin();
  if (p && p->available()) {
    return p;
  }
  return nullptr;
}

void VolumeIcon::modifyVolume(float delta)
{
  auto p = plugin();
  if (p) {
    p->increaseVolume(delta);
  }
}

void VolumeIcon::toggleMute()
{
  auto p = plugin();
  if (p) {
    p->setMuted(!p->isMuted());
  }
}

VolumeIcon::VolumeIcon(QObject* parent)
: BuiltinIcon(parent)
{
  popup = new QFrame(nullptr, Qt::Popup);
  popup->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
  popup->setWindowModality(Qt::ApplicationModal);
  QVBoxLayout* layout = new QVBoxLayout(popup);
  layout->setContentsMargins(0, 0, 0, 0);

  mixerButton = new QToolButton(popup);
  mixerButton->setText("Mi&xer");
  layout->addWidget(mixerButton, 0, Qt::AlignJustify);

  slider = new QSlider(Qt::Vertical, popup);
  slider->setTickPosition(QSlider::TicksRight);
  slider->setRange(0, 100);
  slider->setTickInterval(10);
  slider->setPageStep(10);
  layout->addWidget(slider, 1, Qt::AlignCenter);

  muteCheck = new QCheckBox("&Mute", popup);
  QHBoxLayout* hlayout = new QHBoxLayout;
  layout->addLayout(hlayout, 0);
  hlayout->setContentsMargins(4, 4, 4, 4);
  hlayout->addWidget(muteCheck, 0, Qt::AlignJustify);

  QObject::connect(mixerButton, SIGNAL(clicked()), this, SLOT(launchMixer()));
  QObject::connect(slider, SIGNAL(valueChanged(int)), this, SLOT(setVolume(int)));
  QObject::connect(muteCheck, SIGNAL(toggled(bool)), this, SLOT(setMute(bool)));
  QObject::connect(
      this,
      SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
      this,
      SLOT(iconActivated(QSystemTrayIcon::ActivationReason))
  );

  if (plugin()) {
    QObject* p = VolumePluginShell::instance()->pluginQObject();
    QObject::connect(p, SIGNAL(volumeError()), this, SLOT(volumeError()));
    QObject::connect(p, SIGNAL(volumeChanged(float)), this, SLOT(volumeChanged(float)));
    QObject::connect(p, SIGNAL(muteChanged(bool)), this, SLOT(muteChanged(bool)));
    if (mixerCommand().isEmpty()) {
      layout->setContentsMargins(0, 4, 0, 0);
      mixerButton->hide();
    }
  }
}

VolumeIcon::~VolumeIcon()
{
  delete popup;
}

QIcon VolumeIcon::render(const QSize& size)
{
  float lastLevel = -1;
  bool lastMute = false;
  auto p = plugin();
  if (p) {
    lastMute = p->isMuted();
    lastLevel = p->volumeLevel() * 100;
  }
  if (lastLevel < 0) {
    if (!p) {
      setToolTip("No mixer available");
    } else {
      setToolTip("Error reading mixer state");
    }
    slider->setEnabled(false);
    muteCheck->setEnabled(false);
    mixerButton->setEnabled(false);
    return c_PI->icon("error-correct"); // TODO: find an appropriate icon
  }
  slider->setEnabled(true);
  muteCheck->setEnabled(true);
  mixerButton->setEnabled(true);
  setToolTip(QStringLiteral("%1: %2%").arg(p->pluginName()).arg(lastLevel));
  if (lastMute) {
    return c_PI->icon("audio-volume-muted");
  } else if (lastLevel < 33) {
    return c_PI->icon("audio-volume-low");
  } else if (lastLevel < 67) {
    return c_PI->icon("audio-volume-medium");
  } else if (lastLevel < 100) {
    return c_PI->icon("audio-volume-high");
  } else {
    return c_PI->icon("audio-volume-overamplified");
  }
}

void VolumeIcon::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
  if (reason == QSystemTrayIcon::Trigger) {
    QScreen* screen = qApp->screenAt(geometry().topLeft());
    QRect screenRect = screen->availableGeometry();
    popup->adjustSize();
    QRect geom(geometry().bottomLeft(), popup->size());
    if (geom.bottom() > screenRect.bottom()) {
      geom.moveBottom(screenRect.bottom());
    }
    if (geom.right() > screenRect.right()) {
      geom.moveRight(screenRect.right());
    }
    popup->move(geom.topLeft());
    popup->show();
  } else if (reason == QSystemTrayIcon::Context) {
    // TODO
  }
}

void VolumeIcon::setVolume(int level)
{
  auto p = plugin();
  if (p) {
    p->setVolumeLevel(level / 100.f);
  }
}

void VolumeIcon::setMute(bool mute)
{
  auto p = plugin();
  if (p) {
    p->setMuted(mute);
  }
}

QString VolumeIcon::mixerCommand() const
{
  QString mixerCommand = Settings::instance()->value("volume/mixergui").toString();
  if (mixerCommand.isEmpty()) {
    auto p = plugin();
    if (p) {
      mixerCommand = p->mixerCommand();
    }
    Settings::instance()->setValue("volume/mixergui", mixerCommand);
  }
  return mixerCommand;
}

void VolumeIcon::launchMixer()
{
  popup->hide();
  QString cmd = mixerCommand();
  if (cmd.isEmpty()) {
    return;
  }
  c_PI->runCommand(cmd);
}

void VolumeIcon::volumeChanged(float level)
{
  slider->blockSignals(true);
  slider->setValue(level * slider->maximum());
  slider->blockSignals(false);
  emit dataUpdated();
}

void VolumeIcon::muteChanged(bool mute)
{
  muteCheck->blockSignals(true);
  muteCheck->setChecked(mute);
  muteCheck->blockSignals(false);
  emit dataUpdated();
}

void VolumeIcon::volumeError()
{
  emit dataUpdated();
}
