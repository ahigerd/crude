#ifndef CRUDE_SYSTRAYBUILTIN_H
#define CRUDE_SYSTRAYBUILTIN_H

#include "libcrude/throttle.h"
#include "respawner.h"

#include <QPointer>
class QSystemTrayIcon;

class SystrayBuiltin : public Respawner
{
  Q_OBJECT

public:
  static SystrayBuiltin* create(const QString& type, QObject* parent = nullptr);
  SystrayBuiltin(QSystemTrayIcon* icon, QObject* parent = nullptr);

  virtual bool isRunning() const;
  virtual void start();
  virtual void stop();

signals:
  void visibilityUpdated();

private:
  QPointer<QSystemTrayIcon> icon;
  Throttle updateVisibility;
};

#endif
