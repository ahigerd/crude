#include "platformintegration.h"

#include "threadhelper.h"
#include "xcb/xcbintegration.h"

#include <QAction>
#include <QApplication>
#include <QDir>
#include <QFile>
#include <QIcon>
#include <QImageReader>
#include <QMutex>
#include <QPixmap>
#include <QProcess>
#include <QSettings>
#include <QSocketNotifier>
#include <QStandardPaths>
#include <QStyle>
#include <QtDebug>

#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

static PlatformIntegration* PI_instance = nullptr;
static int unixSignalFd[2] = { 0, 0 };
static QMutex iconLock;

static QSettings savedIcons(QSettings::UserScope, "Alkahest", "icon-cache");

void writeSignalNumber(int sig)
{
  unsigned char sigChar = sig;
  ::write(unixSignalFd[0], &sigChar, 1);
}

PlatformIntegration* PlatformIntegration::instance()
{
  return PI_instance;
}

PlatformIntegration::PlatformIntegration() : QObject(nullptr)
{
  if (PI_instance) {
    qFatal("PlatformIntegration: duplicate initialization");
  }
  PI_instance = this;
  QObject::connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(aboutToQuit()));

  debugEnabled = !qgetenv("CRUDE_DEBUG").isEmpty();

  if (::socketpair(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC, 0, unixSignalFd)) {
    qWarning("PlatformIntegration: failed to create signal socket");
    notifier = nullptr;
  } else {
    notifier = new QSocketNotifier(unixSignalFd[1], QSocketNotifier::Read, this);
    QObject::connect(notifier, SIGNAL(activated(int)), this, SLOT(unixSignalTriggered()));

    struct sigaction action;
    action.sa_handler = &writeSignalNumber;
    sigemptyset(&action.sa_mask);
    action.sa_flags = SA_RESTART;
    if (::sigaction(SIGHUP, &action, nullptr)) {
      qWarning("PlatformIntegration: failed to assign SIGHUP handler");
    }
    if (::sigaction(SIGUSR1, &action, nullptr)) {
      qWarning("PlatformIntegration: failed to assign SIGUSR1 handler");
    }
    if (::sigaction(SIGUSR2, &action, nullptr)) {
      qWarning("PlatformIntegration: failed to assign SIGUSR2 handler");
    }
  }
}

PlatformIntegration::~PlatformIntegration()
{
  if (PI_instance == this) {
    PI_instance = nullptr;
  }
}

void PlatformIntegration::aboutToQuit()
{
  childProcesses.deleteAll();
}

std::unique_ptr<PlatformIntegration> PlatformIntegration::create(Platform platform, int argc, char** argv)
{
  if (platform == X11 || platform == Auto) {
    return std::unique_ptr<PlatformIntegration>(new XcbIntegration(argc, argv));
  }
  qFatal("PlatformIntegration::create: unsupported platform type");
}

std::unique_ptr<PlatformIntegration> PlatformIntegration::create(int argc, char** argv)
{
  return create(Auto, argc, argv);
}

PlatformIntegrator::PlatformIntegrator(PlatformIntegration::Platform platform, int argc, char** argv)
: instance(std::unique_ptr<PlatformIntegration>(PlatformIntegration::create(platform, argc, argv)))
{
  // initializers only
}

PlatformIntegrator::PlatformIntegrator(int argc, char** argv)
: instance(std::unique_ptr<PlatformIntegration>(PlatformIntegration::create(argc, argv)))
{
  // initializers only
}

PlatformIntegration* PlatformIntegrator::operator->() const
{
  return instance.get();
}

PlatformIntegration* PlatformIntegrator::operator->()
{
  return instance.get();
}

bool PlatformIntegration::runCommand(const QString& cmd, const QStringList& args, const QString& cwd, bool forwardOutput)
{
  // This exists because QProcess::startDetached disowns the spawned process, which means that stuff
  // launched by the panel won't terminate on logout.
  QProcess* process = new QProcess(this);
  if (forwardOutput) {
    process->setProcessChannelMode(QProcess::ForwardedChannels);
  }
  QObject::connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), process, SLOT(deleteLater()));
  if (!cwd.isEmpty()) {
    process->setWorkingDirectory(cwd);
  }
  if (args.isEmpty()) {
    process->start(cmd, QIODevice::ReadOnly);
  } else {
    process->start(cmd, args, QIODevice::ReadOnly);
  }
  bool ok = process->waitForStarted(1000);
  if (!ok) {
    process->deleteLater();
    return false;
  }
  childProcesses << process;
  process->closeReadChannel(QProcess::StandardOutput);
  process->closeReadChannel(QProcess::StandardError);
  return true;
}

void PlatformIntegration::unixSignalTriggered()
{
  notifier->setEnabled(false);

  unsigned char sigNumber;
  int bytes;
  do {
    bytes = ::read(unixSignalFd[1], &sigNumber, 1);
    if (bytes > 0) {
      emit unixSignal(sigNumber);
    }
  } while (bytes > 0);

  notifier->setEnabled(true);
}

static QStringList findFilesRecursive(const QDir& path, const QStringList& globs, const QStringList& scanned)
{
  QStringList result;
  for (const QFileInfo& file : path.entryInfoList(
           globs, QDir::Files | QDir::AllDirs | QDir::NoDotAndDotDot | QDir::Readable, QDir::Name | QDir::IgnoreCase
       )) {
    QString absPath = file.absoluteFilePath();
    if (file.isDir()) {
      if (scanned.contains(absPath)) {
        continue;
      }
      result << findFilesRecursive(absPath, globs, scanned);
    } else {
      result << absPath;
    }
  }
  return result;
}

static QIcon svgIcon(const QString& svg, const QString& svg32 = QString())
{
  QIcon result;
  if (!svg32.isEmpty()) {
    QFile f1(svg);
    if (f1.open(QIODevice::ReadOnly | QIODevice::Text)) {
      QByteArray content = f1.readAll();
      if (!content.contains("clipPath")) {
        result.addFile(svg, QSize(32, 32));
      }
    }
  }
  QFile f2(svg);
  if (f2.open(QIODevice::ReadOnly | QIODevice::Text)) {
    QByteArray content = f2.readAll();
    if (!content.contains("clipPath")) {
      result.addFile(svg);
    }
  }
  return result;
}

static QIcon scanThemeForIcon(const QDir& subdir, const QString& name, QString* foundPath, const QStringList& scanned)
{
  bool svgEnabled = QImageReader::supportedImageFormats().contains("svg");
  QIcon result;
  if (svgEnabled) {
    QStringList svgs32, svgs;
    if (subdir.exists("scalable-up-to-32")) {
      svgs32 = findFilesRecursive(subdir.absoluteFilePath("scalable-up-to-32"), { name + ".svg" }, scanned);
    }
    if (subdir.exists("scalable")) {
      svgs = findFilesRecursive(subdir.absoluteFilePath("scalable"), { name + ".svg" }, scanned);
    }
    if (svgs.isEmpty()) {
      svgs = findFilesRecursive(subdir, { name + ".svg" }, scanned);
    }
    if (!svgs32.isEmpty()) {
      if (!svgs.isEmpty()) {
        result = svgIcon(svgs[0], svgs32[0]);
      } else {
        result = svgIcon(svgs32[0]);
      }
    } else if (!svgs.isEmpty()) {
      result = svgIcon(svgs[0]);
    }
    if (!result.isNull()) {
      if (foundPath) {
        *foundPath = "svg:" + (svgs32 + svgs).join(";");
      }
      return result;
    }
  }
  QStringList pngs = findFilesRecursive(subdir, { name + ".png", name + ".symbolic.png" }, scanned);
  if (!pngs.isEmpty()) {
    QStringList symbolic, nonSymbolic;
    for (const QString& png : pngs) {
      if (png.endsWith(".symbolic.png")) {
        symbolic << png;
      } else {
        nonSymbolic << png;
      }
    }
    QStringList good;
    for (const QString& png : nonSymbolic) {
      QPixmap px(png);
      if (!px.isNull()) {
        result.addPixmap(px);
        good << png;
      }
    }
    if (good.isEmpty()) {
      for (const QString& png : symbolic) {
        QPixmap px(png);
        if (!px.isNull()) {
          result.addPixmap(px);
          good << png;
        }
      }
    }
    if (!good.isEmpty()) {
      if (foundPath) {
        *foundPath = good.join(';');
      }
      return result;
    }
  }
  return QIcon();
}

static QIcon scanForIcon(const QString& name, QString* foundPath)
{
  static QStringList themeHierarchy;
  if (themeHierarchy.isEmpty()) {
    QString themeList = QString::fromUtf8(qgetenv("CRUDE_ICON_THEME"));
    if (!themeList.isEmpty()) {
      for (const QString& theme : themeList.split(':')) {
        if (theme == QIcon::themeName()) {
          // This will be handled in advance
          continue;
        }
        for (const QString& path : QIcon::themeSearchPaths()) {
          QDir dir(path);
          if (dir.exists(theme)) {
            themeHierarchy << dir.absoluteFilePath(theme);
          }
        }
      }
    }
    for (const QString& path : QIcon::themeSearchPaths()) {
      QDir dir(path);
      for (const QString& theme : dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        if (theme == QIcon::themeName()) {
          // This will be handled in advance
          continue;
        }
        QString absPath = dir.absoluteFilePath(theme);
        if (!themeHierarchy.contains(absPath)) {
          themeHierarchy << absPath;
        }
      }
    }
    for (const QString& path : QIcon::themeSearchPaths() + QStringList{ "/usr/share/pixmaps" }) {
      if (QFileInfo(path).isDir() && !themeHierarchy.contains(path)) {
        themeHierarchy << path;
      }
    }
  }
  QIcon result;
  // TODO: theme preference hierarchy?
  QStringList scanned;
  for (const QString& path : themeHierarchy) {
    result = scanThemeForIcon(path, name, foundPath, scanned);
    if (!result.isNull()) {
      return result;
    }
    scanned << path;
  }
  return result;
}

QIcon PlatformIntegration::cacheIcon(const QString& name, const QString& path, const QIcon& icon) const
{
  iconCache[name] = icon;
  savedIcons.setValue(name, path);
  return icon;
}

QIcon PlatformIntegration::icon(const QString& name, int fallback) const
{
  if (name.isEmpty()) {
    if (fallback >= 0) {
      return qApp->style()->standardIcon((QStyle::StandardPixmap)fallback);
    } else {
      return QIcon();
    }
  }
  QMutexLocker locker(&iconLock);
  QIcon result = iconCache.value(name);
  if (!result.isNull()) {
    return result;
  }
  if (savedIcons.group() != QIcon::themeName()) {
    if (!savedIcons.group().isEmpty()) {
      savedIcons.endGroup();
    }
    savedIcons.beginGroup(QIcon::themeName());
  }
  QString cachedIcon = savedIcons.value(name).toString();
  if (!cachedIcon.isEmpty()) {
    QString scheme = cachedIcon.section(':', 0, 0);
    if (scheme == "svg") {
      QStringList svgs = cachedIcon.section(':', 1).split(";");
      if (svgs.length() == 2) {
        result = svgIcon(svgs.last(), svgs.first());
      } else if (svgs.length() > 0) {
        result = svgIcon(svgs.first());
      }
    } else if (scheme == "theme") {
      if (cachedIcon.section(':', 1) == "symbolic") {
        result = QIcon::fromTheme(name + "-symbolic");
      } else {
        result = QIcon::fromTheme(name);
      }
    } else if (scheme == "fallback") {
      QStyle::StandardPixmap sp = QStyle::StandardPixmap(cachedIcon.section(':', 1).toInt());
      result = qApp->style()->standardIcon(sp);
    } else if (scheme == "empty") {
      return QIcon();
    } else {
      for (const QString& file : cachedIcon.split(';')) {
        result.addPixmap(file);
      }
    }
    if (result.isNull()) {
      // The icon used to exist, but now it doesn't
      savedIcons.remove(name);
    } else {
      return iconCache[name] = result;
    }
  }
  result = QIcon::fromTheme(name);
  if (!result.isNull()) {
    return cacheIcon(name, "theme:", result);
  }
  result = QIcon::fromTheme(name + "-symbolic");
  if (!result.isNull()) {
    return cacheIcon(name, "theme:symbolic", result);
  }
  QString foundPath;
  result = scanForIcon(name, &foundPath);
  if (!result.isNull()) {
    return cacheIcon(name, foundPath, result);
  }
  result = scanForIcon(name + "-symbolic", &foundPath);
  if (!result.isNull()) {
    return cacheIcon(name, foundPath, result);
  }
  if (fallback >= 0) {
    result = qApp->style()->standardIcon((QStyle::StandardPixmap)fallback);
    return cacheIcon(name, QStringLiteral("fallback:%1").arg(fallback), result);
  }
  return cacheIcon(name, "empty:", QIcon());
}

QIcon PlatformIntegration::icon(const QStringList& names, int fallback) const
{
  if (names.isEmpty()) {
    // Shouldn't happen, but just to be safe
    return QIcon();
  }
  {
    QMutexLocker locker(&iconLock);
    for (const QString& name : names) {
      if (name.isEmpty()) {
        continue;
      }
      QIcon result = iconCache.value(name);
      if (!result.isNull()) {
        return result;
      }
    }
  }
  QIcon result;
  for (const QString& name : names) {
    if (name.isEmpty()) {
      continue;
    }
    result = icon(name);
    if (!result.isNull()) {
      return result;
    }
  }
  if (fallback >= 0) {
    return qApp->style()->standardIcon((QStyle::StandardPixmap)fallback);
  }
  return QIcon();
}

void PlatformIntegration::lazyIcon(QAction* action, const QStringList& names, int fallback) const
{
  if (names.isEmpty()) {
    return;
  }
  ThreadHelper<QIcon>::run(
      [this, names, fallback]() -> QIcon { return icon(names, fallback); },
      [action](const QIcon& icon) { action->setIcon(icon); }
  );
}

void PlatformIntegration::lazyIcon(QAction* action, const QString& names, int fallback) const
{
  lazyIcon(action, QStringList{ names }, fallback);
}

bool PlatformIntegration::isDebugEnabled() const
{
  return debugEnabled;
}

void PlatformIntegration::setDebugEnabled(bool on)
{
  debugEnabled = on;
}
