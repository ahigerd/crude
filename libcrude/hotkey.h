#ifndef CRUDE_HOTKEY_H
#define CRUDE_HOTKEY_H

#include <QKeySequence>
#include <QObject>
#include <QVariant>
class HotkeyPrivate;

class Hotkey : public QObject
{
  Q_OBJECT

public:
  Hotkey(const QKeySequence& shortcut, QObject* parent = nullptr);
  virtual ~Hotkey();

  QKeySequence keySequence() const;
  bool isValid() const;
  bool hasModifier() const;

  QVariant data() const;
  void setData(const QVariant& data);

signals:
  void activated(const QVariant& data);
  void released(const QVariant& data);

protected:
  friend class HotkeyPrivate;
  friend class XcbHotkeyPrivate;
  HotkeyPrivate* d;
};

#endif
