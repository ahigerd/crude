#include "sessionsettings.h"

#include "confignotifier.h"
#include "platformintegration.h"

#include <QMap>
#include <QSet>
#include <QStandardPaths>
#include <QStringList>
#include <QVariant>
#include <QtDebug>

#include <signal.h>

static Settings* CS_instance = nullptr;

Settings* Settings::instance()
{
  return CS_instance;
}

static QMap<QString, QVariant> defaults = {
  { "screensaver", "Black Screen" },
  { "environment/screensaver", "builtin" },
  { "environment/desktopmanager", "builtin" },
  { "environment/desktopmanager_respawn", true },
  { "environment/windowmanager_respawn", true },
  { "environment/zoom", "disabled" },
  { "environment/zoom_respawn", true },
  { "systemtray/battery", "builtin" },
  { "systemtray/volume",
    QStringList() << "pasystray"
                  << "builtin" },
  { "systemtray/display", "" },
  { "systemtray/wifi",
    QStringList() << "nm-applet"
                  << "wpa-gui -t"
                  << "wpa_gui -t" },
  { "systemtray/wifi_kill", true },
  { "systemtray/wifi_respawn", true },
  { "panels/0", "default" },
  { "panels/0_respawn", true },
  { "panels/0_logout", true },
  { "lidaction", "Suspend" },
  { "hotkeys/launchmenu", "Ctrl+Esc" },
  { "hotkeys/backlight+", "Monitor Brightness Up" },
  { "hotkeys/backlight-", "Monitor Brightness Down" },
  { "hotkeys/volume+", "Volume Up" },
  { "hotkeys/volume-", "Volume Down" },
  { "hotkeys/mute", "Volume Mute" },
  { "hotkeys/screenshot", "Alt+Shift+3" },
  { "hotkeys/screenshotrect", "Alt+Shift+4" },
  // TODO:
  // { "hotkeys/keybacklight+", "Keyboard Brightness Up" },
  // { "hotkeys/keybacklight-", "Keyboard Brightness Down" },
};

Settings::Settings()
: globalSettings(QSettings::IniFormat, QSettings::SystemScope, "Alkahest", "crude-session"),
  localSettings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-session"),
  envSettings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-environment"),
  reloadThrottle(Throttle::connectedTo(this, SLOT(deferredReload())))
{
  CS_instance = this;
  globalSettings.setFallbacksEnabled(false);
  localSettings.setFallbacksEnabled(false);
  envSettings.setFallbacksEnabled(false);

  configNotifier = new ConfigNotifier(this);
  QObject::connect(configNotifier, SIGNAL(reloadConfig()), this, SLOT(deferredReload()));
  QObject::connect(c_PI, SIGNAL(unixSignal(int)), this, SLOT(unixSignal(int)));
}

QStringList Settings::appGroups() const
{
  QSet<QString> result{ "environment", "systemtray", "panels" };
  return result.toList();
}

QStringList Settings::groups()
{
  QSet<QString> result;
  for (const QString& group : globalSettings.childGroups()) {
    result << group;
  }
  for (const QString& group : localSettings.childGroups()) {
    result << group;
  }
  return result.toList();
}

QStringList Settings::keys(const QString& group)
{
  QStringList keys;
  QSet<QString> removed;
  if (group == "environment") {
    keys << "windowmanager"
         << "desktopmanager"
         << "screensaver"
         << "terminal"
         << "version";
    for (const QString& key : envSettings.childKeys()) {
      if (!keys.contains(key)) {
        keys << key;
      }
    }
  }
  if (!appGroups().contains(group)) {
    if (!group.isEmpty()) {
      globalSettings.beginGroup(group);
      localSettings.beginGroup(group);
    }
    for (const QString& key : globalSettings.childKeys()) {
      if (!keys.contains(key)) {
        keys << key;
      }
      if (globalSettings.value(key).isNull()) {
        removed << key;
      }
    }
    for (const QString& key : localSettings.childKeys()) {
      if (!keys.contains(key)) {
        keys << key;
      }
      if (localSettings.value(key).isNull()) {
        removed << key;
      } else {
        removed.remove(key);
      }
    }
    for (const QString& path : defaults.keys()) {
      if (group.isEmpty()) {
        if (!path.contains('/') && !keys.contains(path)) {
          keys << path;
        }
      } else if (path.startsWith(group + '/')) {
        QString key = path.section('/', 1);
        if (!keys.contains(key)) {
          keys << key;
        }
      }
    }
    if (!group.isEmpty()) {
      globalSettings.endGroup();
      localSettings.endGroup();
    }
  } else {
    if (localSettings.childGroups().contains(group)) {
      localSettings.beginGroup(group);
      keys = localSettings.childKeys();
      localSettings.endGroup();
    } else if (globalSettings.childGroups().contains(group)) {
      globalSettings.beginGroup(group);
      keys = globalSettings.childKeys();
      globalSettings.endGroup();
    } else {
      for (const QString& path : defaults.keys()) {
        if (path.startsWith(group + "/")) {
          QString key = path.section('/', 1);
          if (!keys.contains(key)) {
            keys << key;
          }
        }
      }
    }
  }
  for (int i = keys.length() - 1; i >= 0; --i) {
    if (keys[i].contains('_') || removed.contains(keys[i])) {
      keys.removeAt(i);
    }
  }
  return keys;
}

QVariant Settings::value(const QString& key)
{
  QString group = key.contains('/') ? key.section('/', 0, 0) : QString();
  if (group == "environment") {
    QString envKey = key.section('/', 1);
    if (envSettings.contains(envKey)) {
      return envSettings.value(envKey);
    } else if (defaults.contains(key)) {
      return defaults.value(key);
    } else {
      return QVariant();
    }
  }

  if (appGroups().contains(group)) {
    if (localSettings.childGroups().contains(group)) {
      return localSettings.value(key);
    } else if (globalSettings.childGroups().contains(group)) {
      return globalSettings.value(key);
    } else if (defaults.contains(key)) {
      return defaults.value(key);
    }
    return QVariant();
  }

  if (localSettings.contains(key)) {
    return localSettings.value(key);
  }
  if (globalSettings.contains(key)) {
    return globalSettings.value(key);
  }
  if (defaults.contains(key)) {
    return defaults.value(key);
  }
  return QVariant();
}

void Settings::setValue(const QString& key, const QVariant& value)
{
  QString group = key.contains('/') ? key.section('/', 0, 0) : QString();
  if (group == "environment") {
    if (value.isValid()) {
      envSettings.setValue(key.section('/', 1), value);
    } else if (defaults.contains(key)) {
      envSettings.setValue(key.section('/', 1), "");
    } else {
      envSettings.remove(key.section('/', 1));
    }
    return;
  }

  if (appGroups().contains(group) && !localSettings.childGroups().contains(group)) {
    for (const QString& globalKey : keys(group)) {
      localSettings.setValue(globalKey, globalSettings.value(group + "/" + globalKey));
    }
  }
  if (value.isValid()) {
    localSettings.setValue(key, value);
  } else if (globalSettings.contains(key) || defaults.contains(key)) {
    localSettings.setValue(key, "");
  } else {
    localSettings.remove(key);
  }
}

void Settings::remove(const QString& key)
{
  setValue(key, QVariant());
}

SessionService Settings::getService(const QString& group, const QString& key)
{
  SessionService result;
  result.settingsGroup = group;
  result.settingsKey = key;
  result.respawnOnCrash = false;
  result.killWithSystray = false;
  result.logoutOnExit = false;
  QString path = group + "/" + key;
  if (localSettings.childGroups().contains(group)) {
    result.commandLine = localSettings.value(path).toString();
    result.stopCommand = localSettings.value(path + "_stop").toString();
    result.respawnOnCrash = localSettings.value(path + "_respawn").toBool();
    if (group == "systemtray") {
      result.killWithSystray = localSettings.value(path + "_kill").toBool();
    }
    result.logoutOnExit = localSettings.value(path + "_logout").toBool();
  } else if (globalSettings.childGroups().contains(group)) {
    result.commandLine = globalSettings.value(path).toString();
    result.stopCommand = globalSettings.value(path + "_stop").toString();
    result.respawnOnCrash = globalSettings.value(path + "_respawn").toBool();
    if (group == "systemtray") {
      result.killWithSystray = globalSettings.value(path + "_kill").toBool();
    }
    result.logoutOnExit = globalSettings.value(path + "_logout").toBool();
  } else if (defaults.contains(path)) {
    bool didLookup = false;
    QVariant commandLine = defaults.value(path);
    if (commandLine.type() == QVariant::StringList) {
      QStringList options = commandLine.toStringList();
      for (const QString& cmd : options) {
        QString name = cmd.section(' ', 0, 0, QString::SectionSkipEmpty);
        if (!QStandardPaths::findExecutable(name).isEmpty()) {
          result.commandLine = cmd;
          didLookup = true;
          break;
        }
      }
    } else {
      result.commandLine = commandLine.toString();
    }
    result.stopCommand = defaults.value(path + "_stop").toString();
    result.respawnOnCrash = defaults.value(path + "_respawn").toBool();
    if (group == "systemtray") {
      result.killWithSystray = defaults.value(path + "_kill").toBool();
    }
    result.logoutOnExit = defaults.value(path + "_logout").toBool();
    if (didLookup) {
      needsSaved << result;
    }
  }
  if (group == "environment") {
    result.commandLine = envSettings.value(key).toString();
    result.stopCommand = envSettings.value(path + "_stop").toString();
  }
  return result;
}

void Settings::saveService(const SessionService& service)
{
  QSettings& s = service.settingsGroup == "environment" ? envSettings : localSettings;
  QString key = service.settingsGroup == "environment" ? service.settingsKey
                                                       : (service.settingsGroup + "/" + service.settingsKey);
  s.setValue(key, service.commandLine);
  if (service.commandLine == "builtin" || service.commandLine.isEmpty()) {
    s.remove(key + "_stop");
    s.remove(key + "_respawn");
    s.remove(key + "_kill");
    s.remove(key + "_logout");
  } else {
    if (service.respawnOnCrash) {
      s.setValue(key + "_respawn", true);
    } else {
      s.remove(key + "_respawn");
    }
    if (service.killWithSystray && service.settingsGroup == "systemtray") {
      s.setValue(key + "_kill", true);
    } else {
      s.remove(key + "_kill");
    }
    if (service.logoutOnExit) {
      s.setValue(key + "_logout", true);
    } else {
      s.remove(key + "_logout");
    }
    if (service.stopCommand.isEmpty()) {
      s.remove(key + "_stop");
    } else {
      s.setValue(key + "_stop", service.stopCommand);
    }
  }
  s.sync();
}

void Settings::savePending()
{
  while (!needsSaved.isEmpty()) {
    saveService(needsSaved.takeLast());
  }
}

void Settings::sync()
{
  globalSettings.sync();
  localSettings.sync();
  envSettings.sync();
}

void Settings::unixSignal(int sig)
{
  if (sig == SIGUSR1) {
    qDebug() << "Settings: caught SIGUSR1, reloading";
    reloadThrottle.trigger();
  }
}

void Settings::deferredReload()
{
  sync();
  emit reloadSettings();
}

void Settings::broadcastReload()
{
  sync();
  configNotifier->broadcast();
}
