#include "windowentry.h"

#include <QSysInfo>

static QString localMachine = QSysInfo::machineHostName();
static uint32_t nextCreationOrder = 1;

WindowEntry::WindowEntry(uint32_t window)
: windowId(window), baseWindowId(0), groupLeaderId(0), pid(0), isClient(false), urgent(false), iconSource(WindowEntry::NoIcon)
{
  // Properties to be populated by platform integration
  creationOrder = nextCreationOrder++;
  lastFocused = creationOrder;
}

bool WindowEntry::isLocal() const
{
  return hostname == localMachine;
}

bool WindowEntry::operator==(const WindowEntry& other) const
{
  return windowId == other.windowId;
}

bool WindowEntry::operator!=(const WindowEntry& other) const
{
  return windowId != other.windowId;
}
