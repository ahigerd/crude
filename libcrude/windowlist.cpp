#include "windowlist.h"

#include "platformintegration.h"

#include <QApplication>
#include <QMetaEnum>
#include <QTimer>

static QMetaEnum groupingModeEnum = QMetaEnum::fromType<WindowList::GroupingOption>();

#define WS_COMPARE(field) \
  { \
    if (lhs->field < rhs->field) \
      return true; \
    else if (lhs->field > rhs->field) \
      return false; \
  }

struct WindowSorter
{
  int groupMode;

  explicit WindowSorter(int groupMode)
  : groupMode(groupMode & WindowList::EnableGrouping ? groupMode & WindowList::GroupMask : 0)
  {}

  inline bool operator()(const WindowRef lhs, const WindowRef rhs)
  {
    WS_COMPARE(isRemote());
    WS_COMPARE(hostname);
    if (groupMode & WindowList::GroupByInstance) {
      WS_COMPARE(instanceClass);
    }
    if (groupMode & WindowList::GroupByClass) {
      WS_COMPARE(windowClass);
    }
    if (groupMode & WindowList::GroupByProgram) {
      WS_COMPARE(program);
    }
    if (groupMode & WindowList::GroupByProcess) {
      WS_COMPARE(pid);
    }
    if ((groupMode & (WindowList::GroupByProgram | WindowList::GroupByProcess)) == 0) {
      // If not grouping by either of these, at least sort by program and process to cluster them
      WS_COMPARE(program);
      WS_COMPARE(pid);
    }
    // Break ties by creation order
    WS_COMPARE(creationOrder);
    return false;
  }
};

static QIcon blankIcon;

WindowList::WindowList(QObject* parent)
: QAbstractItemModel(parent),
  current(nullptr),
  groupMode(WindowList::NoGrouping),
  urgentMode(WindowList::UrgentText),
  nextFocusOrder(0),
  showHost(false),
  urgentFlash(false),
  icons(true),
  labelType(WindowList::WindowTitle)
{
  if (blankIcon.isNull()) {
    QPixmap blankPixmap(1, 1);
    blankPixmap.fill(Qt::transparent);
    blankIcon = QIcon(blankPixmap);
  }

  PlatformIntegration* pi = PlatformIntegration::instance();
  QObject::connect(pi, SIGNAL(fullWindowList(WindowRefList)), this, SLOT(fullWindowList(WindowRefList)));
  QObject::connect(pi, SIGNAL(windowUpdated(WindowRef)), this, SLOT(updateWindow(WindowRef)));
  QObject::connect(pi, SIGNAL(currentWindowChanged(WindowRef)), this, SLOT(updateCurrentWindow(WindowRef)));

  updateWindowList();

  QTimer* urgentTimer = new QTimer(this);
  QObject::connect(urgentTimer, SIGNAL(timeout()), this, SLOT(flashUrgent()));
  urgentTimer->start(500);
}

void WindowList::updateWindowList()
{
  PlatformIntegration::instance()->requestWindowList();
}

void WindowList::fullWindowList(const WindowRefList& list)
{
  bool removed = false, added = false;
  for (const WindowRef& window : windows) {
    if (window->isClient && !list.contains(window)) {
      removed = true;
      break;
    }
  }
  for (const WindowRef& window : list) {
    if (window->isClient && !windows.contains(window)) {
      added = true;
      break;
    }
  }
  if (!removed && !added) {
    // no structural changes, so don't reset model
    return;
  }
  WindowRefList newEntries = list;
  beginResetModel();
  std::sort(newEntries.begin(), newEntries.end(), WindowSorter(groupMode));
  windows.clear();
  for (const WindowRef& entry : newEntries) {
    if (entry->isClient) {
      windows << entry;
    }
    if (entry->lastFocused >= nextFocusOrder) {
      nextFocusOrder = entry->lastFocused + 1;
    }
  }
  regroupWindows();
  endResetModel();
  emit windowListChanged(windows);
}

void WindowList::updateCurrentWindow(WindowRef entry)
{
  if (!entry || !windows.contains(entry)) {
    return;
  }
  for (WindowGroup& group : groups) {
    if (group.contains(entry)) {
      group.lastActive = entry;
    }
  }
  entry->urgent = false;
  if (entry->lastFocused < nextFocusOrder - 1) {
    entry->lastFocused = nextFocusOrder++;
  }
  WindowRef oldCurrent = current;
  current = entry;
  emit currentWindowChanged(current);
  emit currentWindowChanged(index(current));
  if (oldCurrent) {
    emitDataChanged(oldCurrent);
  }
  emitDataChanged(current);
}

void WindowList::updateWindow(WindowRef window)
{
  // TODO: if this causes regrouping it needs to be handled
  emitDataChanged(window);
}

const QList<WindowRef>& WindowList::windowList() const
{
  return windows;
}

WindowRef WindowList::currentWindow() const
{
  return current;
}

WindowList::UrgentFlags WindowList::urgentFlags() const
{
  return urgentMode;
}

void WindowList::setUrgentFlags(WindowList::UrgentFlags flags)
{
  if (flags != urgentMode) {
    urgentMode = flags;
    for (WindowRef entry : windows) {
      if (entry->urgent) {
        emitDataChanged(entry);
      }
    }
  }
}

WindowList::GroupingMode WindowList::groupingMode() const
{
  return groupMode;
}

void WindowList::setGroupingMode(WindowList::GroupingMode mode)
{
  if (groupMode != mode) {
    beginResetModel();
    groupMode = mode;
    regroupWindows();
    endResetModel();
  }
}

QByteArray WindowList::groupingModeToString(WindowList::GroupingMode groupMode)
{
  QByteArray group;
  int grouping = groupMode & WindowList::GroupMask;
  if (grouping) {
    group = "EnableGrouping";
    if (grouping == WindowList::GroupAggressively) {
      group += "|GroupAggressively";
    } else {
      if (grouping & WindowList::GroupByProcess) {
        group += "|GroupByProcess";
      }
      if (grouping & WindowList::GroupByProgram) {
        group += "|GroupByProgram";
      }
      if (grouping & WindowList::GroupByClass) {
        group += "|GroupByClass";
      }
      if (grouping & WindowList::GroupByInstance) {
        group += "|GroupByInstance";
      }
    }
    if (groupMode & WindowList::NoSingleGroups) {
      group += "|NoSingleGroups";
    }
  } else {
    group = "NoGrouping";
  }
  return group;
}

WindowList::GroupingMode WindowList::stringToGroupingMode(const QByteArray& mode)
{
  return WindowList::GroupingMode(groupingModeEnum.keysToValue(mode));
}

WindowList::LabelText WindowList::labelText() const
{
  return labelType;
}

void WindowList::setLabelText(WindowList::LabelText text)
{
  if (labelType != text) {
    labelType = text;
    for (WindowRef entry : windows) {
      emitDataChanged(entry);
    }
  }
}

Qt::ItemFlags WindowList::flags(const QModelIndex& index) const
{
  if (index.parent().isValid()) {
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemNeverHasChildren;
  } else if (hasChildren(index)) {
    return Qt::ItemIsEnabled;
  } else {
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
  }
}

QVariant WindowList::data(const QModelIndex& index, int role) const
{
  if (!index.isValid() || windows.length() == 0) {
    return QVariant();
  }

  if (role == Qt::DisplayRole && labelType == NoText) {
    return QVariant();
  }

  WindowRef entry = (WindowRef) nullptr;
  bool urgent = false;
  QString groupHeader;

  if (groupMode & GroupMask) {
    int group = index.internalId() - 1;
    if (group >= groups.length()) {
      return QVariant();
    } else if (group < 0) {
      // requesting the group header
      if (index.row() >= groups.length()) {
        return QVariant();
      }

      const WindowGroup& group = groups[index.row()];
      if (role == CreationOrderRole) {
        return group.creationOrder;
      } else if (role == FocusOrderRole) {
        return group.lastActive->lastFocused;
      }
      entry = groups[index.row()].lastActive;
      if (role == Qt::DisplayRole || role == Qt::CheckStateRole) {
        for (int i = group.length() - 1; i >= 0; --i) {
          urgent = urgent || group[i]->urgent;
        }
      }
      int rc = (role == WindowTitleRole || role == Qt::DisplayRole || role == Qt::DecorationRole) ? rowCount(index) : 0;
      if (rc > 0) {
        if (role == Qt::DisplayRole && labelType == ProgramName && !entry->program.isEmpty()) {
          groupHeader = entry->program;
        } else if (entry->groupLeaderId) {
          entry = group.leader;
        } else if (group.contains(current)) {
          entry = current;
        }
      }
    } else {
      if (index.row() >= groups[group].length()) {
        return QVariant();
      }
      entry = groups[group][index.row()];
    }
  } else {
    if (index.row() >= windows.count()) {
      return QVariant();
    }
    entry = windows[index.row()];
  }

  if (!entry) {
    return QVariant();
  }

  if (entry->urgent) {
    urgent = true;
  }

  if (role == Qt::DisplayRole || role == WindowTitleRole) {
    QString text = groupHeader.isNull() ? entry->name : groupHeader;
    if (showHost && !entry->isLocal()) {
      text += " @ " + entry->hostname;
    }
    if (role == Qt::DisplayRole && urgent && (urgentMode & UrgentText)) {
      if (urgentFlash) {
        text = "/!\\ " + text;
      } else {
        text = "\\!/ " + text;
      }
    }
    return text;
  } else if (role == Qt::DecorationRole) {
    if (!icons) {
      return QVariant();
    }
    if (entry->icon.isNull()) {
      return blankIcon;
    }
    return entry->icon;
  } else if (role == Qt::CheckStateRole) {
    if (current == entry) {
      return Qt::Checked;
    } else if ((urgentMode & UrgentCheck) && entry->urgent) {
      return urgentFlash ? Qt::PartiallyChecked : Qt::Unchecked;
    } else {
      return QVariant();
    }
  } else if (role == WindowIDRole) {
    return entry->windowId;
  } else if (role == BaseWindowIDRole) {
    return entry->baseWindowId;
  } else if (role == CreationOrderRole) {
    return entry->creationOrder;
  } else if (role == FocusOrderRole) {
    return entry->lastFocused;
  } else {
    return QVariant();
  }
}

QVariant WindowList::headerData(int, Qt::Orientation, int) const
{
  // No meaningful header data
  return QVariant();
}

int WindowList::rowCount(const QModelIndex& parent) const
{
  if (groupMode & GroupMask) {
    if (parent.isValid()) {
      if (parent.internalId() != 0) {
        return 0;
      }
      int ct = groups[parent.row()].count();
      if (groupMode & NoSingleGroups) {
        return ct > 1 ? ct : 0;
      } else {
        return ct;
      }
    } else {
      return groups.count();
    }
  }
  if (parent.isValid()) {
    return 0;
  } else {
    return windows.count();
  }
}

int WindowList::columnCount(const QModelIndex&) const
{
  return 1;
}

QModelIndex WindowList::index(int row, int col, const QModelIndex& parent) const
{
  if ((groupMode & GroupMask) && parent.isValid()) {
    if (parent.internalId() == 0) {
      return createIndex(row, col, parent.row() + 1);
    }
    return QModelIndex();
  } else {
    return createIndex(row, col);
  }
}

QModelIndex WindowList::parent(const QModelIndex& child) const
{
  if (groupMode & GroupMask && child.internalId() > 0) {
    return createIndex(child.internalId() - 1, 0);
  } else {
    return QModelIndex();
  }
}

QModelIndex WindowList::index(WindowRef entry) const
{
  if (groupMode & GroupMask) {
    for (int i = 0; i < groups.length(); i++) {
      int pos = groups[i].indexOf(entry);
      if (pos >= 0) {
        if (groupMode & NoSingleGroups && groups[i].length() < 2) {
          return index(i, 0);
        } else {
          return index(pos, 0, index(i, 0));
        }
      }
    }
  } else {
    for (int i = 0; i < windows.length(); i++) {
      if (windows[i] == entry) {
        return index(i, 0);
      }
    }
  }
  return QModelIndex();
}

static bool isSameGroup(int mode, const WindowRef lhs, const WindowRef rhs)
{
  if (lhs->groupLeaderId && (lhs->groupLeaderId == rhs->groupLeaderId || lhs->groupLeaderId == rhs->windowId)) {
    // Explicit groups are always preserved from the left.
    return true;
  }
  if (rhs->groupLeaderId && rhs->groupLeaderId == lhs->windowId) {
    // Explicit groups are always preserved from the right.
    return true;
  }
  if (!mode) {
    // If not using an implicit grouping mode, only use explicit groups.
    return false;
  }
  if (lhs->hostname != rhs->hostname) {
    // Processes on different hosts are never auto-grouped.
    return false;
  }
  if ((mode & WindowList::GroupByProcess) && lhs->pid == rhs->pid) {
    // Requested group by PID
    return true;
  }
  if ((mode & WindowList::GroupByProgram) && lhs->program == rhs->program) {
    // Requested group by program name
    return true;
  }
  if ((mode & WindowList::GroupByClass) && (lhs->windowClass == rhs->windowClass)) {
    // Requested group by window class in WM_CLASS
    return true;
  }
  if ((mode & WindowList::GroupByInstance) && (lhs->instanceClass == rhs->instanceClass)) {
    // Requested group by instance class in WM_CLASS
    return true;
  }
  return false;
}

void WindowList::regroupWindows()
{
  int mode = groupMode & GroupMask;
  groups.clear();
  if (mode == NoGrouping) {
    return;
  }
  for (WindowRef entry : windows) {
    int groupIdx = -1;
    for (int i = groups.length() - 1; i >= 0; --i) {
      if (isSameGroup(mode, groups[i].leader, entry)) {
        groupIdx = i;
        break;
      }
    }
    if (groupIdx < 0) {
      groupIdx = groups.length();
      WindowGroup newGroup;
      newGroup.leader = entry;
      newGroup.lastActive = entry;
      newGroup.creationOrder = 0xFFFFFFFF;
      groups << newGroup;
    }
    auto& group = groups[groupIdx];
    group << entry;
    if (entry->creationOrder < group.creationOrder) {
      group.creationOrder = entry->creationOrder;
    }
    if (entry->groupLeaderId && !group.leader->groupLeaderId) {
      for (int i = group.length() - 1; i >= 0; --i) {
        if (group[i]->windowId == entry->groupLeaderId) {
          group.leader = group[i];
          break;
        }
      }
    }
  }
  for (WindowGroup& group : groups) {
    uint32_t lastFocused = 0;
    for (const WindowRef& win : group) {
      if (win->lastFocused > lastFocused) {
        lastFocused = win->lastFocused;
        group.lastActive = win;
      }
    }
  }
}

bool WindowList::showIcons() const
{
  return icons;
}

void WindowList::setShowIcons(bool on)
{
  if (icons != on) {
    icons = on;
    for (WindowRef entry : windows) {
      emitDataChanged(entry);
    }
  }
}

bool WindowList::showRemoteHosts() const
{
  return showHost;
}

void WindowList::setShowRemoteHosts(bool on)
{
  if (showHost != on) {
    showHost = on;
    for (WindowRef entry : windows) {
      if (!entry->isLocal()) {
        emitDataChanged(entry);
      }
    }
  }
}

void WindowList::flashUrgent()
{
  urgentFlash = !urgentFlash;
  for (WindowRef entry : windows) {
    if (entry->urgent) {
      emitDataChanged(entry);
    }
  }
}

void WindowList::emitDataChanged(WindowRef entry)
{
  QModelIndex idx = index(entry);
  emit dataChanged(idx, idx);
  if (idx.parent().isValid()) {
    emit dataChanged(idx.parent(), idx.parent());
  }
  emit windowChanged(entry);
}

QModelIndex WindowList::activeChild(const QModelIndex& groupIndex) const
{
  if (groupIndex.parent().isValid() || groupIndex.row() < 0 || groupIndex.row() >= groups.count()) {
    return QModelIndex();
  }
  const WindowGroup& group = groups[groupIndex.row()];
  return index(group.lastActive);
}
