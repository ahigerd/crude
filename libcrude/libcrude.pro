TEMPLATE = lib
TARGET = crude
CONFIG += staticlib create_prl
QT = core widgets
include(../crude-common.pri)

HEADERS += qpointerset.h systemtray_p.h hotkey_p.h ../plugins/powerplugin.h xdgmenu_p.h
SOURCES += trayiconhost.cpp

HEADERS += windowlist.h   processinfo.h   windowentry.h   systemtray.h   displaypower.h   confignotifier.h
SOURCES += windowlist.cpp processinfo.cpp windowentry.cpp systemtray.cpp displaypower.cpp confignotifier.cpp

HEADERS += platformintegration.h   throttle.h   xdglauncher.h   environmentprefs.h   threadhelper.h
SOURCES += platformintegration.cpp throttle.cpp xdglauncher.cpp environmentprefs.cpp threadhelper.cpp

HEADERS += xdgmenu.h   xdgmenumodel.h   hotkey.h   acpimonitor.h   powermanagement.h   pluginshell.h   sessionsettings.h
SOURCES += xdgmenu.cpp xdgmenumodel.cpp hotkey.cpp acpimonitor.cpp powermanagement.cpp pluginshell.cpp sessionsettings.cpp

HEADERS += qxtcommandoptions.h   qxtglobal.h
SOURCES += qxtcommandoptions.cpp

include(xcb/xcb.pri)
