#include "systemtray.h"

#include "xcbsystemtray_p.h"

#include <QHBoxLayout>
#include <QMessageBox>
#include <QStyle>
#include <QTimer>
#include <QtDebug>

SystemTray::SystemTray(QWidget* parent) : QFrame(parent), isVertical(false), shuttingDown(false)
{
  QHBoxLayout* layout = new QHBoxLayout(this);
  layout->setContentsMargins(1, 1, 1, 1);
  layout->setSpacing(1);
  setLayout(layout);

  setMouseTracking(true);
  setFrameShadow(QFrame::Sunken);
  setFrameShape(QFrame::Panel);
  setAutoFillBackground(true);

  // TODO: detect platform, once more than one platform is available
  d = new XcbSystemTrayPrivate(this);
  setOrientation(Qt::Horizontal);
}

SystemTray::~SystemTray()
{
  qDeleteAll(icons);
  delete d;
}

Qt::Orientation SystemTray::orientation() const
{
  return isVertical ? Qt::Vertical : Qt::Horizontal;
}

void SystemTray::setOrientation(Qt::Orientation o)
{
  isVertical = (o == Qt::Vertical);
  if (isVertical) {
    setMinimumSize(22 + style()->pixelMetric(QStyle::PM_DefaultFrameWidth) * 2, 0);
  } else {
    setMinimumSize(0, 22 + style()->pixelMetric(QStyle::PM_DefaultFrameWidth) * 2);
  }
  static_cast<QBoxLayout*>(layout())->setDirection(isVertical ? QBoxLayout::TopToBottom : QBoxLayout::LeftToRight);
  d->updateOrientation();
}

void SystemTray::showMessage(const QString& title, const QString& message, const QPoint& pos)
{
  if (shuttingDown) {
    return;
  }
  // todo
  Q_UNUSED(pos);
  QMessageBox::information(this, title, message, QMessageBox::Ok);
}

void SystemTray::addWidget(QWidget* widget)
{
  addIcon(new TrayIconHost(widget));
}

void SystemTray::addIcon(TrayIconHost* icon)
{
  if (shuttingDown) {
    return;
  }
  if (icon->id) {
    TrayIconHost* existing = iconsById.value(icon->id);
    if (existing) {
      existing->remap();
      return;
    }
  }
  icons << icon;
  if (icon->id) {
    iconsById[icon->id] = icon;
    icon->remap();
  }
  if (icon->widget) {
    iconsByWidget[icon->widget] = icon;
  }
  QObject::connect(icon, SIGNAL(removed(TrayIconHost*)), this, SLOT(removeIcon(TrayIconHost*)));
  static_cast<QBoxLayout*>(layout())->addWidget(icon);
}

void SystemTray::removeWidget(QWidget* icon)
{
  removeIcon(iconsByWidget.value(icon));
}

void SystemTray::removeIcon(TrayIconHost* host)
{
  if (!host) {
    return;
  }
  icons.removeAll(host);
  if (host->widget) {
    iconsByWidget.remove(host->widget);
  }
  if (host->id) {
    iconsById.remove(host->id);
  }
  host->deleteLater();
}

void SystemTray::resizeEvent(QResizeEvent*)
{
  validate();
}

void SystemTray::validate()
{
  for (TrayIconHost* icon : icons) {
    icon->validate();
  }
}

SystemTrayPrivate::SystemTrayPrivate(SystemTray* parent) : QObject(parent)
{
  p = parent;
}
