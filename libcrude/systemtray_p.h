#ifndef CRUDE_SYSTEMTRAY_P_H
#define CRUDE_SYSTEMTRAY_P_H

#include "systemtray.h"
#include <QTimer>

class SystemTrayPrivate : public QObject
{
  Q_OBJECT

public:
  SystemTrayPrivate(SystemTray* parent);

  virtual void updateOrientation() = 0;

  SystemTray* p;
};

class TrayIconHost : public QWidget
{
Q_OBJECT
public:
  TrayIconHost(uint32_t winId, bool useDamage);
  TrayIconHost(QWidget* icon);
  ~TrayIconHost();

  void damaged();
  void validate();
  void remap();
  void ensureMapped();
  void updateIconGeometry();

  uint32_t id;
  QPointer<QWidget> widget;
  QString title;
  QString className;
  uint32_t damageId;

  bool hasHeightForWidth() const;
  int heightForWidth(int w) const;

signals:
  void removed(TrayIconHost*);

protected:
  void showEvent(QShowEvent*);
  void moveEvent(QMoveEvent*);
  void resizeEvent(QResizeEvent*);

private slots:
  bool doValidate();
  void onWidgetDestroyed();

private:
  uint64_t lastDamage;
  QTimer validateTimer;
};

#endif
