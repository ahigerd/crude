#include "processinfo.h"

#include <QDir>
#include <QFile>
#include <QtDebug>

ProcessInfo::ProcessInfo() : _pid(0), valid(false)
{
  // initializers only
}

ProcessInfo::ProcessInfo(pid_t pid) : _pid(pid), valid(true)
{
  // initializers only
}

bool ProcessInfo::isValid() const
{
  return valid;
}

pid_t ProcessInfo::pid() const
{
  return _pid;
}

QString ProcessInfo::procPath() const
{
  if (!valid) {
    return QString();
  }
  return "/proc/" + QString::number(_pid) + "/";
}

QString ProcessInfo::executable() const
{
  if (!valid) {
    return QString();
  }
  QString path = QDir(procPath() + "exe").canonicalPath();
  if (path.isEmpty() || path.startsWith("/proc") || !QFile::exists(path)) {
    return QString();
  }
  return path;
}

QStringList ProcessInfo::commandLine() const
{
  if (!valid) {
    return QStringList();
  }
  QFile proc(procPath() + "cmdline");
  bool ok = proc.open(QIODevice::ReadOnly);
  if (!ok) {
    return QStringList();
  }
  QByteArray data = proc.readAll();
  QList<QByteArray> parts = data.split('\0');
  QStringList argv;
  for (const QByteArray& part : parts) {
    argv << QString::fromUtf8(part);
  }
  return argv;
}

QString ProcessInfo::commandName() const
{
  if (!valid) {
    return QString();
  }
  QString exe = executable();
  if (!exe.isEmpty()) {
    return exe.section('/', -1);
  }
  QStringList argv = commandLine();
  if (argv.length() > 0) {
    return argv[0].section('/', -1);
  }
  return QString();
}
