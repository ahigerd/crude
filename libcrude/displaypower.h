#ifndef CRUDE_DISPLAYPOWER_H
#define CRUDE_DISPLAYPOWER_H

#include <QHash>
#include <QObject>
#include <QStringList>
#include <QTime>
#include "plugins/lightplugin.h"
#include "pluginshell.h"

class DisplayPower : public QObject
{
  Q_OBJECT

protected:
  DisplayPower(QObject* parent = nullptr);

public:
  enum ScreenSaverType {
    Scheduler,
    Locker,
    LockingScheduler,
  };

  struct ScreenSaverConfig
  {
    QString name;
    ScreenSaverType type;
    QHash<QString, QString> config;
  };

  enum StandbyMode {
    On = 0,
    ScreenSaver = 4,
    Standby = 1,
    Suspend = 2,
    Off = 3
  };

  Q_ENUM(StandbyMode);

  struct Schedule
  {
    QTime delay;
    StandbyMode standbyMode;
  };

  virtual bool backlightAvailable() const;
  float backlightLevel() const;

  virtual bool standbyModeAvailable(StandbyMode mode) const = 0;
  virtual StandbyMode standbyMode() const = 0;

  virtual QList<Schedule> schedule() const = 0;
  virtual void setSchedule(const QList<Schedule>& schedule) = 0;
  void restoreSchedule();

  static QList<ScreenSaverConfig> scanScreenSavers();
  static ScreenSaverConfig screenSaverConfig(const QString& name);

public slots:
  virtual void setBacklightLevel(float level);
  void increaseBacklight(float step = .1f);
  void decreaseBacklight(float step = .1f);
  virtual void setStandbyMode(StandbyMode mode) = 0;

signals:
  void backlightAvailableChanged(bool available);
  void backlightChanged(float level);
  void displayCapabilitiesUpdated();
  void standbyModeChanged(StandbyMode mode);

protected:
  static bool screenSaverAvailable();
  void activateScreenSaver();
  int screenSaverTimeout() const;
  void setScreenSaverTimeout(int seconds);
  void saveSchedule() const;
  void saveSchedule(const QList<Schedule>& schedule) const;
  static bool checkAvailable(const QVariant& required);

  float lastLevel;

  class LightPluginShell : public PluginShell<LightPlugin>
  {
  public:
    LightPluginShell();

    using PluginShell<LightPlugin>::plugin;
    using PluginShell<LightPlugin>::selectDefaultPlugin;

    virtual QString pluginPrefix() const;
    virtual QStringList pluginPriority() const;
    virtual QString noPluginName() const;
  };
  LightPluginShell pluginShell;
  bool pluginScanned;

protected slots:
  void scanPlugins();
  void onBacklightChanged(float value);
};

#endif
