#ifndef CRUDE_CONFIGNOTIFIER_H
#define CRUDE_CONFIGNOTIFIER_H

#include <QHash>
#include <QObject>
class QSocketNotifier;
class QTimer;

class ConfigNotifier : public QObject
{
  Q_OBJECT

public:
  ConfigNotifier(QObject* parent = nullptr);

signals:
  void reloadConfig();

public slots:
  void broadcast();

private slots:
  void connectToSocket();
  void pinged();
  void addClient();
  void clientPinged(int clientFd);
  void clientError(int clientFd);
  void triggerReload();

private:
  void initServer(int socketFd);
  void initClient(int socketFd);
  bool drainSocket(int socketFd);

  bool isServer;
  int fd;
  QSocketNotifier *readNotifier, *exceptNotifier;
  QTimer* reloadDelay;
  qint64 waitTime;
  QHash<int, QSocketNotifier*> clients;
  bool recursive;
};

#endif
