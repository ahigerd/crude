#ifndef CRUDE_THROTTLE_H
#define CRUDE_THROTTLE_H

#include <QTimer>

class Throttle : public QObject
{
  Q_OBJECT

public:
  Throttle(Throttle&& other);

  template <typename Functor>
  static Throttle connectedTo(Functor slot);
  static Throttle connectedTo(QObject* obj, const char* slot);

  Throttle& timeout(int msec);

public slots:
  void trigger();

private:
  explicit Throttle(QTimer* timer);
  QTimer* timer;
};

template <typename Functor>
Throttle Throttle::connectedTo(Functor slot)
{
  QTimer* timer = new QTimer();
  QObject::connect(timer, &QTimer::timeout, timer, slot);
  return Throttle(timer);
}

#endif
