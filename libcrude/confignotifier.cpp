#include "confignotifier.h"

#include "environmentprefs.h"

#include <QDateTime>
#include <QDir>
#include <QScopedPointer>
#include <QSocketNotifier>
#include <QTimer>
#include <QtDebug>

#include <fcntl.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#define RELOAD_TIMER 200
#define NOW()        QDateTime::currentMSecsSinceEpoch()

static void initFd(int fd)
{
  if (fd <= 0) {
    return;
  }
  ::fcntl(fd, F_SETFD, ::fcntl(fd, F_GETFD, 0) | FD_CLOEXEC);
  ::fcntl(fd, F_SETFL, ::fcntl(fd, F_GETFL, 0) | O_NONBLOCK);
}

struct WrappedSocket : public ::sockaddr_un
{
  int fd;

  static WrappedSocket* accept(int acceptFd)
  {
    WrappedSocket* sock = new WrappedSocket(acceptFd);
    if (sock->fd > 0) {
      return sock;
    }
    delete sock;
    return nullptr;
  }

  WrappedSocket(const char* filename)
  {
    initAddr();
    if (filename[0] == '\0') {
      sun_path[0] = '\0';
      ::strncpy(sun_path + 1, filename + 1, sizeof(sun_path) - 2);
    } else {
      ::strncpy(sun_path, filename, sizeof(sun_path) - 1);
    }
    fd = ::socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd <= 0) {
      qFatal("Error allocating config socket: %s", ::strerror(errno));
    }
    initFd(fd);
  }

  inline sockaddr* to_sockaddr() { return reinterpret_cast<sockaddr*>(static_cast<sockaddr_un*>(this)); }

  bool listen()
  {
    if (::bind(fd, to_sockaddr(), sizeof(sockaddr_un))) {
      return false;
    }
    if (::listen(fd, 50)) {
      ::close(fd);
      return false;
    }
    return true;
  }

  bool connect()
  {
    while (true) {
      errno = 0;
      int err = ::connect(fd, to_sockaddr(), sizeof(sockaddr_un));
      int errorCode = err == 0 ? 0 : errno;
      if (errorCode && errorCode != EAGAIN) {
        qWarning("Error connecting config socket: %s", ::strerror(errorCode));
        return false;
      }
      return true;
    }
  }

  void close() { ::close(fd); }

private:
  WrappedSocket(int acceptFd)
  {
    initAddr();
    ::socklen_t size = sizeof(sockaddr_un);
    fd = ::accept(acceptFd, to_sockaddr(), &size);
    initFd(fd);
  }

  void initAddr()
  {
    ::memset(static_cast<sockaddr_un*>(this), 0, sizeof(sockaddr_un));
    sun_family = AF_UNIX;
  }
};

ConfigNotifier::ConfigNotifier(QObject* parent)
: QObject(parent),
  isServer(false),
  fd(0),
  readNotifier(nullptr),
  reloadDelay(new QTimer(this)),
  waitTime(NOW()),
  recursive(false)
{
  reloadDelay->setInterval(RELOAD_TIMER);
  reloadDelay->setSingleShot(true);
  QObject::connect(reloadDelay, SIGNAL(timeout()), this, SLOT(triggerReload()));

  connectToSocket();
}

void ConfigNotifier::pinged()
{
  // Consume whatever is on the socket.
  if (drainSocket(fd)) {
    // An error instead of an empty socket
    connectToSocket();
    return;
  }

  // If a ping comes in right after we broadcast a reload, suppress it.
  qint64 now = NOW();
  if (waitTime < now) {
    // If a ping comes in while a previous timer is ticking, restart the timer.
    // This does mean there could potentially be starvation if something is
    // repeatedly updating the config, but that's a bug anyway and we should
    // fail conservatively.
    reloadDelay->start();
  }
  waitTime = now + RELOAD_TIMER;
}

void ConfigNotifier::connectToSocket()
{
  // Since we will assume there was a config update upon connection, the
  // throttling timer needs to be reset. We do it before trying to connect
  // in order to keep the threshold up-to-date even if the connection fails.
  waitTime = NOW() + RELOAD_TIMER;

  if (fd) {
    ::close(fd);
    fd = 0;
  }
  isServer = false;
  if (readNotifier) {
    readNotifier->setEnabled(false);
    readNotifier->deleteLater();
    readNotifier = nullptr;
  }
  for (QSocketNotifier* client : clients) {
    client->setEnabled(false);
    client->deleteLater();
  }
  clients.clear();

  WrappedSocket sock("\0crude.sock");

  if (sock.listen()) {
    // We successfully bound to the socket, so we're the server
    initServer(sock.fd);
  } else if (sock.connect()) {
    // We could not bind to the socket, so try connecting to it instead
    initClient(sock.fd);
  } else {
    // We couldn't listen or connect, so try again later
    // This is intentionally longer than RELOAD_TIMER
    QTimer::singleShot(1000, this, SLOT(connectToSocket()));
    sock.close();
    return;
  }

  // If there was a disconnection, assume that there's a config update
  reloadDelay->start();
}

void ConfigNotifier::initServer(int socketFd)
{
  isServer = true;
  fd = socketFd;
  readNotifier = new QSocketNotifier(fd, QSocketNotifier::Read, this);
  QObject::connect(readNotifier, SIGNAL(activated(int)), this, SLOT(addClient()));
  exceptNotifier = new QSocketNotifier(fd, QSocketNotifier::Exception, this);
  QObject::connect(exceptNotifier, SIGNAL(activated(int)), this, SLOT(connectToSocket()));
}

void ConfigNotifier::initClient(int socketFd)
{
  isServer = false;
  fd = socketFd;
  readNotifier = new QSocketNotifier(fd, QSocketNotifier::Read, this);
  QObject::connect(readNotifier, SIGNAL(activated(int)), this, SLOT(pinged()));
  exceptNotifier = new QSocketNotifier(fd, QSocketNotifier::Exception, this);
  QObject::connect(exceptNotifier, SIGNAL(activated(int)), this, SLOT(connectToSocket()));
}

void ConfigNotifier::addClient()
{
  QScopedPointer<WrappedSocket> sock(WrappedSocket::accept(fd));
  if (!sock) {
    // If it looked like there was supposed to be a new connection, but we
    // couldn't accept it, then there's something wrong. QLocalServer shuts
    // down the server if this happens, so we'll follow its lead.
    // Note: Don't use the timer here, because connectToSocket() tears down
    // any outstanding objects and we want to clean up ASAP. There's no need
    // to delay when trying to reclaim the socket.
    qWarning("Error accepting new client: %s, closing server", ::strerror(errno));
    connectToSocket();
    return;
  }
  QSocketNotifier* client = new QSocketNotifier(sock->fd, QSocketNotifier::Read, this);
  QObject::connect(client, SIGNAL(activated(int)), this, SLOT(clientPinged(int)));
  clients[sock->fd] = client;
}

void ConfigNotifier::clientPinged(int clientFd)
{
  // At the moment, it's only the act of writing to the socket that triggers
  // the reload. If later on we decide to make the payload meaningful, we'll
  // need to capture the data.
  QSocketNotifier* client = clients.value(clientFd);
  if (clientFd != fd && (drainSocket(clientFd) || !client)) {
    // This wasn't a ping, it was a disconnection, or it was an untracked client.
    clientError(clientFd);
    return;
  }
  // Rebroadcast the ping to all of the other clients except for the sender
  for (QSocketNotifier* otherClient : clients) {
    if (otherClient == client) {
      continue;
    }
    ::send(otherClient->socket(), "\0", 1, MSG_NOSIGNAL);
  }
  // Also send it to ourselves, unless we were the sender
  if (clientFd != fd) {
    reloadDelay->start();
  }
}

void ConfigNotifier::clientError(int clientFd)
{
  QSocketNotifier* client = clients.value(clientFd);
  if (client) {
    client->setEnabled(false);
    client->deleteLater();
  }
  clients.remove(clientFd);
  ::close(clientFd);
}

bool ConfigNotifier::drainSocket(int socketFd)
{
  char buf[2];
  int bytes;
  int total = 0;
  while ((bytes = ::read(socketFd, buf, 1)) > 0) {
    total += bytes;
  }
  return total == 0 || (bytes < 0 && errno != EAGAIN);
}

void ConfigNotifier::broadcast()
{
  if (recursive) {
    // Don't allow broadcasting if we're acting in response to a ping
    return;
  }
  if (isServer) {
    clientPinged(fd);
  } else if (fd) {
    ::send(fd, "\0", 1, MSG_NOSIGNAL);
  }
  // If there's no fd, we're not connected, so we do nothing. This isn't
  // a problem because all clients assume that there's a config update
  // upon connecting to the server.
}

void ConfigNotifier::triggerReload()
{
  if (recursive) {
    // If there's already a reload on the stack, don't do another one
    return;
  }
  recursive = true;
  emit reloadConfig();
  recursive = false;
}
