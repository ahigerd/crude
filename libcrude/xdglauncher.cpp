#include "xdglauncher.h"

#include "environmentprefs.h"
#include "platformintegration.h"

#include <QDesktopServices>
#include <QDir>
#include <QFileInfo>
#include <QList>
#include <QLocale>
#include <QProcess>
#include <QSettings>
#include <QStandardPaths>
#include <QUrl>

QString XdgLauncher::BackingGroup::getLocale(const QString& key) const
{
  for (const QString& locale : EnvironmentPrefs::localePrecedence()) {
    QString combinedKey = key + "[" + locale + "]";
    if (keys.contains(combinedKey)) {
      return keys.value(combinedKey);
    }
  }
  return keys.value(key);
}

QString XdgLauncher::BackingGroup::get(const QString& key) const
{
  return keys.value(key);
}

bool XdgLauncher::BackingGroup::getBool(const QString& key) const
{
  QString value = get(key);
  return (value == "1" || value.toLower() == "true");
}

void XdgLauncher::BackingGroup::set(const QString& key, const QString& value, bool localized)
{
  if (localized) {
    set(key, value, EnvironmentPrefs::locale());
    return;
  }
  if (value.isEmpty()) {
    keys.remove(key);
  } else {
    keys[key] = value;
  }
}

void XdgLauncher::BackingGroup::set(const QString& key, const QString& value, const QString& locale)
{
  QString effectiveKey = key;
  if (!locale.isEmpty()) {
    effectiveKey += "[" + locale + "]";
  }
  if (value.isEmpty()) {
    keys.remove(effectiveKey);
  } else {
    keys[effectiveKey] = value;
  }
}

XdgLauncher::XdgLauncher() : d(new XdgLauncher::Data())
{
  d->system = false;
}

XdgLauncher::XdgLauncher(const QString& path) : XdgLauncher(path, EnvironmentPrefs::isSystemPath(path))
{
  // forwarded initializer
}

XdgLauncher::XdgLauncher(const QString& path, bool system) : d(new XdgLauncher::Data())
{
  d->path = path;
  d->system = system;
  QSettings settings(
      path, path.endsWith(".directory") ? EnvironmentPrefs::directoryFormat() : EnvironmentPrefs::desktopFormat()
  );
  BackingGroup* backingGroup;
  for (QString group : settings.childGroups()) {
    settings.beginGroup(group);
    if (group == "Desktop Entry" || group == "KDE Desktop Entry") {
      backingGroup = &d->entry;
    } else {
      backingGroup = &d->backing[group];
    }
    for (const QString& key : settings.childKeys()) {
      backingGroup->set(key, settings.value(key).toString());
    }
    settings.endGroup();
  }
}

bool XdgLauncher::save()
{
  if (d->system) {
    // While nothing technically stops us from saving system files if the fs
    // permissions are available, be a good citizen and don't mess with system
    // files. Those are for the package manager to maintain. The caller can
    // copy the file to the user's local paths and modify that.
    return false;
  }
  // TODO
  return false;
}

bool XdgLauncher::isValid() const
{
  if (d->path.isEmpty() || type() == Unknown) {
    return false;
  }
  return true;
}

bool XdgLauncher::isSystemFile() const
{
  return d->system;
}

QString XdgLauncher::path() const
{
  return d->path;
}

void XdgLauncher::setPath(const QString& path)
{
  setPath(path, EnvironmentPrefs::isSystemPath(path));
}

void XdgLauncher::setPath(const QString& path, bool system)
{
  d->path = path;
  d->system = system;
}

QString XdgLauncher::fileId() const
{
  QString filename;
  for (const QString& dir : EnvironmentPrefs::dataDirs()) {
    QString relative = QDir(dir).relativeFilePath(d->path);
    if (relative.startsWith("..") || relative.startsWith("/")) {
      continue;
    }
    if (relative.startsWith("applications/")) {
      relative = relative.section("/", 1);
    }
    filename = relative;
    break;
  }
  if (filename.isEmpty()) {
    // As a last-ditch fallback, just use the filename instead of obeying the spec
    filename = d->path.section("/", -1);
  }
  return filename.replace("/", "-");
}

XdgLauncher XdgLauncher::byId(const QString& id)
{
  for (const QString& path : EnvironmentPrefs::dataDirs()) {
    QDir dir(path);
    if (dir.exists(id)) {
      return XdgLauncher(dir.absoluteFilePath(id));
    }
    QString pathAttempt = id;
    while (pathAttempt.contains("-")) {
      pathAttempt = pathAttempt.section("-", 0, 0) + "/" + pathAttempt.section("-", 1);
      if (dir.exists(pathAttempt)) {
        return XdgLauncher(dir.absoluteFilePath(pathAttempt));
      }
    }
  }
  // Return an invalid launcher with the specified ID, for resolution purposes
  return XdgLauncher(id, false);
}

XdgLauncher::Type XdgLauncher::type() const
{
  QString typeString = d->entry.get("Type");
  if (typeString == "Application") {
    return XdgLauncher::Application;
  } else if (typeString == "Link") {
    return XdgLauncher::Link;
  } else if (typeString == "Directory") {
    return XdgLauncher::Directory;
  } else {
    return XdgLauncher::Unknown;
  }
}

void XdgLauncher::setType(XdgLauncher::Type type)
{
  QString typeString;
  if (type == Application) {
    typeString = "Application";
  } else if (type == Link) {
    typeString = "Link";
  } else if (type == Directory) {
    typeString = "Directory";
  } else {
    typeString = "Unknown";
  }
  d->entry.set("Type", typeString);
}

bool XdgLauncher::isHidden() const
{
  return type() == Unknown || d->entry.getBool("Hidden") || d->entry.getBool("NoDisplay") || !tryExec();
}

void XdgLauncher::setHidden(bool on)
{
  d->entry.set("Hidden", on ? "true" : "false");
}

QString XdgLauncher::name() const
{
  return d->entry.getLocale("Name");
}

void XdgLauncher::setName(const QString& name, const QString& locale)
{
  d->entry.set("Name", name, locale);
}

QString XdgLauncher::genericName() const
{
  return d->entry.getLocale("GenericName");
}

void XdgLauncher::setGenericName(const QString& name, const QString& locale)
{
  d->entry.set("GenericName", name, locale);
}

QIcon XdgLauncher::icon() const
{
  if (d->icon.isNull()) {
    QString path = iconPath();
    if (path.isEmpty()) {
      return QIcon();
    } else if (path.startsWith("/")) {
      if (QFileInfo::exists(path)) {
        d->icon.addFile(path);
      }
    } else {
      d->icon = c_PI->icon(path);
    }
  }
  return d->icon;
}

QString XdgLauncher::iconPath() const
{
  return d->entry.get("Icon");
}

void XdgLauncher::setIconPath(const QString& path)
{
  d->icon = QIcon();
  d->entry.set("Icon", path);
  icon(); // trigger the icon to load
}

// This overload should not be used to save a launcher.
// It does not set the "Icon" property.
void XdgLauncher::setIconPath(const QStringList& paths)
{
  d->icon = c_PI->icon(paths);
}

bool XdgLauncher::tryExec() const
{
  QString path = d->entry.get("TryExec");
  if (path.isEmpty()) {
    return true;
  } else if (path.startsWith("/")) {
    return QFileInfo(path).isExecutable();
  } else {
    QString searchedPath = QStandardPaths::findExecutable(path);
    return !searchedPath.isEmpty();
  }
}

bool XdgLauncher::exec(bool forwardOutput) const
{
  if (type() == Application) {
    QString cmdline = execPath();
    QStringList args;
    QString arg;
    bool quoteMode = false;
    for (int i = 0; i < cmdline.length(); i++) {
      if (quoteMode) {
        if (cmdline[i] == '\\') {
          ++i;
          if (i >= cmdline.length()) {
            // Invalid escaping. Just return an error for sanity's sake.
            return false;
          }
          arg += cmdline[i];
        } else if (cmdline[i] == '"') {
          quoteMode = false;
        } else {
          arg += cmdline[i];
        }
      } else if (cmdline[i] == '"') {
        quoteMode = true;
      } else if (cmdline[i] == ' ') {
        if (!arg.isEmpty()) {
          args << arg;
        }
        arg.clear();
      } else if (cmdline[i] != '%') {
        arg += cmdline[i];
      } else if (i == cmdline.length() - 1) {
        // Spec isn't completely clear about this, but it says unrecognized codes are errors
        // so this seems sane.
        return false;
      } else {
        switch (cmdline[i + 1].unicode()) {
        case '%': arg += "%"; break;
        case 'f':
        case 'F':
        case 'u':
        case 'U':
        case 'd':
        case 'D':
        case 'n':
        case 'N':
        case 'v':
        case 'm':
          // Deprecated flags are to be removed.
          // As a launcher, we don't need to accept arguments.
          break;
        case 'i': {
          QString icon = iconPath();
          if (!icon.isEmpty()) {
            if (!arg.isEmpty()) {
              args << arg;
            }
            args << "--icon" << icon;
            arg.clear();
          }
        } break;
        case 'c': arg += name(); break;
        case 'k': arg += d->path; break;
        default:
          // Spec says the presence of unrecognized flags is an error
          return false;
        }
        ++i;
      }
    }
    if (!arg.isEmpty()) {
      args << arg;
    }
    if (isTerminal()) {
      QStringList termArgs = EnvironmentPrefs::terminalCommand(args.join(' '));
      QString cmd = termArgs.takeFirst();
      return c_PI->runCommand(cmd, termArgs, workingDirectory());
    } else {
      QString cmd = args.takeFirst();
      return c_PI->runCommand(cmd, args, workingDirectory(), forwardOutput);
    }
  } else if (type() == Link) {
    // TODO: will this persist past shutdown?
    return QDesktopServices::openUrl(QUrl(execPath()));
  } else {
    return false;
  }
}

QString XdgLauncher::execPath() const
{
  if (type() == Application) {
    return d->entry.get("Exec");
  } else if (type() == Link) {
    return d->entry.get("URL");
  } else {
    return QString();
  }
}

QString XdgLauncher::workingDirectory() const
{
  return d->entry.get("Path");
}

void XdgLauncher::setWorkingDirectory(const QString& path)
{
  d->entry.set("Path", path);
}

QStringList XdgLauncher::categories() const
{
  QStringList result;
  for (QString cat : d->entry.get("Categories").split(";")) {
    cat = cat.trimmed();
    if (!cat.isEmpty()) {
      result << cat;
    }
  }
  return result;
}

void XdgLauncher::setCategories(const QStringList& cats)
{
  d->entry.set("Categories", cats.join(";") + ";");
}

bool XdgLauncher::isTerminal() const
{
  return d->entry.getBool("Terminal");
}

void XdgLauncher::setTerminal(bool on)
{
  d->entry.set("Terminal", on ? "true" : "false");
}

// TODO: Actions
// TODO: Keywords
// TODO: startup notifications

static QList<XdgLauncher> XL_cache;

void XdgLauncher::refreshCache()
{
  XL_cache.clear();
}

static QList<XdgLauncher> recurseDir(const QDir& dir)
{
  QList<XdgLauncher> result;
  QFileInfoList entries =
      dir.entryInfoList(QStringList() << "*.desktop", QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files | QDir::Readable);
  for (const QFileInfo& info : entries) {
    if (info.isDir()) {
      result << recurseDir(info.absoluteFilePath());
    } else {
      result << XdgLauncher(info.absoluteFilePath());
    }
  }
  return result;
}

QList<XdgLauncher> XdgLauncher::allItems()
{
  if (XL_cache.isEmpty()) {
    QSet<QString> foundIds;
    for (const QString& dir : EnvironmentPrefs::dataDirs()) {
      for (const XdgLauncher& launcher : recurseDir(QDir(dir).absoluteFilePath("applications"))) {
        QString id = launcher.fileId();
        if (!foundIds.contains(id)) {
          XL_cache << launcher;
          foundIds << id;
        }
      }
    }
  }
  return XL_cache;
}
