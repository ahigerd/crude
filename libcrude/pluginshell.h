#ifndef CRUDE_PLUGINSHELL_H
#define CRUDE_PLUGINSHELL_H

#include <QMap>
#include <QObject>

class PluginShellBase
{
protected:
  PluginShellBase();

public:
  virtual ~PluginShellBase();

  QStringList availablePlugins() const;
  QString activePlugin() const;
  bool setActivePlugin(const QString& name);
  inline QObject* pluginQObject() const { return _plugin; }

  static QStringList pluginPaths();

protected:
  void addStaticPlugin(QObject* plug);
  void findAllPlugins() const;
  void findPlugins(const QString& path) const;
  void selectDefaultPlugin();
  QObject* loadPlugin(const QString& path) const;
  virtual QString checkPlugin(QObject* plug) const = 0;
  virtual void setActivePlugin(QObject* p) = 0;
  virtual void disablePlugin() = 0;

  virtual QString pluginPrefix() const = 0;
  virtual QStringList pluginPriority() const = 0;
  virtual QString noPluginName() const = 0;

  QObject* _plugin;
  mutable QMap<QString, QObject*> _plugins;
  mutable QMap<QString, QString> _pluginPaths;
  mutable bool scanComplete;
};

template <typename PLUGIN>
class PluginShell : public PluginShellBase
{
public:
  virtual ~PluginShell()
  {
    if (_plugin) {
      plugin()->setEnabled(false);
    }
  }

  using PluginShellBase::setActivePlugin;

  void addStaticPlugin(PLUGIN* plug)
  {
    PluginShellBase::addStaticPlugin(dynamic_cast<QObject*>(plug));
  }

protected:
  PluginShell() : PluginShellBase()
  {
    // initializers only
  }

  QString checkPlugin(QObject* obj) const
  {
    PLUGIN* plugin = qobject_cast<PLUGIN*>(obj);
    if (!plugin || !plugin->available()) {
      return QString();
    }
    return plugin->pluginName();
  }

  virtual void setActivePlugin(QObject* p)
  {
    disablePlugin();
    _plugin = p;
    plugin()->setEnabled(true);
  }

  void disablePlugin()
  {
    if (_plugin) {
      plugin()->setEnabled(false);
      _plugin = nullptr;
    }
  }

  PLUGIN* plugin() const { return qobject_cast<PLUGIN*>(_plugin); }

  PLUGIN* plugin(const QString& name) const { return qobject_cast<PLUGIN*>(_plugins.value(name)); }
};

#endif
