#include "threadhelper.h"

QSet<QThread*> TH_pendingThreads;

void waitForPendingThreadHelpers()
{
  for (QThread* thread : TH_pendingThreads) {
    thread->wait();
  }
  TH_pendingThreads.clear();
}
