#include "throttle.h"

Throttle::Throttle(Throttle&& other)
{
  timer = other.timer;
  other.timer = nullptr;
}

Throttle::Throttle(QTimer* timer) : timer(timer)
{
  timer->setSingleShot(true);
  timer->setInterval(100);
}

Throttle Throttle::connectedTo(QObject* obj, const char* slot)
{
  QTimer* timer = new QTimer();
  QObject::connect(timer, SIGNAL(timeout()), obj, slot);
  return Throttle(timer);
}

Throttle& Throttle::timeout(int msec)
{
  timer->setInterval(msec);
  return *this;
}

void Throttle::trigger()
{
  if (!timer->isActive()) {
    timer->start();
  }
}
