#ifndef CRUDE_XDGMENU_H
#define CRUDE_XDGMENU_H

#include "xdglauncher.h"
#include "xdgmenu_p.h"

#include <QList>
#include <QPoint>
#include <QPointer>
#include <QSharedDataPointer>
#include <QString>

class XdgMenuPrivate;
class XdgMenu;
class XdgQMenu;
class QMenu;

struct XdgMenuItem
{
  XdgMenuItem();
  XdgMenuItem(XdgLauncher* item);
  XdgMenuItem(XdgMenu* submenu);
  XdgMenuItem(const XdgMenuItem& other) = default;
  XdgMenuItem(XdgMenuItem&& other) = default;

  XdgMenuItem& operator=(const XdgMenuItem& other) = default;

  QSharedPointer<XdgLauncher> item;
  QSharedPointer<XdgMenu> submenu;

  QString id() const;
  bool isValid() const;

  inline operator bool() const { return isValid(); }
};

class XdgMenu
{
public:
  static void refreshCache(bool force = true);
  static XdgMenu mainMenu();

  XdgMenu();
  XdgMenu(const XdgMenu& other) = default;
  XdgMenu(XdgMenu&& other) = default;
  XdgMenu(const QString& filename);

  XdgMenu& operator=(const XdgMenu& other) = default;
  XdgMenu& operator=(XdgMenu&& other) = default;

  QMenu* menu(QWidget* parent = nullptr) const;
  QString name() const;
  QString displayName() const;
  QIcon icon() const;
  int itemCount() const;
  QList<XdgMenuItem> items() const;
  void setItems(const QList<XdgMenuItem>& items);

  void debug(int depth = 0) const;

private:
  friend class XdgMenuPrivate;
  friend class XdgQMenu;

  XdgQMenu* unpopulatedMenu(QWidget* parent = nullptr) const;

  QSharedDataPointer<XdgMenuPrivate> d;
  bool topLevel;
  mutable QPointer<XdgQMenu> _menu;
};

#endif
