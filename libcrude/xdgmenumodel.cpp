#include "xdgmenumodel.h"

#include <QStack>
#include <QtDebug>

#define ROOT_ID 0xFFFFFFFF

XdgMenuModel::XdgMenuModel(QObject* parent) : XdgMenuModel(XdgMenu::mainMenu(), parent)
{
  // forwarded constructor only
}

XdgMenuModel::XdgMenuModel(const XdgMenu& root, QObject* parent) : QAbstractItemModel(parent), root(new XdgMenu(root))
{
  parents[0] = ROOT_ID;
  children[ROOT_ID] = 0;
}

int XdgMenuModel::columnCount(const QModelIndex& /*parent*/) const
{
  return 1;
}

int XdgMenuModel::rowCount(const QModelIndex& parent) const
{
  const XdgMenuItem& item = menuItem(parent);
  if (item.submenu.isNull()) {
    return 0;
  }
  return item.submenu->itemCount();
}

QModelIndex XdgMenuModel::index(int row, int column, const QModelIndex& parent) const
{
  if (column != 0 || (parent.isValid() && parent.model() != this)) {
    return QModelIndex();
  }
  quint64 parentInternalId = parent.isValid() ? parent.internalId() : ROOT_ID;
  const XdgMenuItem& parentItem = (parent.isValid() ? menuItem(parent) : root);
  if (parentItem.submenu.isNull()) {
    // This shouldn't be possible unless something is very wrong
    return QModelIndex();
  }
  if (row < 0 || row > parentItem.submenu->itemCount()) {
    return QModelIndex();
  }
  quint64 lookup = quint64(parentInternalId & 0xFFFFFFFF) << 32 | row;
  quint32 id = children.value(lookup);
  if (!id) {
    id = children.size();
    children[lookup] = id;
    parents[id] = lookup;
  }
  return createIndex(row, 0, id);
}

QModelIndex XdgMenuModel::parent(const QModelIndex& child) const
{
  if (!child.isValid()) {
    return QModelIndex();
  }
  quint64 lookup = parents.value(child.internalId());
  quint32 parentId = lookup >> 32;
  if (parentId == ROOT_ID) {
    return QModelIndex();
  }
  quint64 grandparent = parents.value(parentId);
  quint32 row = quint32(grandparent & 0xFFFFFFFF);
  return createIndex(row, 0, parentId);
}

Qt::ItemFlags XdgMenuModel::flags(const QModelIndex& index) const
{
  Qt::ItemFlags f = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
  if (!menuItem(index).submenu) {
    f |= Qt::ItemNeverHasChildren;
  }
  return f;
}

QVariant XdgMenuModel::data(const QModelIndex& index, int role) const
{
  XdgMenuItem item = menuItem(index);
  if (!item) {
    return QVariant();
  } else if (role == Qt::UserRole) {
    return item.id();
  } else if (item.submenu) {
    const XdgMenu& menu = *item.submenu;
    switch (role) {
    case Qt::DisplayRole: return menu.displayName();
    case Qt::DecorationRole: return menu.icon();
    case Qt::UserRole + 1: return menu.name();
    default: return QVariant();
    }
  } else {
    const XdgLauncher& launcher = *item.item;
    switch (role) {
    case Qt::DisplayRole: return launcher.name();
    case Qt::ToolTipRole: return launcher.genericName();
    case Qt::DecorationRole: return launcher.icon();
    case Qt::UserRole + 2: return launcher.execPath();
    default: return QVariant();
    }
  }
}

XdgMenuItem XdgMenuModel::menuItem(const QModelIndex& index) const
{
  if (!index.isValid()) {
    return root;
  }
  QStack<int> stack;
  QModelIndex iter = index;
  while (iter.isValid()) {
    if (iter == iter.parent()) {
      throw "error";
    }
    stack.push(iter.row());
    iter = iter.parent();
  }
  XdgMenuItem item = root;
  while (!stack.isEmpty()) {
    item = item.submenu->items()[stack.pop()];
  }
  return item;
}
