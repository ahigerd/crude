#include "acpimonitor.h"

#include <QTimer>

static AcpiMonitor* AM_instance = nullptr;

AcpiMonitor* AcpiMonitor::instance()
{
  if (!AM_instance) {
    AM_instance = new AcpiMonitor();
  }
  return AM_instance;
}

AcpiMonitor::AcpiMonitor() : QObject(nullptr), PluginShell<BatteryPlugin>()
{
  AM_instance = this;
  QTimer::singleShot(0, this, SLOT(selectDefaultPlugin()));
}

void AcpiMonitor::setActivePlugin(QObject* p)
{
  if (_plugin) {
    QObject::disconnect(_plugin, 0, this, 0);
  }
  PluginShell<BatteryPlugin>::setActivePlugin(p);
  if (_plugin == p) {
    QObject::connect(_plugin, SIGNAL(lidChanged(bool)), this, SIGNAL(lidChanged(bool)));
    QObject::connect(_plugin, SIGNAL(plugChanged(bool)), this, SIGNAL(plugChanged(bool)));
    QObject::connect(_plugin, SIGNAL(batteryLevelChanged(int)), this, SIGNAL(batteryLevelChanged(int)));
    emit lidChanged(lidState());
    emit plugChanged(isPluggedIn());
    emit batteryLevelChanged(batteryLevel());
  }
}

bool AcpiMonitor::lidState() const
{
  if (!plugin()) {
    return true;
  }
  return plugin()->lidState();
}

bool AcpiMonitor::isCharging() const
{
  if (!plugin()) {
    return false;
  }
  return plugin()->isCharging();
}

bool AcpiMonitor::isPluggedIn() const
{
  if (!plugin()) {
    return true;
  }
  return plugin()->isPluggedIn();
}

bool AcpiMonitor::hasBattery() const
{
  if (!plugin()) {
    return false;
  }
  return plugin()->hasBattery();
}

int AcpiMonitor::batteryLevel() const
{
  if (!plugin() || !plugin()->hasBattery()) {
    return -1;
  }
  return plugin()->batteryLevel();
}

QTime AcpiMonitor::timeToEmpty() const
{
  if (!plugin()) {
    return QTime();
  }
  return plugin()->timeToEmpty();
}

QTime AcpiMonitor::timeToFull() const
{
  if (!plugin()) {
    return QTime();
  }
  return plugin()->timeToFull();
}

QString AcpiMonitor::pluginPrefix() const
{
  return "battery";
}

QStringList AcpiMonitor::pluginPriority() const
{
  return QStringList() << "UPower"
                       << "acpid";
}

QString AcpiMonitor::noPluginName() const
{
  return "None";
}
