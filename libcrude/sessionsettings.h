#ifndef CRUDE_SETTINGS_H
#define CRUDE_SETTINGS_H

#include "throttle.h"

#include <QSettings>
class ConfigNotifier;

struct SessionService
{
  QString settingsGroup;
  QString settingsKey;
  QString commandLine;
  QString stopCommand;
  bool respawnOnCrash;
  bool killWithSystray;
  bool logoutOnExit;
};

class Settings : public QObject
{
  Q_OBJECT

public:
  static Settings* instance();
  Settings();

  QStringList groups();
  QStringList appGroups() const;
  QStringList keys(const QString& group = QString());

  QVariant value(const QString& key);
  void setValue(const QString& key, const QVariant& value);
  void remove(const QString& key);

  SessionService getService(const QString& group, const QString& key);
  void saveService(const SessionService& service);
  void savePending();

public slots:
  void broadcastReload();
  void sync();

signals:
  void reloadSettings();

private slots:
  void unixSignal(int sig);
  void deferredReload();

private:
  QSettings globalSettings, localSettings, envSettings;
  QList<SessionService> needsSaved;
  Throttle reloadThrottle;
  ConfigNotifier* configNotifier;
};

#endif
