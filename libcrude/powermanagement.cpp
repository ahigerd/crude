#include "powermanagement.h"

#include <QMetaEnum>

QList<PowerManagement::Action> PowerManagement::allActions()
{
  static QList<Action> actions;
  if (actions.isEmpty()) {
    QMetaEnum e = QMetaEnum::fromType<Action>();
    for (int i = 0; i < e.keyCount(); i++) {
      actions << Action(e.value(i));
    }
  }
  return actions;
}

QString PowerManagement::actionName(PowerManagement::Action action)
{
  switch (action) {
  case PowerPlugin::HybridSleep: return "Hybrid Sleep";
  case PowerPlugin::SuspendThenHibernate: return "Suspend, then Hibernate";
  default: return QMetaEnum::fromType<Action>().valueToKey(action);
  }
}

static PowerManagement* PM_instance = nullptr;

PowerManagement* PowerManagement::instance()
{
  if (!PM_instance) {
    PM_instance = new PowerManagement();
  }
  return PM_instance;
}

PowerManagement::PowerManagement() : QObject(), PluginShell<PowerPlugin>()
{
  PM_instance = this;
  selectDefaultPlugin();
}

PowerManagement::~PowerManagement()
{
  PM_instance = nullptr;
}

bool PowerManagement::canPerform(PowerPlugin::Action action) const
{
  if (!plugin() || !plugin()->available()) {
    return false;
  }
  return plugin()->canPerform(action);
}

void PowerManagement::perform(PowerPlugin::Action action)
{
  if (!canPerform(action)) {
    return;
  }
  plugin()->perform(action);
}

QString PowerManagement::pluginPrefix() const
{
  return "power";
}

QStringList PowerManagement::pluginPriority() const
{
  return QStringList() << "systemd"
                       << "ConsoleKit"
                       << "Custom Commands";
}

QString PowerManagement::noPluginName() const
{
  return "No Power Management";
}
