#ifndef CRUDE_ENVIRONMENTPREFS_H
#define CRUDE_ENVIRONMENTPREFS_H

#include "threadhelper.h"

#include <QMutex>
#include <QPair>
#include <QSettings>
#include <QStringList>

class EnvironmentPrefs
{
public:
  using StringPair = QPair<QString, QString>;

  static QSettings::Format desktopFormat();
  static QSettings::Format directoryFormat();

  static QString getenv(const QString& variable, const QString& defaultValue = QString());

  static QStringList localePrecedence();
  static QString locale(bool includeCountry = true, bool includeModifier = true);
  static bool localeHasCountry();
  static bool localeHasModifier();

  static bool isSystemPath(const QString& path);

  static QStringList dataDirs();
  static QStringList systemDataDirs();
  static QString userDataDir();

  static QStringList configDirs();
  static QStringList systemConfigDirs();
  static QString userConfigDir();

  static QString detectedDesktop();
  static QStringList terminalCommand(const QString& run = QString());
  static QStringList availableTerminals();

  static void rescanTerminals(QObject* receiver, const char* slot);

private:
  static EnvironmentPrefs* instance();
  EnvironmentPrefs();

  void init();
  void initLocale();
  void initXdgPaths();
  void detectDesktop();
  void initTerminal(QObject* notify = nullptr, const char* slot = nullptr);

  QString localeFull, localeLang, localeCountry, localeModifier;
  QStringList localeList;

  QList<QString> xdgSystemData, xdgSystemConfig;
  QString xdgUserData, xdgUserConfig;

  ThreadResult<StringPair> termResult;
  QString term, termFlags;
  QString deskEnv;
};

#endif
