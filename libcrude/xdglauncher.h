#ifndef CRUDE_XDGLAUNCHER_H
#define CRUDE_XDGLAUNCHER_H

#include <QIcon>
#include <QMap>
#include <QMetaType>
#include <QSharedPointer>
#include <QStringList>

class XdgLauncher
{
public:
  static void refreshCache();
  static QList<XdgLauncher> allItems();
  static XdgLauncher byId(const QString& id);

  static QStringList paths();
  static QString defaultTerminal();

  enum Type {
    Application,
    Link,
    Directory,
    Unknown
  };

  XdgLauncher();
  XdgLauncher(const QString& path);
  XdgLauncher(const QString& path, bool system);
  XdgLauncher(const XdgLauncher& other) = default;
  XdgLauncher(XdgLauncher&& other) = default;

  XdgLauncher& operator=(const XdgLauncher& other) = default;
  XdgLauncher& operator=(XdgLauncher&& other) = default;

  bool save();

  bool isValid() const;
  bool isSystemFile() const;
  QString fileId() const;
  QString path() const;
  void setPath(const QString& path);
  void setPath(const QString& path, bool system);

  Type type() const;
  void setType(Type type);

  bool isHidden() const;
  void setHidden(bool on);

  QString name() const;
  void setName(const QString& name, const QString& locale = QString());

  QString genericName() const;
  void setGenericName(const QString& name, const QString& locale = QString());

  QIcon icon() const;
  QString iconPath() const;
  void setIconPath(const QString& path);
  void setIconPath(const QStringList& paths);

  QStringList categories() const;
  void setCategories(const QStringList& cats);

  bool isTerminal() const;
  void setTerminal(bool on);

  QStringList extensions() const;
  QString extension(const QString& ext) const;
  void setExtension(const QString& ext, const QString& value);

  bool exec(bool forwardOutput = false) const;
  QString execPath() const;
  void setExecPath(const QString& path);

  QString workingDirectory() const;
  void setWorkingDirectory(const QString& path);

private:
  bool tryExec() const;

  struct BackingGroup
  {
    QString get(const QString& key) const;
    QString getLocale(const QString& key) const;
    bool getBool(const QString& key) const;
    void set(const QString& key, const QString& value, bool localized = false);
    void set(const QString& key, const QString& value, const QString& locale);

    QMap<QString, QString> keys;
  };

  struct Data
  {
    QString path;
    bool system;
    mutable QIcon icon;

    QMap<QString, BackingGroup> backing;
    BackingGroup entry;
  };

  QSharedPointer<Data> d;
};

Q_DECLARE_METATYPE(XdgLauncher);

#endif
