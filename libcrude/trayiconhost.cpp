#include "systemtray_p.h"
#include "xcbintegration.h"
#include "xcbrequest_fixes.h"
#include "xcbrequest_wm.h"
#include "xcbrequest_prop.h"
#include <QWindow>
#include <QPainter>
#include <QTimer>
#include <QDateTime>
#include <QPointer>

TrayIconHost::TrayIconHost(uint32_t winId, bool useDamage)
: QWidget(nullptr), id(winId), lastDamage(0)
{
  setMinimumSize(22, 22);
  if (!useDamage) {
    lastDamage = 0xFFFFFFFFFFFFFFFFULL;
  }

  validateTimer.setInterval(250);
  validateTimer.setSingleShot(true);
  QObject::connect(&validateTimer, SIGNAL(timeout()), this, SLOT(doValidate()));
}

TrayIconHost::TrayIconHost(QWidget* icon)
: QWidget(nullptr), id(0), damageId(0), lastDamage(0xFFFFFFFFFFFFFFFFULL)
{
  setMinimumSize(22, 22);
  icon->setParent(this);
  icon->setGeometry(rect());
  icon->show();
  widget = icon;

  QObject::connect(icon, SIGNAL(destroyed(QObject*)), this, SLOT(onWidgetDestroyed()));
}

TrayIconHost::~TrayIconHost()
{
  if (damageId) {
    callDamageDestroy(damageId);
  }
}

void TrayIconHost::damaged()
{
  // maybe composite later
  bool ok = callDamageSubtract(damageId)->force();
  if (!ok) {
    // Assume icon was destroyed
    damageId = 0;
    emit removed(this);
    return;
  }
  uint64_t now = QDateTime::currentMSecsSinceEpoch();
  if (now < lastDamage + 100) {
    return;
  }
  lastDamage = now;
  callXcbUnmapWindow(id)->force();
  int _id = id;
  QTimer::singleShot(1, [_id]{ callXcbMapWindow(_id)->force(); });
}

void TrayIconHost::moveEvent(QMoveEvent*)
{
  if (id) {
    ensureMapped();
  } else if (widget) {
    widget->setGeometry(rect());
  }
}

void TrayIconHost::remap()
{
  if (!id) {
    return;
  }
  if (damageId) {
    callDamageDestroy(damageId);
  }
  if (lastDamage != 0xFFFFFFFFFFFFFFFFULL) {
    damageId = xcb_generate_id(c_X11);
    callDamageCreate(damageId, id, XCB_DAMAGE_REPORT_LEVEL_NON_EMPTY);
  }
  title = callEwmhGetName(id)->result();
  auto names = callIcccmGetWmClass(id)->result();
  className = names.class_name;

  ensureMapped();
}

void TrayIconHost::ensureMapped()
{
  if (!id) {
    return;
  }
  bool ok = callXcbMapWindow(id)->force();
  if (ok) {
    updateIconGeometry();
  } else {
    emit removed(this);
  }
}

void TrayIconHost::resizeEvent(QResizeEvent*)
{
  moveEvent(nullptr);
}

void TrayIconHost::showEvent(QShowEvent*)
{
  moveEvent(nullptr);
}

void TrayIconHost::onWidgetDestroyed()
{
  emit removed(this);
}

void TrayIconHost::validate()
{
  validateTimer.start();
}

bool TrayIconHost::doValidate()
{
  if (!callXcbGetAttributes(id)->force()) {
    emit removed(this);
    return false;
  }
  return true;
}

bool TrayIconHost::hasHeightForWidth() const
{
  return true;
}

int TrayIconHost::heightForWidth(int w) const
{
  return w;
}

void TrayIconHost::updateIconGeometry()
{
  int _id = id;
  QRect geom = geometry();
  int diff = geom.height() - geom.width();
  if (diff > 0) {
    geom.setHeight(geom.width());
    geom.moveTop(geom.top() + diff / 2);
  } else if (diff < 0) {
    geom.setWidth(geom.height());
    geom.moveLeft(geom.left() + diff / -2);
  }
  QPointer<TrayIconHost> self(this);
  QTimer::singleShot(1, [_id, geom, self]{
    bool ok = callXcbSetGeometry(_id, geom)->force();
    if (!ok && self) {
      self->validate();
    }
  });
}
