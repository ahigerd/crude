#ifndef CRUDE_SYSTEMTRAY_H
#define CRUDE_SYSTEMTRAY_H

#include <QFrame>
#include <QIcon>
#include <QPointer>
class SystemTrayPrivate;
class TrayIconHost;

class SystemTray : public QFrame
{
  Q_OBJECT
  friend class SystemTrayPrivate;
  friend class XcbSystemTrayPrivate;

public:
  SystemTray(QWidget* parent = nullptr);
  ~SystemTray();

  Qt::Orientation orientation() const;
  void setOrientation(Qt::Orientation o);

  void showMessage(const QString& title, const QString& message, const QPoint& pos);

  void addWidget(QWidget* icon);
  void removeWidget(QWidget* icon);
  void validate();

signals:
  void acquired();
  void unavailable();
  void revoked();

protected slots:
  void removeIcon(TrayIconHost* icon);

protected:
  void resizeEvent(QResizeEvent* event);
  void addIcon(TrayIconHost* icon);

  SystemTrayPrivate* d;
  QList<TrayIconHost*> icons;
  QHash<uint32_t, TrayIconHost*> iconsById;
  QHash<QWidget*, TrayIconHost*> iconsByWidget;
  bool isVertical;
  bool shuttingDown;
};

#endif
