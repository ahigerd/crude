#ifndef CRUDE_PROCESSINFO_H
#define CRUDE_PROCESSINFO_H

#include <QStringList>

#include <unistd.h>

class ProcessInfo
{
public:
  ProcessInfo();
  ProcessInfo(pid_t pid);
  ProcessInfo(const ProcessInfo& other) = default;

  bool isValid() const;
  pid_t pid() const;
  QString executable() const;
  QString commandName() const;
  QStringList commandLine() const;

private:
  QString procPath() const;
  pid_t _pid;
  bool valid;
};

#endif
