#ifndef CRUDE_ACPIMONITOR_H
#define CRUDE_ACPIMONITOR_H

#include "plugins/batteryplugin.h"
#include "pluginshell.h"

#include <QMap>
#include <QObject>
#include <QTime>

class AcpiMonitor : public QObject, public PluginShell<BatteryPlugin>
{
  Q_OBJECT

public:
  static AcpiMonitor* instance();
  AcpiMonitor();

  using PluginShell<BatteryPlugin>::setActivePlugin;
  bool lidState() const;
  bool isCharging() const;
  bool isPluggedIn() const;
  bool hasBattery() const;
  int batteryLevel() const;
  QTime timeToEmpty() const;
  QTime timeToFull() const;

signals:
  void lidChanged(bool);
  void plugChanged(bool);
  void batteryLevelChanged(int percent);

#ifdef Q_MOC_RUN
protected slots:
  void selectDefaultPlugin();
#endif

protected:
  virtual void setActivePlugin(QObject* p);
  virtual QString pluginPrefix() const;
  virtual QStringList pluginPriority() const;
  virtual QString noPluginName() const;
};

#endif
