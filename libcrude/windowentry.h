#ifndef CRUDE_WINDOWENTRY_H
#define CRUDE_WINDOWENTRY_H

#include <QFlags>
#include <QIcon>
#include <QSharedPointer>
#include <QString>

class WindowEntry
{
public:
  enum IconSource {
    NoIcon,
    EwmhIcon,
    IcccmIcon,
    ThemeIcon,
  };

  enum WindowAction {
    MoveAction = 1,
    ResizeAction = 2,
    MinimizeAction = 4,
    ShadeAction = 8,
    StickAction = 16,
    HMaximizeAction = 32,
    VMaximizeAction = 64,
    MaximizeAction = HMaximizeAction | VMaximizeAction,
    FullscreenAction = 128,
    ChangeDesktopAction = 256,
    CloseAction = 512,
    AlwaysOnTopAction = 1024,
    AlwaysOnBottomAction = 2048,
    RestoreAction = MinimizeAction | MaximizeAction
  };
  Q_DECLARE_FLAGS(WindowActions, WindowAction);

  explicit WindowEntry(uint32_t window);
  WindowEntry(const WindowEntry& other) = default;
  WindowEntry(WindowEntry&& other) = default;

  uint32_t windowId;
  uint32_t baseWindowId;
  uint32_t groupLeaderId;
  uint32_t pid;
  uint32_t creationOrder;
  uint32_t lastFocused;
  bool isClient;
  bool urgent;
  QString name;
  QString hostname;
  QString program;
  QString instanceClass;
  QString windowClass;
  QIcon icon;
  IconSource iconSource;
  WindowActions actions;

  bool isLocal() const;

  inline bool isRemote() const { return !isLocal(); }

  WindowEntry& operator=(const WindowEntry& other) = default;
  WindowEntry& operator=(WindowEntry&& other) = default;
  bool operator==(const WindowEntry& other) const;
  bool operator!=(const WindowEntry& other) const;
};

typedef QSharedPointer<WindowEntry> WindowRef;
typedef QList<WindowRef> WindowRefList;
typedef QHash<uint32_t, WindowRef> WindowRefMap;
Q_DECLARE_OPERATORS_FOR_FLAGS(WindowEntry::WindowActions);

#endif
