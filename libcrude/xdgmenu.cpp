#include "xdgmenu.h"

#include "environmentprefs.h"
#include "platformintegration.h"
#include "threadhelper.h"
#include "throttle.h"

#include <QApplication>
#include <QDir>
#include <QFile>
#include <QFileSystemWatcher>
#include <QMenu>
#include <QSharedData>
#include <QXmlStreamReader>
#include <QtDebug>

// While QDomDocument would probably be easier in code, QXmlStreamReader is
// faster, uses less memory, and most importantly doesn't add a dependency
// on QtXml or QtXmlPatterns.

static bool XM_cacheReady = false;
static ThreadResult<XdgMenu> XM_cache;
static QFileSystemWatcher* fsw = nullptr;
static Throttle fswThrottle(Throttle::connectedTo([] { XdgMenu::refreshCache(); }));
static QPointer<XdgQMenu> XM_topMenu(nullptr);

static const QMap<QString, QStringList> mainCategories = {
  { "AudioVideo", { "applications-multimedia" } },
  { "Development", { "applications-development", "applications-engineering" } },
  { "Education", { "applications-science" } },
  { "Game", { "applications-games" } },
  { "Graphics", { "applications-graphics" } },
  { "Network", { "applications-internet", "preferences-system-network" } },
  { "Office", { "applications-office", "accessories-text-editor" } },
  { "Science", { "applications-science" } },
  { "Settings", { "preferences-other" } },
  { "System", { "applications-system" } },
  { "Utility", { "applications-utilities" } },
};

static QMap<QString, QString> categoryIconCache;

static QString iconPathForCategory(const QString& category)
{
  QString n = category.toLower();
  if (categoryIconCache.contains(n)) {
    return categoryIconCache[n];
  }
  QStringList prefixes{ "applications-", "preferences-system-", "" };
  for (const QString& iconName : QStringList({ n, n + "s", n + "es", n.left(n.length() - 1) + "ies" })) {
    for (const QString& prefix : prefixes) {
      QIcon icon = c_PI->icon({ prefix + iconName });
      if (!icon.isNull()) {
        categoryIconCache[n] = prefix + iconName;
        return prefix + iconName;
      }
    }
  }
  categoryIconCache[n] = QString();
  return QString();
}

void XdgMenu::refreshCache(bool force)
{
  if (XM_cacheReady && !force) {
    return;
  }
  if (!fsw) {
    fsw = new QFileSystemWatcher(qApp);
    // Don't refresh cache based on filesystem changes more often than once every 15 seconds.
    fswThrottle.timeout(15000);
    QObject::connect(fsw, SIGNAL(fileChanged(QString)), &fswThrottle, SLOT(trigger()));
    QObject::connect(fsw, SIGNAL(directoryChanged(QString)), &fswThrottle, SLOT(trigger()));
  }
  QString dePrefix = EnvironmentPrefs::getenv("XDG_MENU_PREFIX");
  QString menuPath;
  for (const QString& path : EnvironmentPrefs::configDirs()) {
    QDir dir(path);
    if (dir.exists(dePrefix + "applications.menu")) {
      menuPath = dir.absoluteFilePath(dePrefix + "applications.menu");
    }
  }

  XM_cacheReady = true;
  XM_cache = ThreadHelper<XdgMenu>::run([menuPath]() -> XdgMenu {
    XdgMenu cache;
    cache.topLevel = true;
    XdgLauncher::refreshCache();

    if (menuPath.isEmpty()) {
      // no predefined menus were found, so let's synthesize something
      QMap<QString, XdgMenu*> submenus;
      for (const QString& cat : mainCategories.keys()) {
        XdgMenu* m = new XdgMenu();
        XdgMenuPrivate* md = m->d;
        md->name = cat;
        md->directory.setIconPath(mainCategories[cat]);
        md->deleted = true;
        submenus[cat] = m;
        cache.d->items << m;
      }
      XdgMenu* other = new XdgMenu();
      XdgMenuPrivate* od = other->d;
      od->name = "Other";
      od->deleted = true;
      od->directory.setIconPath({ "applications-other", "application-x-executable-symbolic" });
      cache.d->items << other;
      for (const XdgLauncher& launcher : XdgLauncher::allItems()) {
        fsw->addPath(QFileInfo(launcher.path()).absolutePath());
        if (launcher.isHidden()) {
          continue;
        }
        bool foundCat = false;
        for (const QString& cat : launcher.categories()) {
          if (submenus.contains(cat)) {
            submenus[cat]->d->items << new XdgLauncher(launcher);
            submenus[cat]->d->deleted = false;
            foundCat = true;
            break;
          }
        }
        if (!foundCat) {
          // meow
          od->items << new XdgLauncher(launcher);
          od->deleted = false;
        }
      }
    } else {
      cache.d->merge(menuPath);
    }
    return cache;
  });
  if (XM_topMenu) {
    QObject::connect(XM_cache.thread(), SIGNAL(finished()), XM_topMenu, SLOT(deleteLater()));
  }
}

XdgMenu XdgMenu::mainMenu()
{
  if (!XM_cacheReady) {
    refreshCache();
  }
  XdgQMenu* qmenu = qobject_cast<XdgQMenu*>(XM_cache.result().menu());
  if (qmenu) {
    qmenu->populate();
  }
  return XM_cache.result();
}

XdgMenu::XdgMenu() : d(new XdgMenuPrivate), topLevel(false), _menu(nullptr)
{
  d->deleted = false;
  d->notDeleted = false;
}

XdgMenu::XdgMenu(const QString& filename) : XdgMenu()
{
  d->merge(filename);
  if (d->directory.icon().isNull()) {
    d->directory.setIconPath(iconPathForCategory(d->name));
  }
}

XdgQMenu* XdgMenu::unpopulatedMenu(QWidget* parent) const
{
  if (d->deleted || d->items.isEmpty()) {
    return nullptr;
  }
  if (topLevel && !_menu) {
    _menu = XM_topMenu.data();
  }
  if (!_menu) {
    _menu = new XdgQMenu(*this, parent);
    if (topLevel) {
      XM_topMenu = _menu;
    }
  }
  return _menu;
}

QMenu* XdgMenu::menu(QWidget* parent) const
{
  XdgQMenu* m = unpopulatedMenu(parent);
  if (m) {
    m->populate();
  }
  return m;
}

QString XdgMenu::name() const
{
  return d->name;
}

QString XdgMenu::displayName() const
{
  if (d->directory.name().isEmpty()) {
    return d->name;
  }
  return d->directory.name();
}

QIcon XdgMenu::icon() const
{
  return d->directory.icon();
}

int XdgMenu::itemCount() const
{
  return d->items.count();
}

QList<XdgMenuItem> XdgMenu::items() const
{
  return d->items;
}

void XdgMenu::setItems(const QList<XdgMenuItem>& items)
{
  d->items = items;
}

void XdgMenu::debug(int depth) const
{
  for (const XdgMenuItem& item : d->items) {
    QString prefix = QString(depth * 2, ' ') + item.id();
    if (item.submenu) {
      if (item.submenu->d->deleted) {
        continue;
      }
      qDebug() << qPrintable(prefix) << item.submenu->d->directory.fileId();
      item.submenu->debug(depth + 1);
    } else {
      qDebug() << qPrintable(prefix);
    }
  }
}

bool XdgMenuPrivate::merge(const QString& filename)
{
  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly)) {
    return false;
  }

  fsw->addPath(filename);
  QXmlStreamReader xml(&file);
  while (!xml.atEnd() && !xml.hasError()) {
    auto type = xml.readNext();
    if (type == QXmlStreamReader::StartElement && xml.name() == "Menu") {
      return merge(xml);
    }
  }
  return false;
}

bool XdgMenuPrivate::merge(QXmlStreamReader& xml)
{
  QStringList stack;
  stack << xml.name().toString();
  while (!xml.atEnd() && !xml.hasError()) {
    auto type = xml.readNext();
    if (type == QXmlStreamReader::StartElement) {
      QString name = xml.name().toString();
      if (name == "Menu") {
        XdgMenu* submenu = new XdgMenu();
        bool ok = submenu->d->merge(xml);
        if (!ok) {
          delete submenu;
          return false;
        }

        bool merged = false;
        for (XdgMenuItem& item : items) {
          if (item.submenu && item.submenu->name() == submenu->name()) {
            item.submenu->d->merge(*submenu);
            merged = true;
            delete submenu;
            break;
          }
        }
        if (!merged) {
          items << submenu;
        }
        continue;
      }
      stack << name;
    } else if (type == QXmlStreamReader::EndElement) {
      if (stack.last() == "All") {
        if (stack.contains("Include")) {
          for (const XdgLauncher& launcher : XdgLauncher::allItems()) {
            addLauncher(launcher);
          }
        } else if (stack.contains("Exclude")) {
          items.clear();
        } else {
          // Invalid XML
          return false;
        }
      } else if (stack.last() == "Deleted") {
        deleted = true;
        notDeleted = false;
      } else if (stack.last() == "NotDeleted") {
        deleted = false;
        notDeleted = true;
      }
      stack.removeLast();
      if (stack.isEmpty()) {
        return true;
      }
    } else if (type == QXmlStreamReader::Characters) {
      QString contents = xml.text().trimmed().toString();
      if (stack.last() == "Name") {
        name = contents;
      } else if (stack.last() == "Directory") {
        directory = XdgLauncher(contents);
      } else if (stack.last() == "Filename") {
        // TODO: full match logic
        if (stack.contains("Include")) {
          addLauncher(XdgLauncher::byId(contents));
        } else if (stack.contains("Exclude")) {
          removeLauncher(contents);
        } else {
          return false;
        }
      } else if (stack.last() == "Category") {
        bool isInclude = stack.contains("Include");
        if (!isInclude && !stack.contains("Exclude")) {
          // Invalid XML
          return false;
        }
        for (const XdgLauncher& launcher : XdgLauncher::allItems()) {
          if (launcher.categories().contains(contents)) {
            if (isInclude) {
              addLauncher(launcher);
            } else {
              removeLauncher(launcher.fileId());
            }
          }
        }
      }
    }
  }

  return true;
}

void XdgMenuPrivate::merge(const XdgMenu& other)
{
  if (other.d->deleted) {
    deleted = true;
    notDeleted = false;
  } else if (other.d->notDeleted) {
    deleted = false;
    notDeleted = true;
  }
  for (const XdgMenuItem& otherItem : other.d->items) {
    bool merged = false;
    for (XdgMenuItem& myItem : items) {
      if (otherItem.id() == myItem.id()) {
        if (otherItem.item) {
          myItem.item = otherItem.item;
        } else if (otherItem.submenu) {
          myItem.submenu->d->merge(*otherItem.submenu);
        }
        merged = true;
        break;
      }
    }
    if (!merged) {
      items << otherItem;
    }
  }
}

void XdgMenuPrivate::addLauncher(const XdgLauncher& launcher)
{
  bool found = false;
  QString launcherId = "i*" + launcher.fileId();
  for (const XdgMenuItem& item : items) {
    if (item.id() == launcherId) {
      found = true;
      break;
    }
  }
  if (!found) {
    items << new XdgLauncher(launcher);
  }
}

void XdgMenuPrivate::removeLauncher(const QString& id)
{
  for (int i = items.length() - 1; i >= 0; --i) {
    XdgMenuItem* item = &items[i];
    if (item->item && item->item->fileId() == id) {
      items.removeAt(i);
    }
  }
}

XdgMenuItem::XdgMenuItem() : item(nullptr), submenu(nullptr) {}

XdgMenuItem::XdgMenuItem(XdgLauncher* item) : item(item), submenu(nullptr) {}

XdgMenuItem::XdgMenuItem(XdgMenu* submenu) : item(nullptr), submenu(submenu) {}

QString XdgMenuItem::id() const
{
  if (item) {
    return "i*" + item->fileId();
  }
  if (submenu) {
    return "m*" + submenu->name();
  }
  return QString();
}

bool XdgMenuItem::isValid() const
{
  return item || submenu;
}

XdgQMenu::XdgQMenu(const XdgMenu& menu, QWidget* parent) : QMenu(parent), d(menu.d), populated(false)
{
  setTitle(menu.displayName());
  setIcon(menu.icon());
  QObject::connect(this, SIGNAL(aboutToShow()), this, SLOT(populate()));
}

void XdgQMenu::populate()
{
  if (!populated) {
    for (const XdgMenuItem& item : d->items) {
      if (item.item) {
        QAction* action = addAction(item.item->name());
        c_PI->lazyIcon(action, item.item->iconPath());
        action->setData(QVariant::fromValue(*item.item));
      } else if (item.submenu && !item.submenu->d->deleted) {
        QMenu* submenu = item.submenu->unpopulatedMenu(this);
        if (submenu) {
          QObject::connect(submenu, SIGNAL(updated()), this, SIGNAL(updated()));
          addMenu(submenu);
        }
      }
    }
    emit updated();
    populated = true;
  }
}
