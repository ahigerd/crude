#ifndef CRUDE_WINDOWLIST_H
#define CRUDE_WINDOWLIST_H

#include "windowentry.h"

#include <QAbstractItemModel>
#include <QFlags>
#include <QList>

class WindowList : public QAbstractItemModel
{
  Q_OBJECT

public:
  WindowList(QObject* parent = nullptr);

  enum Role {
    WindowIDRole = Qt::UserRole,
    BaseWindowIDRole = Qt::UserRole + 1,
    WindowTitleRole = Qt::UserRole + 2,
    CreationOrderRole = Qt::UserRole + 3,
    FocusOrderRole = Qt::UserRole + 4,
  };

  enum UrgentOption {
    NoUrgency = 0,
    UrgentText = 1,
    UrgentCheck = 2,
  };

  Q_ENUM(UrgentOption);
  Q_DECLARE_FLAGS(UrgentFlags, UrgentOption);

  UrgentFlags urgentFlags() const;
  void setUrgentFlags(UrgentFlags flags);

  enum GroupingOption {
    GroupMask = 31,

    NoGrouping = 0,
    EnableGrouping = 16,
    GroupByProcess = EnableGrouping | 1,
    GroupByProgram = EnableGrouping | 2,
    GroupByClass = EnableGrouping | 4,
    GroupByInstance = EnableGrouping | 8,
    GroupAggressively = GroupMask,
    NoSingleGroups = 256,
  };

  Q_ENUM(GroupingOption);
  Q_DECLARE_FLAGS(GroupingMode, GroupingOption);

  enum LabelText {
    WindowTitle,
    ProgramName,
    NoText,
  };

  Q_ENUM(LabelText);

  GroupingMode groupingMode() const;
  void setGroupingMode(GroupingMode mode);
  static QByteArray groupingModeToString(GroupingMode mode);
  static GroupingMode stringToGroupingMode(const QByteArray& mode);

  LabelText labelText() const;
  void setLabelText(LabelText text);

  bool showIcons() const;
  void setShowIcons(bool on);

  bool showRemoteHosts() const;
  void setShowRemoteHosts(bool on);

  const WindowRefList& windowList() const;
  WindowRef currentWindow() const;

  Qt::ItemFlags flags(const QModelIndex& index) const;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
  QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;
  int rowCount(const QModelIndex& parent = QModelIndex()) const;
  int columnCount(const QModelIndex& parent = QModelIndex()) const;
  QModelIndex index(WindowRef entry) const;
  QModelIndex index(int row, int col, const QModelIndex& parent = QModelIndex()) const;
  QModelIndex parent(const QModelIndex& child) const;
  QModelIndex activeChild(const QModelIndex& group) const;

signals:
  void windowListChanged(const WindowRefList& windowList);
  void currentWindowChanged(WindowRef window);
  void currentWindowChanged(const QModelIndex& index);
  void windowChanged(WindowRef window);

private slots:
  void updateWindowList();
  void fullWindowList(const WindowRefList&);
  void updateWindow(WindowRef);
  void updateCurrentWindow(WindowRef);
  void flashUrgent();

private:
  WindowRefList windows;

  struct WindowGroup : public WindowRefList
  {
    WindowRef leader;
    WindowRef lastActive;
    uint32_t creationOrder;
  };

  QList<WindowGroup> groups;
  WindowRef current;
  GroupingMode groupMode;
  UrgentFlags urgentMode;
  uint32_t nextFocusOrder;
  bool showHost;
  bool urgentFlash;
  bool icons;
  LabelText labelType;

  void regroupWindows();
  void emitDataChanged(WindowRef entry);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(WindowList::UrgentFlags);
Q_DECLARE_OPERATORS_FOR_FLAGS(WindowList::GroupingMode);

#endif
