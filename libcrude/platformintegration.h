#ifndef CRUDE_PLATFORMINTEGRATION_H
#define CRUDE_PLATFORMINTEGRATION_H

#include "qpointerset.h"
#include "windowentry.h"

#include <QIcon>
#include <QList>
#include <QMap>
#include <QObject>

#include <memory>
class QSocketNotifier;
class QWidget;
class QProcess;
class QAction;
class DisplayPower;

class PlatformIntegration : public QObject
{
  Q_OBJECT

public:
  static PlatformIntegration* instance();
  virtual ~PlatformIntegration();

  struct Strut
  {
    WId window;
    QRect geometry;
  };

  enum Platform {
    Auto,
    X11
  };

  static std::unique_ptr<PlatformIntegration> create(Platform platform, int argc, char** argv);
  static std::unique_ptr<PlatformIntegration> create(int argc, char** argv);

  bool runCommand(const QString& cmd, const QStringList& argv = QStringList(), const QString& cwd = QString(), bool forwardOutput = true);
  inline bool runCommand(const QString& cmd, bool forwardOutput) { return runCommand(cmd, QStringList(), QString(), forwardOutput); }

  bool isDebugEnabled() const;
  void setDebugEnabled(bool on);

  virtual void enableWindowList() = 0;

  virtual bool windowManagerExists() = 0;
  virtual bool desktopManagerExists() = 0;

  virtual QList<Strut> findStruts() = 0;

  virtual DisplayPower* displayPower() = 0;

  QIcon icon(const QStringList& names, int fallback = -1) const;
  QIcon icon(const QString& name, int fallback = -1) const;
  void lazyIcon(QAction* action, const QStringList& names, int fallback = -1) const;
  void lazyIcon(QAction* action, const QString& name, int fallback = -1) const;

  virtual WindowRef findWindow(quint32 wid) const = 0;
  virtual QRect geometry(quint32 win) const = 0;
  inline virtual QRect geometry(WindowRef win) const { return geometry(win->windowId); }
  virtual void requestAction(WindowEntry::WindowAction action, uint32_t window) = 0;
  inline void requestAction(WindowEntry::WindowAction action, WindowRef window) { requestAction(action, window->windowId); }

public slots:
  virtual void requestWindowList() = 0;
  virtual void requestCurrentWindow() = 0;
  virtual void setActiveWindow(uint32_t window) = 0;
  virtual void setAsTaskbar(QWidget*) = 0;
  virtual void setStrut(QWidget*, Qt::Edge, int) = 0;

signals:
  void fullWindowList(const WindowRefList&);
  void windowUpdated(WindowRef);
  void currentWindowChanged(WindowRef);
  void screenAreaChanged();
  void systemTrayAvailabilityChanged(bool available);
  void unixSignal(int sig);

protected slots:
  void aboutToQuit();
  void unixSignalTriggered();

protected:
  PlatformIntegration();
  QIcon cacheIcon(const QString& name, const QString& path, const QIcon& icon) const;

  QPointerSet<QProcess> childProcesses;
  QSocketNotifier* notifier;
  mutable QMap<QString, QIcon> iconCache;
  bool debugEnabled;
};

#define c_PI PlatformIntegration::instance()

class PlatformIntegrator
{
public:
  PlatformIntegrator(PlatformIntegration::Platform platform, int argc, char** argv);
  PlatformIntegrator(int argc, char** argv);

  PlatformIntegration* operator->() const;
  PlatformIntegration* operator->();

private:
  std::unique_ptr<PlatformIntegration> instance;
};

#endif
