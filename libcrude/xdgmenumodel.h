#ifndef CRUDE_XDGMENUMODEL_H
#define CRUDE_XDGMENUMODEL_H

#include "xdgmenu.h"

#include <QAbstractItemModel>
#include <QMap>
#include <QPair>

class XdgMenuModel : public QAbstractItemModel
{
  Q_OBJECT

public:
  XdgMenuModel(QObject* parent = nullptr);
  XdgMenuModel(const XdgMenu& root, QObject* parent = nullptr);

  int columnCount(const QModelIndex& parent = QModelIndex()) const;
  int rowCount(const QModelIndex& parent = QModelIndex()) const;

  QModelIndex index(int row, int column = 0, const QModelIndex& parent = QModelIndex()) const;
  QModelIndex parent(const QModelIndex& child) const;

  Qt::ItemFlags flags(const QModelIndex& index) const;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
  XdgMenuItem menuItem(const QModelIndex& index) const;

private:
  XdgMenuItem root;
  mutable QMap<quint32, quint64> parents;
  mutable QMap<quint64, quint32> children;
};

#endif
