#ifndef CRUDE_XDGMENU_P_H
#define CRUDE_XDGMENU_P_H

#include "xdglauncher.h"
#include "xdgmenu.h"

#include <QList>
#include <QMenu>
#include <QSharedData>
#include <QSharedPointer>
class XdgMenu;
class QXmlStreamReader;
class XdgMenuItem;
class QMenu;

class XdgMenuPrivate : public QSharedData
{
public:
  QList<XdgMenuItem> items;
  QString name;
  XdgLauncher directory;
  bool deleted : 1;
  bool notDeleted : 1;

  bool merge(const QString& filename);
  bool merge(QXmlStreamReader& xml);
  void merge(const XdgMenu& other);
  void addLauncher(const XdgLauncher& launcher);
  void removeLauncher(const QString& id);
};

class XdgQMenu : public QMenu
{
  Q_OBJECT

public:
  XdgQMenu(const XdgMenu& menu, QWidget* parent = nullptr);

public slots:
  void populate();

signals:
  void updated();

private:
  QSharedDataPointer<XdgMenuPrivate> d;
  bool populated;
};

#endif
