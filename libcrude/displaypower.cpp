#include "displaypower.h"

#include "environmentprefs.h"
#include "platformintegration.h"
#include "pluginshell.h"
#include "sessionsettings.h"
#include "libcrude/xcb/xcbextension.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QMap>
#include <QStandardPaths>
#include <QTimer>

static const QString BACKLIGHT_PATH("/sys/class/backlight/");

DisplayPower::DisplayPower(QObject* parent)
: QObject(parent), lastLevel(1), pluginScanned(false)
{
  // initializers only
}

void DisplayPower::scanPlugins()
{
  if (!pluginScanned) {
    pluginScanned = true;
    pluginShell.selectDefaultPlugin();
    auto plugin = pluginShell.plugin();
    if (plugin && plugin->available()) {
      QObject* pObject = pluginShell.pluginQObject();
      QObject::connect(pObject, SIGNAL(backlightAvailableChanged(bool)), this, SIGNAL(backlightAvailableChanged(bool)));
      QObject::connect(pObject, SIGNAL(backlightChanged(float)), this, SLOT(onBacklightChanged(float)));
      if (plugin->backlightAvailable()) {
        emit backlightAvailableChanged(true);
        emit backlightChanged(plugin->backlightLevel());
      } else {
        emit backlightAvailableChanged(false);
      }
    }
  }
}

bool DisplayPower::backlightAvailable() const
{
  auto plugin = pluginShell.plugin();
  return plugin && plugin->available() && plugin->backlightAvailable();
}

void DisplayPower::increaseBacklight(float step)
{
  auto plugin = pluginShell.plugin();
  if (!plugin || !plugin->available()) {
    return;
  }
  plugin->increaseBacklight(step);
}

void DisplayPower::decreaseBacklight(float step)
{
  auto plugin = pluginShell.plugin();
  if (!plugin || !plugin->available()) {
    return;
  }
  plugin->decreaseBacklight(step);
}

float DisplayPower::backlightLevel() const
{
  auto plugin = pluginShell.plugin();
  if (!plugin || !plugin->available()) {
    return lastLevel;
  }
  return plugin->backlightLevel();
}

void DisplayPower::setBacklightLevel(float level)
{
  auto plugin = pluginShell.plugin();
  if (!plugin || !plugin->available()) {
    return;
  }

  if (level < 0) {
    level = 0;
  } else if (level > 1) {
    level = 1;
  }
  plugin->setBacklightLevel(level);
}

void DisplayPower::onBacklightChanged(float newLevel)
{
  if (newLevel != lastLevel) {
    lastLevel = newLevel;
    emit backlightChanged(lastLevel);
  }
}

bool DisplayPower::checkAvailable(const QVariant& required)
{
  QStringList names = required.toStringList();
  qDebug() << names;
  if (names.length() == 0) {
    return false;
  }
  for (const QString& name : names) {
    if (QStandardPaths::findExecutable(name).isEmpty()) {
      return false;
    }
  }
  return true;
}

QList<DisplayPower::ScreenSaverConfig> DisplayPower::scanScreenSavers()
{
  QMap<QString, ScreenSaverConfig> result;
  bool hasScheduler = false;
  bool hasLocker = false;

  for (QString path : PluginShellBase::pluginPaths() + QStringList{ EnvironmentPrefs::userConfigDir() + "/Alkahest/" }) {
    path += "crude-screensavers.ini";
    if (!QFile::exists(path)) {
      continue;
    }
    QSettings settings(path, QSettings::IniFormat);
    for (const QString& name : settings.childGroups()) {
      settings.beginGroup(name);
      if (checkAvailable(settings.value("required"))) {
        QString typeString = settings.value("type").toString();
        ScreenSaverType type;
        if (typeString == "screensaver") {
          type = LockingScheduler;
        } else if (typeString == "locker") {
          type = Locker;
          hasLocker = true;
        } else if (typeString == "scheduler") {
          type = Scheduler;
          hasScheduler = true;
        } else {
          settings.endGroup();
          continue;
        }
        ScreenSaverConfig& saver = result[name];
        saver.name = name;
        saver.type = type;
        for (const QString& key : settings.childKeys()) {
          if (key == "type" || key == "required") {
            continue;
          }
          QString value = settings.value(key).toString();
          if (value.isEmpty()) {
            saver.config.remove(key);
          } else {
            saver.config[key] = value;
          }
        }
      }
      settings.endGroup();
    }
  }

  for (const QString& name : result.keys()) {
    auto type = result[name].type;
    if (type == Locker && !hasScheduler) {
      result.remove(name);
    } else if (type == Scheduler && !hasLocker) {
      result.remove(name);
    }
  }

  return result.values();
}

DisplayPower::ScreenSaverConfig DisplayPower::screenSaverConfig(const QString& name)
{
  ScreenSaverConfig saver;

  for (QString path : PluginShellBase::pluginPaths() + QStringList{ EnvironmentPrefs::userConfigDir() + "/Alkahest/" }) {
    path += "crude-screensavers.ini";
    if (!QFile::exists(path)) {
      continue;
    }
    QSettings settings(path, QSettings::IniFormat);
    settings.beginGroup(name);
    if (!checkAvailable(settings.value("required"))) {
      continue;
    }
    QString typeString = settings.value("type").toString();
    ScreenSaverType type;
    if (typeString == "screensaver") {
      type = LockingScheduler;
    } else if (typeString == "locker") {
      type = Locker;
    } else if (typeString == "scheduler") {
      type = Scheduler;
    } else {
      continue;
    }
    saver.name = name;
    saver.type = type;
    for (const QString& key : settings.childKeys()) {
      if (key == "type" || key == "required") {
        continue;
      }
      QString value = settings.value(key).toString();
      if (value.isEmpty()) {
        saver.config.remove(key);
      } else {
        saver.config[key] = value;
      }
    }
  }

  return saver;
}

static DisplayPower::ScreenSaverConfig currentScreenSaver()
{
  Settings* s = Settings::instance();
  if (s) {
    return DisplayPower::screenSaverConfig(s->value("screensaver").toString());
  }
  return DisplayPower::ScreenSaverConfig();
}

bool DisplayPower::screenSaverAvailable()
{
  return !currentScreenSaver().name.isEmpty();
}

void DisplayPower::activateScreenSaver()
{
  ScreenSaverConfig ss = currentScreenSaver();
  QString command = ss.config.value("activate");
  if (!command.isEmpty()) {
    c_PI->runCommand(command);
  }
}

void DisplayPower::setScreenSaverTimeout(int seconds)
{
  if (seconds == 0) {
    Settings::instance()->setValue("dpms/screensaver", "disabled");
  } else {
    Settings::instance()->setValue("dpms/screensaver", QTime::fromMSecsSinceStartOfDay(seconds * 1000).toString());
  }
  Settings::instance()->broadcastReload();
}

int DisplayPower::screenSaverTimeout() const
{
  Settings* s = Settings::instance();
  if (!s) {
    return 0;
  }
  QString ss = s->value("dpms/screensaver").toString();
  if (ss.isEmpty() || ss == "disabled") {
    return 0;
  }
  return QTime::fromString(ss).msecsSinceStartOfDay() / 1000;
}

void DisplayPower::saveSchedule() const
{
  saveSchedule(schedule());
}

void DisplayPower::saveSchedule(const QList<DisplayPower::Schedule>& schedList) const
{
  Settings* s = Settings::instance();
  if (!s) {
    return;
  }
  QString oldSS = s->value("dpms/screensaver").toString();
  QString oldStandby = s->value("dpms/standby").toString();
  QString oldSuspend = s->value("dpms/suspend").toString();
  QString oldOff = s->value("dpms/off").toString();
  s->setValue("dpms/screensaver", "disabled");
  s->setValue("dpms/standby", "disabled");
  s->setValue("dpms/suspend", "disabled");
  s->setValue("dpms/off", "disabled");
  for (const Schedule& entry : schedList) {
    if (entry.standbyMode == Standby) {
      s->setValue("dpms/standby", entry.delay.toString());
    } else if (entry.standbyMode == Suspend) {
      s->setValue("dpms/suspend", entry.delay.toString());
    } else if (entry.standbyMode == Off) {
      s->setValue("dpms/off", entry.delay.toString());
    } else if (entry.standbyMode == ScreenSaver) {
      s->setValue("dpms/screensaver", entry.delay.toString());
    }
  }
  if (oldSS != s->value("dpms/screensaver").toString() || oldStandby != s->value("dpms/standby").toString() ||
      oldSuspend != s->value("dpms/suspend").toString() || oldOff != s->value("dpms/off").toString()) {
    s->broadcastReload();
  }
}

void DisplayPower::restoreSchedule()
{
  Settings* s = Settings::instance();
  if (!s) {
    return;
  }
  QList<DisplayPower::Schedule> schedule;
  QString v = s->value("dpms/screensaver").toString();
  if (!v.isEmpty() && v != "disabled") {
    schedule << (Schedule){ QTime::fromString(v), ScreenSaver };
  }
  v = s->value("dpms/standby").toString();
  if (!v.isEmpty() && v != "disabled") {
    schedule << (Schedule){ QTime::fromString(v), Standby };
  }
  v = s->value("dpms/suspend").toString();
  if (!v.isEmpty() && v != "disabled") {
    schedule << (Schedule){ QTime::fromString(v), Suspend };
  }
  v = s->value("dpms/off").toString();
  if (!v.isEmpty() && v != "disabled") {
    schedule << (Schedule){ QTime::fromString(v), Off };
  }
  setSchedule(schedule);
}

DisplayPower::LightPluginShell::LightPluginShell()
: PluginShell<LightPlugin>()
{
  // initializers only
}

QString DisplayPower::LightPluginShell::pluginPrefix() const
{
  return "light";
}

QStringList DisplayPower::LightPluginShell::pluginPriority() const
{
  return QStringList() << "sysfs"
                       << "logind"
                       << "gnome"
                       << "kde"
                       << "randr";
}

QString DisplayPower::LightPluginShell::noPluginName() const
{
  return "No Backlight Control";
}
