#ifndef CRUDE_QPOINTERSET_H
#define CRUDE_QPOINTERSET_H

#include <QObject>
#include <QSet>

template <typename T>
class QPointerSet : public QObject
{
public:
  using const_iterator = typename QSet<T*>::const_iterator;

  QPointerSet(QObject* parent = nullptr) : QObject(parent)
  {
    // initializers only
  }

  bool isEmpty() { return objects.isEmpty(); }

  void deleteAll()
  {
    QSet<T*> toDelete = objects;
    objects.clear();
    for (T* o : toDelete) {
      QObject::disconnect(o, 0, this, 0);
      o->deleteLater();
    }
  }

  void clear()
  {
    for (T* o : *this) {
      QObject::disconnect(o, 0, this, 0);
    }
    objects.clear();
  }

  bool contains(T* o) const { return objects.contains(o); }

  void insert(T* o)
  {
    if (!o) {
      return;
    }
    QObject::connect(o, &QObject::destroyed, this, &QPointerSet<T>::objectDestroyed);
    objects.insert(o);
  }

  bool remove(T* o)
  {
    if (!contains(o)) {
      return false;
    }
    QObject::disconnect(o, 0, this, 0);
    objects.remove(o);
    return true;
  }

  QPointerSet<T>& operator<<(T* o)
  {
    insert(o);
    return *this;
  }

  const_iterator begin() const { return objects.begin(); }

  const_iterator end() const { return objects.end(); }

private:
  virtual void objectDestroyed(QObject* o) { remove(static_cast<T*>(o)); }

  QSet<T*> objects;
};

#endif
