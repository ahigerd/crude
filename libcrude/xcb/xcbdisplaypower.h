#ifndef CRUDE_XCBDISPLAYPOWER_H
#define CRUDE_XCBDISPLAYPOWER_H

#include "../displaypower.h"
#include "../throttle.h"
#include "xcbintegration.h"

#include <xcb/randr.h>

class XcbDisplayPower : public DisplayPower, public XcbHandlerInterface
{
  Q_OBJECT
  friend class RandrLightPlugin;

public:
  XcbDisplayPower(QObject* parent = nullptr);

  bool standbyModeAvailable(StandbyMode mode) const;
  StandbyMode standbyMode() const;

  QList<Schedule> schedule() const;
  void setSchedule(const QList<Schedule>& schedule);

  bool handleEvent(const CrudeXcb::EventType& event);

public slots:
  void setStandbyMode(StandbyMode mode);

private slots:
  void pollDisplay();
  void primaryDisplayResult_in(uint32_t);
  void updateEdid(xcb_randr_get_output_property_reply_t*);

private:
  Throttle poll;
  bool randrAvailable : 1;
  bool dpmsAvailable : 1;
  bool standbyAvailable : 1;
  bool suspendAvailable : 1;
  bool offAvailable : 1;
  uint32_t primaryOutput;
};

#endif
