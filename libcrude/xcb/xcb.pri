QT += x11extras
LIBS += -lxcb -lxcb-ewmh -lxcb-icccm -lxcb-keysyms -lxcb-randr -lxcb-dpms -lxcb-render -lxcb-composite -lxcb-damage -lxcb-shape -lxcb-xfixes -lQtXkbCommonSupport -lxkbcommon
INCLUDEPATH += ./xcb ../3rdparty/qxkbcommon

HEADERS += xcb/xcbrequest_macro.h xcb/xcbrequest_wm.h xcb/xcbrequest_display.h xcb/xcbrequest_prop.h
HEADERS += xcb/xcbrequest_render.h xcb/xcbrequest_fixes.h xcb/xcbrequest_shape.h

HEADERS += xcb/xcbintegration.h   xcb/xcbrequest.h   xcb/xcbcookie.h   xcb/xcbselectionmanager.h
SOURCES += xcb/xcbintegration.cpp xcb/xcbrequest.cpp xcb/xcbcookie.cpp xcb/xcbselectionmanager.cpp

HEADERS += xcb/xcbsystemtray_p.h xcb/xcbhotkey_p.h xcb/xcbdisplaypower.h   xcb/xcbextension.h
SOURCES += xcb/xcbsystemtray.cpp xcb/xcbhotkey.cpp xcb/xcbdisplaypower.cpp xcb/xcbextension.cpp
