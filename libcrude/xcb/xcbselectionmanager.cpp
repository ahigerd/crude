#include "xcbselectionmanager.h"

#include "xcbrequest_wm.h"

#include <QtDebug>

XcbSelectionManager::XcbSelectionManager(const QByteArray& name, QObject* parent)
: QObject(parent), owner(0), atom(0), shouldAcquire(false), shouldForce(false), didAcquire(false)
{
  XcbIntegration::instance()->addEventHandler(this);
  this->atom = callXcbInternAtom(false, name)->result();
}

XcbSelectionManager::~XcbSelectionManager()
{
  if (didAcquire && owner) {
    callXcbDestroyWindow(owner);
    qWarning("XcbSelectionManager: destroyed while holding selection");
  }
}

void XcbSelectionManager::acquire(bool force)
{
  owner = xcb_generate_id(c_X11);
  callXcbCreateWindow(owner, QX11Info::appRootWindow(0), QRect(0, 0, 1, 1), 0, {})->force();

  if (force) {
    gotCurrentSelection(nullptr);
  } else {
    callXcbGetSelectionOwner(atom)->then(this, SLOT(gotCurrentSelection(xcb_get_selection_owner_reply_t*)));
  }
}

void XcbSelectionManager::gotCurrentSelection(xcb_get_selection_owner_reply_t* reply)
{
  if (reply && reply->owner) {
    emit unavailable();
    release();
    return;
  }
  callXcbSetSelectionOwner(owner, atom)->then(this, SLOT(selectionWasSet()), SLOT(acquireFailed(xcb_generic_error_t*)));
}

void XcbSelectionManager::selectionWasSet()
{
  callXcbGetSelectionOwner(atom)->then(
      this, SLOT(acquiredSuccessfully(xcb_get_selection_owner_reply_t*)), SLOT(acquireFailed(xcb_generic_error_t*))
  );
}

void XcbSelectionManager::acquiredSuccessfully(xcb_get_selection_owner_reply_t* reply)
{
  if (reply->owner != owner) {
    emit acquireFailed(nullptr);
    release();
    return;
  }
  didAcquire = true;
  xcb_client_message_event_t event;
  event.response_type = XCB_CLIENT_MESSAGE;
  event.format = 32;
  event.window = QX11Info::appRootWindow();
  event.type = XCB_ATOM_RESOURCE_MANAGER;
  event.data.data32[0] = XcbIntegration::instance()->lastTimestamp();
  event.data.data32[1] = atom;
  event.data.data32[2] = owner;
  event.data.data32[3] = 0;
  event.data.data32[4] = 0;
  callXcbSendEvent(false, QX11Info::appRootWindow(), XCB_EVENT_MASK_STRUCTURE_NOTIFY, &event)
      ->then(this, SIGNAL(acquired()), SLOT(acquireFailed(xcb_generic_error_t*)));
}

void XcbSelectionManager::acquireFailed(xcb_generic_error_t* err)
{
  qDebug() << "Error acquiring manager selection" << (err ? err->error_code : 0);
  release();
  emit unavailable();
}

void XcbSelectionManager::release()
{
  didAcquire = false;
  shouldAcquire = false;
  callXcbDestroyWindow(owner);
  owner = 0;
  emit released();
}

bool XcbSelectionManager::handleEvent(const CrudeXcb::EventType& event)
{
  if (event.eventType == XCB_CLIENT_MESSAGE) {
    auto me = event.cast<xcb_client_message_event_t>();
    if (me->format == 8) {
      emit message(me->type, QByteArray(reinterpret_cast<const char*>(me->data.data8), 20));
    } else if (me->format == 16) {
      QVector<uint16_t> msg(10);
      std::memcpy(msg.data(), me->data.data16, 20);
      emit message(me->type, msg);
    } else if (me->format == 32) {
      QVector<uint32_t> msg(5);
      std::memcpy(msg.data(), me->data.data32, 20);
      emit message(me->type, msg);
    }
    return true;
  } else if (event.eventType == XCB_SELECTION_CLEAR) {
    auto ce = event.cast<xcb_selection_clear_event_t>();
    if (ce->selection == atom) {
      emit revoked();
      qDebug() << "selection revoked";
      release();
    }
    return true;
  }
  return false;
}

xcb_window_t XcbSelectionManager::winId() const
{
  return owner;
}

xcb_window_t XcbSelectionManager::ownerId() const
{
  auto cookie = callXcbGetSelectionOwner(atom)->uniquePtr();
  if (!cookie->force()) {
    return 0;
  }
  auto result = cookie->result();
  return result->owner;
}
