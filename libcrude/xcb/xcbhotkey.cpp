#include "qxkbcommon_p.h"
#include "xcbhotkey_p.h"
#include "xcbintegration.h"
#include "xcbrequest_wm.h"

#include <QMetaObject>

#include <X11/keysym.h>
#include <xcb/xcb_keysyms.h>

static const uint32_t ignoreMods[] = { 0, XCB_MOD_MASK_2, XCB_MOD_MASK_LOCK, XCB_MOD_MASK_2 | XCB_MOD_MASK_LOCK };
static const uint32_t knownMods = XCB_MOD_MASK_SHIFT | XCB_MOD_MASK_CONTROL | XCB_MOD_MASK_1 | XCB_MOD_MASK_4;
static xcb_key_symbols_t* keysymCtx = nullptr;

static void addKeycodes(xkb_keysym_t keysym, QVector<quint32>& nativeKeys)
{
  xcb_keycode_t* iter = xcb_key_symbols_get_keycode(keysymCtx, keysym);
  if (!iter) {
    iter = xcb_key_symbols_get_keycode(keysymCtx, keysym & 0x00FFFFFF);
  }
  while (iter && *iter != XCB_NO_SYMBOL) {
    if (!nativeKeys.contains(*iter)) {
      nativeKeys << *iter;
    }
    ++iter;
  }
}

QVector<quint32> XcbHotkeyPrivate::keycodesFor(Qt::Key qtKey)
{
  if (!keysymCtx) {
    keysymCtx = xcb_key_symbols_alloc(c_X11);
  }
  uint32_t keyMods = qtKey & 0x7E000000;
  uint32_t key = qtKey & 0x01FFFFFF;
  QVector<quint32> nativeKeys;
  auto keysyms = QXkbCommon::toKeysym(key, Qt::KeyboardModifiers(keyMods));
  for (xkb_keysym_t keysym : keysyms) {
    if (keysym == XK_KP_Space || keysym == XK_space) {
      // Treat these two keysyms as interchangeable and always handle both
      addKeycodes(XK_space, nativeKeys);
      addKeycodes(XK_KP_Space, nativeKeys);
    } else {
      addKeycodes(keysym, nativeKeys);
    }
  }
  return nativeKeys;
}

XcbHotkeyPrivate::XcbHotkeyPrivate(const QKeySequence& shortcut, Hotkey* owner)
: HotkeyPrivate(shortcut, owner), nativeMods(0)
{
  XcbIntegration::instance()->addEventHandler(this);

  uint32_t keyMods = shortcut[0] & 0x7E000000;
  //uint32_t key = shortcut[0] & 0x01FFFFFF;

  if (keyMods & Qt::SHIFT) {
    nativeMods |= XCB_MOD_MASK_SHIFT;
  }
  if (keyMods & Qt::CTRL) {
    nativeMods |= XCB_MOD_MASK_CONTROL;
  }
  if (keyMods & Qt::ALT) {
    nativeMods |= XCB_MOD_MASK_1;
  }
  if (keyMods & Qt::META) {
    nativeMods |= XCB_MOD_MASK_4;
  }

  xcb_window_t root = c_EWMH->screens[0]->root;

  isValid = false;
  nativeKeys = keycodesFor((Qt::Key)shortcut[0]);
  for (xcb_keycode_t keycode : nativeKeys) {
    for (int i = 0; i < 4; i++) {
      // Running these synchronously is necessary to avoid a deadlock
      bool ok = callXcbGrabKey(root, nativeMods | ignoreMods[i], keycode)->force();
      if (ok) {
        // At least one key combination is acceptable.
        isValid = true;
      }
    }
  }
}

XcbHotkeyPrivate::~XcbHotkeyPrivate()
{
  xcb_window_t root = c_EWMH->screens[0]->root;
  for (xcb_keycode_t keycode : nativeKeys) {
    for (int i = 0; i < 4; i++) {
      callXcbUngrabKey(root, nativeMods | ignoreMods[i], keycode);
    }
  }
}

bool XcbHotkeyPrivate::handleEvent(const CrudeXcb::EventType& event)
{
  if (event.eventType == XCB_KEY_PRESS) {
    auto ke = event.cast<xcb_key_press_event_t>();
    if ((ke->state & knownMods) != nativeMods) {
      return false;
    }
    for (uint32_t key : nativeKeys) {
      if (ke->detail == key) {
        QMetaObject::invokeMethod(p, "activated", Qt::QueuedConnection, Q_ARG(QVariant, data));
        return true;
      }
    }
  } else if (event.eventType == XCB_KEY_RELEASE) {
    auto ke = event.cast<xcb_key_release_event_t>();
    if ((ke->state & knownMods) == 0) {
      QMetaObject::invokeMethod(p, "released", Qt::QueuedConnection, Q_ARG(QVariant, data));
      return true;
    }
  }
  return false;
}
