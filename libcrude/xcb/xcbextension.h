#ifndef CRUDE_XCBEXTENSION_H
#define CRUDE_XCBEXTENSION_H

#include <xcb/xproto.h>

namespace CrudeXcb {
enum Extension {
  core = -1,
  randr = 0,
  render,
  composite,
  damage,
  xfixes,
  shape,
  dpms,
};

struct EventType
{
  Extension extension;
  uint8_t eventType;
  bool isSynthetic;
  xcb_generic_event_t* event;

  template <typename T>
  T* cast() const
  {
    return reinterpret_cast<T*>(event);
  };
};

struct ErrorType
{
  Extension extension;
  uint8_t errorCode;
};

void prefetchExtensions();
bool checkExtension(Extension ext, uint32_t majVer = 0, uint32_t minVer = 0);
ErrorType translateError(uint8_t errorCode);

inline ErrorType translateEvent(xcb_generic_error_t* error)
{
  return translateError(error ? error->error_code : 0);
}

EventType translateEvent(uint8_t eventId);

inline EventType translateEvent(xcb_generic_event_t* event)
{
  EventType result = translateEvent(event->response_type);
  result.event = event;
  return result;
}
};

#endif
