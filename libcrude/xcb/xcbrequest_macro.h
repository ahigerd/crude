#ifndef CRUDE_XCBREQUEST_MACRO_H
#define CRUDE_XCBREQUEST_MACRO_H
/*
 * This file exists as a metaprogramming detail for the request methods.
 * It is meant to be included by xcbrequest.h.
 * Do not include this file in any other place.
 */

#ifndef CRUDE_NULLARY_STUB
#  define CRUDE_NULLARY_STUB(xcb_name) \
    inline decltype(::xcb_name(nullptr)) xcb_name(xcb_connection_t* c, int = 0) \
    { \
      return ::xcb_name(c); \
    }
#endif

#define CRUDE_VALUE_REPLY(xcb_name) crude_##xcb_name##_reply
#define CRUDE_GET_ARRAY_REPLY_SIGNATURE(xcb_name, element_t) \
  QVector<element_t> CRUDE_VALUE_REPLY(xcb_name)(xcb_connection_t*, xcb_name##_cookie_t cookie, xcb_generic_error_t * *err)
#define CRUDE_GET_LIST_REPLY_SIGNATURE(xcb_name, element_t) \
  QVector<element_t> CRUDE_VALUE_REPLY(xcb_name)( \
      xcb_connection_t*, xcb_get_property_cookie_t cookie, xcb_generic_error_t * *err \
  )
#define CRUDE_GET_STRING_REPLY_SIGNATURE(xcb_name) \
  QString CRUDE_VALUE_REPLY(xcb_name)(xcb_connection_t*, xcb_get_property_cookie_t cookie, xcb_generic_error_t * *err)
#define CRUDE_GET_PROPERTY_REPLY_SIGNATURE(xcb_name, result_t) \
  result_t CRUDE_VALUE_REPLY(xcb_name)(xcb_connection_t*, xcb_get_property_cookie_t cookie, xcb_generic_error_t * *err)
#define CRUDE_GET_FIELD_REPLY_SIGNATURE(xcb_name, result_t) \
  result_t CRUDE_VALUE_REPLY(xcb_name)(xcb_connection_t*, xcb_name##_cookie_t cookie, xcb_generic_error_t * *err)

#define CRUDE_GET_ARRAY_REPLY(conn, xcb_name, reply_name, element_t, element) \
  CRUDE_GET_ARRAY_REPLY_SIGNATURE(xcb_name, element_t);
#define CRUDE_GET_LIST_REPLY(conn, xcb_name, reply_name, element_t, element) \
  CRUDE_GET_LIST_REPLY_SIGNATURE(xcb_name, element_t);
#define CRUDE_GET_STRING_REPLY(conn, xcb_name)                 CRUDE_GET_STRING_REPLY_SIGNATURE(xcb_name);
#define CRUDE_GET_PROPERTY_REPLY(conn, xcb_name, result_t)     CRUDE_GET_PROPERTY_REPLY_SIGNATURE(xcb_name, result_t);
#define CRUDE_GET_FIELD_REPLY(conn, xcb_name, result_t, field) CRUDE_GET_FIELD_REPLY_SIGNATURE(xcb_name, result_t);

#define CRUDE_XCB_COOKIE_IMPL(conn, XcbName, xcb_name, cookie_t, reply_t, reply_fn, ARGS, ...) \
  using XcbName = XcbCookie<reply_t>; \
  XcbName* call##XcbName ARGS;
#define CRUDE_XCB_COOKIE(conn, XcbName, xcb_name, cookie_t, reply_t, reply_fn, ...) \
  CRUDE_XCB_COOKIE_IMPL(conn, XcbName, xcb_name, cookie_t, reply_t, reply_fn, __VA_ARGS__)

#define CRUDE_XCB_ARGS(...)      (__VA_ARGS__)
#define CRUDE_XCB_CALL_WITH(...) __VA_ARGS__
#define CRUDE_XCB_VOID_COOKIE_CHECKED(XcbName, xcb_name, ...) \
  CRUDE_XCB_COOKIE( \
      c_X11, XcbName, xcb_name##_checked, xcb_void_cookie_t, xcb_generic_reply_t*, crude_generic_reply, __VA_ARGS__ \
  )
#define CRUDE_XCB_VOID_COOKIE(XcbName, xcb_name, ...) \
  CRUDE_XCB_COOKIE(c_X11, XcbName, xcb_name, xcb_void_cookie_t, xcb_generic_reply_t*, crude_generic_reply, __VA_ARGS__)
#define CRUDE_XCB_REQUEST(XcbName, xcb_name, ...) \
  CRUDE_XCB_COOKIE(c_X11, XcbName, xcb_name, xcb_name##_cookie_t, xcb_name##_reply_t*, xcb_name##_reply, __VA_ARGS__)
#define CRUDE_XCB_GET_PROPERTY(XcbName, xcb_name, result_t, ...) \
  CRUDE_GET_PROPERTY_REPLY(c_X11, xcb_name, result_t) \
  CRUDE_XCB_COOKIE(c_X11, XcbName, xcb_name, xcb_get_property_cookie_t, result_t, CRUDE_VALUE_REPLY(xcb_name), __VA_ARGS__)
#define CRUDE_XCB_GET_STRING(XcbName, xcb_name, ...) \
  CRUDE_XCB_COOKIE(c_X11, XcbName, xcb_name, xcb_get_property_cookie_t, QString, CRUDE_VALUE_REPLY(xcb_name), __VA_ARGS__)
#define CRUDE_XCB_GET_LIST(XcbName, xcb_name, reply_name, element_t, element, ...) \
  CRUDE_GET_LIST_REPLY(c_X11, xcb_name, reply_name, element_t, element) \
  CRUDE_XCB_COOKIE( \
      c_X11, XcbName, xcb_name, xcb_get_property_cookie_t, QVector<element_t>, CRUDE_VALUE_REPLY(xcb_name), __VA_ARGS__ \
  )
#define CRUDE_XCB_GET_FIELD(XcbName, xcb_name, result_t, field, ...) \
  CRUDE_GET_FIELD_REPLY(c_X11, xcb_name, result_t, field) \
  CRUDE_XCB_COOKIE(c_X11, XcbName, xcb_name, xcb_name##_cookie_t, result_t, CRUDE_VALUE_REPLY(xcb_name), __VA_ARGS__)
#define CRUDE_XCB_GET_ARRAY(XcbName, xcb_name, reply_name, element_t, element, ...) \
  CRUDE_GET_ARRAY_REPLY(c_X11, xcb_name, reply_name, element_t, element) \
  CRUDE_XCB_COOKIE( \
      c_X11, XcbName, xcb_name, xcb_name##_cookie_t, QVector<element_t>, CRUDE_VALUE_REPLY(xcb_name), __VA_ARGS__ \
  )

#define CRUDE_EWMH_ARGS(...)      (WinAdaptor window, __VA_ARGS__)
#define CRUDE_EWMH_CALL_WITH(...) window, __VA_ARGS__
#define CRUDE_EWMH_COOKIE(XcbName, xcb_name, cookie_t, reply_t, reply_fn, ARGS, ...) \
  CRUDE_XCB_COOKIE(c_EWMH, XcbName, xcb_name, cookie_t, reply_t, reply_fn, ARGS, __VA_ARGS__)
#define CRUDE_EWMH_VOID_COOKIE_CHECKED(XcbName, xcb_name, ...) \
  CRUDE_EWMH_COOKIE(XcbName, xcb_name##_checked, xcb_void_cookie_t, xcb_generic_reply_t*, crude_generic_reply, __VA_ARGS__)
#define CRUDE_EWMH_VOID_COOKIE(XcbName, xcb_name, ...) \
  CRUDE_EWMH_COOKIE(XcbName, xcb_name, xcb_void_cookie_t, xcb_generic_reply_t*, crude_generic_reply, __VA_ARGS__)
#define CRUDE_EWMH_REQUEST(XcbName, xcb_name, ...) \
  CRUDE_EWMH_COOKIE(XcbName, xcb_name, xcb_name##_cookie_t, xcb_name##_reply_t*, xcb_name##_reply, __VA_ARGS__)
#define CRUDE_EWMH_VOID_ATOM_LIST(XcbName, xcb_name) \
  CRUDE_EWMH_VOID_COOKIE_CHECKED( \
      XcbName, xcb_name, CRUDE_EWMH_ARGS(QVector<xcb_atom_t> atoms), CRUDE_EWMH_CALL_WITH(atoms.length(), atoms.data()) \
  )
#define CRUDE_EWMH_GET_LIST(XcbName, xcb_name, reply_name, element_t, element, ...) \
  CRUDE_GET_LIST_REPLY(c_EWMH, xcb_name, reply_name, element_t, element) \
  CRUDE_EWMH_COOKIE( \
      XcbName, xcb_name, xcb_get_property_cookie_t, QVector<element_t>, CRUDE_VALUE_REPLY(xcb_name), __VA_ARGS__ \
  )
#define CRUDE_EWMH_GET_STRING(XcbName, xcb_name, ...) \
  CRUDE_GET_STRING_REPLY(c_EWMH, xcb_name) \
  CRUDE_EWMH_COOKIE(XcbName, xcb_name, xcb_get_property_cookie_t, QString, CRUDE_VALUE_REPLY(xcb_name), __VA_ARGS__)
#define CRUDE_EWMH_GET_PROPERTY(XcbName, xcb_name, result_t, ...) \
  CRUDE_GET_PROPERTY_REPLY(c_EWMH, xcb_name, result_t) \
  CRUDE_EWMH_COOKIE(XcbName, xcb_name, xcb_get_property_cookie_t, result_t, CRUDE_VALUE_REPLY(xcb_name), __VA_ARGS__)

#define C_DEF(value) = value

#endif
