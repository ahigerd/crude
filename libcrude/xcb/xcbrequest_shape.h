#if !defined(CRUDE_XCBREQUEST_SHAPE_H) || defined(CRUDE_XCB_IMPL)
#  ifndef CRUDE_XCBREQUEST_SHAPE_H
#    define CRUDE_XCBREQUEST_SHAPE_H
#  endif

#  include "xcbrequestdefs.h"

#  include <xcb/render.h>
#  include <xcb/shape.h>
#  include <xcb/xfixes.h>

namespace CrudeXcb {
#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbReplyCookie
CRUDE_XCB_VOID_COOKIE_CHECKED(
    ShapeSetRects, xcb_shape_rectangles,
    CRUDE_XCB_ARGS(WinAdaptor&& win, xcb_shape_kind_t kind, const QVector<XcbRectAdaptor>& rects),
    CRUDE_XCB_CALL_WITH(XCB_SHAPE_SO_SET, kind, 0, win, 0, 0, rects.count(), rects.constData())
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    ShapeSelectInput, xcb_shape_select_input, CRUDE_XCB_ARGS(WinAdaptor&& win, bool enable),
    CRUDE_XCB_CALL_WITH(win, enable ? 1 : 0)
);
CRUDE_XCB_REQUEST(
    ShapeGetRects, xcb_shape_get_rectangles, CRUDE_XCB_ARGS(WinAdaptor&& win, uint8_t kind), CRUDE_XCB_CALL_WITH(win, kind)
);
XCB_DEFINE_REPLY_VECTOR(ShapeRectList, ShapeRect, xcb_shape_get_rectangles, rectangles);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    GetWindowRegion, xcb_xfixes_create_region_from_window,
    CRUDE_XCB_ARGS(quint32 region, WinAdaptor&& win, uint8_t kind), CRUDE_XCB_CALL_WITH(region, win, kind)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    RenderSetClipRegion, xcb_xfixes_set_picture_clip_region,
    CRUDE_XCB_ARGS(quint32 picture, quint32 region, quint16 x, quint16 y), CRUDE_XCB_CALL_WITH(picture, region, x, y)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    RenderSetClipRect, xcb_render_set_picture_clip_rectangles,
    CRUDE_XCB_ARGS(quint32 picture, quint16 x, quint16 y, XcbRectAdaptor&& rect),
    CRUDE_XCB_CALL_WITH(picture, x, y, 1, static_cast<xcb_rectangle_t*>(&rect))
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    DestroyRegion, xcb_xfixes_destroy_region, CRUDE_XCB_ARGS(quint32 region), CRUDE_XCB_CALL_WITH(region)
);

#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbValueCookie

}

#endif
