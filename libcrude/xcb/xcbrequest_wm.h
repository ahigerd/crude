#if !defined(CRUDE_XCBREQUEST_WM_H) || defined(CRUDE_XCB_IMPL)
#  ifndef CRUDE_XCBREQUEST_WM_H
#    define CRUDE_XCBREQUEST_WM_H
#  endif

#  include "xcbrequestdefs.h"

#  include <xcb/xcb_ewmh.h>
#  include <xcb/xcb_icccm.h>
#  include <xcb/xproto.h>

namespace CrudeXcb {
#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbReplyCookie
// Metadata and event handling
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbSetSelectionOwner, xcb_set_selection_owner, CRUDE_XCB_ARGS(WinAdaptor&& owner, xcb_atom_t selection),
    CRUDE_XCB_CALL_WITH(owner, selection, XcbIntegration::instance()->lastTimestamp())
);
CRUDE_XCB_REQUEST(
    XcbGetSelectionOwner, xcb_get_selection_owner, CRUDE_XCB_ARGS(xcb_atom_t selection), CRUDE_XCB_CALL_WITH(selection)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbSendEvent, xcb_send_event, CRUDE_XCB_ARGS(bool propagate, WinAdaptor&& dest, uint32_t mask, void* event),
    CRUDE_XCB_CALL_WITH(propagate, dest, mask, reinterpret_cast<const char*>(event))
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbGrabKey, xcb_grab_key, CRUDE_XCB_ARGS(WinAdaptor&& window, uint16_t mods, xcb_keycode_t key),
    CRUDE_XCB_CALL_WITH(1, window, mods, key, XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbUngrabKey, xcb_ungrab_key, CRUDE_XCB_ARGS(WinAdaptor&& window, uint16_t mods, xcb_keycode_t key),
    CRUDE_XCB_CALL_WITH(key, window, mods)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbGrabButton, xcb_grab_button, CRUDE_XCB_ARGS(WinAdaptor&& window, uint16_t mods, uint8_t button, bool passthrough),
    CRUDE_XCB_CALL_WITH(
        passthrough ? 1 : 0, window, XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE, XCB_GRAB_MODE_SYNC,
        XCB_GRAB_MODE_ASYNC, 0, 0, button, mods
    )
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbUngrabButton, xcb_ungrab_button, CRUDE_XCB_ARGS(WinAdaptor&& window, uint16_t mods, uint8_t button),
    CRUDE_XCB_CALL_WITH(button, window, mods)
);
CRUDE_XCB_REQUEST(
    XcbGrabPointer, xcb_grab_pointer,
    CRUDE_XCB_ARGS(WinAdaptor&& window, uint8_t pointerMode, uint8_t keyboardMode, bool passthrough, quint32 cursor),
    CRUDE_XCB_CALL_WITH(
        passthrough ? 1 : 0, window,
        XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_POINTER_MOTION, pointerMode,
        keyboardMode, 0, cursor, XCB_CURRENT_TIME
    )
);
CRUDE_XCB_VOID_COOKIE_CHECKED(XcbUngrabPointer, xcb_ungrab_pointer, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(XCB_CURRENT_TIME));
CRUDE_XCB_REQUEST(
    XcbGrabKeyboard, xcb_grab_keyboard,
    CRUDE_XCB_ARGS(WinAdaptor&& window, uint8_t pointerMode, uint8_t keyboardMode, bool passthrough),
    CRUDE_XCB_CALL_WITH(passthrough ? 1 : 0, window, XCB_CURRENT_TIME, pointerMode, keyboardMode)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbUngrabKeyboard, xcb_ungrab_keyboard, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(XCB_CURRENT_TIME)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbAllowEvents, xcb_allow_events, CRUDE_XCB_ARGS(uint8_t mode), CRUDE_XCB_CALL_WITH(mode, XCB_CURRENT_TIME)
);

// Window management
CRUDE_XCB_REQUEST(XcbQueryTree, xcb_query_tree, CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window));
XCB_DEFINE_REPLY_VECTOR(QueryTreeChildren, QueryTreeChild, xcb_query_tree, children);
CRUDE_XCB_REQUEST(XcbGetGeometry, xcb_get_geometry, CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window));
CRUDE_XCB_REQUEST(
    XcbTranslateCoordinates, xcb_translate_coordinates,
    CRUDE_XCB_ARGS(WinAdaptor&& from, WinAdaptor&& to, int16_t x, int16_t y), CRUDE_XCB_CALL_WITH(from, to, x, y)
);
CRUDE_XCB_REQUEST(
    XcbGetAttributes, xcb_get_window_attributes, CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbChangeAttributes, xcb_change_window_attributes,
    CRUDE_XCB_ARGS(WinAdaptor&& window, uint32_t valueMask, QVector<uint32_t> valueList),
    CRUDE_XCB_CALL_WITH(window, valueMask, valueList.data())
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbConfigureWindow, xcb_configure_window,
    CRUDE_XCB_ARGS(WinAdaptor&& window, uint32_t valueMask, QVector<uint32_t> valueList),
    CRUDE_XCB_CALL_WITH(window, valueMask, valueList.data())
);
// Note: This has the source indication hard-coded to "other" because CRUDE is always going to be doing window management requests.
// If you want to use this code in your own applications, it might be desirable to change that.
CRUDE_EWMH_VOID_COOKIE(
    EwmhRequestChangeActiveWindow, xcb_ewmh_request_change_active_window, CRUDE_XCB_ARGS( WinAdaptor&& target),
    CRUDE_XCB_CALL_WITH(
        0, target, XCB_EWMH_CLIENT_SOURCE_TYPE_OTHER, QX11Info::appUserTime(),
        qApp->activeWindow() ? qApp->activeWindow()->winId() : 0
    )
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbCreateWindow, xcb_create_window,
    CRUDE_XCB_ARGS(WinAdaptor&& wid, WinAdaptor&& parent, const QRect& geom, uint32_t valueMask, QVector<uint32_t> valueList),
    CRUDE_XCB_CALL_WITH(
        XCB_COPY_FROM_PARENT, wid, parent, geom.x(), geom.y(), geom.width(), geom.height(), 0,
        XCB_WINDOW_CLASS_COPY_FROM_PARENT, XCB_COPY_FROM_PARENT, valueMask, valueList.constData()
    )
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbDestroyWindow, xcb_destroy_window, CRUDE_XCB_ARGS(WinAdaptor&& wid), CRUDE_XCB_CALL_WITH(wid)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(XcbMapWindow, xcb_map_window, CRUDE_XCB_ARGS(WinAdaptor&& wid), CRUDE_XCB_CALL_WITH(wid));
CRUDE_XCB_VOID_COOKIE_CHECKED(XcbUnmapWindow, xcb_unmap_window, CRUDE_XCB_ARGS(WinAdaptor&& wid), CRUDE_XCB_CALL_WITH(wid));
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbReparentWindow, xcb_reparent_window,
    CRUDE_XCB_ARGS(WinAdaptor&& window, WinAdaptor&& parent, int16_t x, int16_t y),
    CRUDE_XCB_CALL_WITH(window, parent, x, y)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbSetGeometry, xcb_configure_window, CRUDE_XCB_ARGS(WinAdaptor&& window, const RectAdaptor& r),
    CRUDE_XCB_CALL_WITH(
        window, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, r.values
    )
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbSetFocus, xcb_set_input_focus, CRUDE_XCB_ARGS(WinAdaptor&& window),
    CRUDE_XCB_CALL_WITH(XCB_INPUT_FOCUS_PARENT, window, XCB_CURRENT_TIME)
);
CRUDE_EWMH_VOID_COOKIE(
    EwmhSetActiveWindow, xcb_ewmh_set_active_window, CRUDE_XCB_ARGS(WinAdaptor&& window),
    CRUDE_XCB_CALL_WITH(0, window)
);
CRUDE_EWMH_VOID_COOKIE(
    EwmhCloseWindow, xcb_ewmh_request_close_window, CRUDE_XCB_ARGS(WinAdaptor&& window),
    CRUDE_XCB_CALL_WITH(0, window, XCB_CURRENT_TIME, XCB_EWMH_CLIENT_SOURCE_TYPE_OTHER)
);
CRUDE_EWMH_VOID_COOKIE(
    EwmhMoveWindow, xcb_ewmh_request_wm_moveresize, CRUDE_XCB_ARGS(WinAdaptor&& window, int startX, int startY),
    CRUDE_XCB_CALL_WITH(0, window, startX, startY, XCB_EWMH_WM_MOVERESIZE_MOVE_KEYBOARD, XCB_BUTTON_INDEX_ANY, XCB_EWMH_CLIENT_SOURCE_TYPE_OTHER)
);
CRUDE_EWMH_VOID_COOKIE(
    EwmhResizeWindow, xcb_ewmh_request_wm_moveresize, CRUDE_XCB_ARGS(WinAdaptor&& window, int startX, int startY),
    CRUDE_XCB_CALL_WITH(0, window, startX, startY, XCB_EWMH_WM_MOVERESIZE_SIZE_KEYBOARD, XCB_BUTTON_INDEX_ANY, XCB_EWMH_CLIENT_SOURCE_TYPE_OTHER)
);
CRUDE_EWMH_VOID_COOKIE(
    EwmhSetWmState, xcb_ewmh_request_change_wm_state, CRUDE_XCB_ARGS(WinAdaptor&& window, xcb_atom_t prop1, xcb_atom_t prop2 C_DEF(0)),
    CRUDE_XCB_CALL_WITH(0, window, XCB_EWMH_WM_STATE_ADD, prop1, prop2, XCB_EWMH_CLIENT_SOURCE_TYPE_OTHER)
);
CRUDE_EWMH_VOID_COOKIE(
    EwmhUnsetWmState, xcb_ewmh_request_change_wm_state, CRUDE_XCB_ARGS(WinAdaptor&& window, xcb_atom_t prop1, xcb_atom_t prop2 C_DEF(0)),
    CRUDE_XCB_CALL_WITH(0, window, XCB_EWMH_WM_STATE_REMOVE, prop1, prop2, XCB_EWMH_CLIENT_SOURCE_TYPE_OTHER)
);
CRUDE_EWMH_VOID_COOKIE(
    EwmhToggleWmState, xcb_ewmh_request_change_wm_state, CRUDE_XCB_ARGS(WinAdaptor&& window, xcb_atom_t prop1, xcb_atom_t prop2 C_DEF(0)),
    CRUDE_XCB_CALL_WITH(0, window, XCB_EWMH_WM_STATE_TOGGLE, prop1, prop2, XCB_EWMH_CLIENT_SOURCE_TYPE_OTHER)
);

#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbValueCookie
CRUDE_EWMH_GET_LIST(
    EwmhGetClientList, xcb_ewmh_get_client_list, xcb_ewmh_get_windows_reply, xcb_window_t, windows,
    CRUDE_XCB_ARGS(int screen C_DEF(0)), CRUDE_XCB_CALL_WITH(screen)
);
CRUDE_EWMH_GET_PROPERTY(
    EwmhGetActiveWindow, xcb_ewmh_get_active_window, xcb_window_t, CRUDE_XCB_ARGS(int screen C_DEF(0)), CRUDE_XCB_CALL_WITH(screen)
);

};

#endif
