#ifndef CRUDE_XCBSYSTEMTRAY_P_H
#define CRUDE_XCBSYSTEMTRAY_P_H

#include "systemtray_p.h"
#include "xcbselectionmanager.h"

class XcbSystemTrayPrivate : public SystemTrayPrivate, public XcbHandlerInterface
{
  Q_OBJECT

public:
  XcbSystemTrayPrivate(SystemTray* parent);
  ~XcbSystemTrayPrivate();

  XcbSelectionManager mgr;

  virtual void updateOrientation();
  virtual bool handleEvent(const CrudeXcb::EventType& event);

private slots:
  void acquired();
  void unavailable();
  void revoked();
  void released();
  void message(xcb_atom_t type, const QVector<uint32_t>& msg);

private:
  uint32_t orientAtom, visualAtom, opcodeAtom, messageAtom;
  bool useDamage;
};

#endif
