#if !defined(CRUDE_XCBREQUEST_DISPLAY_H) || defined(CRUDE_XCB_IMPL)
#  ifndef CRUDE_XCBREQUEST_DISPLAY_H
#    define CRUDE_XCBREQUEST_DISPLAY_H
#  endif

#  include "xcbrequestdefs.h"

#  include <xcb/dpms.h>
#  include <xcb/randr.h>

namespace CrudeXcb {
#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbReplyCookie
CRUDE_XCB_VOID_COOKIE_CHECKED(
    RRSelectInput, xcb_randr_select_input, CRUDE_XCB_ARGS(uint32_t wid, uint16_t mask), CRUDE_XCB_CALL_WITH(wid, mask)
);
CRUDE_XCB_REQUEST(
    RRGetOutputProperty, xcb_randr_get_output_property, CRUDE_XCB_ARGS(uint32_t output, xcb_atom_t prop, xcb_atom_t type),
    CRUDE_XCB_CALL_WITH(output, prop, type, 0, 1000, 0, 0)
);
CRUDE_XCB_REQUEST(
    RRQueryOutputProperty, xcb_randr_query_output_property, CRUDE_XCB_ARGS(uint32_t output, xcb_atom_t prop),
    CRUDE_XCB_CALL_WITH(output, prop)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    RRSetOutputPropertyUint32, xcb_randr_change_output_property,
    CRUDE_XCB_ARGS(uint32_t output, xcb_atom_t prop, xcb_atom_t type, const QVector<uint32_t>& v),
    CRUDE_XCB_CALL_WITH(output, prop, type, 32, XCB_PROP_MODE_REPLACE, v.length(), v.constData())
);
CRUDE_XCB_VOID_COOKIE_CHECKED(DPMSForce, xcb_dpms_force_level, CRUDE_XCB_ARGS(uint16_t level), CRUDE_XCB_CALL_WITH(level));
CRUDE_NULLARY_STUB(xcb_dpms_capable);
CRUDE_XCB_REQUEST(DPMSCapable, xcb_dpms_capable, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(0));
CRUDE_XCB_VOID_COOKIE_CHECKED(
    DPMSSetTimeouts, xcb_dpms_set_timeouts, CRUDE_XCB_ARGS(uint16_t standby, uint16_t suspend, uint16_t off),
    CRUDE_XCB_CALL_WITH(standby, suspend, off)
);
CRUDE_NULLARY_STUB(xcb_dpms_get_timeouts);
CRUDE_XCB_REQUEST(DPMSGetTimeouts, xcb_dpms_get_timeouts, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(0));
CRUDE_NULLARY_STUB(xcb_dpms_enable_checked);
CRUDE_XCB_VOID_COOKIE_CHECKED(DPMSEnable, xcb_dpms_enable, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(0));
CRUDE_NULLARY_STUB(xcb_dpms_disable_checked);
CRUDE_XCB_VOID_COOKIE_CHECKED(DPMSDisable, xcb_dpms_disable, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(0));

#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbValueCookie
// XRandR
CRUDE_XCB_GET_FIELD(
    RRGetPrimary, xcb_randr_get_output_primary, uint32_t, output, CRUDE_XCB_ARGS(WinAdaptor&& wid),
    CRUDE_XCB_CALL_WITH(wid)
);
CRUDE_NULLARY_STUB(xcb_dpms_info)
CRUDE_XCB_GET_FIELD(DPMSGetLevel, xcb_dpms_info, uint16_t, power_level, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(0));

}

#endif
