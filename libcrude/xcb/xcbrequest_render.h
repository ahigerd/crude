#if !defined(CRUDE_XCBREQUEST_RENDER_H) || defined(CRUDE_XCB_IMPL)
#  ifndef CRUDE_XCBREQUEST_RENDER_H
#    define CRUDE_XCBREQUEST_RENDER_H
#  endif

#  include "xcbrequestdefs.h"

#  include <QColor>
#  include <QTransform>

#  include <xcb/composite.h>
#  include <xcb/render.h>

namespace CrudeXcb {

#  ifndef CRUDE_XCB_IMPL
struct TransformAdaptor : public xcb_render_transform_t
{
public:
  inline TransformAdaptor(const QTransform& t)
  : xcb_render_transform_t({ int(t.m11() * 0x10000),
                             int(t.m21() * 0x10000),
                             int(t.m31() * 0x10000),
                             int(t.m12() * 0x10000),
                             int(t.m22() * 0x10000),
                             int(t.m32() * 0x10000),
                             int(t.m13() * 0x10000),
                             int(t.m23() * 0x10000),
                             int(t.m33() * 0x10000) })
  {}
};
#  endif

#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbReplyCookie
// Composite
CRUDE_XCB_REQUEST(
    CompositeQueryVersion, xcb_composite_query_version, CRUDE_XCB_ARGS(uint32_t majVer, uint32_t minVer),
    CRUDE_XCB_CALL_WITH(majVer, minVer)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    CompositeRedirectWindow, xcb_composite_redirect_window, CRUDE_XCB_ARGS(WinAdaptor&& window, uint8_t update),
    CRUDE_XCB_CALL_WITH(window, update)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    CompositeRedirectSubwindows, xcb_composite_redirect_subwindows, CRUDE_XCB_ARGS(WinAdaptor&& window, uint8_t update),
    CRUDE_XCB_CALL_WITH(window, update)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    CompositeUnredirectWindows, xcb_composite_unredirect_subwindows,
    CRUDE_XCB_ARGS(WinAdaptor&& window, uint8_t update), CRUDE_XCB_CALL_WITH(window, update)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    CompositeNameWindowPixmap, xcb_composite_name_window_pixmap,
    CRUDE_XCB_ARGS(WinAdaptor&& window, xcb_pixmap_t pixmap), CRUDE_XCB_CALL_WITH(window, pixmap)
);
CRUDE_XCB_REQUEST(
    CompositeGetOverlay, xcb_composite_get_overlay_window, CRUDE_XCB_ARGS(WinAdaptor&& root), CRUDE_XCB_CALL_WITH(root)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    CompositeReleaseOverlay, xcb_composite_release_overlay_window, CRUDE_XCB_ARGS(WinAdaptor&& root),
    CRUDE_XCB_CALL_WITH(root)
);

// Render
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbCopyArea, xcb_copy_area,
    CRUDE_XCB_ARGS(xcb_drawable_t src, xcb_drawable_t dest, xcb_gcontext_t gc, RectAdaptor&& rect, const QPoint& pos),
    CRUDE_XCB_CALL_WITH(src, dest, gc, rect.values[0], rect.values[1], pos.x(), pos.y(), rect.values[2], rect.values[3])
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbCreateGc, xcb_create_gc_aux,
    CRUDE_XCB_ARGS(xcb_gcontext_t gc, WinAdaptor&& win, uint32_t mask, const xcb_create_gc_value_list_t& value),
    CRUDE_XCB_CALL_WITH(gc, win, mask, &value)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(XcbFreeGc, xcb_free_gc, CRUDE_XCB_ARGS(xcb_gcontext_t gc), CRUDE_XCB_CALL_WITH(gc));
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbCreatePixmap, xcb_create_pixmap,
    CRUDE_XCB_ARGS(uint8_t depth, xcb_pixmap_t pid, WinAdaptor&& screen, uint16_t width, uint16_t height),
    CRUDE_XCB_CALL_WITH(depth, pid, screen, width, height)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(XcbFreePixmap, xcb_free_pixmap, CRUDE_XCB_ARGS(xcb_pixmap_t pid), CRUDE_XCB_CALL_WITH(pid));
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbPutImage, xcb_put_image,
    CRUDE_XCB_ARGS(uint8_t format, WinAdaptor&& win, xcb_gcontext_t gc, RectAdaptor&& rect, uint8_t depth, const QImage& image),
    CRUDE_XCB_CALL_WITH(
        format, win, gc, rect.values[2], rect.values[3], rect.values[1], rect.values[2], 0, depth, image.sizeInBytes(),
        image.constBits()
    )
);
CRUDE_XCB_REQUEST(
    RenderQueryVersion, xcb_render_query_version, CRUDE_XCB_ARGS(uint32_t majVer, uint32_t minVer),
    CRUDE_XCB_CALL_WITH(majVer, minVer)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    RenderCreatePicture, xcb_render_create_picture_aux,
    CRUDE_XCB_ARGS(
        xcb_render_picture_t pid, xcb_drawable_t draw, xcb_render_pictformat_t fmt, uint32_t value_mask,
        const xcb_render_create_picture_value_list_t& value
    ),
    CRUDE_XCB_CALL_WITH(pid, draw, fmt, value_mask, &value)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    RenderFreePicture, xcb_render_free_picture, CRUDE_XCB_ARGS(xcb_render_picture_t pid), CRUDE_XCB_CALL_WITH(pid)
);
/*
CRUDE_XCB_VOID_COOKIE_CHECKED(RenderCompositeMasked, xcb_render_composite, \
    CRUDE_XCB_ARGS(uint8_t op, xcb_render_picture_t src, xcb_render_picture_t mask, xcb_render_picture_t dest, int16_t srcX, int16_t srcY, int16_t maskX, int16_t maskY,
      int16_t destX, int16_t destY, int16_t width, int16_t height),
*/
CRUDE_XCB_VOID_COOKIE_CHECKED(
    RenderComposite, xcb_render_composite,
    CRUDE_XCB_ARGS(
        uint8_t op, xcb_render_picture_t src, xcb_render_picture_t dest, int16_t srcX, int16_t srcY, int16_t destX,
        int16_t destY, int16_t width, int16_t height
    ),
    CRUDE_XCB_CALL_WITH(op, src, XCB_NONE, dest, srcX, srcY, 0, 0, destX, destY, width, height)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    RenderFillRect, xcb_render_fill_rectangles,
    CRUDE_XCB_ARGS(uint8_t op, xcb_render_picture_t dest, quint64 color, XcbRectAdaptor&& rect),
    CRUDE_XCB_CALL_WITH(op, dest, *reinterpret_cast<xcb_render_color_t*>(&color), 1, &rect)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    RenderSetSourceTransform, xcb_render_set_picture_transform,
    CRUDE_XCB_ARGS(xcb_render_picture_t pic, TransformAdaptor&& transform), CRUDE_XCB_CALL_WITH(pic, transform)
);

#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbValueCookie

// Render
CRUDE_XCB_REQUEST(
    XcbGetImage, xcb_get_image, CRUDE_XCB_ARGS(uint8_t format, WinAdaptor&& win, RectAdaptor&& rect, uint32_t mask),
    CRUDE_XCB_CALL_WITH(format, win, rect.values[0], rect.values[1], rect.values[2], rect.values[3], mask)
);
CRUDE_NULLARY_STUB(xcb_render_query_pict_formats);
CRUDE_XCB_REQUEST(RenderQueryPictFormats, xcb_render_query_pict_formats, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(0));
XCB_DEFINE_REPLY_VECTOR(RenderPictFormats, RenderPictFormat, xcb_render_query_pict_formats, formats);
XCB_DEFINE_REPLY_ITER_VECTOR(
    RenderPictScreens, RenderPictScreen, xcb_render_query_pict_formats, screens, xcb_render_pictscreen_next
);
XCB_DEFINE_ITER_VECTOR(
    RenderPictDepths, RenderPictDepth, xcb_render_pictscreen_t, xcb_render_pictscreen_depths, xcb_render_pictdepth_next
);
XCB_DEFINE_VECTOR(RenderPictVisuals, RenderPictVisual, xcb_render_pictdepth_t, xcb_render_pictdepth_visuals);

}

#endif
