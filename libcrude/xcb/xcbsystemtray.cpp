#include "xcbextension.h"
#include "xcbrequest_fixes.h"
#include "xcbrequest_prop.h"
#include "xcbrequest_wm.h"
#include "xcbsystemtray_p.h"
#include "xcbintegration.h"

#include <QLabel>
#include <QStyle>
#include <QTimer>
#include <QWindow>

#define SYSTEM_TRAY_REQUEST_DOCK   0
#define SYSTEM_TRAY_BEGIN_MESSAGE  1
#define SYSTEM_TRAY_CANCEL_MESSAGE 2

XcbSystemTrayPrivate::XcbSystemTrayPrivate(SystemTray* parent) : SystemTrayPrivate(parent), mgr("_NET_SYSTEM_TRAY_S0")
{
  auto orientRequest = callXcbInternAtom(false, "_NET_SYSTEM_TRAY_ORIENTATION");
  auto visualRequest = callXcbInternAtom(false, "_NET_SYSTEM_TRAY_VISUAL");
  auto opcodeRequest = callXcbInternAtom(false, "_NET_SYSTEM_TRAY_OPCODE");
  auto messageRequest = callXcbInternAtom(false, "_NET_SYSTEM_TRAY_MESSAGE_DATA");
  useDamage = CrudeXcb::checkExtension(CrudeXcb::damage, 1, 1);

  QObject::connect(&mgr, SIGNAL(acquired()), this, SLOT(acquired()));
  QObject::connect(&mgr, SIGNAL(unavailable()), this, SLOT(unavailable()));
  QObject::connect(&mgr, SIGNAL(revoked()), this, SLOT(revoked()));
  QObject::connect(&mgr, SIGNAL(released()), this, SLOT(released()));
  QObject::connect(
      &mgr, SIGNAL(message(xcb_atom_t, QVector<uint32_t>)), this, SLOT(message(xcb_atom_t, QVector<uint32_t>))
  );
  XcbIntegration::instance()->addEventHandler(this);
  mgr.acquire();

  orientAtom = orientRequest->takeResult();
  visualAtom = visualRequest->takeResult();
  opcodeAtom = opcodeRequest->takeResult();
  messageAtom = messageRequest->takeResult();

  callXcbChangeAttributes(
    p->winId(),
    XCB_CW_EVENT_MASK,
    { XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_STRUCTURE_NOTIFY }
  )->force();
}

XcbSystemTrayPrivate::~XcbSystemTrayPrivate()
{
  p->shuttingDown = true;
  mgr.release();
}

void XcbSystemTrayPrivate::acquired()
{
  callXcbSetProperty(mgr.winId(), visualAtom, XCB_ATOM_VISUALID, c_EWMH->screens[0]->root_visual);
  emit p->acquired();
}

void XcbSystemTrayPrivate::unavailable()
{
  emit p->unavailable();
  released();
}

void XcbSystemTrayPrivate::revoked()
{
  emit p->revoked();
}

void XcbSystemTrayPrivate::released()
{
  while (!p->icons.isEmpty()) {
    p->removeIcon(p->icons.last());
  }

  QLabel* iconWidget = new QLabel;
  QIcon icon = p->style()->standardIcon(QStyle::SP_MessageBoxWarning);
  iconWidget->setPixmap(icon.pixmap(22));
  iconWidget->setToolTip("System Tray Unavailable");
  p->addIcon(new TrayIconHost(iconWidget));
}

void XcbSystemTrayPrivate::updateOrientation()
{
  callXcbSetProperty(mgr.winId(), orientAtom, XCB_ATOM_CARDINAL, (uint32_t)(p->isVertical ? 1 : 0));
}

void XcbSystemTrayPrivate::message(xcb_atom_t type, const QVector<uint32_t>& msg)
{
  if (type == opcodeAtom) {
    // msg[0] is a timestamp
    uint32_t opcode = msg[1];
    xcb_window_t child = msg[2];

    if (opcode == SYSTEM_TRAY_REQUEST_DOCK) {
      callXcbReparentWindow(child, p->winId(), 0, 0)->force();
      // Explicitly set a background on the systray icon, because Qt (wrongly) assumes
      // that the embedded widget is opaque. Also install an event filter to detect
      // the window being destroyed so we can remove it before Qt complains.
      bool ok = callXcbChangeAttributes(
          child,
          XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK,
          { p->palette().button().color().rgb(), XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_PROPERTY_CHANGE }
      )->force();
      if (!ok) {
        qDebug() << "Error setting attributes on systray icon, not embedding";
        return;
      }
      p->addIcon(new TrayIconHost(child, useDamage));
    }
  }
  // TODO: balloon messages
}

bool XcbSystemTrayPrivate::handleEvent(const CrudeXcb::EventType& event)
{
  auto XEMBED_INFO = callXcbInternAtom(false, "_XEMBED_INFO")->takeResult();
  if (event.extension == CrudeXcb::damage) {
    auto de = event.cast<xcb_damage_notify_event_t>();
    TrayIconHost* icon = p->iconsById.value(de->drawable);
    if (icon) {
      icon->damaged();
      return true;
    }
    return false;
  }
  if (event.eventType == XCB_PROPERTY_NOTIFY) {
    auto pe = event.cast<xcb_property_notify_event_t>();
    if (pe->atom == c_EWMH->_NET_WM_NAME || pe->atom == XCB_ATOM_WM_CLASS) {
      TrayIconHost* icon = p->iconsById.value(pe->window);
      if (icon) {
        if (pe->atom == XCB_ATOM_WM_CLASS) {
          auto cookie = callIcccmGetWmClass(pe->window);
          if (cookie->force()) {
            auto names = cookie->result();
            icon->className = names.class_name;
          } else {
            icon->className = "[unknown]";
          }
        } else {
          icon->title = callEwmhGetName(pe->window)->result();
        }
      }
    } else if (pe->atom == XEMBED_INFO) {
      TrayIconHost* icon = p->iconsById.value(pe->window);
      if (icon) {
        auto cookie = callXcbGetProperty(pe->window, pe->atom, pe->atom);
        if (cookie->force()) {
          auto fields = xcbPropertyValue<QVector<quint32>>(cookie->result());
          if (fields.size() == 2 && (fields[1] & 0x1) == 0) {
            p->removeIcon(icon);
          }
        }
      }
    }
  } else if (event.eventType == XCB_CONFIGURE_NOTIFY) {
    TrayIconHost* icon = p->iconsById.value(event.cast<xcb_configure_notify_event_t>()->window);
    if (icon) {
      icon->updateIconGeometry();
    }
  } else if (event.eventType == XCB_UNMAP_NOTIFY || event.eventType == XCB_DESTROY_NOTIFY) {
    p->validate();
  }
  return false;
}
