#include "xcbextension.h"

#include "xcbintegration.h"

#include <functional>
#include <memory>
#include <vector>
#include <xcb/composite.h>
#include <xcb/damage.h>
#include <xcb/dpms.h>
#include <xcb/randr.h>
#include <xcb/render.h>
#include <xcb/shape.h>
#include <xcb/xfixes.h>

namespace CrudeXcb {

using QueryFn = std::function<const xcb_query_extension_reply_t*(int, int)>;

struct XcbExtension
{
  XcbExtension(Extension id, xcb_extension_t* ext, QueryFn query)
  : id(id), query(query), extension(ext), firstEvent(0), firstError(0)
  {}

  Extension id;
  QueryFn query;
  xcb_extension_t* extension;
  uint8_t firstEvent, firstError;
};

// shape and dpms have APIs that aren't perfectly parallel with the other extensions
#define xcb_shape_query_version(c, maj, min) xcb_shape_query_version(c)
#define xcb_dpms_query_version               xcb_dpms_get_version
#define xcb_dpms_query_version_reply         xcb_dpms_get_version_reply

#define EXT(id) \
  { \
    CrudeXcb::id, &xcb_##id##_id, [](uint32_t maj, uint32_t min) -> const xcb_query_extension_reply_t* { \
      auto data = xcb_get_extension_data(c_X11, &xcb_##id##_id); \
      if (!data->present) \
        return nullptr; \
      xcb_generic_error_t* err = nullptr; \
      auto reply = xcb_##id##_query_version_reply(c_X11, xcb_##id##_query_version(c_X11, maj, min), &err); \
      bool ok = \
          !(err || !reply || reply->major_version < maj || (reply->major_version == maj && reply->minor_version < min)); \
      if (reply) \
        std::free(reply); \
      if (err) \
        std::free(err); \
      return ok ? data : nullptr; \
    } \
  }

static std::vector<XcbExtension> exts = {
  EXT(randr), EXT(render), EXT(composite), EXT(damage), EXT(xfixes), EXT(shape),
#define major_version server_major_version
#define minor_version server_minor_version
  EXT(dpms),
};

void prefetchExtensions()
{
  for (auto& ext : exts) {
    xcb_prefetch_extension_data(c_X11, ext.extension);
  }
}

bool checkExtension(Extension id, uint32_t majVer, uint32_t minVer)
{
  if (id == core) {
    return true;
  }
  if (id >= int(exts.size())) {
    return false;
  }
  auto& ext = exts[id];
  auto data = ext.query(majVer, minVer);
  if (data) {
    ext.firstEvent = data->first_event;
    ext.firstError = data->first_error;
  }
  return data;
}

ErrorType translateError(uint8_t errorCode)
{
  ErrorType result = { core, errorCode };
  for (auto& ext : exts) {
    if (errorCode >= ext.firstError && errorCode - ext.firstError < result.errorCode) {
      result.extension = ext.id;
      result.errorCode = errorCode - ext.firstError;
    }
  }
  return result;
}

EventType translateEvent(uint8_t eventId)
{
  EventType result = { core, uint8_t(eventId & 0x7F), (eventId & 0x80) != 0, nullptr };
  for (auto& ext : exts) {
    if (eventId >= ext.firstEvent && eventId - ext.firstEvent < result.eventType) {
      result.extension = ext.id;
      result.eventType = eventId - ext.firstEvent;
    }
  }
  return result;
}

};
