#ifndef CRUDE_XCBREQUESTDEFS_H
#define CRUDE_XCBREQUESTDEFS_H

#include "xcbcookie.h"
#include "xcbrequest_macro.h"

#include <QApplication>
#include <QIcon>
#include <QWidget>
#include <QWindow>
#include <QX11Info>

#include <cstring>
#include <type_traits>
#include <vector>

#define XCB_FIELD_TYPE(xcb_field) typename std::remove_pointer<decltype(::xcb_field(nullptr))>::type
#define XCB_DEFINE_VECTOR(VectorName, ElementName, container_t, xcb_field) \
  using ElementName = XCB_FIELD_TYPE(xcb_field); \
  struct VectorName : public std::vector<ElementName> \
  { \
    VectorName(const container_t* container); \
    VectorName(const container_t& container); \
  }
#define XCB_DEFINE_REPLY_VECTOR(VectorName, ElementName, xcb_name, field) \
  XCB_DEFINE_VECTOR(VectorName, ElementName, xcb_name##_reply_t, xcb_name##_##field)
#define XCB_ITER_FIELD_TYPE(xcb_iter) typename std::remove_pointer<decltype(::xcb_iter##_iterator(nullptr).data)>::type
// Iter vectors are pointers because the data isn't movable
#define XCB_DEFINE_ITER_VECTOR(VectorName, ElementName, container_t, xcb_iter, xcb_next) \
  using ElementName = XCB_ITER_FIELD_TYPE(xcb_iter); \
  struct VectorName : public std::vector<ElementName*> \
  { \
    VectorName(const container_t* container); \
    VectorName(const container_t& container); \
  }
#define XCB_DEFINE_REPLY_ITER_VECTOR(VectorName, ElementName, xcb_name, field, xcb_next) \
  XCB_DEFINE_ITER_VECTOR(VectorName, ElementName, xcb_name##_reply_t, xcb_name##_##field, xcb_next)

namespace CrudeXcb {

struct WinAdaptor
{
  inline WinAdaptor(QWidget* w) : winId(w->winId()) {}

  inline WinAdaptor(QWindow* w) : winId(w->winId()) {}

  inline WinAdaptor(uint32_t w) : winId(w) {}

  inline WinAdaptor(const WinAdaptor& other) = default;
  inline WinAdaptor(WinAdaptor&& other) = default;

  inline operator uint32_t() const { return winId; }
#ifndef QT_NO_DEBUG
  inline operator QVariant() const { return winId; }
#endif
  uint32_t winId;
};

struct RectAdaptor
{
  RectAdaptor(const QRect& r);
  uint32_t values[4];
};

struct XcbRectAdaptor : public xcb_rectangle_t
{
  XcbRectAdaptor(const QRect& r);
};

#undef CRUDE_COOKIE_BASE_CLASS
#define CRUDE_COOKIE_BASE_CLASS XcbValueCookie
CRUDE_XCB_GET_FIELD(
    XcbInternAtom, xcb_intern_atom, uint32_t, atom, CRUDE_XCB_ARGS(bool only_if_exists, const QByteArray& name),
    CRUDE_XCB_CALL_WITH(only_if_exists, name.length(), name.constData())
);

}

using namespace CrudeXcb;

#endif
