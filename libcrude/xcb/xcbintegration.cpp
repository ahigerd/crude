#include "xcbintegration.h"

#include "../processinfo.h"
#include "../throttle.h"
#include "xcbdisplaypower.h"
#include "xcbextension.h"
#include "xcbrequest_prop.h"
#include "xcbrequest_render.h"
#include "xcbrequest_wm.h"

#include <QCoreApplication>
#include <QScreen>
#include <QSocketNotifier>
#include <QStyle>
#include <QWindow>
#include <QX11Info>
#include <QtDebug>
#include <QtGlobal>

#define TIMEOUT_AGE 0

static const QSet<xcb_atom_t> ignorePropsBase{ XCB_ATOM_WM_ICON_NAME };
static QSet<xcb_atom_t> ignoreProps;

xcb_ewmh_connection_t* XcbIntegration::ewmhConnection()
{
  XcbIntegration* xi = instance();
  if (!xi) {
    return nullptr;
  }
  return &xi->ewmh;
}

xcb_connection_t* XcbIntegration::x11Connection()
{
  xcb_ewmh_connection_t* ec = ewmhConnection();
  if (!ec) {
    return nullptr;
  }
  return ec->connection;
}

XcbIntegration* XcbIntegration::instance()
{
  XcbIntegration* xi = qobject_cast<XcbIntegration*>(PlatformIntegration::instance());
  return xi;
}

XcbIntegration::XcbIntegration(int argc, char** argv)
: PlatformIntegration(),
  xcb_fd(0),
  rootEventMask(XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_PROPERTY_CHANGE),
  systrayId(0),
  notifier(nullptr),
  trackWindowList(false),
  backlightObj(nullptr)
{
  if (argc > 0 && argv) {
    for (int i = 0; i < argc - 1; i++) {
      if (QString::fromUtf8(argv[i]) == "-display") {
        display = argv[i + 1];
        break;
      }
    }
  }
  if (display.isEmpty()) {
    display = qgetenv("DISPLAY");
    if (display.isEmpty()) {
      qFatal("XcbIntegration: unable to determine DISPLAY");
    }
  }

  int defaultScreen = 0;
  ewmh.connection = nullptr;
  xcb_connection_t* conn = xcb_connect(display.isEmpty() ? nullptr : display.constData(), &defaultScreen);
  if (!conn || xcb_connection_has_error(conn)) {
    xcb_disconnect(conn);
    qFatal("XcbIntegration: unable to connect to display %s", display.constData());
  }

  // We don't wrap these functions with the xcbrequest macros because they're all one-use things
  // that need to be handled synchronously anyway until they're connected.
  xcb_fd = xcb_get_file_descriptor(conn);
  notifier = new QSocketNotifier(xcb_fd, QSocketNotifier::Read, this);
  QObject::connect(notifier, SIGNAL(activated(int)), this, SLOT(pollXcb()));

  auto cookie = xcb_ewmh_init_atoms(conn, &ewmh);
  xcb_generic_error_t* err = nullptr;
  bool ok = xcb_ewmh_init_atoms_replies(&ewmh, cookie, &err);
  if (!ok) {
    xcb_disconnect(conn);
    qFatal("XcbIntegration: failed to initialize EWMH");
  }

  CrudeXcb::prefetchExtensions();

  ignoreProps = ignorePropsBase;
  ignoreProps << ewmh._NET_WM_ICON_NAME << ewmh._NET_WM_VISIBLE_ICON_NAME << ewmh._NET_WM_ALLOWED_ACTIONS
              << ewmh._NET_WM_USER_TIME << callXcbInternAtom(false, "_NET_WM_WINDOW_OPACITY")->takeResult();

  systrayAtom = callXcbInternAtom(false, "_NET_SYSTEM_TRAY_S0")->takeResult();

  startTimer(50);
  xcb_window_t root = QX11Info::appRootWindow();
  rootEventMask |= XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY;
  callXcbChangeAttributes(root, XCB_CW_EVENT_MASK, { rootEventMask });
  pollXcb();
}

XcbIntegration::~XcbIntegration()
{
  xcb_disconnect(ewmh.connection);
}

void XcbIntegration::enableWindowList()
{
  trackWindowList = true;
  xcb_window_t root = QX11Info::appRootWindow();
  rootEventMask |= XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY;
  callXcbChangeAttributes(root, XCB_CW_EVENT_MASK, { rootEventMask });
}

bool XcbIntegration::enableWindowManagement()
{
  xcb_window_t root = QX11Info::appRootWindow();
  int oldMask = rootEventMask;
  rootEventMask |= XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT | XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY;
  auto cookie = callXcbChangeAttributes(root, XCB_CW_EVENT_MASK, { rootEventMask });
  if (!cookie->force()) {
    rootEventMask = oldMask;
    return false;
  }
  return true;
}

WindowRef XcbIntegration::findWindow(uint32_t wid) const
{
  return knownWindows[wid];
}

std::vector<uint32_t> XcbIntegration::windowStackingOrder() const
{
  return QueryTreeChildren(callXcbQueryTree(QX11Info::appRootWindow())->result());
}

bool XcbIntegration::windowManagerExists()
{
  if (rootEventMask & XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT) {
    // There is one... and we're it!
    return true;
  }
  xcb_window_t root = QX11Info::appRootWindow();
  auto cookie =
      callXcbChangeAttributes(root, XCB_CW_EVENT_MASK, { rootEventMask | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT });
  if (!cookie->force()) {
    // The set failed, so it doesn't need to be reset
    return true;
  } else {
    callXcbChangeAttributes(root, XCB_CW_EVENT_MASK, { rootEventMask });
    return false;
  }
}

// Note: This is a blocking call that could potentially take some time to run.
// Invoke it judiciously.
bool XcbIntegration::desktopManagerExists()
{
  xcb_window_t root = QX11Info::appRootWindow();
  auto reply = callXcbQueryTree(root)->takeResult();
  int ct = xcb_query_tree_children_length(reply);
  xcb_window_t* children = xcb_query_tree_children(reply);
  QList<EwmhGetWindowType*> cookies;
  for (int i = 0; i < ct; i++) {
    cookies << callEwmhGetWindowType(children[i]);
  }
  bool found = false;
  for (int i = 0; i < ct; i++) {
    if (cookies[i]->result().contains(c_EWMH->_NET_WM_WINDOW_TYPE_DESKTOP)) {
      found = true;
      break;
    }
  }
  qDeleteAll(cookies);
  return found;
}

template <bool hasTimestamp = false>
struct TimestampGetter
{
  template <typename T>
  static int getTimestamp(const T* const)
  {
    return 0;
  }
};

template <>
struct TimestampGetter<true>
{
  template <typename T>
  static int getTimestamp(const T* const obj)
  {
    return obj->time;
  }
};

template <typename T>
struct TimestampDetector
{
  struct Base
  {
    int time;
  };

  struct Eval : T, Base
  {};
  template <typename U, U>
  struct Test;
  using Pass = char;
  using Fail = int;
  static_assert(sizeof(Pass) != sizeof(Fail), "can't distinguish between pass and fail");
  template <typename U>
  static Fail& test(Test<int Base::*, &U::time>*);
  template <typename U>
  static Pass& test(...);

  enum {
    value = sizeof(test<Eval>(0)) == sizeof(Pass)
  };
};

template <typename T>
int getTimestamp(T* event)
{
  return TimestampGetter<TimestampDetector<T>::value>::getTimestamp(event);
}

int XcbIntegration::lastTimestamp() const
{
  return lastTS;
}

void XcbIntegration::pollXcb()
{
  notifier->setEnabled(false);
  static Throttle scanLimiter = Throttle::connectedTo(this, SLOT(scanWindows()));
  xcb_generic_event_t* event;
  uint32_t lastSequence;
  while ((event = xcb_poll_for_event(ewmh.connection))) {
    lastSequence = event->sequence;
    int ts = getTimestamp(event);
    if (ts) {
      lastTS = ts;
    }
    auto eventType = CrudeXcb::translateEvent(event);
    if (dispatchEvent(eventType)) {
      continue;
    }
    if (eventType.extension == CrudeXcb::core) {
      switch (eventType.eventType) {
      case XCB_PROPERTY_NOTIFY: propertyNotify(eventType.cast<xcb_property_notify_event_t>()); break;
      case XCB_CONFIGURE_NOTIFY: {
        auto ce = eventType.cast<xcb_configure_notify_event_t>();
        if (!ce->override_redirect) {
          configureNotify(ce);
        }
      } break;
      case XCB_CLIENT_MESSAGE: clientMessage(eventType.cast<xcb_client_message_event_t>()); break;
      case XCB_DESTROY_NOTIFY:
        if (eventType.cast<xcb_destroy_notify_event_t>()->window == systrayId) {
          systrayId = 0;
          emit systemTrayAvailabilityChanged(false);
        }
        /* FALLTHRU */
      case XCB_CREATE_NOTIFY:
      case XCB_MAP_NOTIFY:
      case XCB_UNMAP_NOTIFY:
      case XCB_REPARENT_NOTIFY: scanLimiter.trigger(); break;
      case XCB_GE_GENERIC: genericEvent(eventType.cast<xcb_ge_generic_event_t>()); break;
      case XCB_EXPOSE:
      case XCB_FOCUS_IN:
      case XCB_FOCUS_OUT:
      case XCB_ENTER_NOTIFY:
      case XCB_LEAVE_NOTIFY:
      case XCB_KEY_RELEASE:
        // Some events are filtered out because we never care.
        // Even integrations shouldn't need these because Qt handles it.
        break;
      default:
        // Log other events so we can track them during development.
        if (debugEnabled) {
          qDebug() << "unhandled event" << event->response_type;
        }
      }
    } else {
      // Log other events so we can track them during development.
      if (debugEnabled) {
        qDebug() << "unhandled event" << event->response_type;
      }
    }
  }
  int err = xcb_connection_has_error(c_X11);
  if (err) {
    qWarning("XcbIntegration: xcb connection interrupted (error %i)", err);
    QCoreApplication::instance()->quit();
    return;
  }
  while (cookies.length() > 0 && (uint32_t)cookies[0]->sequence() <= lastSequence) {
    forceNextCookie();
  }
  notifier->setEnabled(true);
}

void XcbIntegration::requestWindowList()
{
  scanWindows();
}

void XcbIntegration::timerEvent(QTimerEvent*)
{
  int len = cookies.length();
  for (int i = 0; i < len; i++) {
    XcbCookieBase* cookie = cookies[i];
    cookie->age++;
  }
  while (cookies.length() > 0 && cookies[0]->age > TIMEOUT_AGE) {
    forceNextCookie();
  }
}

void XcbIntegration::expect(XcbCookieBase* cookie)
{
  cookies << cookie;
}

void XcbIntegration::clear(XcbCookieBase* cookie)
{
  cookies.removeAll(cookie);
}

void XcbIntegration::forceNextCookie()
{
  XcbCookieBase* cookie = cookies.takeFirst();
  cookie->force();
  delete cookie;
}

void XcbIntegration::propertyNotify(xcb_property_notify_event_t* event)
{
  uint32_t window = event->window, atom = event->atom;
  static Throttle limiter = Throttle::connectedTo(this, SLOT(queryPendingWindows()));
  if (dispatchEvent(CrudeXcb::translateEvent((xcb_generic_event_t*)event))) {
    // handled by listener
    return;
  } else if (ignoreProps.contains(atom)) {
    return;
  } else if (atom == ewmh._NET_WORKAREA) {
    emit screenAreaChanged();
    return;
  } else if (!trackWindowList) {
    return;
  } else if (atom == XCB_ATOM_WM_NAME || atom == ewmh._NET_WM_NAME || atom == ewmh._NET_WM_VISIBLE_NAME) {
    pendingProperties[window] << WmName;
  } else if (atom == ewmh._NET_WM_ICON) {
    pendingProperties[window] << WmIcon;
  } else if (atom == XCB_ATOM_WM_HINTS || atom == ewmh._NET_WM_STATE || atom == ewmh._NET_WM_ALLOWED_ACTIONS) {
    pendingProperties[window] << WmHints;
  } else if (atom == XCB_ATOM_WM_CLASS) {
    pendingProperties[window] << WmClass;
  } else if (atom == ewmh._NET_ACTIVE_WINDOW) {
    requestCurrentWindow();
  } else if (atom == ewmh._NET_CLIENT_LIST || atom == ewmh._NET_CLIENT_LIST_STACKING) {
    // This is handled elsewhere.
    // TODO: If for some unexplainable reason we miss restacking events and it matters,
    // add a throttled scanWindow call here.
    return;
  } else {
    if (debugEnabled) {
      if (atom == XCB_ATOM_WM_NORMAL_HINTS || atom == ewmh._NET_WM_STRUT || atom == ewmh._NET_WM_STRUT_PARTIAL || atom == ewmh._NET_FRAME_EXTENTS) {
        // In the interest of limiting debug spam, these are explicitly ignored because we don't use them.
      } else {
        static QSet<QString> hide{ "WM_STATE", "_KDE_NET_WM_FRAME_STRUT" };
        QString name = getAtomName(atom);
        if (!hide.contains(name)) {
          qDebug() << "property" << window << name;
        }
      }
    }
  }
  limiter.trigger();
}

void XcbIntegration::configureNotify(xcb_configure_notify_event_t*)
{
  requestCurrentWindow();
  // TODO: if CRUDE ever grows a window manager, this will need a dispatchEvent() call
  /*
  static Throttle limiter = Throttle::connectedTo(this, SLOT(requestCurrentWindow()));
  limiter.trigger();
  */
}

void XcbIntegration::genericEvent(xcb_ge_generic_event_t* ev)
{
  // Consume silently for now
  // TODO: dispatchEvent() if needed
  if (debugEnabled) {
    qDebug() << "generic" << ev->event_type;
  }
}

void XcbIntegration::clientMessage(xcb_client_message_event_t* event)
{
  // Most client messages that will be picked up here are for window management.
  // One is of particular importance, though: the system tray showing up.
  if (event->window != QX11Info::appRootWindow()) {
    return;
  }
  if (event->type != XCB_ATOM_RESOURCE_MANAGER) {
    return;
  }
  if (event->data.data32[1] != systrayAtom) {
    return;
  }
  systrayId = event->data.data32[2];
  callXcbChangeAttributes(systrayId, XCB_CW_EVENT_MASK, { XCB_EVENT_MASK_STRUCTURE_NOTIFY });
  emit systemTrayAvailabilityChanged(true);
}

void XcbIntegration::scanWindows()
{
  if (!trackWindowList) {
    return;
  }
  callEwmhGetClientList()->then(this, SLOT(gotWindowList(QVector<xcb_window_t>)));
}

void XcbIntegration::gotWindowList(const QVector<xcb_window_t>& list)
{
  QSet<xcb_window_t> removed = QSet<xcb_window_t>::fromList(knownWindows.keys());
  for (xcb_window_t window : list) {
    bool isNew = !removed.remove(window);
    if (isNew) {
      pendingProperties[window] << NewWindowProperties;
    }
  }
  for (xcb_window_t window : removed) {
    knownWindows.remove(window);
    pendingProperties.remove(window);
  }
  queryPendingWindows();
  callEwmhGetActiveWindow()->then(this, SLOT(broadcastWindowList(xcb_window_t)));
}

QString XcbIntegration::getAtomName(uint32_t atom)
{
  static QMap<uint32_t, QString> atoms;
  if (!atoms.contains(atom)) {
    // Not worth the effort of making an XcbRequest for this, since it's debugging-only,
    // always synchronous, and a different API from the methods we do use.
    auto cookie = xcb_get_atom_name(c_X11, atom);
    xcb_generic_error_t* err = nullptr;
    auto reply = xcb_get_atom_name_reply(c_X11, cookie, &err);
    if (!reply || err) {
      atoms[atom] = "[unknown atom: " + QString::number(atom) + "]";
    } else {
      QByteArray utf8(xcb_get_atom_name_name(reply), xcb_get_atom_name_name_length(reply));
      atoms[atom] = QString::fromUtf8(utf8);
    }
    if (reply) {
      free(reply);
    }
  }
  return atoms[atom];
}

#define Q_RESULT(type) static_cast<type*>(query.cookie)->result()

void XcbIntegration::queryPendingWindows()
{
  struct Query
  {
    enum RequestType {
      WindowType,
      EwmhName,
      IcccmName,
      VisibleName,
      EwmhIcon,
      Hints,
      State,
      QueryTree,
      Pid,
      Hostname,
      Class,
      Actions
    };

    uint32_t window;
    RequestType request;
    XcbCookieBase* cookie;
  };

  QList<Query> queries;
  QList<uint32_t> windows = pendingProperties.keys();
  for (uint32_t window : windows) {
    if (!knownWindows.contains(window)) {
      pendingProperties[window] << NewWindowProperties;
      knownWindows[window] = WindowRef::create(window);
    }
    QSet<PropertyGroup> props = pendingProperties.take(window);
    if (props.contains(NewWindowProperties)) {
      props << QueryTree << WmName << WmIcon << WmHints << WmClass;
    }
    for (PropertyGroup group : props) {
      switch (group) {
      case NewWindowProperties:
        queries << (Query){ window, Query::Pid, callEwmhGetPid(window) };
        queries << (Query){ window, Query::Hostname, callIcccmGetMachine(window) };
        break;
      case QueryTree:
        queries << (Query){ window, Query::WindowType, callEwmhGetWindowType(window) };
        queries << (Query){ window, Query::QueryTree, callXcbQueryTree(window) };
        break;
      case WmName:
        // Order is important: later entries have higher priority
        queries << (Query){ window, Query::IcccmName, callIcccmGetName(window) };
        queries << (Query){ window, Query::EwmhName, callEwmhGetName(window) };
        queries << (Query){ window, Query::VisibleName, callEwmhGetVisibleName(window) };
        break;
      case WmIcon:
        queries << (Query){ window, Query::EwmhIcon, callEwmhGetIcon(window) };
        break;
      case WmHints:
        knownWindows[window]->urgent = false;
        queries << (Query){ window, Query::WindowType, callEwmhGetWindowType(window) };
        queries << (Query){ window, Query::State, callEwmhGetState(window) };
        queries << (Query){ window, Query::Hints, callIcccmGetHints(window) };
        queries << (Query){ window, Query::Actions, callEwmhGetAllowedActions(window) };
        break;
      case WmClass:
        queries << (Query){ window, Query::Class, callIcccmGetWmClass(window) };
        break;
      }
    }
  }
  for (const Query& query : queries) {
    if (!query.cookie->force()) {
      if (debugEnabled) {
        qDebug() << "cannot get" << query.request << "due to error" << query.cookie->error();
      }
      continue;
    }
    WindowRef win = knownWindows[query.window];
    switch (query.request) {
    case Query::Pid:
      win->pid = Q_RESULT(EwmhGetPid);
      win->program = ProcessInfo(win->pid).commandName();
      break;
    case Query::Hostname: win->hostname = Q_RESULT(IcccmGetMachine); break;
    case Query::WindowType: {
      auto types = Q_RESULT(EwmhGetWindowType);
      bool isClient = true;
      for (xcb_atom_t atom : types) {
        if (atom == ewmh._NET_WM_WINDOW_TYPE_DOCK ||
            atom == ewmh._NET_WM_WINDOW_TYPE_DESKTOP ||
            atom == ewmh._NET_WM_WINDOW_TYPE_TOOLBAR ||
            atom == ewmh._NET_WM_WINDOW_TYPE_MENU ||
            atom == ewmh._NET_WM_WINDOW_TYPE_DROPDOWN_MENU ||
            atom == ewmh._NET_WM_WINDOW_TYPE_POPUP_MENU ||
            atom == ewmh._NET_WM_WINDOW_TYPE_TOOLTIP ||
            atom == ewmh._NET_WM_WINDOW_TYPE_NOTIFICATION ||
            atom == ewmh._NET_WM_WINDOW_TYPE_COMBO ||
            atom == ewmh._NET_WM_WINDOW_TYPE_DND) {
          isClient = false;
          break;
        }
      }
      win->isClient = isClient;
    } break;
    case Query::QueryTree: {
      auto tree = Q_RESULT(XcbQueryTree);
      if (!tree || !tree->parent || tree->parent == QX11Info::appRootWindow()) {
        win->baseWindowId = win->windowId;
      } else {
        win->baseWindowId = tree->parent;
      }
      callXcbChangeAttributes(win->windowId, XCB_CW_EVENT_MASK, { XCB_EVENT_MASK_PROPERTY_CHANGE });
      callXcbChangeAttributes(win->baseWindowId, XCB_CW_EVENT_MASK, { XCB_EVENT_MASK_PROPERTY_CHANGE });
    } break;
    case Query::IcccmName:
      if (!Q_RESULT(IcccmGetName).isEmpty()) {
        win->name = Q_RESULT(IcccmGetName);
      }
      break;
    case Query::EwmhName:
      if (!Q_RESULT(EwmhGetName).isEmpty()) {
        win->name = Q_RESULT(EwmhGetName);
      }
      break;
    case Query::VisibleName:
      if (!Q_RESULT(EwmhGetVisibleName).isEmpty()) {
        win->name = Q_RESULT(EwmhGetVisibleName);
      }
      break;
    case Query::EwmhIcon: {
      QIcon icon = Q_RESULT(EwmhGetIcon);
      if (!icon.isNull()) {
        win->icon = icon;
        win->iconSource = WindowEntry::EwmhIcon;
      } else {
        if (win->iconSource == WindowEntry::EwmhIcon) {
          win->iconSource = WindowEntry::NoIcon;
        }
        if (win->icon.isNull() && !pendingProperties[query.window].contains(WmHints)) {
          queries << (Query){ query.window, Query::Hints, callIcccmGetHints(query.window) };
        }
      }
    } break;
    case Query::State: {
      auto state = Q_RESULT(EwmhGetState);
      if (state.contains(ewmh._NET_WM_STATE_DEMANDS_ATTENTION)) {
        win->urgent = true;
      }
    } break;
    case Query::Hints: {
      auto hints = Q_RESULT(IcccmGetHints);
      if (hints.flags & 0x100) {
        win->urgent = true;
      }
      if (hints.flags & 0x040) {
        win->groupLeaderId = hints.window_group;
      } else {
        win->groupLeaderId = 0;
      }
      if (win->iconSource != WindowEntry::EwmhIcon && (hints.flags & 0x004)) {
        QIcon icccmIcon = getIcon(hints.icon_pixmap, (hints.flags & 0x020) ? hints.icon_mask : XCB_NONE);
        if (!icccmIcon.isNull()) {
          win->icon = icccmIcon;
          win->iconSource = WindowEntry::IcccmIcon;
        }
      }
    } break;
    case Query::Actions: {
      auto atoms = Q_RESULT(EwmhGetAllowedActions);
      WindowEntry::WindowActions actions;
      for (xcb_atom_t atom : atoms) {
        if (atom == ewmh._NET_WM_ACTION_MOVE) {
          actions |= WindowEntry::MoveAction;
        } else if (atom == ewmh._NET_WM_ACTION_RESIZE) {
          actions |= WindowEntry::ResizeAction;
        } else if (atom == ewmh._NET_WM_ACTION_MINIMIZE) {
          actions |= WindowEntry::MinimizeAction;
        } else if (atom == ewmh._NET_WM_ACTION_SHADE) {
          actions |= WindowEntry::ShadeAction;
        } else if (atom == ewmh._NET_WM_ACTION_STICK) {
          actions |= WindowEntry::StickAction;
        } else if (atom == ewmh._NET_WM_ACTION_MAXIMIZE_HORZ) {
          actions |= WindowEntry::HMaximizeAction;
        } else if (atom == ewmh._NET_WM_ACTION_MAXIMIZE_VERT) {
          actions |= WindowEntry::VMaximizeAction;
        } else if (atom == ewmh._NET_WM_ACTION_FULLSCREEN) {
          actions |= WindowEntry::FullscreenAction;
        } else if (atom == ewmh._NET_WM_ACTION_CHANGE_DESKTOP) {
          actions |= WindowEntry::ChangeDesktopAction;
        } else if (atom == ewmh._NET_WM_ACTION_CLOSE) {
          actions |= WindowEntry::CloseAction;
        } else if (atom == ewmh._NET_WM_ACTION_ABOVE) {
          actions |= WindowEntry::AlwaysOnTopAction;
        } else if (atom == ewmh._NET_WM_ACTION_BELOW) {
          actions |= WindowEntry::AlwaysOnBottomAction;
        } else {
          qDebug() << "unknown action " << getAtomName(atom);
        }
      }
      win->actions = actions;
    } break;
    case Query::Class: {
      auto names = Q_RESULT(IcccmGetWmClass);
      win->instanceClass = names.instance_name;
      win->windowClass = names.class_name;
      setFallbackIcon(win);
    } break;
    }
  }
  for (uint32_t window : windows) {
    WindowRef win = knownWindows.value(window);
    if (win) {
      setFallbackIcon(win);
    }
  }
  for (const Query& query : queries) {
    delete query.cookie;
  }
  windowList.clear();
  for (WindowRef entry : knownWindows) {
    windowList << entry;
  }
  pendingProperties.clear();
  for (uint32_t window : windows) {
    emit windowUpdated(knownWindows[window]);
  }
}

void XcbIntegration::setFallbackIcon(WindowRef win)
{
  if (!win->icon.isNull()) {
    return;
  }
  QStringList iconSources{ win->program, win->name, win->windowClass, win->instanceClass, "window" };
  win->icon = icon(iconSources, QStyle::SP_FileIcon);
  if (win->icon.isNull()) {
    // NB: Shouldn't ever happen, since there's a fallback
    win->iconSource = WindowEntry::NoIcon;
  } else {
    setWindowIcon(win->windowId, win->icon);
    win->iconSource = WindowEntry::ThemeIcon;
  }
}

void XcbIntegration::requestCurrentWindow()
{
  callEwmhGetActiveWindow(0)->then(this, SLOT(gotActiveWindow(xcb_window_t)));
}

void XcbIntegration::broadcastWindowList(xcb_window_t active)
{
  emit fullWindowList(windowList);
  gotActiveWindow(active);
}

void XcbIntegration::gotActiveWindow(xcb_window_t active)
{
  if (knownWindows.contains(active)) {
    emit currentWindowChanged(knownWindows[active]);
  } else {
    emit currentWindowChanged((WindowRef) nullptr);
  }
}

void XcbIntegration::setActiveWindow(uint32_t window)
{
  auto cookie = callEwmhRequestChangeActiveWindow(window);
  cookie->force();
}

void XcbIntegration::setStrut(QWidget* widget, Qt::Edge edge, int strutSize)
{
  widget->setAttribute(Qt::WA_X11NetWmWindowTypeDock, true);
  callEwmhSetAllowedActions(widget, QVector<xcb_atom_t>());
  QRect screenGeom = widget->window()->windowHandle()->screen()->geometry();
  uint32_t l = 0, r = 0, t = 0, b = 0, w = screenGeom.width(), h = screenGeom.height();
  switch (edge) {
  case Qt::TopEdge: t = strutSize; break;
  case Qt::BottomEdge: b = strutSize; break;
  case Qt::LeftEdge: l = strutSize; break;
  case Qt::RightEdge: r = strutSize; break;
  }
  callEwmhSetStrut(widget, l, r, t, b);
  callEwmhSetStrutPartial(widget, l, r, t, b, w, h);
}

// Note: This is a blocking call that could potentially take some time to run.
// Invoke it judiciously.
QList<PlatformIntegration::Strut> XcbIntegration::findStruts()
{
  QList<Strut> result;
  xcb_window_t root = QX11Info::appRootWindow();
  auto reply = callXcbQueryTree(root)->takeResult();
  int ct = xcb_query_tree_children_length(reply);
  xcb_window_t* children = xcb_query_tree_children(reply);
  QVector<xcb_window_t> windows = callEwmhGetClientList(0)->takeResult();
  QList<XcbListProperties*> cookies;
  for (int i = 0; i < ct; i++) {
    if (!windows.contains(children[i])) {
      windows << children[i];
    }
  }
  ct = windows.length();
  for (int i = 0; i < ct; i++) {
    cookies << callXcbListProperties(windows[i]);
  }
  Strut strut;
  for (int i = 0; i < ct; i++) {
    if (!cookies[i]->force()) {
      continue;
    }
    int ct = xcb_list_properties_atoms_length(cookies[i]->result());
    xcb_atom_t* atoms = xcb_list_properties_atoms(cookies[i]->result());
    for (int j = 0; j < ct; j++) {
      if (atoms[j] == c_EWMH->_NET_WM_STRUT || atoms[j] == c_EWMH->_NET_WM_STRUT_PARTIAL) {
        auto geom = callXcbGetGeometry(windows[i])->takeResult();
        auto coord = callXcbTranslateCoordinates(windows[i], root, geom->x, geom->y)->takeResult();
        strut.window = windows[i];
        strut.geometry = QRect(coord->dst_x, coord->dst_y, geom->width, geom->height);
        result << strut;
      }
    }
  }
  qDeleteAll(cookies);
  return result;
}

void XcbIntegration::setAsTaskbar(QWidget* widget)
{
  callEwmhSetHandledIcons(widget->window(), true);
}

void XcbIntegration::addEventHandler(XcbHandlerInterface* handler)
{
  handlers << handler;
}

void XcbIntegration::removeEventHandler(XcbHandlerInterface* handler)
{
  handlers.removeAll(handler);
}

bool XcbIntegration::dispatchEvent(const CrudeXcb::EventType& event)
{
  for (XcbHandlerInterface* handler : handlers) {
    if (handler->handleEvent(event)) {
      return true;
    }
  }
  return false;
}

DisplayPower* XcbIntegration::displayPower()
{
  if (!backlightObj) {
    backlightObj = new XcbDisplayPower(this);
  }
  return backlightObj;
}

QPair<QSize, int> XcbIntegration::getDrawableInfo(uint32_t drawable)
{
  auto geomCookie = callXcbGetGeometry(drawable);
  if (geomCookie->force()) {
    auto geom = geomCookie->result();
    return qMakePair(QSize(geom->width, geom->height), int(geom->depth));
  }
  return qMakePair(QSize(), 0);
}

QImage XcbIntegration::getPixmap(uint32_t pixmap, const QSize& _size, int depth)
{
  QPair<QSize, int> info = (_size.isEmpty() || depth == 0) ? getDrawableInfo(pixmap) : qMakePair(_size, depth);
  if (!info.first.isEmpty()) {
    // TODO: can the format be predetected?
    auto cookie = callXcbGetImage(XCB_IMAGE_FORMAT_Z_PIXMAP, pixmap, QRect(QPoint(0, 0), info.first), 0x00FFFFFF);
    if (!cookie->force() && cookie->error()->error_code == XCB_MATCH) {
      cookie =
          callXcbGetImage(XCB_IMAGE_FORMAT_XY_PIXMAP, pixmap, QRect(QPoint(0, 0), info.first), (1 << info.second) - 1);
    }
    if (cookie->force()) {
      // TODO: pixel layout
      return QImage(xcb_get_image_data(cookie->result()), info.first.width(), info.first.height(), QImage::Format_RGBX8888)
          .copy();
    }
  }
  return QImage();
}

QBitmap XcbIntegration::getBitmap(uint32_t pixmap, const QSize& _size)
{
  QSize size = _size.isEmpty() ? getDrawableInfo(pixmap).first : _size;
  if (!size.isNull()) {
    // TODO: XY_BITMAP?
    auto cookie = callXcbGetImage(XCB_IMAGE_FORMAT_XY_PIXMAP, pixmap, QRect(QPoint(0, 0), size), 1);
    if (cookie->force()) {
      // TODO: bit order from xcb_setup_t
      return QBitmap::fromImage(
          QImage(xcb_get_image_data(cookie->result()), size.width(), size.height(), QImage::Format_MonoLSB)
      );
    }
  }
  return QBitmap();
}

QIcon XcbIntegration::getIcon(uint32_t pixmapID, uint32_t maskID)
{
  QImage image = getPixmap(pixmapID);
  if (image.isNull()) {
    return QIcon();
  }
  QPixmap pixmap = QPixmap::fromImage(image);
  if (maskID != XCB_NONE) {
    QBitmap mask = getBitmap(maskID, image.size());
    if (!mask.isNull()) {
      pixmap.setMask(mask);
    }
  }
  return QIcon(pixmap);
}

void XcbIntegration::requestAction(WindowEntry::WindowAction action, uint32_t window)
{
  QRect geom = (action == WindowEntry::MoveAction || action == WindowEntry::ResizeAction) ? geometry(window) : QRect();
  switch (action) {
  case WindowEntry::MoveAction:
    callEwmhMoveWindow(window, geom.center().x(), geom.center().y());
    break;
  case WindowEntry::ResizeAction:
    callEwmhResizeWindow(window, geom.right(), geom.bottom());
    break;
  case WindowEntry::RestoreAction:
    callEwmhUnsetWmState(window, ewmh._NET_WM_STATE_MAXIMIZED_VERT, ewmh._NET_WM_STATE_MAXIMIZED_HORZ);
    callEwmhUnsetWmState(window, ewmh._NET_WM_STATE_HIDDEN);
    break;
  case WindowEntry::MinimizeAction:
    // TODO: This might not be right?
    callEwmhSetWmState(window, ewmh._NET_WM_STATE_HIDDEN);
    break;
  case WindowEntry::MaximizeAction:
    callEwmhSetWmState(window, ewmh._NET_WM_STATE_MAXIMIZED_VERT, ewmh._NET_WM_STATE_MAXIMIZED_HORZ);
    break;
  case WindowEntry::HMaximizeAction:
    callEwmhSetWmState(window, ewmh._NET_WM_STATE_MAXIMIZED_HORZ);
    break;
  case WindowEntry::VMaximizeAction:
    callEwmhSetWmState(window, ewmh._NET_WM_STATE_MAXIMIZED_VERT);
    break;
  case WindowEntry::ShadeAction:
    callEwmhToggleWmState(window, ewmh._NET_WM_STATE_SHADED);
    break;
  case WindowEntry::FullscreenAction:
    callEwmhToggleWmState(window, ewmh._NET_WM_STATE_FULLSCREEN);
    break;
  case WindowEntry::AlwaysOnTopAction:
    callEwmhToggleWmState(window, ewmh._NET_WM_STATE_ABOVE);
    break;
  case WindowEntry::AlwaysOnBottomAction:
    callEwmhToggleWmState(window, ewmh._NET_WM_STATE_BELOW);
    break;
  case WindowEntry::CloseAction:
    callEwmhCloseWindow(window);
    break;
  default:
    qDebug() << "no implementation for" << action;
  }
}

QRect XcbIntegration::geometry(uint32_t win) const
{
  auto geom = callXcbGetGeometry(win)->result();
  return QRect(geom->x, geom->y, geom->width, geom->height);
}

void XcbIntegration::setWindowIcon(uint32_t win, const QIcon& icon)
{
  QPixmap px = icon.pixmap(32, 32);
  if (px.isNull()) {
    int smallest = 999999;
    for (const QSize& sz : icon.availableSizes()) {
      int check = sz.width() > sz.height() ? sz.width() : sz.height();
      if (check < smallest) {
        smallest = check;
      }
    }
    px = icon.pixmap(smallest, smallest);
    if (px.isNull()) {
      qDebug() << "XXX: invalid icon in setWindowIcon";
      return;
    }
  }
  const QImage img(px.toImage());
  QByteArray imgData(reinterpret_cast<const char*>(img.bits()), img.sizeInBytes());
  uint32_t dims[2] = { img.width(), img.height() };
  imgData.insert(0, reinterpret_cast<char*>(dims), 8);
  callXcbSetProperty(XCB_PROP_MODE_REPLACE, win, ewmh._NET_WM_ICON, XCB_ATOM_CARDINAL, 32, imgData.size() >> 2, imgData.constData())->force();
}

XcbHandlerInterface::~XcbHandlerInterface()
{
  // If the integration went away before we did, we don't need to clean up
  if (XcbIntegration::instance()) {
    XcbIntegration::instance()->removeEventHandler(this);
  }
}
