#if !defined(CRUDE_XCBREQUEST_FIXES_H) || defined(CRUDE_XCB_IMPL)
#  ifndef CRUDE_XCBREQUEST_FIXES_H
#    define CRUDE_XCBREQUEST_FIXES_H
#  endif

#  include "xcbrequestdefs.h"

#  include <xcb/damage.h>
#  include <xcb/xfixes.h>

namespace CrudeXcb {
#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbReplyCookie
CRUDE_XCB_VOID_COOKIE_CHECKED(HideCursor, xcb_xfixes_hide_cursor, CRUDE_XCB_ARGS(WinAdaptor&& win), CRUDE_XCB_CALL_WITH(win));
CRUDE_XCB_VOID_COOKIE_CHECKED(ShowCursor, xcb_xfixes_show_cursor, CRUDE_XCB_ARGS(WinAdaptor&& win), CRUDE_XCB_CALL_WITH(win));
CRUDE_XCB_VOID_COOKIE_CHECKED(
    SelectCursorInput, xcb_xfixes_select_cursor_input, CRUDE_XCB_ARGS(WinAdaptor&& win, uint32_t event_mask),
    CRUDE_XCB_CALL_WITH(win, event_mask)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(
    DamageCreate, xcb_damage_create, CRUDE_XCB_ARGS(uint32_t id, WinAdaptor&& win, uint8_t level),
    CRUDE_XCB_CALL_WITH(id, win, level)
);
CRUDE_XCB_VOID_COOKIE_CHECKED(DamageDestroy, xcb_damage_destroy, CRUDE_XCB_ARGS(uint32_t id), CRUDE_XCB_CALL_WITH(id));
CRUDE_XCB_VOID_COOKIE_CHECKED(
    DamageSubtract, xcb_damage_subtract, CRUDE_XCB_ARGS(uint32_t id), CRUDE_XCB_CALL_WITH(id, XCB_NONE, XCB_NONE)
);
CRUDE_NULLARY_STUB(xcb_grab_server_checked);
CRUDE_XCB_VOID_COOKIE_CHECKED(GrabServer, xcb_grab_server, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(0));
CRUDE_NULLARY_STUB(xcb_ungrab_server_checked);
CRUDE_XCB_VOID_COOKIE_CHECKED(UngrabServer, xcb_ungrab_server, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(0));

#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbValueCookie
CRUDE_NULLARY_STUB(xcb_xfixes_get_cursor_image);
CRUDE_XCB_REQUEST(GetCursorImage, xcb_xfixes_get_cursor_image, CRUDE_XCB_ARGS(), CRUDE_XCB_CALL_WITH(0));
XCB_DEFINE_REPLY_VECTOR(CursorImage, CursorPixel, xcb_xfixes_get_cursor_image, cursor_image);

}

#endif
