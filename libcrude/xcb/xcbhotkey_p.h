#ifndef CRUDE_XCBHOTKEY_P_H
#define CRUDE_XCBHOTKEY_P_H

#include "../hotkey_p.h"
#include "xcbintegration.h"

#include <QVector>

class XcbHotkeyPrivate : public HotkeyPrivate, public XcbHandlerInterface
{
  friend class Hotkey;

public:
  static QVector<quint32> keycodesFor(Qt::Key key);

  XcbHotkeyPrivate(const QKeySequence& shortcut, Hotkey* owner);
  ~XcbHotkeyPrivate();

  bool handleEvent(const CrudeXcb::EventType& event);

private:
  uint32_t nativeMods;
  QVector<uint32_t> nativeKeys;
};

#endif
