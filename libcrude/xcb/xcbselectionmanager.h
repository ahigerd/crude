#ifndef CRUDE_XCBSELECTIONMANAGER_CPP
#define CRUDE_XCBSELECTIONMANAGER_CPP

#include "xcbintegration.h"

#include <QWidget>
class XcbCookieBase;

class XcbSelectionManager : public QObject, public XcbHandlerInterface
{
  Q_OBJECT

public:
  XcbSelectionManager(const QByteArray& name, QObject* parent = nullptr);
  ~XcbSelectionManager();

  void acquire(bool force = false);
  void release();

  virtual bool handleEvent(const CrudeXcb::EventType& event);
  xcb_window_t winId() const;
  xcb_window_t ownerId() const;

signals:
  void acquired();
  void unavailable();
  void revoked();
  void released();
  void message(xcb_atom_t type, const QByteArray& msg);
  void message(xcb_atom_t type, const QVector<uint16_t>& msg);
  void message(xcb_atom_t type, const QVector<uint32_t>& msg);

private slots:
  void gotCurrentSelection(xcb_get_selection_owner_reply_t* reply);
  void selectionWasSet();
  void acquiredSuccessfully(xcb_get_selection_owner_reply_t* reply);
  void acquireFailed(xcb_generic_error_t* err);

private:
  xcb_window_t owner;
  quint32 atom;
  bool shouldAcquire : 1;
  bool shouldForce : 1;
  bool didAcquire : 1;
};

#endif
