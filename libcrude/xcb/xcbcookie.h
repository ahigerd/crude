#ifndef CRUDE_XCBCOOKIE_H
#define CRUDE_XCBCOOKIE_H

#include <QMetaObject>
#include <QtDebug>

#include <functional>
#include <memory>
#include <xcb/xcb.h>

class XcbCookieBase
{
  friend class XcbIntegration;

public:
  static ::xcb_generic_error_t* ValidationError;

  virtual ~XcbCookieBase();

#ifndef QT_NO_DEBUG
  QByteArray method;
#endif

  quint64 sequence() const;
  bool force();
  xcb_generic_error_t* error() const;
  virtual void* resultPointer() const;

  XcbCookieBase* then(QObject* object, const char* slot, const char* onError = nullptr);
  XcbCookieBase* thenCatch(QObject* object, const char* slot);
  XcbCookieBase* cleanup(const std::function<void()>& functor);

protected:
  XcbCookieBase(quint64 seq);
  virtual const char* replyTypeName() const = 0;
  virtual void _force() = 0;
  void dispatch();
  void detach();

  quint64 _sequence;
  quint8 age;
  bool done : 1;
  bool hasSignalValue : 1;
  void* _result;
  xcb_generic_error_t* _error;
  std::function<void()> _cleanup;

  QObject* cbObject;
  int cbSlot;
  QObject* errObject;
  int errSlot;
};

template <typename reply_t>
class XcbCookie : public XcbCookieBase
{
public:
  virtual reply_t result() = 0;

  inline reply_t takeResult()
  {
    // This only makes a difference in terms of where performance is spent.
    // It will be cleaned up one way or another.
    reply_t r = result();
    delete this;
    return r;
  }

  inline std::unique_ptr<XcbCookie<reply_t>> uniquePtr()
  {
    // By using uniquePtr, the caller asserts responsibility
    // for forcing and deleting the cookie.
    detach();
    return std::unique_ptr<XcbCookie<reply_t>>(this);
  }

protected:
  XcbCookie(quint64 seq) : XcbCookieBase(seq)
  {
    // initializers only
  }
};

#endif
