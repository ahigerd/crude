#include "xcbcookie.h"

#include "xcbintegration.h"

#include <QMetaMethod>

#include <cstdlib>

static const QByteArray genericReplyName = "xcb_generic_reply_t*";
static ::xcb_generic_error_t ValidationErrorStatic;
::xcb_generic_error_t* XcbCookieBase::ValidationError = &ValidationErrorStatic;

XcbCookieBase::XcbCookieBase(quint64 seq)
: _sequence(seq),
  age(0),
  done(false),
  hasSignalValue(true),
  _result(nullptr),
  _error(nullptr),
  cbObject(nullptr),
  cbSlot(-1),
  errObject(nullptr),
  errSlot(-1)
{
  XcbIntegration::instance()->expect(this);
}

XcbCookieBase::~XcbCookieBase()
{
  detach();
  if (_result) {
    if (!done) {
      xcb_discard_reply(c_X11, _sequence);
    }
    std::free(_result);
    _result = nullptr;
  }
  if (_error) {
    if (_error != ValidationError) {
      std::free(_error);
    }
    _error = nullptr;
  }
  if (_cleanup) {
    _cleanup();
  }
}

void XcbCookieBase::detach()
{
  XcbIntegration::instance()->clear(this);
}

quint64 XcbCookieBase::sequence() const
{
  return _sequence;
}

bool XcbCookieBase::force()
{
  if (!done) {
    _force();
    done = true;
    dispatch();
  }
  return !error();
}

void XcbCookieBase::dispatch()
{
  if (_error && (!errObject || errSlot < 0)) {
    // Error result, but no callback; just log it
#ifndef QT_NO_DEBUG
    if (c_PI->isDebugEnabled()) {
      qDebug() << method << replyTypeName() << "error" << _error->error_code;
    }
#endif
    return;
  }
  if (!_error && (!cbObject || cbSlot < 0)) {
    // Non-error result, but no callback; nothing to be done
    return;
  }

  QObject* obj = cbObject;
  int slot = cbSlot;
  const char* type;
  void* param;
  void* paramPointer = &param;
  if (_error) {
    obj = errObject;
    slot = errSlot;
    type = "xcb_generic_error_t*";
    param = _error;
#ifndef QT_NO_DEBUG
    if (c_PI->isDebugEnabled()) {
      qDebug() << method << replyTypeName() << "error" << _error->error_code;
    }
#endif
  } else if (hasSignalValue) {
    type = replyTypeName();
    QByteArray t(replyTypeName());
    if (t.endsWith('*')) {
      param = resultPointer();
    } else {
      paramPointer = resultPointer();
    }
  } else {
    type = "XcbCookieBase*";
    param = this;
  }

  QMetaMethod method = obj->metaObject()->method(slot);
  bool ok;
  switch (method.parameterCount()) {
  case 0: ok = method.invoke(obj); break;
  case 1: ok = method.invoke(obj, QGenericArgument(type, paramPointer)); break;
  default: ok = method.invoke(obj, QGenericArgument(type, paramPointer), Q_ARG(XcbCookieBase*, this));
  }
  if (!ok) {
    qWarning(
        "XcbCookieBase::dispatch: failed to invoke method %s::%s",
        obj->metaObject()->className(),
        method.methodSignature().constData()
    );
  }
}

xcb_generic_error_t* XcbCookieBase::error() const
{
  return _error;
}

void* XcbCookieBase::resultPointer() const
{
  return XcbCookieBase::_result;
}

XcbCookieBase* XcbCookieBase::then(QObject* object, const char* slot, const char* onError)
{
  if (hasSignalValue) {
    hasSignalValue = (genericReplyName != this->replyTypeName());
  }
  if (onError) {
    thenCatch(object, onError);
  }
  cbObject = object;
  QByteArray method = QMetaObject::normalizedSignature(slot + 1);

  QByteArray expected = hasSignalValue
      ? QMetaObject::normalizedSignature("(" + QByteArray(replyTypeName()) + ", XcbCookieBase*)")
      : QMetaObject::normalizedSignature("(XcbCookieBase*)");
  if (!QMetaObject::checkConnectArgs(expected.constData(), method.constData())) {
    qWarning(
        "XcbCookieBase::then: incompatible slot %s::%s, expected %s",
        object->metaObject()->className(),
        method.constData(),
        expected.constData()
    );
    return this;
  }

  cbSlot = object->metaObject()->indexOfMethod(method.constData());
  if (cbSlot < 0) {
    qWarning("XcbCookieBase::then: unknown slot %s::%s", object->metaObject()->className(), method.constData());
  }
  return this;
}

XcbCookieBase* XcbCookieBase::thenCatch(QObject* object, const char* slot)
{
  errObject = object;
  QByteArray method = QMetaObject::normalizedSignature(slot + 1);
  if (!QMetaObject::checkConnectArgs(
          QMetaObject::normalizedSignature("result(xcb_generic_error_t*, XcbCookieBase*)").constData(), method.constData()
      )) {
    qWarning("XcbCookieBase::thenCatch: incompatible slot %s::%s", object->metaObject()->className(), method.constData());
    return this;
  }
  errSlot = object->metaObject()->indexOfMethod(method.constData());
  if (errSlot < 0) {
    qWarning("XcbCookieBase::thenCatch: unknown slot %s::%s", object->metaObject()->className(), method.constData());
  }
  return this;
}

XcbCookieBase* XcbCookieBase::cleanup(const std::function<void()>& functor)
{
  _cleanup = functor;
  return this;
}
