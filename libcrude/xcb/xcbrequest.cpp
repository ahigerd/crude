#include "xcbrequest.h"

#include <QImage>
#include <QPixmap>
#include <QRect>

namespace CrudeXcb {

RectAdaptor::RectAdaptor(const QRect& r)
{
  values[0] = r.x();
  values[1] = r.y();
  values[2] = r.width();
  values[3] = r.height();
}

XcbRectAdaptor::XcbRectAdaptor(const QRect& r)
{
  x = r.x();
  y = r.y();
  width = r.width();
  height = r.height();
}

template <typename cookie_t, typename reply_t, reply_t(force_reply)(xcb_connection_t*, cookie_t, xcb_generic_error_t**)>
class XcbReplyCookie : public XcbCookie<reply_t>
{
public:
  virtual void _force() { XcbCookieBase::_result = force_reply(c_X11, cookie, &(XcbCookieBase::_error)); }

  reply_t result()
  {
    XcbCookieBase::force();
    return reply_t(XcbCookieBase::_result);
  }

protected:
  XcbReplyCookie(cookie_t cookie) : XcbCookie<reply_t>(cookie.sequence), cookie(cookie)
  {
    // initializers only
  }

  cookie_t cookie;
};

#include <typeinfo>

template <typename cookie_t, typename result_t, result_t(force_reply)(xcb_connection_t*, cookie_t, xcb_generic_error_t**)>
class XcbValueCookie : public XcbCookie<result_t>
{
public:
  virtual void _force() { value = force_reply(c_X11, cookie, &(XcbCookieBase::_error)); }

  result_t result()
  {
    XcbCookieBase::force();
    return value;
  }

protected:
  XcbValueCookie(cookie_t cookie) : XcbCookie<result_t>(cookie.sequence), cookie(cookie)
  {
    //XcbCookieBase::hasSignalValue = false;
  }

  virtual void* resultPointer() const { return &value; }

  mutable result_t value;
  cookie_t cookie;
};

xcb_generic_reply_t* crude_generic_reply(xcb_connection_t* conn, xcb_void_cookie_t cookie, xcb_generic_error_t** err)
{
  *err = xcb_request_check(conn, cookie);
  return nullptr;
}

xcb_get_property_cookie_t crude_xcb_get_property_string(
    xcb_connection_t* c, uint8_t _delete, xcb_window_t window, xcb_atom_t property, xcb_atom_t type,
    uint32_t long_offset, uint32_t long_length
)
{
  return xcb_get_property(c, _delete, window, property, type, long_offset, long_length);
}

uint8_t crude_xcb_get_property_string_reply(
    xcb_connection_t* c, xcb_get_property_cookie_t cookie, xcb_ewmh_get_utf8_strings_reply_t* reply,
    xcb_generic_error_t** e
)
{
  reply->_reply = xcb_get_property_reply(c, cookie, e);
  if (!reply->_reply || *e || reply->_reply->type == XCB_ATOM_NONE) {
    if (reply->_reply) {
      std::free(reply->_reply);
    }
    reply->_reply = 0;
    return 0;
  }
  reply->strings_len = xcb_get_property_value_length(reply->_reply);
  reply->strings = (char*)xcb_get_property_value(reply->_reply);
  return 1;
}

xcb_get_property_cookie_t crude_xcb_ewmh_get_wm_icon(xcb_ewmh_connection_t* ewmh, xcb_window_t window)
{
  return xcb_ewmh_get_wm_icon(ewmh, window);
}

uint8_t crude_xcb_ewmh_get_wm_icon_reply(
    xcb_ewmh_connection_t* ewmh, xcb_get_property_cookie_t cookie, QIcon* result, xcb_generic_error_t** err
)
{
  xcb_ewmh_get_wm_icon_reply_t wm_icon;
  uint8_t ok = xcb_ewmh_get_wm_icon_reply(ewmh, cookie, &wm_icon, err);
  if (!ok) {
    *result = QIcon();
    return false;
  }
  QIcon icon;
  unsigned int ct = xcb_ewmh_get_wm_icon_length(&wm_icon);
  xcb_ewmh_wm_icon_iterator_t iter = xcb_ewmh_get_wm_icon_iterator(&wm_icon);
  while (ct > 0) {
    QImage img(reinterpret_cast<uchar*>(iter.data), iter.width, iter.height, QImage::Format_ARGB32);
    icon.addPixmap(QPixmap::fromImage(img));
    xcb_ewmh_get_wm_icon_next(&iter);
    --ct;
  }
  xcb_ewmh_get_wm_icon_reply_wipe(&wm_icon);
  *result = icon;
  return true;
}

#include "xcbrequest_macro.h"
#define CRUDE_XCB_IMPL

#undef CRUDE_XCB_COOKIE_IMPL
#ifdef QT_NO_DEBUG
#  define CRUDE_XCB_COOKIE_IMPL(conn, XcbName, xcb_name, cookie_t, reply_t, reply_fn, ARGS, ...) \
    struct XcbName##Impl : public CRUDE_COOKIE_BASE_CLASS<cookie_t, reply_t, reply_fn> \
    { \
      XcbName##Impl(cookie_t cookie) : CRUDE_COOKIE_BASE_CLASS(cookie) {} \
      const char* replyTypeName() const \
      { \
        return #reply_t; \
      } \
    }; \
    XcbName* call##XcbName ARGS \
    { \
      cookie_t cookie = xcb_name(conn, __VA_ARGS__); \
      return new XcbName##Impl(cookie); \
    }
#else
#  define CRUDE_XCB_COOKIE_IMPL(conn, XcbName, xcb_name, cookie_t, reply_t, reply_fn, ARGS, ...) \
    struct XcbName##Impl : public CRUDE_COOKIE_BASE_CLASS<cookie_t, reply_t, reply_fn> \
    { \
      XcbName##Impl(cookie_t cookie) : CRUDE_COOKIE_BASE_CLASS(cookie) {} \
      const char* replyTypeName() const \
      { \
        return #reply_t; \
      } \
    }; \
    XcbName* call##XcbName ARGS \
    { \
      cookie_t cookie = xcb_name(conn, __VA_ARGS__); \
      XcbName* r_##XcbName = new XcbName##Impl(cookie); \
      r_##XcbName->method = #XcbName; \
      return r_##XcbName; \
    }
#endif

#undef CRUDE_GET_ARRAY_REPLY
#define CRUDE_GET_ARRAY_REPLY(conn, xcb_name, reply_name, element_t, element) \
  CRUDE_GET_ARRAY_REPLY_SIGNATURE(xcb_name, element_t) \
  { \
    reply_name##_t* reply = xcb_name##_reply(conn, cookie, err); \
    if (!reply || *err) { \
      if (c_PI->isDebugEnabled()) \
        qDebug() << #xcb_name << "error" << *err; \
      return QVector<element_t>(); \
    } \
    auto ptr = xcb_name##_##element(reply); \
    auto len = xcb_name##_##element##_length(reply); \
    QVector<element_t> list(len); \
    std::copy(ptr, ptr + len, list.data()); \
    return list; \
  }

#undef CRUDE_GET_LIST_REPLY
#define CRUDE_GET_LIST_REPLY(conn, xcb_name, reply_name, element_t, element) \
  CRUDE_GET_LIST_REPLY_SIGNATURE(xcb_name, element_t) \
  { \
    reply_name##_t reply; \
    bool ok = xcb_name##_reply(conn, cookie, &reply, err); \
    QVector<element_t> list; \
    if (ok) { \
      for (uint32_t i = 0; i < reply.element##_len; i++) \
        list << reply.element[i]; \
      reply_name##_wipe(&reply); \
    } else if (*err) { \
      if (c_PI->isDebugEnabled()) \
        qDebug() << #xcb_name << "error" << *err; \
    } \
    return list; \
  }

#undef CRUDE_GET_STRING_REPLY
#define CRUDE_GET_STRING_REPLY(conn, xcb_name) \
  CRUDE_GET_STRING_REPLY_SIGNATURE(xcb_name) \
  { \
    xcb_ewmh_get_utf8_strings_reply_t reply; \
    bool ok = xcb_name##_reply(conn, cookie, &reply, err); \
    QString result; \
    if (ok) { \
      result = QString::fromUtf8(reply.strings, reply.strings_len); \
      xcb_ewmh_get_utf8_strings_reply_wipe(&reply); \
    } else if (*err) { \
      if (c_PI->isDebugEnabled()) \
        qDebug() << #xcb_name << "error" << *err; \
    } \
    return result; \
  }
#undef CRUDE_GET_PROPERTY_REPLY
#define CRUDE_GET_PROPERTY_REPLY(conn, xcb_name, result_t) \
  CRUDE_GET_PROPERTY_REPLY_SIGNATURE(xcb_name, result_t) \
  { \
    result_t reply; \
    *err = 0; \
    uint8_t ok = xcb_name##_reply(conn, cookie, &reply, err); \
    if (!ok && !*err) { \
      *err = XcbCookieBase::ValidationError; \
    } \
    if (*err) { \
      if (c_PI->isDebugEnabled()) \
        qDebug() << #xcb_name << "error" << *err << (*err == XcbCookieBase::ValidationError); \
    } \
    return reply; \
  }
#undef CRUDE_GET_FIELD_REPLY
#define CRUDE_GET_FIELD_REPLY(conn, xcb_name, result_t, field) \
  CRUDE_GET_FIELD_REPLY_SIGNATURE(xcb_name, result_t) \
  { \
    xcb_name##_reply_t* reply = xcb_name##_reply(conn, cookie, err); \
    result_t result; \
    if (!reply || *err) { \
      if (c_PI->isDebugEnabled()) \
        qDebug() << #xcb_name << "error" << *err; \
      return result_t(); \
    } else { \
      result = reply->field; \
    } \
    std::free(reply); \
    return result; \
  }
}

#undef XCB_DEFINE_VECTOR
#define XCB_DEFINE_VECTOR(VectorName, ElementName, container_t, xcb_field) \
  VectorName::VectorName(const container_t* container) \
  : std::vector<ElementName>(::xcb_field(container), ::xcb_field(container) + ::xcb_field##_length(container)) \
  {} \
  VectorName::VectorName(const container_t& container) : VectorName(&container) {}
#undef XCB_DEFINE_ITER_VECTOR
#define XCB_DEFINE_ITER_VECTOR(VectorName, ElementName, container_t, xcb_iter, xcb_next) \
  VectorName::VectorName(const container_t* container) : std::vector<ElementName*>() \
  { \
    reserve(::xcb_iter##_length(container)); \
    for (auto iter = ::xcb_iter##_iterator(container); iter.rem; ::xcb_next(&iter)) { \
      push_back(iter.data); \
    } \
  } \
  VectorName::VectorName(const container_t& container) : VectorName(&container) {}

#undef CRUDE_NULLARY_STUB
#define CRUDE_NULLARY_STUB(xcb_name)

#undef C_DEF
#define C_DEF(value)

#include "xcbrequest.h"

namespace CrudeXcb {

// Keep this in sync with xcbrequestdefs.h
CRUDE_XCB_GET_FIELD(
    XcbInternAtom, xcb_intern_atom, uint32_t, atom, CRUDE_XCB_ARGS(bool only_if_exists, const QByteArray& name),
    CRUDE_XCB_CALL_WITH(only_if_exists, name.length(), name.constData())
);

template <typename T>
inline static QVector<T> propertyArrayResult(xcb_get_property_reply_t* reply)
{
  int bytes = xcb_get_property_value_length(reply);
  QVector<T> result(bytes / sizeof(T));
  std::memcpy(result.data(), xcb_get_property_value(reply), bytes);
  return result;
}

template <>
uint8_t xcbPropertyValue<uint8_t>(xcb_get_property_reply_t* reply)
{
  return propertyArrayResult<uint8_t>(reply)[0];
}

template <>
uint16_t xcbPropertyValue<uint16_t>(xcb_get_property_reply_t* reply)
{
  return propertyArrayResult<uint16_t>(reply)[0];
}

template <>
uint32_t xcbPropertyValue<uint32_t>(xcb_get_property_reply_t* reply)
{
  return propertyArrayResult<uint32_t>(reply)[0];
}

template <>
QVector<uint8_t> xcbPropertyValue<QVector<uint8_t>>(xcb_get_property_reply_t* reply)
{
  return propertyArrayResult<uint8_t>(reply);
}

template <>
QVector<uint16_t> xcbPropertyValue<QVector<uint16_t>>(xcb_get_property_reply_t* reply)
{
  return propertyArrayResult<uint16_t>(reply);
}

template <>
QVector<uint32_t> xcbPropertyValue<QVector<uint32_t>>(xcb_get_property_reply_t* reply)
{
  return propertyArrayResult<uint32_t>(reply);
}

template <>
QString xcbPropertyValue<QString>(xcb_get_property_reply_t* reply)
{
  return QString::fromUtf8(
      reinterpret_cast<const char*>(xcb_get_property_value(reply)), xcb_get_property_value_length(reply)
  );
}

template <>
QByteArray xcbPropertyValue<QByteArray>(xcb_get_property_reply_t* reply)
{
  return QByteArray(reinterpret_cast<const char*>(xcb_get_property_value(reply)), xcb_get_property_value_length(reply));
}

}
