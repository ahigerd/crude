#include "xcbdisplaypower.h"

#include "xcbextension.h"
#include "xcbrequest_display.h"

#include <QX11Info>

static uint32_t backlightAtom()
{
  static uint32_t atom = 0;
  if (!atom) {
    atom = callXcbInternAtom(true, "Backlight")->takeResult();
  }
  if (!atom) {
    atom = callXcbInternAtom(true, "BACKLIGHT")->takeResult();
  }
  return atom;
}

static uint32_t edidAtom()
{
  static uint32_t atom = 0;
  if (!atom) {
    atom = callXcbInternAtom(true, "EDID")->takeResult();
  }
  if (!atom) {
    atom = callXcbInternAtom(true, "EDID_DATA")->takeResult();
  }
  if (!atom) {
    atom = callXcbInternAtom(true, "XFree86_DDC_EDID1_RAWDATA")->takeResult();
  }
  return atom;
}

class RandrLightPlugin : public QObject, public LightPlugin
{
  Q_OBJECT
  Q_INTERFACES(LightPlugin)

public:
  RandrLightPlugin(XcbDisplayPower* dp) : QObject(nullptr), enabled(false), min(0), max(100), raw(100), dp(dp) {}
  ~RandrLightPlugin() {}

  virtual void setEnabled(bool) {}
  virtual QString pluginName() const { return "randr"; }
  virtual bool available() const { return dp->randrAvailable && backlightAtom(); }

  virtual bool backlightAvailable() const
  {
    return enabled;
  }

  virtual float backlightLevel() const
  {
    return (raw - min) / (max - min);
  }

  virtual void setBacklightLevel(float level)
  {
    if (!enabled) {
      return;
    }
    if (level < 0) {
      level = 0;
    } else if (level > 1) {
      level = 1;
    }
    uint32_t newRaw = level * (max - min) + min;
    if (newRaw == raw) {
      return;
    }
    // Must be synchronous to avoid deadlock.
    callRRSetOutputPropertyUint32(dp->primaryOutput, backlightAtom(), XCB_ATOM_INTEGER, { newRaw })->force();
  }

  virtual void displaysUpdated()
  {
    if (backlightAtom()) {
      callRRQueryOutputProperty(dp->primaryOutput, backlightAtom())
          ->then(this, SLOT(updateBacklightRange(xcb_randr_query_output_property_reply_t*)), SLOT(randrFail()));
      callRRGetOutputProperty(dp->primaryOutput, backlightAtom(), XCB_ATOM_NONE)
          ->then(this, SLOT(updateBacklightLevel(xcb_randr_get_output_property_reply_t*)), SLOT(randrFail()));
    } else {
      enabled = false;
    }
  }

  bool enabled;
  uint32_t min, max, raw;
  XcbDisplayPower* dp;

public slots:
  void updateBacklightRange(xcb_randr_query_output_property_reply_t* reply)
  {
    int len = xcb_randr_query_output_property_valid_values_length(reply);
    uint32_t* range = reinterpret_cast<uint32_t*>(xcb_randr_query_output_property_valid_values(reply));
    bool newAvailable = (len == 2) && (range[1] > range[0]);
    if (!newAvailable) {
      randrFail();
      return;
    } else if (!enabled) {
      enabled = true;
      emit backlightAvailableChanged(true);
    }
    min = range[0];
    max = range[1];
  }

  void updateBacklightLevel(xcb_randr_get_output_property_reply_t* reply)
  {
    if (!enabled) {
      return;
    }
    raw = *reinterpret_cast<uint32_t*>(xcb_randr_get_output_property_data(reply));
    float newLevel = 1;
    if (max > min) {
      newLevel = float(raw - min) / float(max - min);
      emit backlightChanged(newLevel);
    }
  }

  void randrFail()
  {
    if (enabled) {
      enabled = false;
      emit backlightAvailableChanged(false);
    }
  }

signals:
  void backlightAvailableChanged(bool available);
  void backlightChanged(float level);
};

XcbDisplayPower::XcbDisplayPower(QObject* parent)
: DisplayPower(parent),
  poll(Throttle::connectedTo(this, SLOT(pollDisplay()))),
  randrAvailable(false),
  dpmsAvailable(false),
  standbyAvailable(false),
  suspendAvailable(false),
  offAvailable(false)
{
  poll.timeout(100);
  XcbIntegration::instance()->addEventHandler(this);

  randrAvailable = CrudeXcb::checkExtension(CrudeXcb::randr, 1, 2);
  if (randrAvailable) {
    pluginShell.addStaticPlugin(new RandrLightPlugin(this));
    callRRSelectInput(
        QX11Info::appRootWindow(), XCB_RANDR_NOTIFY_MASK_OUTPUT_CHANGE | XCB_RANDR_NOTIFY_MASK_OUTPUT_PROPERTY
    );
    pollDisplay();
    dpmsAvailable = CrudeXcb::checkExtension(CrudeXcb::dpms);
    if (dpmsAvailable) {
      auto cookie = callDPMSCapable()->uniquePtr();
      if (!cookie->force() || !cookie->result()) {
        dpmsAvailable = false;
      }
    }
  } else {
    dpmsAvailable = false;
    QTimer::singleShot(0, this, SLOT(scanPlugins()));
  }
}

bool XcbDisplayPower::standbyModeAvailable(DisplayPower::StandbyMode mode) const
{
  // TODO: It's not likely that this will be relevant until we add Wayland support,
  // but screensaver mode and sysfs bl_power should be handled in the base class.
  if (mode == ScreenSaver) {
    return screenSaverAvailable();
  }
  if (!dpmsAvailable) {
    return false;
  }
  switch (mode) {
  case Off: return dpmsAvailable && offAvailable;
  case Standby: return dpmsAvailable && standbyAvailable;
  case Suspend: return dpmsAvailable && suspendAvailable;
  default: return true;
  }
  return false;
}

// The caller is responsible for delaying the call to avoid conflicting with the event that
// triggered the request. The caller is also responsible for escalating to the appropriate
// DPMS level if an intermediate level is not supported.
void XcbDisplayPower::setStandbyMode(DisplayPower::StandbyMode mode)
{
  if (!standbyModeAvailable(mode)) {
    return;
  }
  if (mode == ScreenSaver) {
    activateScreenSaver();
  } else {
    callDPMSForce((uint16_t)mode);
  }
}

DisplayPower::StandbyMode XcbDisplayPower::standbyMode() const
{
  if (dpmsAvailable) {
    return StandbyMode(callDPMSGetLevel()->takeResult());
  }
  return On;
}

void XcbDisplayPower::primaryDisplayResult_in(uint32_t output)
{
  primaryOutput = output;
  scanPlugins();
  auto plugin = pluginShell.plugin();
  if (plugin && plugin->available()) {
    plugin->displaysUpdated();
  }
  if (dpmsAvailable && edidAtom()) {
    callRRGetOutputProperty(output, edidAtom(), XCB_ATOM_NONE)
        ->then(this, SLOT(updateEdid(xcb_randr_get_output_property_reply_t*)), SIGNAL(displayCapabilitiesUpdated()));
  }
}

void XcbDisplayPower::updateEdid(xcb_randr_get_output_property_reply_t* reply)
{
  uint32_t edidLen = xcb_randr_get_output_property_data_length(reply);
  if (edidLen >= 25) {
    uint8_t* edid = xcb_randr_get_output_property_data(reply);
    uint8_t video = edid[20];
    uint8_t dpms = edid[24];
    standbyAvailable = dpms & 0x80;
    suspendAvailable = dpms & 0x40;
    offAvailable = dpms & 0x20;
    if (video & 0x80) {
      // Digital display interface -- these can be turned off via the DPMS
      // extension, even if they don't support real DPMS.
      if (!suspendAvailable && !offAvailable) {
        standbyAvailable = true;
      }
    } else {
      // Analog display interface -- trust the EDID. Xorg will just blank the
      // screen if you try to turn it off and it doesn't support it, and we
      // want the screensaver options to be responsible for that.
    }
  } else {
    // This could be a virtual display like VNC or it could be a nonstandard
    // (embedded?) display panel. There's no hope in putting the former into
    // power-save mode, and if the latter has any support for this kind of
    // thing at all it's almost certainly going to be triggered by the screen
    // blanking function as well, so let the screensaver options handle it.
    standbyAvailable = false;
    suspendAvailable = false;
    offAvailable = false;
  }
  emit displayCapabilitiesUpdated();
}

void XcbDisplayPower::pollDisplay()
{
  if (randrAvailable) {
    callRRGetPrimary(QX11Info::appRootWindow())->then(this, SLOT(primaryDisplayResult_in(uint32_t)), SLOT(scanPlugins()));
  }
}

bool XcbDisplayPower::handleEvent(const CrudeXcb::EventType& extEvent)
{
  if (randrAvailable) {
    // TODO: if CRUDE starts caring about other RANDR events, this will need to be more specific.
    if (extEvent.extension == CrudeXcb::randr && (extEvent.eventType == 0 || extEvent.eventType == 1)) {
      poll.trigger();
      return true;
    }
  }
  return false;
}

QList<DisplayPower::Schedule> XcbDisplayPower::schedule() const
{
  QList<Schedule> schedule;
  int lessThan = 65535;
  if (dpmsAvailable) {
    auto cookie = callDPMSGetTimeouts();
    auto reply = cookie->result();
    if (reply) {
      if (reply->off_timeout && reply->off_timeout < lessThan) {
        schedule << (Schedule){ QTime::fromMSecsSinceStartOfDay(1000 * reply->off_timeout), Off };
        lessThan = reply->off_timeout;
      }
      if (reply->suspend_timeout && reply->suspend_timeout < lessThan) {
        schedule.prepend({ QTime::fromMSecsSinceStartOfDay(1000 * reply->suspend_timeout), Suspend });
        lessThan = reply->suspend_timeout;
      }
      if (reply->standby_timeout && reply->standby_timeout < lessThan) {
        schedule.prepend({ QTime::fromMSecsSinceStartOfDay(1000 * reply->standby_timeout), Standby });
        lessThan = reply->standby_timeout;
      }
    }
  }
  int ssTimeout = screenSaverTimeout();
  // Screensavers don't check lessThan because you probably want to lock the screen when the display is turned off.
  if (ssTimeout) {
    schedule.prepend({ QTime::fromMSecsSinceStartOfDay(1000 * ssTimeout), ScreenSaver });
  }
  return schedule;
}

void XcbDisplayPower::setSchedule(const QList<DisplayPower::Schedule>& schedule)
{
  int standby = 0, suspend = 0, off = 0, saver = 0;
  for (const Schedule& entry : schedule) {
    if (entry.standbyMode == Standby) {
      standby = entry.delay.msecsSinceStartOfDay() / 1000;
    } else if (entry.standbyMode == Suspend) {
      suspend = entry.delay.msecsSinceStartOfDay() / 1000;
    } else if (entry.standbyMode == Off) {
      off = entry.delay.msecsSinceStartOfDay() / 1000;
    } else if (entry.standbyMode == ScreenSaver) {
      saver = entry.delay.msecsSinceStartOfDay() / 1000;
    }
  }
  if (suspend && suspend < standby) {
    standby = 0;
  }
  if (off && off < standby) {
    standby = 0;
  }
  if (off && off < suspend) {
    suspend = 0;
  }
  if (standby == 0 && suspend == 0 && off == 0) {
    if (dpmsAvailable) {
      callDPMSDisable();
    }
  } else {
    if (dpmsAvailable) {
      callDPMSEnable();
      callDPMSSetTimeouts(standby, suspend, off);
    } else {
      qWarning("XcbDisplayPower: setSchedule called without DPMS support");
    }
  }
  setScreenSaverTimeout(saver);
  saveSchedule(schedule);
}

#include "xcbdisplaypower.moc"
