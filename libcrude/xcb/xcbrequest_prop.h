#if !defined(CRUDE_XCBREQUEST_PROP_H) || defined(CRUDE_XCB_IMPL)
#  ifndef CRUDE_XCBREQUEST_PROP_H
#    define CRUDE_XCBREQUEST_PROP_H
#  endif

#  include "xcbrequestdefs.h"

#  include <xcb/xcb_ewmh.h>
#  include <xcb/xcb_icccm.h>

namespace CrudeXcb {
#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbReplyCookie
CRUDE_XCB_REQUEST(XcbListProperties, xcb_list_properties, CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window));
CRUDE_XCB_VOID_COOKIE_CHECKED(
    XcbSetProperty, xcb_change_property,
    CRUDE_XCB_ARGS(
        uint8_t mode, WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, uint8_t format, uint32_t len, const void* data
    ),
    CRUDE_XCB_CALL_WITH(mode, window, prop, type, format, len, data)
);
CRUDE_XCB_REQUEST(
    XcbDeleteProperty, xcb_get_property, CRUDE_XCB_ARGS(WinAdaptor&& win, xcb_atom_t prop, xcb_atom_t type),
    CRUDE_XCB_CALL_WITH(1, win, prop, type, 0, 0)
);
CRUDE_XCB_REQUEST(
    XcbGetProperty, xcb_get_property, CRUDE_XCB_ARGS(WinAdaptor&& win, xcb_atom_t prop, xcb_atom_t type),
    CRUDE_XCB_CALL_WITH(0, win, prop, type, 0, 1000)
);
CRUDE_EWMH_VOID_COOKIE_CHECKED(
    EwmhSetStrut, xcb_ewmh_set_wm_strut, CRUDE_EWMH_ARGS(quint32 l, quint32 r, quint32 t, quint32 b),
    CRUDE_EWMH_CALL_WITH(l, r, t, b)
);
CRUDE_EWMH_VOID_COOKIE_CHECKED(
    EwmhSetStrutPartial, xcb_ewmh_set_wm_strut_partial,
    CRUDE_EWMH_ARGS(quint32 l, quint32 r, quint32 t, quint32 b, quint32 w, quint32 h),
    CRUDE_EWMH_CALL_WITH({ l, r, t, b, 0, l ? h : 0, 0, r ? h : 0, 0, t ? w : 0, 0, b ? w : 0 })
);
CRUDE_EWMH_VOID_ATOM_LIST(EwmhSetWindowType, xcb_ewmh_set_wm_window_type);
CRUDE_EWMH_VOID_ATOM_LIST(EwmhSetAllowedActions, xcb_ewmh_set_wm_allowed_actions);
CRUDE_EWMH_VOID_COOKIE_CHECKED(
    EwmhSetHandledIcons, xcb_ewmh_set_wm_handled_icons, CRUDE_EWMH_ARGS(uint32_t handled), CRUDE_EWMH_CALL_WITH(handled)
);

template <typename T>
T xcbPropertyValue(xcb_get_property_reply_t* reply);

#  undef CRUDE_COOKIE_BASE_CLASS
#  define CRUDE_COOKIE_BASE_CLASS XcbValueCookie
#  ifndef CRUDE_XCB_IMPL
template <uint8_t mode, typename PropType>
struct XcbPropertySetter_
{
  enum {
    size = sizeof(PropType)
  };

  static inline XcbSetProperty* call(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const QVector<PropType>& data)
  {
    return callXcbSetProperty(mode, std::move(window), prop, type, size, data.length(), data.constData());
  }

  static inline XcbSetProperty* call(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, PropType data)
  {
    return callXcbSetProperty(mode, std::move(window), prop, type, size, 1, &data);
  }
};

template <uint8_t mode>
struct XcbPropertySetter_<mode, QByteArray>
{
  static inline XcbSetProperty* call(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const QByteArray& data)
  {
    return callXcbSetProperty(mode, std::move(window), prop, type, 8, data.length(), data.constData());
  }
};

template <uint8_t mode>
struct XcbPropertySetter_<mode, QString>
{
  static inline XcbSetProperty* call(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const QString& data)
  {
    return XcbPropertySetter_<mode, QByteArray>(window, prop, type, data.toUtf8());
  }
};

template <typename PropType>
inline XcbSetProperty* callXcbSetProperty(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const PropType& data)
{
  return XcbPropertySetter_<XCB_PROP_MODE_REPLACE, PropType>::call(std::move(window), prop, type, data);
}

template <typename PropType>
inline XcbSetProperty*
    callXcbSetProperty(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const QVector<PropType>& data)
{
  return XcbPropertySetter_<XCB_PROP_MODE_REPLACE, PropType>::call(std::move(window), prop, type, data);
}

template <typename PropType>
inline XcbSetProperty* callXcbAppendProperty(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const PropType& data)
{
  return XcbPropertySetter_<XCB_PROP_MODE_APPEND, PropType>::call(std::move(window), prop, type, data);
}

template <typename PropType>
inline XcbSetProperty*
    callXcbAppendProperty(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const QVector<PropType>& data)
{
  return XcbPropertySetter_<XCB_PROP_MODE_APPEND, PropType>::call(std::move(window), prop, type, data);
}

template <typename PropType>
inline XcbSetProperty* callXcbPrependProperty(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const PropType& data)
{
  return XcbPropertySetter_<XCB_PROP_MODE_PREPEND, PropType>::call(std::move(window), prop, type, data);
}

template <typename PropType>
inline XcbSetProperty*
    callXcbPrependProperty(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const QVector<PropType>& data)
{
  return XcbPropertySetter_<XCB_PROP_MODE_PREPEND, PropType>::call(std::move(window), prop, type, data);
}

template <typename PropType, typename std::enable_if<std::is_integral<PropType>::value, int>::type = 0>
inline XcbSetProperty*
    callXcbPrependProperty(WinAdaptor&& window, xcb_atom_t prop, xcb_atom_t type, const QVector<PropType>& data)
{
  return callXcbSetProperty(
      XCB_PROP_MODE_PREPEND, std::move(window), prop, type, sizeof(PropType) * 8, data.length(), data.constData()
  );
}
#  endif

CRUDE_GET_STRING_REPLY(c_X11, crude_xcb_get_property_string)
CRUDE_EWMH_GET_LIST(
    EwmhGetWindowType, xcb_ewmh_get_wm_window_type, xcb_ewmh_get_atoms_reply, xcb_atom_t, atoms,
    CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window)
);
CRUDE_EWMH_GET_LIST(
    EwmhGetState, xcb_ewmh_get_wm_state, xcb_ewmh_get_atoms_reply, xcb_atom_t, atoms,
    CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window)
);
CRUDE_EWMH_GET_STRING(EwmhGetName, xcb_ewmh_get_wm_name, CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window));
CRUDE_EWMH_GET_STRING(
    EwmhGetVisibleName, xcb_ewmh_get_wm_visible_name, CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window)
);
CRUDE_XCB_GET_STRING(
    IcccmGetName, crude_xcb_get_property_string, CRUDE_XCB_ARGS(WinAdaptor&& window),
    CRUDE_XCB_CALL_WITH(0, window, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 0, 1000)
);
CRUDE_XCB_GET_STRING(
    IcccmGetMachine, crude_xcb_get_property_string, CRUDE_XCB_ARGS(WinAdaptor&& window),
    CRUDE_XCB_CALL_WITH(0, window, XCB_ATOM_WM_CLIENT_MACHINE, XCB_ATOM_STRING, 0, 1000)
);
xcb_get_property_cookie_t crude_xcb_ewmh_get_wm_icon(xcb_ewmh_connection_t* ewmh, xcb_window_t window);
CRUDE_EWMH_GET_PROPERTY(
    EwmhGetIcon, crude_xcb_ewmh_get_wm_icon, QIcon, CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window)
);
CRUDE_XCB_GET_PROPERTY(
    IcccmGetHints, xcb_icccm_get_wm_hints, xcb_icccm_wm_hints_t, CRUDE_XCB_ARGS(WinAdaptor&& window),
    CRUDE_XCB_CALL_WITH(window)
);
CRUDE_EWMH_GET_PROPERTY(
    EwmhGetPid, xcb_ewmh_get_wm_pid, uint32_t, CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window)
);
CRUDE_EWMH_GET_LIST(
    EwmhGetAllowedActions, xcb_ewmh_get_wm_allowed_actions, xcb_ewmh_get_atoms_reply, xcb_atom_t, atoms,
    CRUDE_XCB_ARGS(WinAdaptor&& window), CRUDE_XCB_CALL_WITH(window)
);
CRUDE_XCB_GET_PROPERTY(
    IcccmGetWmClass, xcb_icccm_get_wm_class, xcb_icccm_get_wm_class_reply_t, CRUDE_XCB_ARGS(WinAdaptor&& win),
    CRUDE_XCB_CALL_WITH(win)
);

}

#endif
