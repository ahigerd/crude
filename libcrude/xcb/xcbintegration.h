#ifndef CRUDE_XCBINTEGRATION_H
#define CRUDE_XCBINTEGRATION_H

#include "../platformintegration.h"
#include "xcbextension.h"

#include <QBitmap>
#include <QMap>
#include <QSet>
#include <vector>

#include <xcb/xcb.h>
#include <xcb/xcb_ewmh.h>
class QSocketNotifier;
class XcbCookieBase;
class XcbDisplayPower;

struct XcbHandlerInterface
{
  virtual ~XcbHandlerInterface();
  virtual bool handleEvent(const CrudeXcb::EventType& event) = 0;
};

class XcbIntegration : public PlatformIntegration
{
  Q_OBJECT

public:
  static xcb_ewmh_connection_t* ewmhConnection();
  static xcb_connection_t* x11Connection();
  static XcbIntegration* instance();

  XcbIntegration(int argc, char** argv);
  ~XcbIntegration();

  void enableWindowList();
  bool enableWindowManagement();
  WindowRef findWindow(uint32_t wid) const;
  std::vector<uint32_t> windowStackingOrder() const;

  void expect(XcbCookieBase* cookie);
  void clear(XcbCookieBase* cookie);

  void addEventHandler(XcbHandlerInterface* handler);
  void removeEventHandler(XcbHandlerInterface* handler);

  int lastTimestamp() const;
  QString getAtomName(uint32_t atom);

  virtual bool windowManagerExists();
  virtual bool desktopManagerExists();

  virtual QList<Strut> findStruts();

  virtual DisplayPower* displayPower();

  QPair<QSize, int> getDrawableInfo(uint32_t drawable);
  QImage getPixmap(uint32_t pixmap, const QSize& size = QSize(), int depth = 0);
  QBitmap getBitmap(uint32_t pixmap, const QSize& size = QSize());
  QIcon getIcon(uint32_t pixmap, uint32_t mask = XCB_NONE);

  void requestAction(WindowEntry::WindowAction action, uint32_t window);
  virtual QRect geometry(uint32_t win) const;

  void setWindowIcon(uint32_t win, const QIcon& icon);

public slots:
  virtual void requestWindowList();
  virtual void requestCurrentWindow();
  virtual void setActiveWindow(uint32_t window);
  virtual void setAsTaskbar(QWidget*);
  virtual void setStrut(QWidget*, Qt::Edge, int);

protected:
  void timerEvent(QTimerEvent*);

private slots:
  void pollXcb();
  void queryPendingWindows();
  void scanWindows();
  void gotWindowList(const QVector<xcb_window_t>&);
  void broadcastWindowList(xcb_window_t);
  void gotActiveWindow(xcb_window_t);

private:
  void forceNextCookie();
  void propertyNotify(xcb_property_notify_event_t* event);
  void configureNotify(xcb_configure_notify_event_t* event);
  void genericEvent(xcb_ge_generic_event_t* event);
  void clientMessage(xcb_client_message_event_t* event);
  bool dispatchEvent(const CrudeXcb::EventType& event);
  void setFallbackIcon(WindowRef win);

  xcb_ewmh_connection_t ewmh;
  QByteArray display;
  int xcb_fd;
  int lastTS;
  uint32_t rootEventMask;
  uint32_t systrayId, systrayAtom;
  QSocketNotifier* notifier;
  QList<XcbCookieBase*> cookies;
  QList<XcbHandlerInterface*> handlers;
  WindowRefMap knownWindows;
  WindowRefList windowList;
  bool trackWindowList;

  enum PropertyGroup {
    NewWindowProperties,
    QueryTree,
    WmName,
    WmIcon,
    WmHints,
    WmClass
  };

  QMap<uint32_t, QSet<PropertyGroup>> pendingProperties;

  XcbDisplayPower* backlightObj;
};

#define c_EWMH XcbIntegration::ewmhConnection()
#define c_X11  XcbIntegration::x11Connection()

#endif
