#include "pluginshell.h"

#include <QApplication>
#include <QDir>
#include <QMap>
#include <QPluginLoader>
#include <QSettings>

#ifndef CRUDE_PLUGIN_PATH
#  define CRUDE_PLUGIN_PATH "/usr/local/lib/crude/"
#endif

QStringList PluginShellBase::pluginPaths()
{
  return QStringList{ CRUDE_PLUGIN_PATH, qApp->applicationDirPath() + "/plugins/" };
}

PluginShellBase::PluginShellBase() : _plugin(nullptr), scanComplete(false)
{
  // initializers only
}

PluginShellBase::~PluginShellBase()
{
  for (QObject* p : _plugins) {
    p->deleteLater();
  }
}

void PluginShellBase::addStaticPlugin(QObject* plug)
{
  // Note: Takes ownership of plugin object
  QString name = checkPlugin(plug);
  if (name.isEmpty()) {
    if (plug) {
      plug->deleteLater();
    }
    return;
  }
  _plugins[name] = plug;
  _pluginPaths[name] = "builtin:" + name;
}

void PluginShellBase::findAllPlugins() const
{
  for (const QString& path : pluginPaths()) {
    findPlugins(path);
  }
  scanComplete = true;
}

void PluginShellBase::selectDefaultPlugin()
{
  QSettings globalSettings(QSettings::IniFormat, QSettings::SystemScope, "Alkahest", "crude-session");
  QSettings localSettings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-session");
  QString savedPlugin = localSettings.value(pluginPrefix() + "plugin").toString();
  if (savedPlugin.isEmpty()) {
    savedPlugin = globalSettings.value(pluginPrefix() + "plugin").toString();
  }
  if (!savedPlugin.isEmpty()) {
    if (savedPlugin == "disabled") {
      disablePlugin();
      return;
    }
    QObject* plugin = loadPlugin(savedPlugin);
    if (plugin) {
      setActivePlugin(plugin);
      return;
    }
  }
  if (!scanComplete) {
    findAllPlugins();
  }
  for (const QString& name : pluginPriority()) {
    if (_plugins.contains(name)) {
      setActivePlugin(name);
      break;
    }
  }
  if (!_plugin && !_plugins.isEmpty()) {
    setActivePlugin(_plugins.keys().first());
  }
}

void PluginShellBase::findPlugins(const QString& path) const
{
  QDir dir(path);
  for (const QString& fn : dir.entryList(QStringList() << ("lib" + pluginPrefix() + "-*.so"))) {
    loadPlugin(dir.absoluteFilePath(fn));
  }
}

QObject* PluginShellBase::loadPlugin(const QString& path) const
{
  if (path.startsWith("builtin:")) {
    return _plugins.value(path);
  }
  QPluginLoader loader(path);
  QObject* obj = loader.instance();
  QString name = checkPlugin(obj);
  if (name.isEmpty()) {
    loader.unload();
    return nullptr;
  }
  _plugins[name] = obj;
  _pluginPaths[name] = path;
  return obj;
}

QStringList PluginShellBase::availablePlugins() const
{
  if (!scanComplete) {
    findAllPlugins();
  }
  return QStringList() << noPluginName() << _plugins.keys();
}

QString PluginShellBase::activePlugin() const
{
  QString name = checkPlugin(_plugin);
  if (name.isEmpty()) {
    return noPluginName();
  }
  return name;
}

bool PluginShellBase::setActivePlugin(const QString& name)
{
  if (name == activePlugin()) {
    return true;
  }
  QSettings localSettings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-session");
  if (name.isEmpty() || name == noPluginName()) {
    localSettings.setValue(pluginPrefix() + "plugin", "disabled");
    localSettings.sync();
    disablePlugin();
    return true;
  }
  if (!_plugins.contains(name)) {
    return false;
  }
  _plugin = _plugins.value(name);
  localSettings.setValue(pluginPrefix() + "plugin", _pluginPaths.value(name));
  localSettings.sync();
  return true;
}
