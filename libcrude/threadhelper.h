#ifndef CRUDE_THREADHELPER_H
#define CRUDE_THREADHELPER_H

#include <QMetaObject>
#include <QMetaType>
#include <QMutex>
#include <QPointer>
#include <QSemaphore>
#include <QSet>
#include <QSharedPointer>
#include <QThread>

extern QSet<QThread*> TH_pendingThreads;
void waitForPendingThreadHelpers();

template <typename T>
class ThreadHelper;

template <typename T>
class ThreadResult
{
private:
  struct Data
  {
    mutable QMutex lock;
    QSemaphore wait;
    T value;
    bool done;
    QPointer<QThread> thread;

    Data(QThread* thread) : wait(1), done(false), thread(thread) {}
  };

  friend class ThreadHelper<T>;

  void setResult(const T& val)
  {
    QMutexLocker locker(&d->lock);
    d->value = val;
    d->done = true;
    d->wait.release(1);
  }

  QSharedPointer<Data> d;

  ThreadResult(QThread* thread) : d(QSharedPointer<Data>(new Data(thread))) { d->wait.acquire(1); }

public:
  ThreadResult() : d(QSharedPointer<Data>(new Data(nullptr))) { d->done = true; }

  bool isFinished() const
  {
    QMutexLocker locker(&d->lock);
    return d->done;
  }

  T result() const
  {
    d->wait.acquire(1);
    QMutexLocker locker(&d->lock);
    T result = d->value;
    d->wait.release(1);
    return result;
  }

  QThread* thread() const
  {
    QMutexLocker locker(&d->lock);
    return d->thread;
  }
};

template <typename T>
class ThreadHelper : public QThread
{
public:
  template <typename Functor>
  static ThreadResult<T> run(Functor functor, QObject* cbObj, const char* cbSlot);

  template <typename Functor, typename Slot>
  static ThreadResult<T> run(Functor functor, Slot cbSlot, QObject* context = nullptr);

  template <typename Functor>
  static ThreadResult<T> run(Functor functor);

  ~ThreadHelper() { TH_pendingThreads.remove(this); }

protected:
  ThreadResult<T> result;

  ThreadHelper() : QThread(nullptr), result(this) { TH_pendingThreads << this; }

  void setResult(const T& value) { result.setResult(value); }
};

template <typename T, typename Functor>
class ThreadHelperImpl : public ThreadHelper<T>
{
  friend class ThreadHelper<T>;

protected:
  void run() { this->setResult(fn()); }

private:
  ThreadHelperImpl(Functor functor) : fn(functor) {}

  Functor fn;
};

template <typename T>
template <typename Functor>
ThreadResult<T> ThreadHelper<T>::run(Functor functor)
{
  ThreadHelper<T>* thread = new ThreadHelperImpl<T, Functor>(functor);
  QObject::connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()), Qt::QueuedConnection);
  thread->start();
  return thread->result;
}

template <typename T>
template <typename Functor>
ThreadResult<T> ThreadHelper<T>::run(Functor functor, QObject* cbObj, const char* cbSlot)
{
  if (!cbObj || !cbSlot) {
    return ThreadHelper<T>::run(functor);
  }
  QPointer<QObject> pCbObj(cbObj);
  QByteArray slot = QMetaObject::normalizedSignature(cbSlot);
  if (slot[0] >= '0' && slot[0] <= '9') {
    slot = slot.mid(1);
  }
  QByteArray typeName;
  bool noParam = slot.contains("()");
  if (!noParam) {
    typeName = QMetaType::typeName(qMetaTypeId<T>());
    if (!QMetaObject::checkConnectArgs(("2finished(" + typeName + ")").constData(), slot.constData())) {
      qWarning("ThreadHelper<%s>: incompatible slot: SLOT(%s)", typeName.constData(), cbSlot);
      // Since the callback will never be dispatched, just run the thread with no callback.
      return ThreadHelper<T>::run(functor);
    }
  }
  slot = slot.left(slot.indexOf('('));
  return run(
      functor,
      [pCbObj, slot, typeName](const T& value) {
        if (pCbObj.isNull()) {
          // The object was destroyed before the callback could be invoked.
          return;
        }
        if (typeName.isEmpty()) {
          QMetaObject::invokeMethod(pCbObj.data(), slot.constData(), Qt::QueuedConnection);
        } else {
          QMetaObject::invokeMethod(
              pCbObj.data(), slot.constData(), Qt::QueuedConnection, QGenericArgument(typeName.constData(), &value)
          );
        }
      },
      cbObj
  );
}

template <typename T>
template <typename Functor, typename Slot>
ThreadResult<T> ThreadHelper<T>::run(Functor functor, Slot cbSlot, QObject* context)
{
  ThreadHelper<T>* thread = new ThreadHelperImpl<T, Functor>(functor);
  if (!context) {
    context = QThread::currentThread();
  }
  QObject::connect(
      thread, &QThread::finished, context, [thread, cbSlot]() { cbSlot(thread->result.d->value); }, Qt::QueuedConnection
  );
  QObject::connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()), Qt::QueuedConnection);
  thread->start();
  return thread->result;
}

#endif
