#include "hotkey.h"

#include "hotkey_p.h"
#include "xcb/xcbhotkey_p.h"

Hotkey::Hotkey(const QKeySequence& shortcut, QObject* parent) : QObject(parent), d(nullptr)
{
  // TODO: platform detection
  d = new XcbHotkeyPrivate(shortcut, this);
}

Hotkey::~Hotkey()
{
  delete d;
}

QKeySequence Hotkey::keySequence() const
{
  return d->seq;
}

bool Hotkey::isValid() const
{
  // A valid hotkey has at least one successfully grabbed key combination.
  return d->isValid;
}

bool Hotkey::hasModifier() const
{
  return d->seq[0] & (Qt::SHIFT | Qt::CTRL | Qt::ALT | Qt::META);
}

QVariant Hotkey::data() const
{
  return d->data;
}

void Hotkey::setData(const QVariant& data)
{
  d->data = data;
}

HotkeyPrivate::HotkeyPrivate(const QKeySequence& seq, Hotkey* owner) : p(owner), seq(seq)
{
  // initializers only
}

HotkeyPrivate::~HotkeyPrivate()
{
  // Not having a virtual destructor declared here causes Hotkey::~Hotkey to fail.
  // The implicit cleanup is the only action necessary.
}
