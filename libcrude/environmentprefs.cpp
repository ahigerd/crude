#include "environmentprefs.h"

#include <QDir>
#include <QFile>
#include <QIcon>
#include <QLocale>
#include <QProcess>
#include <QSettings>
#include <QStandardPaths>
#include <QThread>
#include <QtDebug>

static EnvironmentPrefs* EP_instance = nullptr;

inline EnvironmentPrefs* EnvironmentPrefs::instance()
{
  if (!EP_instance) {
    EP_instance = new EnvironmentPrefs();
    EP_instance->init();
  }
  return EP_instance;
}

static QDir absoluteDir(const QString& path)
{
  QDir dir(path.startsWith("~/") ? QDir::homePath() + path.mid(1) : path);
  return QDir(dir.canonicalPath());
}

static QString queryCommand(const QString& program, const QStringList& args)
{
  QProcess p;
  p.start(program, args);
  bool ok = p.waitForFinished();
  if (ok) {
    return QString::fromUtf8(p.readAllStandardOutput()).trimmed();
  }
  return QString();
}

static QString
    readKdeConfig(const QString& file, const QString& group, const QString& key, const QString& defaultValue = QString())
{
  QStringList args = QStringList() << "--file" << file << "--group" << group << "--key" << key;
  if (!defaultValue.isEmpty()) {
    args << "--default" << defaultValue;
  }
  return queryCommand("kreadconfig", args);
}

static QString readGSettings(const QString& key, const QString& prop)
{
  QString result = queryCommand("gsettings", QStringList() << "get" << key << prop);
  if (result.startsWith('\'') && result.endsWith('\'')) {
    result = result.mid(1, result.length() - 2);
  }
  return result;
}

static QString readGConf(const QString& key)
{
  return queryCommand("gconftool-2", QStringList() << "--get" << key);
}

EnvironmentPrefs::EnvironmentPrefs() {}

void EnvironmentPrefs::init()
{
  initLocale();
  initXdgPaths();
  detectDesktop();
  initTerminal();
}

void EnvironmentPrefs::initLocale()
{
  QLocale sys = QLocale::system();
  localeLang = sys.name().section('_', 0, 0);
  localeCountry = sys.name().section('_', 1, 1);
  QString lc = getenv("LC_MESSAGES", getenv("LC_ALL", getenv("LANG")));
  if (lc == "C") {
    // Explicitly disable locales
    localeLang = QString();
    localeCountry = QString();
  } else {
    localeModifier = lc.section('@', -1);
  }
  localeFull = localeLang;
  if (!localeLang.isEmpty()) {
    if (!localeCountry.isEmpty()) {
      localeFull += "_" + localeCountry;
    }
    if (!localeModifier.isEmpty()) {
      localeFull += "@" + localeModifier;
    }
    localeList << localeFull;
    if (!localeCountry.isEmpty() && !localeModifier.isEmpty()) {
      localeList << localeLang + "_" + localeCountry;
      localeList << localeLang + "@" + localeModifier;
    }
    if (!localeCountry.isEmpty() || !localeModifier.isEmpty()) {
      localeList << localeLang;
    }
  }
}

void EnvironmentPrefs::initXdgPaths()
{
  for (const QString& path : getenv("XDG_DATA_DIRS", "/usr/local/share:/usr/share").split(":")) {
    QDir dir = absoluteDir(path);
    if (dir.exists() && dir.isAbsolute()) {
      xdgSystemData << dir.canonicalPath();
    }
  }
  xdgUserData = absoluteDir(getenv("XDG_DATA_HOME", "~/.local/share")).canonicalPath();

  for (const QString& path : getenv("XDG_CONFIG_DIRS", "/etc/xdg").split(":")) {
    QDir dir = absoluteDir(path);
    if (dir.exists() && dir.isAbsolute()) {
      xdgSystemConfig << dir.canonicalPath();
    }
  }
  xdgUserConfig = absoluteDir(getenv("XDG_CONFIG_HOME", "~/.config")).canonicalPath();
}

struct DeTermConfig
{
  const char* de;
  const char* term;
  const char* flags;
};

static const DeTermConfig deTerms[] = {
  { "xfce", "xfce4=terminal", "-e" },
  { "lxde", "lxterminal", "-e" },
  { "lxqt", "qterminal", "-e" },
  { "enlightenment", "terminology", "-e" },
  { "dde", "deepin-terminal", "-e" },
  { "", "urxvt", "-e" },
  { "", "rxvt-unicode", "-e" },
  { "", "xterm", "-e" },
  { "", "rxvt", "-e" },
};
static QList<EnvironmentPrefs::StringPair> detectedTerminals;

static void addTerminal(const QString& term, const QString& flags)
{
  EnvironmentPrefs::StringPair dt(term, flags);
  if (!detectedTerminals.contains(dt)) {
    detectedTerminals << dt;
  }
}

void EnvironmentPrefs::initTerminal(QObject* notify, const char* slot)
{
  // Since this might take a bit, kick it off in a thread
  QString de = deskEnv;
  termResult = ThreadHelper<StringPair>::run(
      [de]() -> StringPair {
        // CRUDE saved settings
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-environment");
        QString value = settings.value("terminal").toString();
        if (!value.isEmpty() && !QStandardPaths::findExecutable(value.section(' ', 0, 0)).isEmpty()) {
          addTerminal(value.section(' ', 0, 0), value.section(' ', 1));
        }

        // XDG semi-standard
        if (!QStandardPaths::findExecutable("x-terminal-emulator").isEmpty()) {
          addTerminal("x-terminal-emulator", "-e");
        }

        // KDE saved settings
        value = readKdeConfig("kdeglobals", "General", "TerminalApplication", "konsole");
        if (!value.isEmpty()) {
          addTerminal(value, "-e");
        }

        // GNOME 3 saved settings
        value = readGSettings("org." + de + ".desktop.default-applications.terminal", "exec");
        if (!value.isEmpty()) {
          QString flags = readGSettings("org." + de + ".desktop.default-applications.terminal", "exec-arg");
          addTerminal(value, flags);
        }

        // GNOME 2 saved settings
        value = readGConf("/desktop/gnome/applications/terminal/exec");
        if (!value.isEmpty()) {
          QString flags = readGConf("/desktop/gnome/applications/terminal/exec_arg");
          addTerminal(value, flags);
        }

        // XFCE saved settings
        QFile f(userConfigDir() + "/xfce4/helpers.rc");
        if (f.open(QIODevice::ReadOnly)) {
          while (f.canReadLine()) {
            QString line = f.readLine();
            if (line.startsWith("TerminalEmulator=")) {
              // XXX: Investigate how XFCE passes flags
              addTerminal(line.section('=', 1), "-e");
              break;
            }
          }
        }

        // First pass: match with DE
        for (const auto& deTerm : deTerms) {
          if (de == deTerm.de && !QStandardPaths::findExecutable(QString(deTerm.term).section(' ', 0, 0)).isEmpty()) {
            addTerminal(deTerm.term, deTerm.flags);
          }
        }
        // Second pass: do whatever works
        for (const auto& deTerm : deTerms) {
          if (!QStandardPaths::findExecutable(QString(deTerm.term).section(' ', 0, 0)).isEmpty()) {
            addTerminal(deTerm.term, deTerm.flags);
          }
        }
        if (detectedTerminals.isEmpty()) {
          return StringPair("", "");
        }
        return detectedTerminals.first();
      },
      notify,
      slot
  );
}

void EnvironmentPrefs::rescanTerminals(QObject* receiver, const char* slot)
{
  instance()->initTerminal(receiver, slot);
}

QStringList EnvironmentPrefs::availableTerminals()
{
  if (instance()->term.isEmpty()) {
    instance()->termResult.result();
  }
  QStringList result;
  for (const StringPair& dt : detectedTerminals) {
    if (dt.second.isEmpty()) {
      result << dt.first;
    } else {
      result << dt.first + " " + dt.second;
    }
  }
  return result;
}

QString EnvironmentPrefs::getenv(const QString& variable, const QString& defaultValue)
{
#if QT_VERSION < QT_VERSION_CHECK(5, 10, 0)
  QByteArray env = qgetenv(qPrintable(variable));
  if (env.isEmpty()) {
    return defaultValue;
  }
  return QString::fromUtf8(env);
#else
  return qEnvironmentVariable(qPrintable(variable), defaultValue);
#endif
}

QString EnvironmentPrefs::locale(bool includeCountry, bool includeModifier)
{
  if (includeCountry && includeModifier) {
    return instance()->localeFull;
  } else if (includeCountry && localeHasCountry()) {
    return instance()->localeLang + "_" + instance()->localeCountry;
  } else if (includeModifier && localeHasModifier()) {
    return instance()->localeLang + "@" + instance()->localeCountry;
  } else {
    return instance()->localeLang;
  }
}

bool EnvironmentPrefs::localeHasCountry()
{
  return !instance()->localeCountry.isEmpty();
}

bool EnvironmentPrefs::localeHasModifier()
{
  return !instance()->localeModifier.isEmpty();
}

QStringList EnvironmentPrefs::localePrecedence()
{
  return instance()->localeList;
}

QStringList EnvironmentPrefs::dataDirs()
{
  return QStringList() << userDataDir() << systemDataDirs();
}

QStringList EnvironmentPrefs::systemDataDirs()
{
  return instance()->xdgSystemData;
}

QString EnvironmentPrefs::userDataDir()
{
  return instance()->xdgUserData;
}

QStringList EnvironmentPrefs::configDirs()
{
  return QStringList() << userConfigDir() << systemConfigDirs();
}

QStringList EnvironmentPrefs::systemConfigDirs()
{
  return instance()->xdgSystemConfig;
}

QString EnvironmentPrefs::userConfigDir()
{
  return instance()->xdgUserConfig;
}

bool EnvironmentPrefs::isSystemPath(const QString& path)
{
  QStringList dirs = QStringList() << systemDataDirs() << systemConfigDirs();
  for (const QString& dir : dirs) {
    QString relative = QDir(dir).relativeFilePath(path);
    if (!relative.startsWith("..") && !relative.startsWith("/")) {
      return true;
    }
  }
  return false;
}

void EnvironmentPrefs::detectDesktop()
{
  // This logic is vaguely based on the detectDE() function from xdg-utils.
  QString de = getenv("XDG_CURRENT_DESKTOP", "generic").trimmed().toLower().replace("x-", "");
  if (de.contains("gnome")) {
    de = "gnome";
  } else if (de.contains("deepin")) {
    de = "dde";
  }
  if (de.isEmpty()) {
    QString session = getenv("DESKTOP_SESSION").trimmed().toLower();
    if (session.contains("gnome")) {
      de = "gnome";
    } else if (session.contains("xfce")) {
      de = "xfce";
    } else if (session.contains("lxde") || session.contains("lubuntu")) {
      de = "lxde";
    } else if (session.contains("mate")) {
      de = "mate";
    }
  }
  if (de.isEmpty()) {
    if (!getenv("KDE_FULL_SESSION").isEmpty()) {
      de = "kde";
    } else if (!getenv("GNOME_DESKTOP_SESSION_ID").isEmpty()) {
      de = "gnome";
    } else if (!getenv("MATE_DESKTOP_SESSION_ID").isEmpty()) {
      de = "mate";
    } else if (!getenv("LXQT_SESSION_CONFIG").isEmpty()) {
      de = "lxqt";
    } else if (getenv("DESKTOP").toLower().contains("enlightenment")) {
      de = "enlightenment";
    } else {
      de = "generic";
    }
  }
  deskEnv = de;
}

QString EnvironmentPrefs::detectedDesktop()
{
  return instance()->deskEnv;
}

QStringList EnvironmentPrefs::terminalCommand(const QString& run)
{
  if (instance()->term.isEmpty()) {
    StringPair result = instance()->termResult.result();
    instance()->term = result.first;
    instance()->termFlags = result.second;
  }
  QStringList cmd = instance()->term.split(" ");
  if (!run.isEmpty()) {
    if (!instance()->termFlags.isEmpty()) {
      cmd << instance()->termFlags;
    }
    cmd << "sh"
        << "-c" << run;
  }
  return cmd;
}

bool readXdg(QIODevice& dev, QSettings::SettingsMap& map)
{
  QString group = "";
  QString line;
  while (!(line = dev.readLine()).isEmpty()) {
    line = line.trimmed();
    if (line.startsWith('[')) {
      group = line.replace("[", "").replace("]", "");
      if (group == "General") {
        group = "";
      } else {
        group += "/";
      }
      continue;
    }
    if (!line.contains('=')) {
      continue;
    }
    map[group + line.section("=", 0, 0).trimmed()] = line.section("=", 1).trimmed();
  }
  return true;
}

bool writeXdg(QIODevice& dev, const QSettings::SettingsMap& map)
{
  Q_UNUSED(dev)
  qDebug() << map;
  return false;
}

static QSettings::Format EP_desktop = QSettings::InvalidFormat, EP_directory = QSettings::InvalidFormat;

QSettings::Format EnvironmentPrefs::desktopFormat()
{
  if (EP_desktop == QSettings::InvalidFormat) {
    EP_desktop = QSettings::registerFormat("desktop", readXdg, writeXdg);
  }
  return EP_desktop;
}

QSettings::Format EnvironmentPrefs::directoryFormat()
{
  if (EP_directory == QSettings::InvalidFormat) {
    EP_directory = QSettings::registerFormat("directory", readXdg, writeXdg);
  }
  return EP_directory;
}
