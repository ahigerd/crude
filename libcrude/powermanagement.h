#ifndef CRUDE_POWERMANAGEMENT_H
#define CRUDE_POWERMANAGEMENT_H

#include "plugins/powerplugin.h"
#include "pluginshell.h"

#include <QObject>

class PowerManagement : public QObject, public PluginShell<PowerPlugin>
{
  Q_OBJECT

public:
  using Action = PowerPlugin::Action;

  static PowerManagement* instance();
  PowerManagement();
  ~PowerManagement();

  using PluginShell<PowerPlugin>::setActivePlugin;
  virtual bool canPerform(Action action) const;
  static QList<Action> allActions();
  static QString actionName(Action action);

public slots:
  virtual void perform(Action action);

protected:
  virtual QString pluginPrefix() const;
  virtual QStringList pluginPriority() const;
  virtual QString noPluginName() const;
};

#endif
