#ifndef CRUDE_HOTKEY_P_H
#define CRUDE_HOTKEY_P_H

#include "hotkey.h"

class HotkeyPrivate
{
  friend class Hotkey;

public:
  HotkeyPrivate(const QKeySequence& seq, Hotkey* owner);
  virtual ~HotkeyPrivate();

  Hotkey* p;
  QKeySequence seq;
  QVariant data;
  bool isValid;
};

#endif
