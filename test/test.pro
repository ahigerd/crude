QT = core widgets
TEMPLATE = app
TARGET = test
LIBS += -L.. -lcrude
include(../crude-common.pri)
DESTDIR = .

SOURCES += test.cpp 

PRE_TARGETDEPS += ../libcrude.a
