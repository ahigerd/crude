#include "libcrude/displaypower.h"
#include "libcrude/hotkey.h"
#include "libcrude/platformintegration.h"
#include "libcrude/threadhelper.h"
#include "libcrude/xcb/xcbselectionmanager.h"
#include "libcrude/xdgmenu.h"
#include "libcrude/xdgmenumodel.h"

#include <QApplication>
#include <QIcon>
#include <QLabel>
#include <QMenu>
#include <QMessageBox>
#include <QSessionManager>
#include <QStyle>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QtDebug>

void dumpModel(QAbstractItemModel* model, const QModelIndex& parent = QModelIndex(), const QString& prefix = QString())
{
  int rc = model->rowCount(parent);
  QString head = prefix;
  if (!head.isEmpty()) {
    head[head.length() - 1] = '-';
  }
  head += rc ? "+ " : "- ";
  qDebug() << qPrintable(head + model->data(parent).toString());
  head = QString(prefix).replace("\\", " ");
  for (int i = 0; i < rc; i++) {
    QModelIndex idx = model->index(i, 0, parent);
    dumpModel(model, idx, head + (i == rc - 1 ? "\\ " : "| "));
  }
}

int main(int argc, char** argv)
{
  /*
  if (argc > 1) {
    if (argv[1][0] == '-') {
      XdgMenu::mainMenu().debug();
    } else {
      XdgMenu menu(argv[1]);
      menu.debug();
    }
    waitForPendingThreadHelpers();
    return 0;
  }
  */

  QApplication app(argc, argv);
  PlatformIntegrator platform(argc, argv);

  /*
  app.setFallbackSessionManagementEnabled(false);
  QObject::connect(&app, &QApplication::commitDataRequest, [](QSessionManager& sm) {
    if (sm.allowsInteraction()) {
      int ret = QMessageBox::warning(
          nullptr,
          "My Application",
          "Save changes to document?",
          QMessageBox::Save | QMessageBox::Cancel);
      sm.release();
      if (ret == QMessageBox::Cancel) {
        sm.cancel();
      }
    }
  });

  QLabel l1("Foo");
  l1.setWindowTitle("L1");
  l1.setWindowIcon(QIcon::fromTheme("checkers"));
  l1.show();

  QLabel l2("Bar", &l1);
  l2.setWindowFlags(Qt::Window);
  l2.setWindowTitle("L2");
  l2.setWindowIcon(app.style()->standardIcon(QStyle::SP_ArrowRight));
  l2.show();

  QMenu menu;
  menu.addAction("Foo");
  menu.addAction("Bar");
  menu.addSeparator();
  menu.addAction("Exit", &app, SLOT(quit()));

  QSystemTrayIcon trayIcon(app.style()->standardIcon(QStyle::SP_ArrowUp));
  trayIcon.setToolTip("Test Tooltip");
  trayIcon.setContextMenu(&menu);
  trayIcon.show();

  bool toggle = true;
  QTimer timer;
  QObject::connect(&timer, &QTimer::timeout, [&](){
    qDebug() << "alert";
    app.alert(&l1);
    toggle = !toggle;
    l2.setPixmap(app.style()->standardPixmap(toggle ? QStyle::SP_ArrowRight : QStyle::SP_ArrowLeft));
    l2.setWindowIcon(app.style()->standardIcon(toggle ? QStyle::SP_ArrowRight : QStyle::SP_ArrowLeft));
    trayIcon.setIcon(app.style()->standardIcon(toggle ? QStyle::SP_ArrowUp : QStyle::SP_ArrowDown));
  });
  timer.start(1000);

  QStringList keys = QStringList() << "Ctrl+Q" << "Meta+Z" << "Shift+Esc" << "F4" << "Meta+Esc" << "Ctrl+Esc" << "Meta+Ctrl+Esc" << "Alt+Esc" << "Alt+X";
  for (const QString& key : keys) {
    Hotkey* hk = new Hotkey(QKeySequence(key));
    QObject::connect(hk, SIGNAL(activated(QVariant)), &app, SLOT(quit()));
  }

  QObject::connect(c_PI, &PlatformIntegration::systemTrayAvailabilityChanged, [&trayIcon](bool on) {
    qDebug() << "systray" << on;
    trayIcon.setVisible(!on);
    trayIcon.setVisible(on);
  });

  */
  DisplayPower* dp = c_PI->displayPower();
  QObject::connect(dp, SIGNAL(backlightAvailableChanged(bool)), &app, SLOT(quit()));
  int rv = app.exec();
  qDebug() << dp->backlightLevel();
  return rv;
  /*
  auto sched = dp->schedule();
  for (const auto& s : sched) qDebug() << s.standbyMode << s.delay;
  QTimer::singleShot(10000, [dp]{ dp->setStandbyMode(DisplayPower::Off); });

  return app.exec();
  XdgMenuModel model;
  dumpModel(&model);
  */
}
