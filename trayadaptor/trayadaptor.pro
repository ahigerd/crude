TEMPLATE = app
TARGET = crude-trayadaptor
QT = core widgets dbus
LIBS += -L.. -lcrude
include(../crude-common.pri)

HEADERS += snwatcher.h   snitem.h
SOURCES += snwatcher.cpp snitem.cpp main.cpp

PRE_TARGETDEPS += ../libcrude.a
