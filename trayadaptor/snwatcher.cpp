#include "snwatcher.h"

#include "snitem.h"

#include <QApplication>
#include <QDBusConnection>
#include <QDBusError>
#include <QSystemTrayIcon>
#include <QtDebug>

SNWatcher::SNWatcher(QObject* parent) : QObject(parent), impl(new SNWatcherImpl(this, this))
{
  // Initializers only
}

SNWatcherImpl::SNWatcherImpl(QDBusContext* ctx, QObject* parent)
: QDBusAbstractAdaptor(parent), ctx(ctx), reportedSystray(false)
{
  hostObject = new QObject(this);
  QObject::connect(&pollTimer, SIGNAL(timeout()), this, SLOT(pollForSystray()));
  pollTimer.setTimerType(Qt::VeryCoarseTimer);
  pollTimer.start();
}

void SNWatcherImpl::RegisterStatusNotifierItem(const QString& path)
{
  qDebug() << "RegisterStatusNotifierItem" << path;
  if (ctx->calledFromDBus()) {
    qDebug() << ctx->message().service();
  } else {
    qDebug() << "no context?";
  }

  SNItem* item = new SNItem(ctx->message().service(), path);
  QObject::connect(item, SIGNAL(unregistered()), this, SLOT(serviceUnregistered()));
  items << item;
  emit StatusNotifierItemRegistered(path);
}

void SNWatcherImpl::RegisterStatusNotifierHost(const QString& service)
{
  qDebug() << "RegisterStatusNotifierHost" << service;
}

QStringList SNWatcherImpl::registeredItems() const
{
  QStringList result;
  for (SNItem* item : items) {
    result << item->path;
  }
  return result;
}

int SNWatcherImpl::protocolVersion() const
{
  // TODO: ???
  return 1;
}

void SNWatcherImpl::pollForSystray()
{
  bool available = QSystemTrayIcon::isSystemTrayAvailable();
  if (!reportedSystray && available) {
    bool ok = false;
    ok = QDBusConnection::sessionBus().registerObject("/StatusNotifierHost", "org.kde.StatusNotifierHost", hostObject);
    ok = ok &&
        QDBusConnection::sessionBus().registerService(QString("org.kde.StatusNotifierHost-%1").arg(qApp->applicationPid())
        );
    if (!ok) {
      return;
    }
    emit StatusNotifierHostRegistered();
  }
  reportedSystray = available;
}

void SNWatcherImpl::serviceUnregistered()
{
  SNItem* item = static_cast<SNItem*>(sender());
  items.removeAll(item);
  qDebug() << "unregistered" << item->service << item->path;
  item->deleteLater();
  emit StatusNotifierItemUnregistered(item->path);
}
