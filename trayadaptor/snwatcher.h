#ifndef CRUDE_SNWATCHER_H
#define CRUDE_SNWATCHER_H

#include <QDBusAbstractAdaptor>
#include <QDBusContext>
#include <QList>
#include <QStringList>
#include <QTimer>
class SNItem;

class SNWatcherImpl : public QDBusAbstractAdaptor
{
  Q_OBJECT
  Q_CLASSINFO("D-Bus Interface", "org.kde.StatusNotifierWatcher")
  Q_PROPERTY(QStringList RegisteredStatusNotifierItems READ registeredItems)
  Q_PROPERTY(int ProtocolVersion READ protocolVersion)

public:
  SNWatcherImpl(QDBusContext* ctx, QObject* parent = nullptr);

  QStringList registeredItems() const;
  int protocolVersion() const;

signals:
  void StatusNotifierItemRegistered(const QString& path);
  void StatusNotifierItemUnregistered(const QString& path);
  void StatusNotifierHostRegistered();

public slots:
  void RegisterStatusNotifierItem(const QString& service);
  void RegisterStatusNotifierHost(const QString& service);

private slots:
  void serviceUnregistered();
  void pollForSystray();

private:
  QList<SNItem*> items;
  QTimer pollTimer;
  QObject* hostObject;
  QDBusContext* ctx;
  bool reportedSystray;
};

class SNWatcher : public QObject, protected QDBusContext
{
  Q_OBJECT

public:
  SNWatcher(QObject* parent = nullptr);

private:
  SNWatcherImpl* impl;
};

#endif
