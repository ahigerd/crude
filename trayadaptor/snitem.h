#ifndef CRUDE_SNITEM_H
#define CRUDE_SNITEM_H

#include <QDBusInterface>
#include <QSystemTrayIcon>
class QDBusArgument;

class SNItem : public QSystemTrayIcon
{
  Q_OBJECT

public:
  SNItem(const QString& service, const QString& path, QObject* parent = nullptr);
  ~SNItem();

  QString service, path;

signals:
  void unregistered();

private slots:
  void onActivate(QSystemTrayIcon::ActivationReason reason);
  void updateTitle();
  void updateIcon(bool compose = true);
  void updateAttentionIcon(bool compose = true);
  void updateOverlayIcon(bool compose = true);
  void updateTooltip();
  void updateStatus();
  void composeTooltip();
  void composeIcon();

private:
  QMenu* constructMenu(const QDBusArgument& arg);

  QDBusInterface iface;
  QString category, id, title, tooltipTitle, tooltipContent, status;
  QIcon mainIcon, attentionIcon, overlayIcon;
  QMenu* dbusMenu;
};

#endif
