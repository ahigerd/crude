#include "snitem.h"

#include "libcrude/platformintegration.h"

#include <QApplication>
#include <QDBusArgument>
#include <QDBusServiceWatcher>
#include <QFile>
#include <QMenu>
#include <QPainter>
#include <QPixmap>
#include <QStyle>
#include <QtDebug>

SNItem::SNItem(const QString& service, const QString& path, QObject* parent)
: QSystemTrayIcon(parent),
  service(service),
  path(path),
  iface(service, path, "org.kde.StatusNotifierItem"),
  dbusMenu(nullptr)
{
  qDebug() << "SNItem" << service << path;
  QDBusServiceWatcher* sw =
      new QDBusServiceWatcher(service, QDBusConnection::sessionBus(), QDBusServiceWatcher::WatchForUnregistration, this);
  QObject::connect(sw, SIGNAL(serviceUnregistered(QString)), this, SIGNAL(unregistered()));
  QObject::connect(sw, SIGNAL(serviceUnregistered(QString)), this, SLOT(hide()));

  id = iface.property("Id").toString();
  category = iface.property("Category").toString();
  updateTitle();
  updateTooltip();
  updateIcon(false);
  updateAttentionIcon(false);
  updateOverlayIcon(false);
  updateStatus();

  QObject::connect(
      this, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(onActivate(QSystemTrayIcon::ActivationReason))
  );

  show();
}

SNItem::~SNItem()
{
  if (dbusMenu) {
    dbusMenu->deleteLater();
  }
}

void SNItem::onActivate(QSystemTrayIcon::ActivationReason reason)
{
  qDebug() << "window id =" << iface.property("WindowId");
  QString mode;
  switch (reason) {
  case Context: mode = "ContextMenu"; break;
  case MiddleClick: mode = "SecondaryActivate"; break;
  default:
    if (iface.property("IsMenu").toBool()) {
      mode = "ContextMenu";
    } else {
      mode = "Activate";
    }
    break;
  }
  if (mode == "ContextMenu") {
    QString menuPath = iface.property("Menu").value<QDBusObjectPath>().path();
    if (!menuPath.isEmpty()) {
      QDBusInterface menuIface(service, menuPath, "com.canonical.dbusmenu");
      auto args = menuIface.call("GetLayout", 0, -1, QStringList()).arguments();
      for (auto a : args) {
        auto arg = a.value<QDBusArgument>();
        qDebug() << arg.asVariant() << arg.currentType() << arg.atEnd();
      }
      if (dbusMenu) {
        dbusMenu->deleteLater();
      }
      //dbusMenu = constructMenu(result);
      setContextMenu(dbusMenu);
    }
  }
  QPoint pos = geometry().center();
  iface.call(QDBus::NoBlock, mode, pos.x(), pos.y());
}

void SNItem::updateTitle()
{
  title = iface.property("Title").toString();
  if (title.isEmpty()) {
    title = id;
  }
}

void SNItem::updateIcon(bool compose)
{
  qDebug() << "icon" << iface.property("IconName") << iface.property("IconPixmap");
  QString iconName = iface.property("IconName").toString();
  if (QFile::exists(iconName)) {
    mainIcon = QIcon(iconName);
  } else if (!iconName.contains('/')) {
    mainIcon = c_PI->icon(iconName);
  } else {
    mainIcon = QIcon();
  }
  if (mainIcon.isNull()) {
    mainIcon = iface.property("IconPixmap").value<QPixmap>();
  }
  if (mainIcon.isNull()) {
    mainIcon = qApp->style()->standardIcon(QStyle::SP_MessageBoxQuestion);
  }
  if (compose) {
    composeIcon();
  }
}

void SNItem::updateAttentionIcon(bool compose)
{
  qDebug() << "attn" << iface.property("AttentionIconName") << iface.property("AttentionIconPixmap");
  QString iconName = iface.property("AttentionIconName").toString();
  if (QFile::exists(iconName)) {
    attentionIcon = QIcon(iconName);
  } else if (!iconName.contains('/')) {
    attentionIcon = c_PI->icon(iconName);
  } else {
    attentionIcon = QIcon();
  }
  if (attentionIcon.isNull()) {
    attentionIcon = iface.property("AttentionIconPixmap").value<QPixmap>();
  }
  if (attentionIcon.isNull()) {
    attentionIcon = qApp->style()->standardIcon(QStyle::SP_MessageBoxQuestion);
  }
  if (compose) {
    composeIcon();
  }
}

void SNItem::updateOverlayIcon(bool compose)
{
  qDebug() << "over" << iface.property("OverlayIconName") << iface.property("OverlayIconPixmap");
  QString iconName = iface.property("OverlayIconName").toString();
  if (QFile::exists(iconName)) {
    overlayIcon = QIcon(iconName);
  } else if (!iconName.contains('/')) {
    overlayIcon = c_PI->icon(iconName);
  } else {
    overlayIcon = QIcon();
  }
  if (overlayIcon.isNull()) {
    overlayIcon = iface.property("OverlayIconPixmap").value<QPixmap>();
  }
  if (overlayIcon.isNull()) {
    overlayIcon = qApp->style()->standardIcon(QStyle::SP_MessageBoxQuestion);
  }
  if (compose) {
    composeIcon();
  }
}

void SNItem::updateTooltip()
{
  composeTooltip();
}

void SNItem::updateStatus()
{
  status = iface.property("Status").toString();
  composeIcon();
}

void SNItem::composeTooltip()
{
  QString t1 = tooltipTitle.isEmpty() ? title : tooltipTitle;
  QString t2 = tooltipContent.isEmpty() ? "" : ": " + tooltipContent;
  setToolTip(t1 + t2);
  qDebug() << "tt" << category << id << title << tooltipTitle << tooltipContent << status << (t1 + t2);
}

void SNItem::composeIcon()
{
  QIcon icon = mainIcon;
  if (status == "NeedsAttention" && !attentionIcon.isNull()) {
    icon = attentionIcon;
  }
  if (!overlayIcon.isNull()) {
    QPixmap pm = mainIcon.pixmap(22, 22);
    QPainter p(&pm);
    overlayIcon.paint(&p, 0, 0, 22, 22);
    icon = QIcon(pm);
  }
  setIcon(icon);
}

QMenu* SNItem::constructMenu(const QDBusArgument& arg)
{
  QMenu* menu = new QMenu;
  /*
  QDBusArgument iter = arg;
  iter.beginMap();
  while (!iter.atEnd()) {
    QString key;
    QVariant value;
    arg.beginMapEntry();
    iter >> key >> value;
    qDebug() << key << value;
    arg.endMapEntry();
  }
  iter.endMap();
  */
  qDebug() << arg.asVariant() << arg.currentType();
  return menu;
}
