#include "libcrude/platformintegration.h"
#include "snwatcher.h"

#include <QApplication>
#include <QDBusConnection>
#include <QDBusError>

int main(int argc, char** argv)
{
  QApplication app(argc, argv);
  if (!QDBusConnection::sessionBus().isConnected()) {
    qFatal("Could not connect to D-BUS session bus");
  }

  SNWatcher watcher;
  QDBusConnection::sessionBus().registerObject("/StatusNotifierWatcher", &watcher);
  Q_UNUSED(watcher);

  if (!QDBusConnection::sessionBus().registerService("org.kde.StatusNotifierWatcher")) {
    qFatal("Unable to register service: %s", qPrintable(QDBusConnection::sessionBus().lastError().message()));
  }

  PlatformIntegrator pi(argc, argv);

  return app.exec();
}
