#ifndef CRUDE_ALSAVOLUMEPLUGIN_H
#define CRUDE_ALSAVOLUMEPLUGIN_H

#include "../volumeplugin.h"
#include <QProcess>
#include <QTimer>

class AlsaVolumePlugin : public QObject, public VolumePlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID VolumePlugin_iid)
  Q_INTERFACES(VolumePlugin)

public:
  AlsaVolumePlugin();
  virtual ~AlsaVolumePlugin() {}

  virtual QString pluginName() const;
  virtual bool available() const;
  virtual void setEnabled(bool on);

  virtual QString mixerCommand() const;

  virtual float volumeLevel() const;
  virtual void setVolumeLevel(float level);

  virtual bool isMuted() const;
  virtual void setMuted(bool mute);

signals:
  void volumeError();
  void muteChanged(bool muted);
  void volumeChanged(float level);

private slots:
  void monitorReadyRead();
  void monitorError(QProcess::ProcessError);
  void monitorFinished(int, QProcess::ExitStatus);
  void startMonitor();

private:
  void readMixer();
  void modifyVolume(const QVariant& adjust);

  QTimer restartThrottle;
  QProcess* monitor;
  QString alsactl;
  QString mixer;
  float lastLevel;
  bool lastMute;
  bool hasTools;
  qint64 lastRestart;
  int restartCount;
};

Q_DECLARE_INTERFACE(AlsaVolumePlugin, VolumePlugin_iid)

#endif
