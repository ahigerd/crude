#include "alsavolumeplugin.h"
#include <QFileInfo>
#include <QStandardPaths>

// TODO: might need to pass a card identifier to interface with a non-default card
static QString ALSA_MONITOR_COMMAND("stdbuf -oL %1 monitor");
static QString ALSA_GET_COMMAND("amixer -M sget 'Master'");
static QString ALSA_SET_COMMAND("amixer -M sset 'Master' %1");
static QRegExp MIXER_RE("Playback \\d+ \\[(\\d+)%\\] \\[[-0-9.]+dB\\] ?(\\[on\\]|\\[off\\])?");

AlsaVolumePlugin::AlsaVolumePlugin()
: QObject(nullptr), monitor(nullptr), lastLevel(0)
{
  alsactl = QStandardPaths::findExecutable("alsactl");
  if (alsactl.isEmpty()) {
    alsactl = "/usr/sbin/alsactl";
  }
  hasTools = !QStandardPaths::findExecutable("stdbuf").isEmpty() &&
    !QStandardPaths::findExecutable("amixer").isEmpty() &&
    QFileInfo(alsactl).isExecutable();

  mixer = QStandardPaths::findExecutable("alsamixergui");
  if (mixer.isEmpty()) {
    mixer = QStandardPaths::findExecutable("alxamixer");
    if (!mixer.isEmpty()) {
      mixer = "*" + mixer;
    }
  }

  restartThrottle.setSingleShot(true);
  restartThrottle.setInterval(1000);
  QTimer::connect(&restartThrottle, SIGNAL(timeout()), this, SLOT(startMonitor()));
}

QString AlsaVolumePlugin::pluginName() const
{
  return "ALSA";
}

bool AlsaVolumePlugin::available() const
{
  return hasTools;
}

void AlsaVolumePlugin::setEnabled(bool on)
{
  if (on) {
    if (!monitor) {
      monitor = new QProcess(this);
      QObject::connect(monitor, SIGNAL(readyRead()), this, SLOT(monitorReadyRead()));
      QObject::connect(
          monitor, SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(monitorError(QProcess::ProcessError))
      );
      QObject::connect(
          monitor, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(monitorFinished(int, QProcess::ExitStatus))
      );

      monitor->setProcessChannelMode(QProcess::MergedChannels);
    }
    startMonitor();
  } else {
    if (monitor) {
      monitor->deleteLater();
      monitor = nullptr;
    }
  }
}

void AlsaVolumePlugin::startMonitor()
{
  qint64 now = QDateTime::currentMSecsSinceEpoch();
  if (now < lastRestart + 15000) {
    restartCount++;
    if (restartCount > 3) {
      qWarning("alsactl exited too fast, disabling monitor");
      lastLevel = -2;
      emit volumeError();
      return;
    }
  } else {
    restartCount = 0;
  }
  lastRestart = now;
  monitor->start(ALSA_MONITOR_COMMAND.arg(alsactl), QIODevice::ReadOnly);
  readMixer();
}

void AlsaVolumePlugin::monitorReadyRead()
{
  readMixer();
}

void AlsaVolumePlugin::monitorFinished(int code, QProcess::ExitStatus status)
{
  qDebug() << "monitor finished" << code << status;
  restartThrottle.start();
}

void AlsaVolumePlugin::readMixer()
{
  QProcess p;
  p.start(ALSA_GET_COMMAND, QIODevice::ReadOnly);
  bool ok = p.waitForFinished(1000);
  if (ok) {
    QString result = QString::fromUtf8(p.readAll());
    int pos = MIXER_RE.indexIn(result);
    if (pos < 0) {
      qDebug() << "could not find value in output" << result;
    } else {
      bool mute = MIXER_RE.cap(2).contains("off");
      float level = MIXER_RE.cap(1).toInt() / 100.0;
      if (level != lastLevel) {
        lastLevel = level;
        emit volumeChanged(level);
      }
      if (mute != lastMute) {
        lastMute = mute;
        emit muteChanged(mute);
      }
    }
  } else {
    qDebug() << "failed to run amixer";
    lastLevel = -2;
    emit volumeError();
  }
}

void AlsaVolumePlugin::monitorError(QProcess::ProcessError code)
{
  qDebug() << "monitorError" << code;
  if (code == QProcess::FailedToStart) {
    lastLevel = -2;
    emit volumeError();
  } else {
    restartThrottle.start();
  }
}

float AlsaVolumePlugin::volumeLevel() const
{
  return lastLevel;
}

void AlsaVolumePlugin::setVolumeLevel(float level)
{
  if (level < 0) {
    level = 0;
  }
  modifyVolume(QString("%1%").arg(int(level * 100)));
}

bool AlsaVolumePlugin::isMuted() const
{
  return lastMute;
}

void AlsaVolumePlugin::setMuted(bool mute)
{
  modifyVolume(mute ? "off" : "on");
}

void AlsaVolumePlugin::modifyVolume(const QVariant& adjust)
{
  QProcess::startDetached(ALSA_SET_COMMAND.arg(adjust.toString()));
  QTimer::singleShot(100, this, SLOT(readMixer()));
}

QString AlsaVolumePlugin::mixerCommand() const
{
  return mixer;
}
