TARGET = light-sysfs
QT = core

HEADERS += sysfslightplugin.h   sysfslight.h
SOURCES += sysfslightplugin.cpp sysfslight.cpp

include(../lightplugin.pri)
