#include "sysfslightplugin.h"

SysfsLightPlugin::SysfsLightPlugin()
: QObject(nullptr), light(true)
{
  // initializers only
}

SysfsLightPlugin::~SysfsLightPlugin()
{
  // no-op
}

void SysfsLightPlugin::setEnabled(bool)
{
  // no-op
}

QString SysfsLightPlugin::pluginName() const
{
  return "sysfs";
}

bool SysfsLightPlugin::available() const
{
  return light.isAvailable();
}

float SysfsLightPlugin::backlightLevel() const
{
  return light.backlightLevel();
}

void SysfsLightPlugin::setBacklightLevel(float level)
{
  bool success = light.setBacklightLevel(level);
  if (success) {
    emit backlightChanged(backlightLevel());
  }
}

void SysfsLightPlugin::increaseBacklight(float step)
{
  setBacklightLevel(light.getAdjustedLevel(step));
}

void SysfsLightPlugin::decreaseBacklight(float step)
{
  setBacklightLevel(light.getAdjustedLevel(step));
}

void SysfsLightPlugin::displaysUpdated()
{
  // TODO: detect other backlights
}
