#ifndef CRUDE_SYSFSLIGHT_H
#define CRUDE_SYSFSLIGHT_H

#include <QString>

class SysfsLight {
public:
  SysfsLight(const QString& deviceName);
  SysfsLight(bool requireWritable);

  bool isAvailable() const;
  bool isWritable() const;

  float backlightLevel() const;
  float maxBacklightLevel() const;
  bool setBacklightLevel(float level);
  float getAdjustedLevel(float step) const;

  QString deviceName() const;

private:
  QString devicePath;
  float maxLevel;
  bool writable;
};

#endif
