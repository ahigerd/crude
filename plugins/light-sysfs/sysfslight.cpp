#include "sysfslight.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>

#include <cmath>

static const QString BACKLIGHT_PATH("/sys/class/backlight/");

SysfsLight::SysfsLight(bool requireWritable)
: maxLevel(1.0), writable(false)
{
  // TODO: It may be necessary to expose a configuration option for this.
  for (const QString& f : QDir(BACKLIGHT_PATH).entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
    QString prefix = BACKLIGHT_PATH + f;
    QString brightnessPath = prefix + "/brightness";
    QFileInfo fi(prefix + "/brightness");
    if (!fi.exists() || !fi.isReadable()) {
      continue;
    }
    writable = fi.isWritable();
    if (requireWritable && !fi.isWritable()) {
      continue;
    }
    fi = QFileInfo(prefix + "/max_brightness");
    if (!fi.exists() || !fi.isReadable()) {
      continue;
    }
    QFile currentBrightness(prefix + "/max_brightness");
    if (currentBrightness.open(QIODevice::ReadOnly)) {
      maxLevel = currentBrightness.readAll().trimmed().toInt();
    } else {
      maxLevel = 0;
    }
    if (maxLevel) {
      devicePath = prefix;
      return;
    }
  }
}

SysfsLight::SysfsLight(const QString& deviceName)
{
  // TODO
}

bool SysfsLight::isAvailable() const
{
  return !devicePath.isEmpty();
}

bool SysfsLight::isWritable() const
{
  return writable;
}

float SysfsLight::backlightLevel() const
{
  QFile currentBrightness(devicePath + "/brightness");
  if (currentBrightness.open(QIODevice::ReadOnly)) {
    return currentBrightness.readAll().trimmed().toInt() / maxLevel;
  }
  return 1.0;
}

bool SysfsLight::setBacklightLevel(float level)
{
  if (writable) {
    QFile currentBrightness(devicePath + "/brightness");
    if (currentBrightness.open(QIODevice::WriteOnly)) {
      currentBrightness.write(QByteArray::number(int(std::round(level * maxLevel))));
      return true;
    }
  }
  return false;
}

float SysfsLight::maxBacklightLevel() const
{
  return maxLevel;
}

QString SysfsLight::deviceName() const
{
  return devicePath.section('/', -1);
}

float SysfsLight::getAdjustedLevel(float step) const
{
  QFile currentBrightness(devicePath + "/brightness");
  if (!currentBrightness.open(QIODevice::ReadOnly)) {
    return 1.0;
  }
  int rawLevel = currentBrightness.readAll().trimmed().toInt();
  int intStep = step * maxLevel;
  if (intStep == 0) {
    intStep = step > 0 ? 1 : -1;
  }
  return (rawLevel + intStep) / maxLevel;
}
