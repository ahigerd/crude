#ifndef CRUDE_SYSFSLIGHTPLUGIN_H
#define CRUDE_SYSFSLIGHTPLUGIN_H

#include "../lightplugin.h"
#include "sysfslight.h"

class SysfsLightPlugin : public QObject, public LightPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID LightPlugin_iid)
  Q_INTERFACES(LightPlugin)

public:
  SysfsLightPlugin();
  ~SysfsLightPlugin();

  virtual void setEnabled(bool on);
  virtual QString pluginName() const;
  virtual bool available() const;

  virtual float backlightLevel() const;
  virtual void setBacklightLevel(float level);
  virtual void increaseBacklight(float step);
  virtual void decreaseBacklight(float step);

  virtual void displaysUpdated();

signals:
  void backlightAvailableChanged(bool available);
  void backlightChanged(float level);

private:
  SysfsLight light;
};

#endif
