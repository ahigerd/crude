#include "consolekitpowerplugin.h"

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusMessage>
#include <QMetaEnum>

#define CK(v) \
  QDBusInterface v( \
      "org.freedesktop.ConsoleKit", \
      "/org/freedesktop/ConsoleKit/Manager", \
      "org.freedesktop.ConsoleKit.Manager", \
      QDBusConnection::systemBus() \
  )

ConsoleKitPowerPlugin::ConsoleKitPowerPlugin() : QObject(nullptr)
{
  CK(ck);
  ckAvailable = ck.isValid();
  if (ckAvailable) {
    QDBusMessage msg = ck.call("CanPowerOff");
    usePowerOff = (msg.type() == QDBusMessage::ReplyMessage);
  }
}

ConsoleKitPowerPlugin::~ConsoleKitPowerPlugin()
{
  // no-op
}

void ConsoleKitPowerPlugin::setEnabled(bool)
{
  // no-op
}

QString ConsoleKitPowerPlugin::pluginName() const
{
  return "ConsoleKit";
}

bool ConsoleKitPowerPlugin::available() const
{
  return ckAvailable;
}

bool ConsoleKitPowerPlugin::canPerform(PowerPlugin::Action action) const
{
  if (action == SuspendThenHibernate) {
    return false;
  }
  CK(ck);
  QString method = action == Shutdown ? QString(usePowerOff ? "CanPowerOff" : "CanStop")
                                      : QString("Can") + QMetaEnum::fromType<Action>().valueToKey(action);
  QString msg = ck.call(method).arguments().at(0).toString().toLower();
  return msg == "1" || msg == "true" || msg == "yes";
}

void ConsoleKitPowerPlugin::perform(PowerPlugin::Action action)
{
  if (action == SuspendThenHibernate) {
    return;
  }
  CK(ck);
  QString method =
      action == Shutdown ? (usePowerOff ? "PowerOff" : "Stop") : QMetaEnum::fromType<Action>().valueToKey(action);
  ck.call(method, true);
}
