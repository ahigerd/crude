#ifndef CRUDE_UPOWERBATTERYPLUGIN_H
#define CRUDE_UPOWERBATTERYPLUGIN_H

#include "../batteryplugin.h"
class QTimer;

class UPowerBatteryPlugin : public QObject, public BatteryPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID BatteryPlugin_iid)
  Q_INTERFACES(BatteryPlugin)

public:
  UPowerBatteryPlugin();

  virtual ~UPowerBatteryPlugin() {}

  virtual void setEnabled(bool on);
  virtual QString pluginName() const;
  virtual bool available() const;

  virtual bool lidState() const;
  virtual bool isCharging() const;
  virtual bool isPluggedIn() const;
  virtual bool hasBattery() const;
  virtual int batteryLevel() const;
  virtual QTime timeToEmpty() const;
  virtual QTime timeToFull() const;

  // TODO: configurator UI

signals:
  void lidChanged(bool);
  void plugChanged(bool);
  void batteryLevelChanged(int percent);

private slots:
  void updatePowerState();

private:
  bool upAvailable;
  bool hasLid;
  bool isConnected;
  bool lastLid;
  bool lastPlug;
  bool batteryPresent;
  int lastLevel;
};

#endif
