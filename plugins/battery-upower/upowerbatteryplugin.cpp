#include "upowerbatteryplugin.h"

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusMessage>
#include <QTimer>
#include <QtDebug>

#define UP(v) \
  QDBusInterface v( \
      "org.freedesktop.UPower", "/org/freedesktop/UPower", "org.freedesktop.UPower", QDBusConnection::systemBus() \
  )
#define UPDD(v) \
  QDBusInterface v( \
      "org.freedesktop.UPower", \
      "/org/freedesktop/UPower/devices/DisplayDevice", \
      "org.freedesktop.UPower.Device", \
      QDBusConnection::systemBus() \
  )

UPowerBatteryPlugin::UPowerBatteryPlugin() : QObject(nullptr), hasLid(false), isConnected(false)
{
  UP(up);
  upAvailable = up.isValid();
  if (upAvailable) {
    hasLid = up.property("LidIsPresent").toBool();
  }
}

void UPowerBatteryPlugin::setEnabled(bool on)
{
  if (on && !isConnected) {
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower",
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged",
        this,
        SLOT(updatePowerState())
    );
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower/devices/DisplayDevice",
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged",
        this,
        SLOT(updatePowerState())
    );
    lastLid = lidState();
    lastPlug = isPluggedIn();
    updatePowerState();
  } else if (!on && isConnected) {
    QDBusConnection::systemBus().disconnect(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower",
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged",
        this,
        SLOT(updatePowerState())
    );
    QDBusConnection::systemBus().disconnect(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower/devices/DisplayDevice",
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged",
        this,
        SLOT(updatePowerState())
    );
  }
  isConnected = on;
}

QString UPowerBatteryPlugin::pluginName() const
{
  return "UPower";
}

bool UPowerBatteryPlugin::available() const
{
  return upAvailable;
}

void UPowerBatteryPlugin::updatePowerState()
{
  int lid = lidState();
  if (lid != lastLid) {
    lastLid = lid;
    emit lidChanged(lid);
  }
  bool plug = isPluggedIn();
  if (plug != lastPlug) {
    lastPlug = plug;
    emit plugChanged(plug);
  }
  UPDD(updd);
  updd.call("Refresh");
  // TODO: this is probably wrong, find a non-laptop to test this with
  batteryPresent = updd.property("IsPresent").toBool();
  if (batteryPresent) {
    int level = updd.property("Percentage").toInt();
    if (level != lastLevel) {
      lastLevel = level;
      emit batteryLevelChanged(lastLevel);
    }
  } else {
    if (lastLevel != -1) {
      lastLevel = -1;
      emit batteryLevelChanged(lastLevel);
    }
  }
}

bool UPowerBatteryPlugin::lidState() const
{
  if (!hasLid) {
    return true;
  }
  UP(up);
  return !up.property("LidIsClosed").toBool();
}

bool UPowerBatteryPlugin::isCharging() const
{
  if (!batteryPresent) {
    return false;
  }
  UPDD(updd);
  return updd.property("State").toInt() == 1;
}

bool UPowerBatteryPlugin::isPluggedIn() const
{
  if (!batteryPresent) {
    return true;
  }
  UP(up);
  return !up.property("OnBattery").toBool();
}

bool UPowerBatteryPlugin::hasBattery() const
{
  return batteryPresent;
}

int UPowerBatteryPlugin::batteryLevel() const
{
  if (!batteryPresent) {
    return 0;
  }
  return lastLevel;
}

static QTime secondsToTime(int seconds)
{
  int hours = seconds / 3600;
  seconds -= hours * 3600;
  int minutes = seconds / 60;
  seconds -= minutes * 60;
  return QTime(hours, minutes, seconds);
}

QTime UPowerBatteryPlugin::timeToEmpty() const
{
  if (isPluggedIn()) {
    return QTime();
  }
  UPDD(updd);
  return secondsToTime(updd.property("TimeToEmpty").toInt());
}

QTime UPowerBatteryPlugin::timeToFull() const
{
  if (!isPluggedIn()) {
    return QTime();
  }
  UPDD(updd);
  return secondsToTime(updd.property("TimeToFull").toInt());
}
