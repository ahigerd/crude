#ifndef CRUDE_LIGHTPLUGIN_H
#define CRUDE_LIGHTPLUGIN_H

#include <QObject>
#include <QString>
#include <QtPlugin>

#define LightPlugin_iid "com.alkahest.crude.LightPlugin/1.0"

class LightPlugin
{
  Q_GADGET

public:
  virtual ~LightPlugin() {}

  virtual QString pluginName() const = 0;
  virtual bool available() const = 0;
  virtual void setEnabled(bool on) = 0;

  virtual bool backlightAvailable() const { return true; }
  virtual float backlightLevel() const = 0;
  virtual void setBacklightLevel(float level) = 0;
  virtual void increaseBacklight(float step) { setBacklightLevel(backlightLevel() + step); }
  virtual void decreaseBacklight(float step) { setBacklightLevel(backlightLevel() - step); }

  virtual void displaysUpdated() = 0;

  // TODO: configurator UI
#if false
  // Signals are part of the API but not part of the interface class.
  // QMetaObject takes care of it but the interface isn't a QObject.
signals:
  void backlightAvailableChanged(bool available);
  void backlightChanged(float level);
#endif
};

Q_DECLARE_INTERFACE(LightPlugin, LightPlugin_iid)

#endif
