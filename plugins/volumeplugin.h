#ifndef CRUDE_VOLUMEPLUGIN_H
#define CRUDE_VOLUMEPLUGIN_H

#include <QObject>
#include <QString>
#include <QtPlugin>

#define VolumePlugin_iid "com.alkahest.crude.VolumePlugin/1.0"

class VolumePlugin
{
  Q_GADGET

public:
  virtual ~VolumePlugin() {}

  virtual QString pluginName() const = 0;
  virtual bool available() const = 0;
  virtual void setEnabled(bool on) = 0;

  virtual QString mixerCommand() const = 0;

  virtual float volumeLevel() const = 0;
  virtual void setVolumeLevel(float level) = 0;
  virtual void increaseVolume(float step) { setVolumeLevel(volumeLevel() + step); }
  virtual void decreaseVolume(float step) { setVolumeLevel(volumeLevel() - step); }

  virtual bool isMuted() const = 0;
  virtual void setMuted(bool mute) = 0;

  // TODO: configurator UI
#if false
  // Signals are part of the API but not part of the interface class.
  // QMetaObject takes care of it but the interface isn't a QObject.
signals:
  void volumeError();
  void muteChanged(bool muted);
  void volumeChanged(float level);
#endif
};

Q_DECLARE_INTERFACE(VolumePlugin, VolumePlugin_iid)

#endif
