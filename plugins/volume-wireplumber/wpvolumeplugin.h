#ifndef CRUDE_WPVOLUMEPLUGIN_H
#define CRUDE_WPVOLUMEPLUGIN_H

#include "../volumeplugin.h"
#include <QProcess>
#include <QTimer>

class WpVolumePlugin : public QObject, public VolumePlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID VolumePlugin_iid)
  Q_INTERFACES(VolumePlugin)

public:
  WpVolumePlugin();
  virtual ~WpVolumePlugin();

  virtual QString pluginName() const;
  virtual bool available() const;
  virtual void setEnabled(bool on);

  virtual QString mixerCommand() const;

  virtual float volumeLevel() const;
  virtual void setVolumeLevel(float level);

  virtual bool isMuted() const;
  virtual void setMuted(bool mute);

signals:
  void volumeError();
  void muteChanged(bool muted);
  void volumeChanged(float level);

private slots:
  void monitorReadyRead();
  void monitorError(QProcess::ProcessError);
  void monitorFinished(int, QProcess::ExitStatus);
  void startMonitor();

private:
  void modifyVolume(const QVariant& adjust);

  QTimer restartThrottle;
  QProcess* monitor;
  QString wpctl;
  QString wpexec;
  QString luaScript;
  QString mixer;
  float lastLevel;
  bool lastMute;
  bool hasTools;
  qint64 lastRestart;
  int restartCount;
};

Q_DECLARE_INTERFACE(WpVolumePlugin, VolumePlugin_iid)

#endif
