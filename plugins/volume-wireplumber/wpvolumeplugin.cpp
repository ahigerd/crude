#include "wpvolumeplugin.h"
#include <QFileInfo>
#include <QStandardPaths>
#include <QDir>
#include <QFile>

static QString WP_MONITOR_COMMAND("stdbuf -oL %1 %2");
static QString WP_SET_COMMAND("%1 set-volume @DEFAULT_AUDIO_SINK@ %2");
static QString WP_MUTE_COMMAND("%1 set-mute @DEFAULT_AUDIO_SINK@ %2");

static const char* monitorScript = R"(
Core.require_api("default-nodes", "mixer", function(...)
  local default_nodes, mixer = ...

  mixer.scale = "cubic"

  function print_volume()
    local id = default_nodes:call("get-default-node", "Audio/Sink")
    local volume = mixer:call("get-volume", id)
    local mute = "on"
    if volume.mute then
      mute = "off"
    end
    print(string.format("%s:%.3f", mute, volume.volume))
  end

  default_nodes:connect("changed", print_volume)
  mixer:connect("changed", print_volume)
  print_volume()
end)
)";

WpVolumePlugin::WpVolumePlugin()
: QObject(nullptr), monitor(nullptr), lastLevel(0), lastRestart(0), restartCount(0)
{
  wpctl = QStandardPaths::findExecutable("wpctl");
  wpexec = QStandardPaths::findExecutable("wpexec");
  hasTools = !wpctl.isEmpty() && !wpexec.isEmpty();

  QDir dataDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
  dataDir.mkpath(".");
  luaScript = dataDir.absoluteFilePath("volume-wireplumber.lua");
  QFile script(luaScript);
  if (script.open(QIODevice::WriteOnly)) {
    script.write(monitorScript);
    script.close();
  } else {
    luaScript.clear();
  }

  mixer = QStandardPaths::findExecutable("pavucontrol-qt");
  if (mixer.isEmpty()) {
    mixer = QStandardPaths::findExecutable("pavucontrol");
  }

  restartThrottle.setSingleShot(true);
  restartThrottle.setInterval(1000);
  QTimer::connect(&restartThrottle, SIGNAL(timeout()), this, SLOT(startMonitor()));
}

WpVolumePlugin::~WpVolumePlugin()
{
}

QString WpVolumePlugin::pluginName() const
{
  return "WirePlumber";
}

bool WpVolumePlugin::available() const
{
  return hasTools && !luaScript.isEmpty();
}

void WpVolumePlugin::setEnabled(bool on)
{
  if (on) {
    if (!monitor) {
      monitor = new QProcess(this);
      QObject::connect(monitor, SIGNAL(readyRead()), this, SLOT(monitorReadyRead()));
      QObject::connect(
          monitor, SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(monitorError(QProcess::ProcessError))
      );
      QObject::connect(
          monitor, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(monitorFinished(int, QProcess::ExitStatus))
      );

      monitor->setProcessChannelMode(QProcess::MergedChannels);
    }
    startMonitor();
  } else {
    monitor->terminate();
  }
}

void WpVolumePlugin::startMonitor()
{
  if (!monitor || monitor->state() != QProcess::NotRunning) {
    return;
  }
  qint64 now = QDateTime::currentMSecsSinceEpoch();
  if (now < lastRestart + 15000) {
    restartCount++;
    if (restartCount > 3) {
      qWarning("wpexec exited too fast, disabling monitor");
      lastLevel = -2;
      emit volumeError();
      return;
    }
  } else {
    restartCount = 0;
  }
  lastRestart = now;
  monitor->start(WP_MONITOR_COMMAND.arg(wpexec).arg(luaScript), QIODevice::ReadWrite);
}

void WpVolumePlugin::monitorReadyRead()
{
  QByteArrayList data = monitor->readAll().trimmed().split('\n');
  if (data.isEmpty()) {
    // spurious read
    return;
  }
  QByteArrayList parts = data.last().split(':');
  if (parts.length() != 2) {
    qDebug() << "unparseable message" << data;
    return;
  }
  bool mute = parts[0] == "off";
  bool ok = false;
  float level = parts[1].toFloat(&ok);
  if (!ok) {
    qDebug() << "unparseable message" << data;
    return;
  }
  if (mute != lastMute) {
    lastMute = mute;
    emit muteChanged(mute);
  }
  if (level != lastLevel) {
    lastLevel = level;
    emit volumeChanged(level);
  }
}

void WpVolumePlugin::monitorFinished(int code, QProcess::ExitStatus status)
{
  qDebug() << "monitor finished" << code << status;
  restartThrottle.start();
}

void WpVolumePlugin::monitorError(QProcess::ProcessError code)
{
  qDebug() << "monitorError" << code;
  if (code == QProcess::FailedToStart) {
    lastLevel = -2;
    emit volumeError();
  } else {
    restartThrottle.start();
  }
}

float WpVolumePlugin::volumeLevel() const
{
  return lastLevel;
}

void WpVolumePlugin::setVolumeLevel(float level)
{
  if (level < 0) {
    level = 0;
  }
  QProcess::startDetached(WP_SET_COMMAND.arg(wpctl).arg(level));
}

bool WpVolumePlugin::isMuted() const
{
  return lastMute;
}

void WpVolumePlugin::setMuted(bool mute)
{
  QProcess::startDetached(WP_MUTE_COMMAND.arg(wpctl).arg(mute ? 1 : 0));
}

QString WpVolumePlugin::mixerCommand() const
{
  return mixer;
}
