#ifndef CRUDE_ACPIDBATTERYPLUGIN_H
#define CRUDE_ACPIDBATTERYPLUGIN_H

#include "../batteryplugin.h"
class QTimer;
class QSocketNotifier;

class AcpidBatteryPlugin : public QObject, public BatteryPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID BatteryPlugin_iid)
  Q_INTERFACES(BatteryPlugin)

public:
  AcpidBatteryPlugin();

  virtual ~AcpidBatteryPlugin() {}

  virtual void setEnabled(bool on);
  virtual QString pluginName() const;
  virtual bool available() const;

  virtual bool lidState() const;
  virtual bool isCharging() const;
  virtual bool isPluggedIn() const;
  virtual bool hasBattery() const;
  virtual int batteryLevel() const;
  virtual QTime timeToEmpty() const;
  virtual QTime timeToFull() const;

  // TODO: configurator UI

signals:
  void lidChanged(bool);
  void plugChanged(bool);
  void batteryLevelChanged(int percent);

private slots:
  void readyRead();
  void updatePowerState();
  void connectToSocket();

private:
  QByteArray socketBuffer;
  QTimer* pollTimer;
  int socketFD;
  QSocketNotifier* socket;
  qint64 lastError;
  bool lastOpen : 1, lastAC : 1, lastCharging : 1, batteryPresent : 1;
  unsigned int errorCount : 4;
  int lastChargePercent : 8;
  float hoursToEmpty, hoursToFull;

  void setPluggedIn(bool isAC, bool doUpdate = true);
};

#endif
