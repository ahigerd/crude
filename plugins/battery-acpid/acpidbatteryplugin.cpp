#include "acpidbatteryplugin.h"

#include <QDateTime>
#include <QFile>
#include <QSocketNotifier>
#include <QTimer>
#include <QtDebug>

#include <cstring>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

// TODO: Do any systems ever use a different path for this in practice?
// Answer; Yes. Some use /run/acpid.socket, and some have one symlinked to the other.
static const char* acpiSocket1 = "/var/run/acpid.socket";
static const char* acpiSocket2 = "/run/acpid.socket";
static const char* acOnline = "/sys/class/power_supply/AC/online";
static const char* battInfo = "/sys/class/power_supply/BAT0/uevent";

AcpidBatteryPlugin::AcpidBatteryPlugin()
: QObject(nullptr),
  pollTimer(nullptr),
  socketFD(0),
  socket(nullptr),
  lastError(0),
  lastOpen(true),
  lastAC(true),
  lastCharging(false),
  batteryPresent(false),
  errorCount(0)
{}

void AcpidBatteryPlugin::setEnabled(bool on)
{
  if (on && !pollTimer) {
    pollTimer = new QTimer(this);
    QObject::connect(pollTimer, SIGNAL(timeout()), this, SLOT(updatePowerState()));
    pollTimer->setTimerType(Qt::VeryCoarseTimer);
    pollTimer->start(15000);

    updatePowerState();
    connectToSocket();
  } else if (!on && pollTimer) {
    pollTimer->deleteLater();
    pollTimer = nullptr;
    if (socket) {
      socket->deleteLater();
      socket = nullptr;
    }
    if (socketFD) {
      ::close(socketFD);
      socketFD = 0;
    }
  }
}

QString AcpidBatteryPlugin::pluginName() const
{
  return "acpid";
}

bool AcpidBatteryPlugin::available() const
{
  return QFile::exists(acpiSocket1) || QFile::exists(acpiSocket2);
}

void AcpidBatteryPlugin::connectToSocket()
{
  if (socketFD) {
    ::close(socketFD);
    socketFD = 0;
  }
  if (socket) {
    delete socket;
    socket = nullptr;
  }
  int result = -1;
  int fd = ::socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
  sockaddr_un address;
  std::memset(&address, 0, sizeof(sockaddr_un));
  address.sun_family = AF_UNIX;
  if (QFile::exists(acpiSocket1)) {
    std::strncpy(address.sun_path, acpiSocket1, sizeof(address.sun_path) - 1);
    result = ::connect(fd, (sockaddr*)&address, sizeof(address));
  }
  if (result < 0 && QFile::exists(acpiSocket2)) {
    std::strncpy(address.sun_path, acpiSocket2, sizeof(address.sun_path) - 1);
    result = ::connect(fd, (sockaddr*)&address, sizeof(address));
  }
  if (result < 0) {
    qDebug() << "Error connecting to acpid." << (errno);
    // Unavailable. Lid/battery state changes won't be available.
    // TODO: can at least poll for battery state
    socket = nullptr;
    return;
  }

  // QSocketNotifier is used instead of QAbstractSocket to avoid needing to link to QtNetwork.
  // If QtNetwork ever becomes a dependency for another reason, go ahead and switch it.
  socketFD = fd;
  socket = new QSocketNotifier(socketFD, QSocketNotifier::Read, this);
  QObject::connect(socket, SIGNAL(activated(int)), this, SLOT(readyRead()));
  socketBuffer.clear();
}

static char readBuffer[100];

void AcpidBatteryPlugin::readyRead()
{
  socket->setEnabled(false);
  int len;
  bool failed = true;
  while ((len = ::read(socketFD, readBuffer, 100)) > 0) {
    failed = false;
    socketBuffer += QByteArray(readBuffer, len);
    int linePos;
    while ((linePos = socketBuffer.indexOf("\n")) > -1) {
      QString line = QString::fromUtf8(socketBuffer.left(linePos));
      socketBuffer = socketBuffer.mid(linePos + 1);
      if (line.contains(" LID ")) {
        if (line.endsWith("close")) {
          lastOpen = false;
        } else if (line.endsWith("open")) {
          lastOpen = true;
        }
        emit lidChanged(lastOpen);
      } else if (line.startsWith("ac_adapter")) {
        setPluggedIn(line.endsWith("1"));
      }
    }
  }
  if (failed) {
    qint64 ts = QDateTime::currentMSecsSinceEpoch();
    if (lastError < ts - 1000) {
      errorCount = 0;
    }
    lastError = ts;
    errorCount++;
  }
  if ((len < 0 && errno != EAGAIN && errno != EWOULDBLOCK) || (failed && errorCount > 8)) {
    qDebug() << "Error communicating with acpid. Reconnecting...";
    QTimer::singleShot(5000, this, SLOT(connectToSocket()));
  } else {
    socket->setEnabled(true);
  }
}

void AcpidBatteryPlugin::updatePowerState()
{
  QFile ac(acOnline);
  if (ac.open(QIODevice::ReadOnly)) {
    setPluggedIn(ac.readAll().contains("1"), false);
  }

  if (!lastAC && !lastOpen) {
    // No point in tracking battery status if the user isn't going to see it.
    // TODO: Unless they have two monitors...
    return;
  }

  QFile batt(battInfo);
  int oldCharge = lastChargePercent;
  bool oldCharging = lastCharging;
  float fullChargeLevel = 0, chargeLevel = 0, chargeRate = 0;
  if (batt.open(QIODevice::ReadOnly)) {
    QStringList lines = QString::fromUtf8(batt.readAll()).split("\n");
    float chargeNow = -1, energyNow = -1, powerNow = -1, currentNow = -1, energyFull = -1, chargeFull = -1;
    for (const QString& line : lines) {
      QStringList sections = line.trimmed().replace("POWER_SUPPLY_", "").split("=");
      if (sections[0] == "PRESENT") {
        batteryPresent = (sections[1] == "1");
        if (!batteryPresent) {
          // no point in continuing if there's no battery
          lastChargePercent = -1;
          break;
        }
      } else if (sections[0] == "STATUS") {
        if (sections[1] == "Charging") {
          lastCharging = true;
        } else {
          lastCharging = false;
        }
      } else if (sections[0] == "CAPACITY") {
        lastChargePercent = sections[1].toInt();
      } else if (sections[0] == "CHARGE_NOW") {
        chargeNow = sections[1].toInt() / 1000000.0;
      } else if (sections[0] == "CHARGE_FULL") {
        chargeFull = sections[1].toInt() / 1000000.0;
      } else if (sections[0] == "ENERGY_NOW") {
        energyNow = sections[1].toInt() / 1000000.0;
      } else if (sections[0] == "ENERGY_FULL") {
        energyFull = sections[1].toInt() / 1000000.0;
      } else if (sections[0] == "POWER_NOW") {
        powerNow = sections[1].toInt() / 1000000.0;
      } else if (sections[0] == "CURRENT_NOW") {
        currentNow = sections[1].toInt() / 1000000.0;
      }
    }
    // sysfs battery status has inconsistent power units based on what's provided by the hardware.
    // This can all be processed, but it requires looking at which values are exposed to determine units.
    // https://linux-acpi.vger.kernel.narkive.com/q20kKdoq/battery-information-in-sysfs-for-userspace-tools
    if (energyNow >= 0) {
      // If ENERGY_NOW is present, then ENERGY_NOW is uWh, POWER_NOW is uW, and CURRENT_NOW is uW.
      chargeLevel = energyNow;
      fullChargeLevel = energyFull;
      if (powerNow >= 0) {
        // Power is more correct if we have it, so prefer it if it's available.
        chargeRate = powerNow;
      } else {
        // Current is as good as we're going to get otherwise.
        chargeRate = currentNow;
      }
    } else if (chargeNow >= 0) {
      // If ENERGY_NOW is not present, but CHARGE_NOW is, then CHARGE_NOW is uAh and CURRENT_NOW is uA.
      // POWER_NOW cannot be present if ENERGY_NOW is not.
      chargeLevel = chargeNow;
      fullChargeLevel = chargeFull;
      chargeRate = currentNow;
    } else {
      // If neither of them have usable values, then suppress time to full/empty reporting.
      chargeLevel = -1;
      fullChargeLevel = -1;
      chargeRate = -1;
    }
  } else {
    batteryPresent = false;
  }
  bool charging = isCharging();
  if (!charging && chargeLevel > 0 && chargeRate > 0) {
    float rawTime = chargeLevel / chargeRate;
    float smoothedTime = rawTime;
    if (hoursToEmpty != 0) {
      smoothedTime = (rawTime + hoursToEmpty) / 2;
    }
    hoursToEmpty = smoothedTime;
    hoursToFull = 0;
  } else if (charging && chargeLevel > 0 && chargeRate > 0 && fullChargeLevel > 0) {
    float rawTime = (fullChargeLevel - chargeLevel) / chargeRate;
    float smoothedTime = rawTime;
    if (hoursToFull != 0) {
      smoothedTime = (rawTime + hoursToFull) / 2;
    }
    hoursToEmpty = 0;
    hoursToFull = smoothedTime;
  } else {
    hoursToEmpty = 0;
    hoursToFull = 0;
  }

  if (lastChargePercent != oldCharge || lastCharging != oldCharging) {
    emit batteryLevelChanged(lastChargePercent);
  }
}

bool AcpidBatteryPlugin::lidState() const
{
  return lastOpen;
}

bool AcpidBatteryPlugin::isCharging() const
{
  return batteryPresent && lastCharging && lastAC;
}

bool AcpidBatteryPlugin::isPluggedIn() const
{
  return lastAC;
}

bool AcpidBatteryPlugin::hasBattery() const
{
  return batteryPresent;
}

int AcpidBatteryPlugin::batteryLevel() const
{
  if (!batteryPresent) {
    return 0;
  }
  return lastChargePercent;
}

QTime AcpidBatteryPlugin::timeToEmpty() const
{
  if (!batteryPresent || lastAC || hoursToEmpty <= 0) {
    return QTime();
  }
  float smoothedTime = hoursToEmpty;
  int hours = smoothedTime;
  smoothedTime = (smoothedTime - hours) * 60;
  int minutes = smoothedTime;
  smoothedTime = (smoothedTime - minutes) * 60;
  int seconds = smoothedTime;
  return QTime(hours, minutes, seconds);
}

QTime AcpidBatteryPlugin::timeToFull() const
{
  if (!batteryPresent || !lastAC || !lastCharging || hoursToFull <= 0) {
    return QTime();
  }
  float smoothedTime = hoursToFull;
  int hours = smoothedTime;
  smoothedTime = (smoothedTime - hours) * 60;
  int minutes = smoothedTime;
  smoothedTime = (smoothedTime - minutes) * 60;
  int seconds = smoothedTime;
  return QTime(hours, minutes, seconds);
}

void AcpidBatteryPlugin::setPluggedIn(bool isAC, bool doUpdate)
{
  if (isAC != lastAC) {
    lastAC = isAC;
    emit plugChanged(isAC);
    if (doUpdate) {
      updatePowerState();
    }
  }
}
