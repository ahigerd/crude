#ifndef CRUDE_BATTERYPLUGIN_H
#define CRUDE_BATTERYPLUGIN_H

#include <QObject>
#include <QString>
#include <QTime>
#include <QtPlugin>

#define BatteryPlugin_iid "com.alkahest.crude.BatteryPlugin/1.0"

class BatteryPlugin
{
  Q_GADGET

public:
  virtual ~BatteryPlugin() {}

  virtual QString pluginName() const = 0;
  virtual bool available() const = 0;
  virtual void setEnabled(bool on) = 0;

  virtual bool lidState() const = 0;
  virtual bool isCharging() const = 0;
  virtual bool isPluggedIn() const = 0;
  virtual bool hasBattery() const = 0;
  virtual int batteryLevel() const = 0;
  virtual QTime timeToEmpty() const = 0;
  virtual QTime timeToFull() const = 0;

  // TODO: configurator UI

#if false
  // Signals are part of the API but not part of the interface class.
  // QMetaObject takes care of it but the interface isn't a QObject.
signals:
  void lidChanged(bool);
  void plugChanged(bool);
  void batteryLevelChanged(int percent);
#endif
};

Q_DECLARE_INTERFACE(BatteryPlugin, BatteryPlugin_iid)

#endif
