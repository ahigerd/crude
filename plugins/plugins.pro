TEMPLATE = subdirs
SUBDIRS = power-command battery-acpid light-sysfs volume-alsa volume-wireplumber

qtHaveModule(dbus) {
  SUBDIRS += power-systemd power-consolekit
  SUBDIRS += battery-upower
  SUBDIRS += light-logind # light-gnome light-kde
}
