#include "logindlightplugin.h"

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusMessage>

#include <cmath>

#define LOGIND(v) \
  QDBusInterface v( \
      "org.freedesktop.login1", "/org/freedesktop/login1/session/auto", "org.freedesktop.login1.Session", QDBusConnection::systemBus() \
  )

LogindLightPlugin::LogindLightPlugin()
: QObject(nullptr), logindAvailable(false), light(false)
{
  if (light.isAvailable()) {
    LOGIND(logind);
    logindAvailable = logind.isValid();
  }
}

LogindLightPlugin::~LogindLightPlugin()
{
  // no-op
}

void LogindLightPlugin::setEnabled(bool)
{
  // no-op
}

QString LogindLightPlugin::pluginName() const
{
  return "logind";
}

bool LogindLightPlugin::available() const
{
  return logindAvailable && light.isAvailable();
}

float LogindLightPlugin::backlightLevel() const
{
  return light.backlightLevel();
}

void LogindLightPlugin::setBacklightLevel(float level)
{
  LOGIND(logind);
  uint newLevel = std::round(level * light.maxBacklightLevel());
  logind.call("SetBrightness", "backlight", light.deviceName(), newLevel);
  emit backlightChanged(backlightLevel());
}

void LogindLightPlugin::increaseBacklight(float step)
{
  setBacklightLevel(light.getAdjustedLevel(step));
}

void LogindLightPlugin::decreaseBacklight(float step)
{
  setBacklightLevel(light.getAdjustedLevel(-step));
}

void LogindLightPlugin::displaysUpdated()
{
  // TODO: detect other backlights
}
