TARGET = light-logind
QT = core dbus

HEADERS += logindlightplugin.h   ../light-sysfs/sysfslight.h
SOURCES += logindlightplugin.cpp ../light-sysfs/sysfslight.cpp

include(../lightplugin.pri)
