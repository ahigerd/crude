#ifndef CRUDE_LOGINDLIGHTPLUGIN_H
#define CRUDE_LOGINDLIGHTPLUGIN_H

#include "../lightplugin.h"
#include "../light-sysfs/sysfslight.h"

class LogindLightPlugin : public QObject, public LightPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID LightPlugin_iid)
  Q_INTERFACES(LightPlugin)

public:
  LogindLightPlugin();
  ~LogindLightPlugin();

  virtual void setEnabled(bool on);
  virtual QString pluginName() const;
  virtual bool available() const;

  virtual float backlightLevel() const;
  virtual void setBacklightLevel(float level);
  virtual void increaseBacklight(float step);
  virtual void decreaseBacklight(float step);

  virtual void displaysUpdated();

signals:
  void backlightAvailableChanged(bool available);
  void backlightChanged(float level);

private:
  bool logindAvailable;
  SysfsLight light;
};

#endif
