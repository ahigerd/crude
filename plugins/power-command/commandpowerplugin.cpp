#include "commandpowerplugin.h"

#include <QMetaEnum>
#include <QProcess>
#include <QSettings>

CommandPowerPlugin::CommandPowerPlugin() : QObject(nullptr)
{
  QSettings user(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-commandpowerplugin");
  if (user.contains("configured")) {
    loadSettings(user);
  } else {
    QSettings system(QSettings::IniFormat, QSettings::SystemScope, "Alkahest", "crude-commandpowerplugin");
    loadSettings(system);
  }
}

CommandPowerPlugin::~CommandPowerPlugin()
{
  // no-op
}

void CommandPowerPlugin::setEnabled(bool)
{
  // no-op
}

QString CommandPowerPlugin::pluginName() const
{
  return "Custom Commands";
}

bool CommandPowerPlugin::available() const
{
  return !commands.isEmpty();
}

bool CommandPowerPlugin::canPerform(PowerPlugin::Action action) const
{
  return commands.contains(action);
}

void CommandPowerPlugin::perform(PowerPlugin::Action action)
{
  QString command = commands.value(action);
  if (!command.isEmpty()) {
    QProcess::execute(command);
  }
}

void CommandPowerPlugin::loadSettings(QSettings& settings)
{
  loadSetting(settings, Suspend);
  loadSetting(settings, Hibernate);
  loadSetting(settings, HybridSleep);
  loadSetting(settings, SuspendThenHibernate);
  loadSetting(settings, Reboot);
  loadSetting(settings, Shutdown);
}

void CommandPowerPlugin::loadSetting(QSettings& settings, PowerPlugin::Action action)
{
  QString name = QMetaEnum::fromType<Action>().valueToKey(action);
  if (settings.contains(name)) {
    commands[action] = settings.value(name).toString();
  }
}
