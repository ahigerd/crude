#ifndef CRUDE_COMMANDPOWERPLUGIN_H
#define CRUDE_COMMANDPOWERPLUGIN_H

#include "../powerplugin.h"

#include <QMap>
class QSettings;

class CommandPowerPlugin : public QObject, public PowerPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID PowerPlugin_iid)
  Q_INTERFACES(PowerPlugin)

public:
  CommandPowerPlugin();
  ~CommandPowerPlugin();

  virtual void setEnabled(bool on);
  virtual QString pluginName() const;
  virtual bool available() const;
  virtual bool canPerform(Action action) const;
  virtual void perform(Action action);

private:
  void loadSettings(QSettings& settings);
  void loadSetting(QSettings& settings, Action action);
  QMap<Action, QString> commands;
};

#endif
