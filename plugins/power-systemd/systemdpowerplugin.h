#ifndef CRUDE_CONSOLEKITPOWERPLUGIN_H
#define CRUDE_CONSOLEKITPOWERPLUGIN_H

#include "../powerplugin.h"

#include <QMap>

class SystemdPowerPlugin : public QObject, public PowerPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID PowerPlugin_iid)
  Q_INTERFACES(PowerPlugin)

public:
  SystemdPowerPlugin();
  ~SystemdPowerPlugin();

  virtual void setEnabled(bool on);
  virtual QString pluginName() const;
  virtual bool available() const;
  virtual bool canPerform(Action action) const;
  virtual void perform(Action action);

private:
  bool logindAvailable;
};

#endif
