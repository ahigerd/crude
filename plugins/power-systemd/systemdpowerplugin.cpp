#include "systemdpowerplugin.h"

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusMessage>
#include <QMetaEnum>

#define LOGIND(v) \
  QDBusInterface v( \
      "org.freedesktop.login1", "/org/freedesktop/login1", "org.freedesktop.login1.Manager", QDBusConnection::systemBus() \
  )

SystemdPowerPlugin::SystemdPowerPlugin() : QObject(nullptr)
{
  LOGIND(logind);
  logindAvailable = logind.isValid();
}

SystemdPowerPlugin::~SystemdPowerPlugin()
{
  // no-op
}

void SystemdPowerPlugin::setEnabled(bool)
{
  // no-op
}

QString SystemdPowerPlugin::pluginName() const
{
  return "systemd";
}

bool SystemdPowerPlugin::available() const
{
  return logindAvailable;
}

bool SystemdPowerPlugin::canPerform(PowerPlugin::Action action) const
{
  if (action == SuspendThenHibernate) {
    return false;
  }
  LOGIND(logind);
  QString method =
      action == Shutdown ? QString("CanPowerOff") : QString("Can") + QMetaEnum::fromType<Action>().valueToKey(action);
  QString msg = logind.call(method).arguments().at(0).toString().toLower();
  return msg == "1" || msg == "true" || msg == "yes";
}

void SystemdPowerPlugin::perform(PowerPlugin::Action action)
{
  if (action == SuspendThenHibernate) {
    return;
  }
  LOGIND(logind);
  QString method = action == Shutdown ? "PowerOff" : QMetaEnum::fromType<Action>().valueToKey(action);
  logind.call(method, true);
}
