TARGET = power-systemd
QT = core dbus

HEADERS += systemdpowerplugin.h
SOURCES += systemdpowerplugin.cpp

include(../powerplugin.pri)
