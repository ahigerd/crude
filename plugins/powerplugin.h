#ifndef CRUDE_POWERPLUGIN_H
#define CRUDE_POWERPLUGIN_H

#include <QObject>
#include <QString>
#include <QtPlugin>

#define PowerPlugin_iid "com.alkahest.crude.PowerPlugin/1.0"

class PowerPlugin
{
  Q_GADGET

public:
  virtual ~PowerPlugin() {}

  enum Action {
    Suspend,
    Hibernate,
    HybridSleep,
    SuspendThenHibernate,
    Reboot,
    Shutdown
  };
  Q_ENUM(Action)

  virtual QString pluginName() const = 0;
  virtual bool available() const = 0;
  virtual void setEnabled(bool on) = 0;

  virtual bool canPerform(Action action) const = 0;
  virtual void perform(Action action) = 0;

  // TODO: configurator UI
};

Q_DECLARE_INTERFACE(PowerPlugin, PowerPlugin_iid)

#endif
