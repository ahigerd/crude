CRUDE
=====
A compact, robust, useful desktop environment.

*Compact*: CRUDE doesn't try to do too much. You won't find an overengineered
mess here. It doesn't drag in a mountain of dependencies or load up a bunch of
daemons. It doesn't provide reimplementations of every little thing that you
might want.

*Robust*: CRUDE does try to provide what you need. Everyone needs a way to
launch applications and manage their running programs. Everyone needs to do
something with their system tray icons, whether it's interacting with them or
hiding them.

*Useful*: CRUDE tries to make sure everything you want to do is easy to do the
way you want to do it. This means it's configurable, and it means that both the
keyboard and the mouse can be used for everything -- if you don't want to use a
mouse, you don't have to.

Configuration
-------------
See the documentation for the individual components for configuration:

* [`crude-panel`](doc/crude-panel.md)

Notifications
-------------
CRUDE does not come with a built-in notification daemon for two reasons, both
of which boil down to enabling user choice.

First, there are a number of fine projects that provide desktop notification
functionality, and CRUDE opts to leave the choice up to the user, unlike some
other desktop environments that provide built-in notification services that
cannot be overridden.

Second, the system notification item specification uses D-BUS as its underlying
transport. This is a good choice from an architectural perspective that enables
the aforementioned selection of projects to exist. This frees CRUDE from having
to bring in additional dependencies of its own in order to support it,
especially if you don't need it.

CRUDE recommends [dunst](https://dunst-project.org/) as a lightweight
notification daemon with minimal dependencies. Some other noteworthy options
that work across desktop environments include:

* [GNOME Notification Daemon](https://github.com/GNOME/notification-daemon)
* [Deadd Notification Center](https://github.com/phuhl/linux_notification_center)
* [MATE Notification Daemon](https://github.com/mate-desktop/mate-notification-daemon/)
* [Notify OSD](https://launchpad.net/notify-osd)
* [statnot](https://github.com/halhen/statnot)
* [twmn](https://github.com/sboli/twmn)

Dependencies
------------
CRUDE requires the Qt5 base libraries, the Qt5 X11 extras library, and the xcb
support libraries.

To run CRUDE, the following packages are required:

* Debian, Ubuntu, and other Debian-derived distributions:
    * `libqt5core5a`
    * `libqt5gui5`
    * `libqt5widgets5`
    * `libqt5x11extras5`
    * Optional: `libqt5dbus5`
    * Optional: `libqt5svg5`
* Gentoo:
    * `qtcore`
    * `qtwidgets`
    * `qtx11extras`
    * `xkbcommon`
    * Optional: `qtdbus`
    * Optional: `qtsvg`
* Arch:
    * `qt5-base`
    * `qt5-x11extras`

In addition, the following packages are required to build CRUDE on Debian and
related distributions:

* `qtbase5-dev`
* `qtbase5-dev-tools`
* `libqt5x11extras5-dev`
* `libxcb1-dev`
* `libxcb-composite0-dev`
* `libxcb-damage0-dev`
* `libxcb-dpms0-dev`
* `libxcb-ewmh-dev`
* `libxcb-icccm4-dev`
* `libxcb-keysyms1-dev`
* `libxcb-randr0-dev`
* `libxcb-render0-dev`
* `libxcb-shape0-dev`
* `libxcb-xfixes0-dev`
* `libxcb-xkb-dev`
* `libxkbcommon-dev`
* `libsm-dev`
* `libice-dev`

Optionally, to enable support for power management, battery monitoring,
screen savers, and media control, CRUDE supports the following tools:

* Power management:
    * systemd (requires D-BUS)
    * ConsoleKit (requires D-BUS)
    * custom commands
* Battery monitoring:
    * acpid
    * upower (requires D-BUS)
* Screen saver
    * `xscreensaver`
    * `xautolock`
    * `xss-lock`
    * `slock`
    * `xlock` / `xlockmore`
    * `xsecurelock`
* Media control
    * ALSA (requires `amixer` and `alsactl`, provided by `alsa-tools`)
    * (pending) PulseAudio

Building
--------

Steps to install:

* `qmake -r`
    * This may need to be `qmake -qt=qt5 -r` on Debian-derived distributions.
* `make`

License
-------
CRUDE is provided under the same open-source licensing terms as Qt.

CRUDE is copyright © 2019-2023 Adam Higerd.

This program is free software: you can redistribute it and/or modify it under
the terms of version 3 of the GNU General Public License as published by the
Free Software Foundation.  Alternatively, this program may be used under the
terms of the GNU General Public License version 2.0 or (at your option) the
GNU General Public license version 3 or any later version approved by the KDE
Free Qt Foundation.

These licenses are as published by the Free Software foundation and appear in
the files [LICENSE.LGPL3](LICENSE.LGPL3), [LICENSE.GPL2](LICENSE.GPL2), and
[LICENSE.GPL3](LICENSE.GPL3) included in the packaging of this program.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the licenses listed above for more details.

You should have received a copy of the GNU Lesser General Public License
version 3, the GNU General Public License version 2.0, and the GNU General
Public License version 3 along with this program.  If not, see
[https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).

Some parts of CRUDE may be available to be used under a Qt commercial license
agreement. If you wish to license a portion of this software for commercial
use, contact the author for details.
