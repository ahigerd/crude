TEMPLATE = subdirs
SUBDIRS = 3rdparty libcrude libcrudecomp plugins session panel wizard config trayadaptor zoom shot test
libcrude.depends = 3rdparty
session.depends = libcrude
plugins.depends = libcrude
panel.depends = libcrude
wizard.depends = libcrude
config.depends = libcrude
trayadaptor.depends = libcrude
zoom.depends = libcrude libcrudecomp
shot.depends = libcrude libcrudecomp
test.depends = libcrude

include(crude-common.pri)

screensaverini.path = $${INSTALL_PLUGINS}
screensaverini.files = plugins/crude-screensavers.ini
INSTALLS += screensaverini

wpvolume.path = $${INSTALL_PLUGINS}
vpvolume.files = plugins/volume-wireplumber.lua
INSTALLS += wpvolume
