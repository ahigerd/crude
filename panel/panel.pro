TEMPLATE = app
TARGET = crude-panel
QT = core widgets x11extras
LIBS += -L.. -lcrude -lSM -lICE
include(../crude-common.pri)

HEADERS = panel.h   treecombobox.h   buttontreeview.h   screendock.h   configurator.h
SOURCES = panel.cpp treecombobox.cpp buttontreeview.cpp screendock.cpp configurator.cpp main.cpp

HEADERS += panelwidget.h   launchbutton.h   taskswitcher.h   clockwidget.h   systraywidget.h   shortcutwidget.h
SOURCES += panelwidget.cpp launchbutton.cpp taskswitcher.cpp clockwidget.cpp systraywidget.cpp shortcutwidget.cpp

HEADERS += switchoverlay.h
SOURCES += switchoverlay.cpp

PRE_TARGETDEPS += ../libcrude.a
