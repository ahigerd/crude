#include "clockwidget.h"
#include "configurator.h"
#include "launchbutton.h"
#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"
#include "libcrude/windowlist.h"
#include "panel.h"
#include "panelwidget.h"
#include "taskswitcher.h"

#include <QApplication>
#include <QSessionManager>
#include <QtDebug>

int main(int argc, char** argv)
{
  QApplication app(argc, argv);
  PlatformIntegrator platform(argc, argv);
  Settings settings;
  platform->enableWindowList();

  QString configFile;
  {
    QStringList args = app.arguments();
    args.removeFirst();
    for (int i = args.length() - 1; i >= 0; --i) {
      if (!args[i].startsWith("--")) {
        configFile = args[i];
        break;
      }
    }
  }

  PanelConfigurator config(configFile);

  Panel panel;
  config.apply(&panel);
  config.save(&panel);
  panel.show();

  QObject::connect(&settings, &Settings::reloadSettings, [&panel, &config]() {
    config.apply(&panel);
    config.save(&panel);
  });

  return app.exec();
}
