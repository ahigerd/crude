#ifndef CRUDE_TREECOMBOBOX_H
#define CRUDE_TREECOMBOBOX_H

#include <QComboBox>
#include <QModelIndex>
class QTreeView;

class TreeComboBox : public QComboBox
{
  Q_OBJECT

public:
  TreeComboBox(QAbstractItemModel* model, QWidget* parent = nullptr);

  QModelIndex selectedIndex() const;
  void setSelectedIndex(const QModelIndex& index);

  bool eventFilter(QObject* obj, QEvent* event);

signals:
  void currentIndexChanged(const QModelIndex& index);
  void contextMenuRequested(const QModelIndex& index, const QRect& itemRect);

protected:
  void showPopup();
  void hidePopup();
  void contextMenuEvent(QContextMenuEvent* event);

private:
  QTreeView* treeView;
  QPersistentModelIndex selection;
};

#endif
