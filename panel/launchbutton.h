#ifndef CRUDE_LAUNCHBUTTON_H
#define CRUDE_LAUNCHBUTTON_H

#include "panelwidget.h"
#include "plugins/powerplugin.h"

#include <QTimer>
#include <QToolButton>
class Hotkey;
class QMenu;
class QLineEdit;
class QWidgetAction;

class LaunchButton : public QToolButton, public PanelWidget
{
  Q_OBJECT

public:
  LaunchButton(QWidget* parent = nullptr);

  static void openRunDialog(QWidget* parent = nullptr);
  static void openXdgList(QWidget* parent = nullptr);
  static void runAction(QAction* action, QWidget* parent = nullptr);

  void loadSettings(QSettings& settings);
  void saveSettings(QSettings& settings);

  bool eventFilter(QObject*, QEvent* event);

protected:
  void showEvent(QShowEvent*);

private slots:
  void _openRunDialog();
  void openQuickRun();
  void promptLogout();
  void promptPower(PowerPlugin::Action action);
  void reloadHotkeys();
  void updateSearch();
  void updateSearch(const QString& pattern, bool force = false);
  void runSearchAction(QAction* action);
  void runFirstSearch();

private:
  bool promptConfirm(const QString& actionText);
  void recursiveSearch(QMenu* menu, const QString& pattern);

  bool useMenu;
  bool didInstallFilter;
  Hotkey* menuHotkey;
  Hotkey* runHotkey;
  QMenu* searchMenu;
  QWidgetAction* searchAction;
  QLineEdit* searchBox;
  QTimer updateThrottle;
  QString lastSearch;
};

#endif
