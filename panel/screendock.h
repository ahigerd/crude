#ifndef CRUDE_SCREENDOCK_H
#define CRUDE_SCREENDOCK_H

#include "libcrude/throttle.h"

#include <QWidget>

class ScreenDock : public QWidget
{
  Q_OBJECT

public:
  ScreenDock(QWidget* parent = nullptr);

  Qt::Orientation orientation() const;
  Qt::Edge screenEdge() const;
  void setScreenEdge(Qt::Edge e);

  int edgeSize() const;
  void setEdgeSize(int size);

signals:
  void screenEdgeChanged(Qt::Edge e);
  void orientationChanged(Qt::Orientation o);

protected:
  void showEvent(QShowEvent*);
  void resizeEvent(QResizeEvent*);
  void moveEvent(QMoveEvent*);

private slots:
  void updatePosition();

private:
  quint32 strutSize;
  int crossSize, lastSize;
  Qt::Edge edge, lastEdge;
  Throttle updateThrottle;
};

#endif
