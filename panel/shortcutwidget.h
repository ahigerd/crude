#ifndef CRUDE_SHORTCUTWIDGET_H
#define CRUDE_SHORTCUTWIDGET_H

#include "panelwidget.h"

#include <QButtonGroup>
#include <QMap>
#include <QWidget>

class ShortcutWidget : public QWidget, public PanelWidget
{
  Q_OBJECT

public:
  ShortcutWidget(QWidget* parent = nullptr);

  void loadSettings(QSettings& settings);
  void saveSettings(QSettings& settings);

private slots:
  void onClick(QAbstractButton* button) const;

private:
  QButtonGroup buttons;
  QMap<QAbstractButton*, QString> commands;
};

#endif
