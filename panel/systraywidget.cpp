#include "systraywidget.h"

#include <QDateTime>
#include <QTimer>

REGISTER_PANEL_WIDGET(SysTray, SysTrayWidget);

SysTrayWidget::SysTrayWidget(QWidget* parent) : SystemTray(parent), PanelWidget()
{
  auto lambda = [this] { emit panelWidgetReady(this); };
  QObject::connect(this, &SystemTray::acquired, lambda);
  QObject::connect(this, &SystemTray::unavailable, lambda);
}

void SysTrayWidget::loadSettings(QSettings& settings)
{
  Q_UNUSED(settings);
}

void SysTrayWidget::saveSettings(QSettings& settings)
{
  Q_UNUSED(settings);
}
