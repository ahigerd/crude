#ifndef CRUDE_SYSTRAYWIDGET_H
#define CRUDE_SYSTRAYWIDGET_H

#include "libcrude/systemtray.h"
#include "panelwidget.h"

class SysTrayWidget : public SystemTray, public PanelWidget
{
  Q_OBJECT

public:
  SysTrayWidget(QWidget* parent = nullptr);

  void loadSettings(QSettings& settings);
  void saveSettings(QSettings& settings);

signals:
  void panelWidgetReady(PanelWidget* item);
};

#endif
