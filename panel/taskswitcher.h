#ifndef CRUDE_TASKSWITCHER_H
#define CRUDE_TASKSWITCHER_H

#include "panelwidget.h"

#include <QFlags>
#include <QLinkedList>
#include <QModelIndex>
#include <QPointer>
#include <QWidget>
class TreeComboBox;
class ButtonTreeView;
class SwitchOverlay;
class WindowList;
class QModelIndex;
class Hotkey;

class TaskSwitcher : public QWidget, public PanelWidget
{
  Q_OBJECT

public:
  enum Type {
    DropDown,
    Taskbar
  };

  enum ButtonTypes {
    WindowTitle = 0,
    ProgramName = 1,
    NoText = 2,
    NoIcons = 4,
    Flat = 8,
  };

  Q_ENUM(ButtonTypes);
  Q_DECLARE_FLAGS(ButtonType, ButtonTypes);

  TaskSwitcher(QWidget* parent = nullptr);
  TaskSwitcher(Type type, QWidget* parent = nullptr);

  Qt::Orientation orientation() const;
  WindowList* model() const;

  void setType(Type type);

  void loadSettings(QSettings& settings);
  void saveSettings(QSettings& settings);

  QModelIndex currentWindow() const;
  QModelIndex currentGroup() const;

public slots:
  void setOrientation(Qt::Orientation o);

private slots:
  void switchWindow(const QModelIndex& index);
  void setCurrentWindow(const QModelIndex& index);
  void switchToNextApp(const QVariant& data);
  void switchToPreviousApp(const QVariant& data);
  void switchToNextAppWindow(const QVariant& data);
  void switchToPreviousAppWindow(const QVariant& data);
  void itemContextMenu(const QModelIndex& index, const QRect& itemRect);
  void hotkeyReleased();
  void contextAction(QAction* action);

private:
  void makeDropDown();
  void makeTaskBar();
  void switchWindow(int direction, bool switchApp, Hotkey* hotkey);
  void reloadHotkeys();
  void closeAllChildren(const QModelIndex& group);

  TreeComboBox* cb;
  ButtonTreeView* tb;
  WindowList* windowList;
  QList<Hotkey*> hotkeys;
  ButtonType buttonType;
  int iconSize;
  bool useHotkeys, useOverlay, linear;
  QPointer<SwitchOverlay> overlay;
  QModelIndex contextIndex;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(TaskSwitcher::ButtonType);

#endif
