#ifndef CRUDE_PANEL_H
#define CRUDE_PANEL_H

#include "libcrude/windowlist.h"
#include "screendock.h"
class PanelWidget;
class QSessionManager;

class Panel : public ScreenDock
{
  Q_OBJECT

public:
  Panel(QWidget* parent = nullptr);

  void addItem(PanelWidget* item, bool stretch = false);
  QList<PanelWidget*> items;
  PanelWidget* systray;

  void shutdown(const QString& method);

  void clear();

signals:
  void ready();

private slots:
  void updateOrientation();
  void panelWidgetReady(PanelWidget* item = nullptr);
  void saveStateRequest(QSessionManager& sm);
  void commitDataRequest(QSessionManager& sm);

private:
  QList<PanelWidget*> notReady;
  QString shutdownMethod;
};

#endif
