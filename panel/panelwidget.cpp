#include "panelwidget.h"

#include <QWidget>

static QMap<PanelWidget::Type, PanelWidget::RegistrarBase*>& PB_registry()
{
  static QMap<PanelWidget::Type, PanelWidget::RegistrarBase*> registry;
  return registry;
}

void PanelWidget::registerWidget(PanelWidget::Type t, PanelWidget::RegistrarBase* b)
{
  PB_registry()[t] = b;
}

PanelWidget* PanelWidget::create(Type t, QWidget* parent)
{
  PanelWidget* widget = PB_registry()[t]->create(parent);
  widget->componentType = t;
  return widget;
}

PanelWidget::PanelWidget() : stretch(false) {}

QWidget* PanelWidget::asWidget()
{
  return dynamic_cast<QWidget*>(this);
}

bool PanelWidget::shouldStretch() const
{
  return stretch;
}

void PanelWidget::setShouldStretch(bool stretch)
{
  this->stretch = stretch;
}
