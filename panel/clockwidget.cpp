#include "clockwidget.h"

#include <QCalendarWidget>
#include <QDateTime>
#include <QFrame>
#include <QMenu>
#include <QStyle>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidgetAction>

REGISTER_PANEL_WIDGET(Clock, ClockWidget);

ClockWidget::ClockWidget(QWidget* parent) : QLabel(parent), PanelWidget(), fmt("hh:mm AP")
{
  setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

  QTimer::singleShot(1000 - (QDateTime::currentMSecsSinceEpoch() % 1000), Qt::PreciseTimer, this, SLOT(startClock()));
  timerEvent();
}

void ClockWidget::startClock()
{
  startTimer(1000, Qt::VeryCoarseTimer);
  timerEvent();
}

void ClockWidget::timerEvent(QTimerEvent*)
{
  QDateTime now = QDateTime::currentDateTime();
  setText(now.toString(fmt));
  setToolTip(now.toString(Qt::DefaultLocaleLongDate));
}

QString ClockWidget::format() const
{
  return fmt;
}

void ClockWidget::setFormat(const QString& format)
{
  fmt = format;
  timerEvent();
}

void ClockWidget::loadSettings(QSettings& settings)
{
  if (settings.contains("format")) {
    setFormat(settings.value("format").toString());
  }
}

void ClockWidget::saveSettings(QSettings& settings)
{
  settings.setValue("format", format());
}

void ClockWidget::mouseReleaseEvent(QMouseEvent*)
{
  showCalendar();
}

void ClockWidget::showCalendar()
{
  QMenu* popup = new QMenu(this);
  QWidgetAction* wa = new QWidgetAction(popup);
  QFrame* frame = new QFrame(popup);
  QVBoxLayout* layout = new QVBoxLayout(frame);
  QCalendarWidget* cal = new QCalendarWidget(frame);
  int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
  layout->setContentsMargins(frameWidth, frameWidth, frameWidth, frameWidth);
  layout->addWidget(cal);
  cal->setGridVisible(true);
  cal->setSelectionMode(QCalendarWidget::NoSelection);
  cal->setDateEditEnabled(false);
  wa->setDefaultWidget(frame);
  popup->addAction(wa);

  QObject::connect(popup, SIGNAL(aboutToHide()), popup, SLOT(deleteLater()));
  popup->popup(mapToGlobal(geometry().topLeft()));
}
