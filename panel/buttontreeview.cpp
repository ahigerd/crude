#include "buttontreeview.h"

#include <QContextMenuEvent>
#include <QPaintEvent>
#include <QScreen>
#include <QStylePainter>
#include <QTreeView>
#include <QWindow>

/*
 * This isn't a QAbstractItemView because it's a fairly specialized tool.
 * Most of the functionality of the view class isn't relevant, and the
 * fact that it's a scroll area actively gets in the way.
 * The minor hassle of reimplementing the useful parts is worth it.
 */

ButtonTreeView::ButtonTreeView(QWidget* parent)
: QFrame(parent),
  _model(nullptr),
  minWidth(50),
  maxWidth(150),
  vertical(false),
  buttonWidth(150),
  activeButton(-1),
  _iconSize(16),
  flat(false),
  overlay(false),
  _displayRole(Qt::DisplayRole)
{
  popup = new QTreeView(this);
  popup->setWindowFlags(Qt::Popup | Qt::WindowStaysOnTopHint);
  popup->setAttribute(Qt::WA_Hover);
  popup->installEventFilter(this);
  popup->viewport()->installEventFilter(this);
  popup->setSelectionMode(QTreeView::NoSelection);
  popup->setHeaderHidden(true);
  popup->setItemsExpandable(false);
  popup->setRootIsDecorated(false);
  popup->setSortingEnabled(false);
  popup->hide();
  QObject::connect(popup, SIGNAL(clicked(QModelIndex)), this, SLOT(hidePopup()), Qt::DirectConnection);
  QObject::connect(popup, SIGNAL(clicked(QModelIndex)), this, SIGNAL(itemClicked(QModelIndex)), Qt::QueuedConnection);
  installEventFilter(this);
  setFrameShape(QFrame::NoFrame);
  setContentsMargins(0, 0, 0, 0);
  setContextMenuPolicy(Qt::DefaultContextMenu);

  longPress.setInterval(250);
  longPress.setSingleShot(true);
  QObject::connect(&longPress, SIGNAL(timeout()), this, SLOT(buttonHeld()));
}

int ButtonTreeView::minimumButtonWidth() const
{
  return minWidth;
}

void ButtonTreeView::setMinimumButtonWidth(int width)
{
  minWidth = width;
  updateButtonWidth();
}

int ButtonTreeView::maximumButtonWidth() const
{
  return maxWidth;
}

void ButtonTreeView::setMaximumButtonWidth(int width)
{
  maxWidth = width;
  updateButtonWidth();
}

int ButtonTreeView::iconSize() const
{
  return overlay ? _iconSize / 4 : _iconSize;
}

// In horizontal mode, this specifies a maximum.
void ButtonTreeView::setIconSize(int size)
{
  _iconSize = overlay ? size * 4 : size;
  popup->setIconSize(QSize(size, size));
  updateButtonWidth();
}

bool ButtonTreeView::isFlat() const
{
  return flat;
}

void ButtonTreeView::setFlat(bool on)
{
  flat = on;
  update();
}

bool ButtonTreeView::isOverlay() const
{
  return overlay;
}

void ButtonTreeView::setOverlay(bool on)
{
  overlay = on;
  updateButtonWidth();
}

int ButtonTreeView::displayRole() const
{
  return _displayRole;
}

void ButtonTreeView::setDisplayRole(int role)
{
  _displayRole = role;
  updateButtonWidth();
}

Qt::Orientation ButtonTreeView::orientation() const
{
  return vertical ? Qt::Vertical : Qt::Horizontal;
}

void ButtonTreeView::setOrientation(Qt::Orientation o)
{
  vertical = (o == Qt::Vertical);
  updateButtonWidth();
}

QAbstractItemModel* ButtonTreeView::model() const
{
  return _model;
}

void ButtonTreeView::setModel(QAbstractItemModel* model)
{
  if (_model) {
    QObject::disconnect(_model, 0, this, 0);
  }
  _model = model;
  popup->setModel(_model);
  if (_model) {
    QObject::connect(
        _model, SIGNAL(dataChanged(QModelIndex, QModelIndex, QVector<int>)), this, SLOT(onDataChanged(QModelIndex))
    );
    QObject::connect(_model, SIGNAL(rowsInserted(QModelIndex, int, int)), this, SLOT(updateButtonWidth()));
    QObject::connect(_model, SIGNAL(rowsRemoved(QModelIndex, int, int)), this, SLOT(updateButtonWidth()));
    QObject::connect(_model, SIGNAL(rowsMoved(QModelIndex, int, int, QModelIndex, int)), this, SLOT(update()));
    QObject::connect(_model, SIGNAL(modelReset()), this, SLOT(reset()));
  }
  reset();
}

void ButtonTreeView::reset()
{
  resizeEvent(nullptr);
  hidePopup();
  update();
}

QRect ButtonTreeView::itemRect(int index) const
{
  auto margins = contentsMargins();
  if (vertical) {
    return QRect(margins.top(), margins.left() + (buttonHeight + spacing) * index, buttonWidth, buttonHeight);
  } else {
    return QRect(margins.top() + (buttonWidth + spacing) * index, margins.left(), buttonWidth, buttonHeight);
  }
}

void ButtonTreeView::onDataChanged(const QModelIndex& index)
{
  if (popup->isVisible()) {
    if (!popupIndex.isValid()) {
      popup->hide();
    } else {
      openPopup(popupIndex);
    }
  }
  if (!index.parent().isValid()) {
    update(itemRect(index.row()));
  }
}

void ButtonTreeView::resizeEvent(QResizeEvent*)
{
  spacing = style()->pixelMetric(QStyle::PM_ToolBarItemSpacing);
  buttonMargin = style()->pixelMetric(QStyle::PM_ButtonMargin);
  updateButtonWidth();
}

void ButtonTreeView::updateButtonWidth()
{
  auto margins = contentsMargins();
  int contentsWidth = width() - margins.left() - margins.right();
  if (vertical) {
    buttonWidth = contentsWidth;
    buttonHeight = _iconSize + 2 * buttonMargin;
  } else {
    buttonHeight = height() - margins.top() - margins.bottom();
    if (_displayRole < 0) {
      buttonWidth = buttonHeight;
    } else if (_model && _model->rowCount() > 0) {
      buttonWidth = (contentsWidth + spacing) / _model->rowCount() - spacing;
      if (buttonWidth > maxWidth) {
        buttonWidth = maxWidth;
      } else if (buttonWidth < minWidth) {
        buttonWidth = minWidth;
      }
    } else {
      buttonWidth = maxWidth;
    }
  }
  update();
}

void ButtonTreeView::paintEvent(QPaintEvent* event)
{
  QStylePainter painter(this);

  {
    QStyleOptionFrame option;
    initStyleOption(&option);
    option.rect = rect();
    style()->drawControl(QStyle::CE_ShapedFrame, &option, &painter, this);
  }

  if (!_model) {
    // No model, so leave it all blank
    return;
  }

  QStyleOptionToolButton option;
  option.initFrom(this);
  int iconSize = height() - 2 * buttonMargin;
  if (vertical || iconSize > _iconSize) {
    iconSize = _iconSize;
  }
  option.iconSize = QSize(iconSize, iconSize);

  int maxX = width();
  int maxY = height();
  int count = _model->rowCount();
  for (int i = 0; i < count; i++) {
    option.rect = itemRect(i);
    if (vertical ? (option.rect.bottom() > maxY) : (option.rect.right() > maxX)) {
      // out of bounds
      // TODO: draw scroll buttons
      break;
    }
    if (!option.rect.intersects(event->rect())) {
      // Outside of the update region
      continue;
    }
    QModelIndex idx = _model->index(i, 0);
    QVariant icon = _model->data(idx, Qt::DecorationRole);
    bool hasChildren = _model->hasChildren(idx);
    QVariant checkState = _model->data(idx, Qt::CheckStateRole);
    if (activeButton == i) {
      option.state = QStyle::State_Enabled | QStyle::State_Sunken | QStyle::State_Active | QStyle::State_On |
          QStyle::State_MouseOver | QStyle::State_HasFocus;
    } else if (overlay ? (idx == currentIndex || idx == currentIndex.parent()) : checkState == Qt::Checked) {
      option.state = QStyle::State_Enabled | QStyle::State_Sunken | QStyle::State_On;
    } else if (checkState.isNull() || (overlay && checkState == Qt::Checked)) {
      option.state = QStyle::State_Enabled;
    } else if (checkState == Qt::PartiallyChecked) {
      option.state = QStyle::State_Enabled | QStyle::State_Active;
    } else {
      option.state = QStyle::State_Enabled | QStyle::State_Active | QStyle::State_MouseOver;
    }
    if (!flat && !(option.state & QStyle::State_Sunken)) {
      option.state |= QStyle::State_Raised;
    }
    option.features = hasChildren ? QStyleOptionToolButton::Menu : QStyleOptionToolButton::None;
    option.subControls.setFlag(QStyle::SC_ToolButtonMenu, hasChildren);
    option.text = _displayRole >= 0 ? _model->data(idx, _displayRole).toString() : QString();
    if (icon.isNull()) {
      option.toolButtonStyle = Qt::ToolButtonTextOnly;
    } else {
      QPixmap px = icon.value<QIcon>().pixmap(iconSize);
      if (px.size().height() < iconSize * .9375) {
        px = icon.value<QIcon>().pixmap(iconSize * 2).scaledToHeight(iconSize);
      }
      option.icon = px;
      if (option.text.isEmpty()) {
        option.toolButtonStyle = Qt::ToolButtonIconOnly;
      } else {
        option.toolButtonStyle = overlay ? Qt::ToolButtonTextUnderIcon : Qt::ToolButtonTextBesideIcon;
      }
    }
    painter.drawComplexControl(QStyle::CC_ToolButton, option);
  }
}

void ButtonTreeView::mousePressEvent(QMouseEvent* event)
{
  if (!_model) {
    // no model, no interaction
    return;
  }
  if (event->button() == Qt::LeftButton) {
    activeButton = indexAt(event->pos());
    update();
    QModelIndex idx = _model->index(activeButton, 0);
    if (_model->hasChildren(idx)) {
      if (idx != currentIndex && idx != currentIndex.parent()) {
        longPress.start();
      } else {
        openPopup(idx);
      }
    }
  }
}

int ButtonTreeView::indexAt(const QPoint& pos) const
{
  if (!_model) {
    return -1;
  }
  int i = 0;
  int max = _model->rowCount();
  while (i < max) {
    if (vertical ? (itemRect(i).bottom() > pos.y()) : (itemRect(i).right() > pos.x())) {
      return i;
    }
    ++i;
  }
  return -1;
}

void ButtonTreeView::mouseMoveEvent(QMouseEvent* event)
{
  if (!_model || !event->buttons() || activeButton < 0) {
    return;
  }
  if (indexAt(event->pos()) != activeButton) {
    longPress.stop();
    activeButton = -1;
    update();
  }
}

void ButtonTreeView::mouseReleaseEvent(QMouseEvent*)
{
  releaseMouse();
  longPress.stop();
  bool popupVisible = popup->isVisible();
  if (!_model || (activeButton < 0 && !popupVisible)) {
    return;
  }
  QModelIndex idx = _model->index(activeButton, 0);
  if ((idx == currentIndex || idx == currentIndex.parent()) && _model->hasChildren(idx) && !popupVisible) {
    openPopup(idx);
  } else {
    emit itemClicked(idx);
  }
  activeButton = -1;
  update();
}

void ButtonTreeView::buttonHeld()
{
  if (activeButton >= 0) {
    QModelIndex idx = _model->index(activeButton, 0);
    if (_model->hasChildren(idx)) {
      openPopup(idx);
    }
  }
}

void ButtonTreeView::openPopup(const QModelIndex& index)
{
  if (!index.isValid() || model()->rowCount(index) == 0) {
    popup->hide();
    return;
  }
  popupIndex = index;
  popup->setRootIndex(popupIndex);
  popup->setGeometry(popupRect(index.row()));
  popup->setCurrentIndex(currentIndex);
  if (overlay) {
    popup->setWindowFlags(Qt::Window | Qt::X11BypassWindowManagerHint | Qt::WindowStaysOnTopHint);
    popup->setVisible(true);
    popup->raise();
  } else {
    popup->setWindowFlags(Qt::Popup | Qt::WindowStaysOnTopHint);
    popup->show();
    popup->setFocus(Qt::PopupFocusReason);
    popup->grabKeyboard();
  }
}

bool ButtonTreeView::eventFilter(QObject* obj, QEvent* event)
{
  if (!popup->isVisible()) {
    return false;
  }
  if (event->type() == QEvent::MouseButtonRelease) {
    QMouseEvent* me = static_cast<QMouseEvent*>(event);
    if (me->button() != Qt::LeftButton) {
      return false;
    }
    QPoint pos = popup->mapFromGlobal(me->globalPos());
    QModelIndex idx = popup->indexAt(pos);
    if (!idx.isValid()) {
      return false;
    }
    hidePopup();
    emit itemClicked(idx);
    return true;
  }
  if (obj != popup) {
    return false;
  }
  if (event->type() == QEvent::KeyPress && static_cast<QKeyEvent*>(event)->key() == Qt::Key_Escape) {
    // User pressed Esc, so close the popup.
    hidePopup();
    return false;
  } else if (event->type() == QEvent::MouseButtonPress) {
    // Since clicking in the viewport of a scroll region dispatches the events there,
    // any events that make it into the filter are by definition outside of the popup.
    hidePopup();
    return false;
  } else if (event->type() == QEvent::ContextMenu) {
    QContextMenuEvent* me = static_cast<QContextMenuEvent*>(event);
    QModelIndex index = popup->indexAt(me->pos());
    if (!index.isValid()) {
      return false;
    }
    QRect rect = popup->visualRect(index);
    rect.moveTopLeft(popup->viewport()->mapToGlobal(rect.topLeft()));
    emit contextMenuRequested(index, rect);
    return true;
  }
  QHoverEvent* hover = dynamic_cast<QHoverEvent*>(event);
  if (hover) {
    // Mouse is over the popup, so highlight the item under the cursor
    QModelIndex idx = popup->indexAt(hover->pos());
    if (idx.isValid()) {
      popup->selectionModel()->setCurrentIndex(idx, QItemSelectionModel::NoUpdate);
    }
  }
  return false;
}

void ButtonTreeView::hidePopup()
{
  popup->releaseKeyboard();
  popup->hide();
  activeButton = -1;
  update();
}

QRect ButtonTreeView::popupRect(int index) const
{
  if (!window() || !window()->windowHandle()) {
    return QRect();
  }
  QRect screenRect = window()->windowHandle()->screen()->geometry();
  int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
  int rows = _model->rowCount(_model->index(index, 0));
  QPoint pos = mapToGlobal(itemRect(index).topLeft());
  int popupWidth = maxWidth * 2;
  if (pos.x() + popupWidth > screenRect.width()) {
    popupWidth = screenRect.width() - pos.x();
    if (popupWidth < maxWidth) {
      popupWidth = maxWidth;
    }
  }
  int rowHeight = popup->sizeHintForRow(0);
  if (rowHeight < popup->iconSize().height()) {
    rowHeight = popup->iconSize().height();
  }
  int popupHeight = rows * popup->sizeHintForRow(0) + frameWidth * 2;
  int maxHeight = screenRect.height() / 2 - height();
  if (popupHeight > maxHeight) {
    popupHeight = maxHeight;
  }
  QRect popupRect(pos.x() + (vertical ? buttonWidth : 0), pos.y() + (vertical ? 0 : buttonHeight), popupWidth, popupHeight);
  if (!screenRect.contains(popupRect)) {
    if (popupRect.left() < screenRect.left()) {
      popupRect.moveLeft(screenRect.left());
    } else if (popupRect.right() > screenRect.right()) {
      if (vertical) {
        popupRect.moveRight(pos.x());
      } else {
        popupRect.moveRight(pos.x() + buttonWidth);
      }
    }
    if (popupRect.top() < screenRect.top()) {
      popupRect.moveTop(screenRect.top());
    } else if (popupRect.bottom() > screenRect.bottom()) {
      if (vertical) {
        popupRect.moveBottom(pos.y() + buttonHeight);
      } else {
        popupRect.moveBottom(pos.y());
      }
    }
  }
  return popupRect;
}

QModelIndex ButtonTreeView::selectedIndex() const
{
  return currentIndex;
}

void ButtonTreeView::setSelectedIndex(const QModelIndex& index)
{
  if (currentIndex == index) {
    return;
  }
  currentIndex = index;
  popup->selectionModel()->setCurrentIndex(index, QItemSelectionModel::ClearAndSelect);
  if (overlay) {
    if (index.parent().isValid()) {
      openPopup(index.parent());
    } else {
      openPopup(index);
    }
  }
  update();
}

void ButtonTreeView::contextMenuEvent(QContextMenuEvent* event)
{
  int row = indexAt(event->pos());
  if (row < 0) {
    return;
  }
  QModelIndex index = _model->index(row, 0);
  QRect rect = itemRect(row);
  rect.moveTopLeft(mapToGlobal(rect.topLeft()));
  emit contextMenuRequested(index, rect);
}
