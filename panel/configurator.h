#ifndef LIBCRUDE_PANEL_CONFIGURATOR_H
#define LIBCRUDE_PANEL_CONFIGURATOR_H

#include <QList>
#include <QMap>
#include <QSettings>
#include <QString>
class PanelWidget;
class Panel;

class PanelConfigurator
{
  Q_GADGET

public:
  PanelConfigurator(const QString& path = QString());
  ~PanelConfigurator();

  QSettings& settings();
  void apply(Panel* panel);
  void save(Panel* panel);

private:
  QSettings* _settings;
  void saveSettings(const QList<PanelWidget*>& items);
  void loadSettings(PanelWidget* item);
};

#endif
