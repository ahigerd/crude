#include "taskswitcher.h"

#include "buttontreeview.h"
#include "libcrude/hotkey.h"
#include "libcrude/platformintegration.h"
#include "libcrude/sessionsettings.h"
#include "libcrude/windowlist.h"
#include "switchoverlay.h"
#include "treecombobox.h"

#include <QAbstractItemView>
#include <QMetaEnum>
#include <QMetaObject>
#include <QMenu>
#include <QScreen>
#include <QVBoxLayout>
#include <QWindow>

REGISTER_PANEL_WIDGET(Switcher, TaskSwitcher);

TaskSwitcher::TaskSwitcher(QWidget* parent) : TaskSwitcher(TaskSwitcher::Taskbar, parent)
{
  // forwarded constructor only
}

TaskSwitcher::TaskSwitcher(TaskSwitcher::Type type, QWidget* parent)
: QWidget(parent), PanelWidget(), cb(nullptr), tb(nullptr), iconSize(16), overlay(nullptr)
{
  QVBoxLayout* layout = new QVBoxLayout(this);
  layout->setContentsMargins(0, 0, 0, 0);

  windowList = new WindowList(this);
  QObject::connect(windowList, SIGNAL(currentWindowChanged(QModelIndex)), this, SLOT(setCurrentWindow(QModelIndex)));
  setType(type);
}

Qt::Orientation TaskSwitcher::orientation() const
{
  if (!tb) {
    return Qt::Horizontal;
  }
  return tb->orientation();
}

void TaskSwitcher::setOrientation(Qt::Orientation o)
{
  if (tb) {
    tb->setOrientation(o);
  }
}

void TaskSwitcher::switchWindow(const QModelIndex& index)
{
  int windowID = index.data(WindowList::WindowIDRole).value<uint32_t>();
  if (overlay) {
    WindowRef ref = c_PI->findWindow(windowID);
    QModelIndex newIndex = windowList->index(ref);
    overlay->deleteLater();
    overlay = nullptr;
    QMetaObject::invokeMethod(this, "switchWindow", Qt::QueuedConnection, Q_ARG(QModelIndex, newIndex));
  } else {
    PlatformIntegration::instance()->setActiveWindow(windowID);
  }
}

void TaskSwitcher::setCurrentWindow(const QModelIndex& index)
{
  if (cb) {
    cb->blockSignals(true);
    cb->setSelectedIndex(index);
    cb->blockSignals(false);
  } else if (tb) {
    tb->blockSignals(true);
    tb->setSelectedIndex(index);
    tb->blockSignals(false);
  }
}

WindowList* TaskSwitcher::model() const
{
  return windowList;
}

void TaskSwitcher::makeDropDown()
{
  if (cb) {
    return;
  }
  if (tb) {
    delete tb;
    tb = nullptr;
  }
  cb = new TreeComboBox(windowList, this);
  cb->setMinimumWidth(200);
  QObject::connect(cb, SIGNAL(currentIndexChanged(QModelIndex)), this, SLOT(switchWindow(QModelIndex)));
}

void TaskSwitcher::makeTaskBar()
{
  if (tb) {
    return;
  }
  if (cb) {
    delete cb;
    cb = nullptr;
  }
  tb = new ButtonTreeView(this);
  tb->setModel(windowList);
  QObject::connect(tb, SIGNAL(itemClicked(QModelIndex)), this, SLOT(switchWindow(QModelIndex)), Qt::QueuedConnection);
}

void TaskSwitcher::setType(Type type)
{
  if (type == DropDown) {
    makeDropDown();
    cb->view()->setIconSize(QSize(iconSize, iconSize));
  } else {
    makeTaskBar();
    tb->setFlat(buttonType & Flat);
    tb->setIconSize(iconSize);
    if (buttonType & NoText) {
      tb->setDisplayRole(-1);
    }
  }
  QWidget* w = cb ? static_cast<QWidget*>(cb) : static_cast<QWidget*>(tb);
  QObject::connect(w, SIGNAL(contextMenuRequested(QModelIndex, QRect)), this, SLOT(itemContextMenu(QModelIndex, QRect)), Qt::UniqueConnection);
  static_cast<QBoxLayout*>(layout())->addWidget(w, 1);
}

static QMetaEnum urgentFlags = QMetaEnum::fromType<WindowList::UrgentOption>();
static QMetaEnum buttonTypeFlags = QMetaEnum::fromType<TaskSwitcher::ButtonTypes>();

void TaskSwitcher::loadSettings(QSettings& settings)
{
  buttonType =
      ButtonType(buttonTypeFlags.keysToValue(settings.value("display", QByteArray("WindowName|ShowIcon")).toByteArray()));
  model()->setShowIcons(!(buttonType & NoIcons));
  if (buttonType & NoText) {
    model()->setLabelText(WindowList::NoText);
  } else if (buttonType & ProgramName) {
    model()->setLabelText(WindowList::ProgramName);
  } else {
    model()->setLabelText(WindowList::WindowTitle);
  }

  iconSize = settings.value("iconSize", 16).toInt();

  WindowList::GroupingMode group = WindowList::stringToGroupingMode(settings.value("group").toByteArray());
  model()->setGroupingMode(group);

  int urgent = urgentFlags.keysToValue(settings.value("urgent").toByteArray());
  model()->setUrgentFlags(WindowList::UrgentFlags(urgent));

  setType(settings.value("subtype", "Taskbar").toString() == "DropDown" ? DropDown : Taskbar);

  setShouldStretch(cb ? false : true);

  linear = settings.value("linear", false).toBool();
  useOverlay = settings.value("overlay", true).toBool();
  useHotkeys = settings.value("useHotkeys", false).toBool();
  reloadHotkeys();
  update();
}

void TaskSwitcher::saveSettings(QSettings& settings)
{
  settings.setValue("subtype", cb ? "DropDown" : "Taskbar");

  WindowList::GroupingMode groupMode(model()->groupingMode());
  QString group = WindowList::groupingModeToString(groupMode);
  settings.setValue("group", group);

  QByteArray urgent = urgentFlags.valueToKeys(model()->urgentFlags());
  settings.setValue("urgent", QString(urgent));

  QByteArray display = buttonTypeFlags.valueToKeys(buttonType);
  if (!model()->showIcons()) {
    display += "|NoIcons";
  }
  settings.setValue("display", QString(display));

  settings.setValue("iconSize", iconSize);

  settings.setValue("useHotkeys", useHotkeys);
}

void TaskSwitcher::reloadHotkeys()
{
  qDeleteAll(hotkeys);
  hotkeys.clear();

  if (!useHotkeys) {
    return;
  }

  static const char* hotkeySlots[][2] = {
    { "hotkeys/nextapp", SLOT(switchToNextApp(QVariant)) },
    { "hotkeys/prevapp", SLOT(switchToPreviousApp(QVariant)) },
    { "hotkeys/nextappwin", SLOT(switchToNextAppWindow(QVariant)) },
    { "hotkeys/prevappwin", SLOT(switchToPreviousAppWindow(QVariant)) },
  };

  for (const char** pair : hotkeySlots) {
    QString seq = Settings::instance()->value(pair[0]).toString();
    if (!seq.isEmpty()) {
      Hotkey* hotkey = new Hotkey(QKeySequence(seq, QKeySequence::PortableText), this);
      hotkey->setData(QVariant::fromValue<Hotkey*>(hotkey));
      QObject::connect(hotkey, SIGNAL(activated(QVariant)), this, pair[1]);
      QObject::connect(hotkey, SIGNAL(released(QVariant)), this, SLOT(hotkeyReleased()));
      hotkeys << hotkey;
    }
  }
}

void TaskSwitcher::switchToNextApp(const QVariant& data)
{
  switchWindow(+1, true, data.value<Hotkey*>());
}

void TaskSwitcher::switchToPreviousApp(const QVariant& data)
{
  switchWindow(-1, true, data.value<Hotkey*>());
}

void TaskSwitcher::switchToNextAppWindow(const QVariant& data)
{
  switchWindow(+1, false, data.value<Hotkey*>());
}

void TaskSwitcher::switchToPreviousAppWindow(const QVariant& data)
{
  switchWindow(-1, false, data.value<Hotkey*>());
}

void TaskSwitcher::hotkeyReleased()
{
  if (overlay) {
    switchWindow(overlay->selectedIndex());
  }
}

QModelIndex TaskSwitcher::currentWindow() const
{
  return windowList->index(windowList->currentWindow());
}

QModelIndex TaskSwitcher::currentGroup() const
{
  QModelIndex index = currentWindow();
  while (index.parent().isValid()) {
    index = index.parent();
  }
  return index;
}

void TaskSwitcher::switchWindow(int direction, bool switchApp, Hotkey* hotkey)
{
  bool enableOverlay = useOverlay && hotkey && hotkey->hasModifier();
  if (enableOverlay && (overlay || switchApp)) {
    if (windowList->rowCount() == 0) {
      // No windows to switch between; don't show the overlay.
      return;
    }
    if (!overlay) {
      overlay = new SwitchOverlay(windowList, linear, this);
      QObject::connect(overlay, SIGNAL(itemClicked(QModelIndex)), this, SLOT(switchWindow(QModelIndex)));
      overlay->setSelectedWindow(currentWindow());
      overlay->present();
    }
    overlay->selectNext(direction, switchApp);
    return;
  } else if (linear || !switchApp) {
    // TODO: switch with focus history order without overlay
    QModelIndex index = currentWindow();
    QModelIndex parent = index.parent();
    int count = windowList->rowCount(index.parent());
    if (count < 2) {
      // no switching needed
      return;
    } else if (!switchApp && !parent.isValid()) {
      // trying to switch windows within a group of one
      return;
    }
    int row = (index.row() + count + direction) % count;
    index = index.siblingAtRow(row);
    if (switchApp) {
      switchWindow(windowList->activeChild(index));
    } else {
      switchWindow(index);
    }
  }
}

void TaskSwitcher::itemContextMenu(const QModelIndex& index, const QRect& itemRect)
{
  contextIndex = index;
  QMenu* menu = new QMenu(this);
  uint32_t winId = index.data(WindowList::WindowIDRole).value<uint32_t>();
  WindowRef win = c_PI->findWindow(winId);
  menu->addAction("&Focus", [this, index]{ switchWindow(index); });
  menu->addSeparator();

  // TODO: checkstates where appropriate
  if (win->actions & WindowEntry::RestoreAction) {
    menu->addAction("R&estore")->setData(WindowEntry::RestoreAction);
  }
  if (win->actions & WindowEntry::MinimizeAction) {
    menu->addAction("&Minimize")->setData(WindowEntry::MinimizeAction);
  }
  if (win->actions & WindowEntry::MaximizeAction) {
    menu->addAction("Ma&ximize")->setData(WindowEntry::MaximizeAction);
  }
  if (win->actions & WindowEntry::ShadeAction) {
    menu->addAction("&Shade")->setData(WindowEntry::ShadeAction);
  }
  if (win->actions & WindowEntry::MoveAction) {
    menu->addAction("Mo&ve")->setData(WindowEntry::MoveAction);
  }
  if (win->actions & WindowEntry::ResizeAction) {
    menu->addAction("&Resize")->setData(WindowEntry::ResizeAction);
  }
  if (win->actions & WindowEntry::FullscreenAction) {
    menu->addAction("&Full Screen")->setData(WindowEntry::FullscreenAction);
  }
  if (win->actions & WindowEntry::AlwaysOnTopAction) {
    menu->addAction("Always on &Top")->setData(WindowEntry::AlwaysOnTopAction);
  }
  if (win->actions & WindowEntry::AlwaysOnBottomAction) {
    menu->addAction("Always on &Bottom")->setData(WindowEntry::AlwaysOnBottomAction);
  }
  // TODO: stick/move to desktop

  menu->addSeparator();

  if (win->actions & WindowEntry::CloseAction) {
    menu->addAction("&Close")->setData(WindowEntry::CloseAction);
    if (index.model()->rowCount(index) > 1) {
      menu->addAction("Close &All", [this, index]{ closeAllChildren(index); });
    }
  }

  QRect screenRect = window()->windowHandle()->screen()->geometry();
  bool alignRight = itemRect.left() + menu->width() > screenRect.right();
  if (itemRect.bottom() + menu->height() > screenRect.bottom()) {
    menu->popup(alignRight ? itemRect.topRight() : itemRect.topLeft());
  } else {
    menu->popup(alignRight ? itemRect.bottomRight() : itemRect.bottomLeft());
  }
  QObject::connect(menu, SIGNAL(triggered(QAction*)), this, SLOT(contextAction(QAction*)));
}

void TaskSwitcher::contextAction(QAction* action)
{
  int flags = action->data().toInt();
  if (!flags || !contextIndex.isValid()) {
    return;
  }
  uint32_t winId = contextIndex.data(WindowList::WindowIDRole).toInt();
  WindowRef win = c_PI->findWindow(winId);
  c_PI->requestAction(WindowEntry::WindowAction(flags), win->baseWindowId);
}

void TaskSwitcher::closeAllChildren(const QModelIndex& group)
{
  auto model = group.model();
  int count = model->rowCount(group);
  QList<uint32_t> ids;
  for (int i = 0; i < count; i++) {
    uint32_t winId = model->data(model->index(i, 0, group), WindowList::WindowIDRole).toInt();
    if (winId) {
      ids << winId;
    }
  }
  for (uint32_t id : ids) {
    c_PI->requestAction(WindowEntry::CloseAction, id);
  }
}
