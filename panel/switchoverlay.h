#ifndef CRUDE_SWITCHOVERLAY_H
#define CRUDE_SWITCHOVERLAY_H

#include "buttontreeview.h"

#include <QKeySequence>
class QSortFilterProxyModel;
class WindowList;

class SwitchOverlay : public ButtonTreeView
{
  Q_OBJECT

public:
  SwitchOverlay(WindowList* windowList, bool linear, QWidget* parent = nullptr);

  void present();

  bool eventFilter(QObject* obj, QEvent* event);
  void selectNext(int direction, bool switchApp);

public slots:
  void recenter();
  void setSelectedWindow(const QModelIndex& index);

protected:
  void mouseMoveEvent(QMouseEvent* event);

private:
  QList<QKeySequence> clearKeys;
  WindowList* windowList;
  QSortFilterProxyModel* proxy;
  bool linear;
};

#endif
