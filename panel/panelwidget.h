#ifndef CRUDE_PANELWIDGET_H
#define CRUDE_PANELWIDGET_H

#include "configurator.h"

#include <QSettings>
class QWidget;

class PanelWidget
{
  Q_GADGET

public:
  enum Type {
    Launcher,
    Switcher,
    Clock,
    SysTray,
    Shortcuts,
    Spacer
  };
  Q_ENUM(Type)
  PanelWidget();

  Type componentType;
  QString settingsName;

  bool shouldStretch() const;
  QWidget* asWidget();
  virtual void loadSettings(QSettings& settings) = 0;
  virtual void saveSettings(QSettings& settings) = 0;

protected:
  void setShouldStretch(bool stretch);

private:
  bool stretch;

public:
  struct RegistrarBase
  {
    virtual PanelWidget* create(QWidget* parent) = 0;
  };

  static void registerWidget(Type t, RegistrarBase* b);
  static PanelWidget* create(Type t, QWidget* parent);

  template <typename T>
  struct Registrar : public RegistrarBase
  {
    Registrar(Type t) { PanelWidget::registerWidget(t, this); }

    PanelWidget* create(QWidget* parent) { return new T(parent); }
  };
};

#define REGISTER_PANEL_WIDGET(type, Class) static PanelWidget::Registrar<Class> type##Registrar(PanelWidget::type)

#endif
