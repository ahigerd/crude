#ifndef CRUDE_CLOCKWIDGET_H
#define CRUDE_CLOCKWIDGET_H

#include "panelwidget.h"

#include <QLabel>

class ClockWidget : public QLabel, public PanelWidget
{
  Q_OBJECT

public:
  ClockWidget(QWidget* parent = nullptr);

  QString format() const;
  void setFormat(const QString& format);

  void loadSettings(QSettings& settings);
  void saveSettings(QSettings& settings);

protected:
  void timerEvent(QTimerEvent* = nullptr);
  void mouseReleaseEvent(QMouseEvent* = nullptr);

private slots:
  void startClock();
  void showCalendar();

private:
  QString fmt;
};

#endif
