#include "launchbutton.h"

#include "libcrude/environmentprefs.h"
#include "libcrude/hotkey.h"
#include "libcrude/platformintegration.h"
#include "libcrude/powermanagement.h"
#include "libcrude/sessionsettings.h"
#include "libcrude/xdglauncher.h"
#include "libcrude/xdgmenu.h"
#include "panel.h"
#include "libcrude/xcb/xcbrequest.h"

#include <QApplication>
#include <QCursor>
#include <QInputDialog>
#include <QKeyEvent>
#include <QLineEdit>
#include <QList>
#include <QMenu>
#include <QMessageBox>
#include <QMetaObject>
#include <QProcess>
#include <QScopedPointer>
#include <QSet>
#include <QStyle>
#include <QWidgetAction>

REGISTER_PANEL_WIDGET(Launcher, LaunchButton);

LaunchButton::LaunchButton(QWidget* parent)
: QToolButton(parent), PanelWidget(), didInstallFilter(false), menuHotkey(nullptr), runHotkey(nullptr)
{
  XdgMenu::refreshCache(false);

  setIcon(c_PI->icon("application-x-executable"));
  setText("Start");
  setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
  setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
  useMenu = true; // TODO: add configuration options

  searchMenu = new QMenu(this);
  searchAction = new QWidgetAction(this);
  searchAction->setDefaultWidget(searchBox = new QLineEdit);
  searchMenu->addAction(searchAction);

  QObject::connect(searchBox, SIGNAL(textChanged(QString)), this, SLOT(updateSearch(QString)));
  QObject::connect(searchBox, SIGNAL(returnPressed()), this, SLOT(runFirstSearch()));
  QObject::connect(searchMenu, SIGNAL(triggered(QAction*)), this, SLOT(runSearchAction(QAction*)));
  QObject::connect(this, SIGNAL(clicked()), this, SLOT(_openRunDialog()));
  reloadHotkeys();

  updateThrottle.setInterval(100);
  updateThrottle.setSingleShot(true);
  QObject::connect(&updateThrottle, SIGNAL(timeout()), this, SLOT(updateSearch()));

  /*
  QMenu* menu = XdgMenu::mainMenu().menu();
  if (menu) {
    menu->installEventFilter(this);
    QObject::connect(menu, SIGNAL(updated()), &updateThrottle, SLOT(start()));
    didInstallFilter = true;
  }
  */
}

void LaunchButton::reloadHotkeys()
{
  if (menuHotkey) {
    delete menuHotkey;
  }
  if (runHotkey) {
    delete runHotkey;
  }

  QString seq = Settings::instance()->value("hotkeys/launchmenu").toString();
  if (!seq.isEmpty()) {
    menuHotkey = new Hotkey(QKeySequence(seq, QKeySequence::PortableText), this);
    QObject::connect(menuHotkey, SIGNAL(activated(QVariant)), this, SLOT(_openRunDialog()));
  }

  seq = Settings::instance()->value("hotkeys/quickrun").toString();
  if (!seq.isEmpty()) {
    runHotkey = new Hotkey(QKeySequence(seq, QKeySequence::PortableText), this);
    QObject::connect(runHotkey, SIGNAL(activated(QVariant)), this, SLOT(openQuickRun()));
  }
}

void LaunchButton::openQuickRun()
{
  openRunDialog(this);
}

void LaunchButton::openRunDialog(QWidget* parent)
{
  QPointer<QWidget> pParent(parent);
  QInputDialog* dlg = new QInputDialog(parent, Qt::Dialog | Qt::WindowStaysOnTopHint);
  dlg->setWindowTitle(tr("Run Command"));
  dlg->setLabelText(tr("Enter command to run:"));
  QObject::connect(dlg, SIGNAL(finished(int)), dlg, SLOT(deleteLater()));
  QObject::connect(dlg, &QDialog::accepted, [dlg, pParent]{
    QString cmd = dlg->textValue();
    if (cmd.isEmpty()) {
      return;
    }
    bool ok = c_PI->runCommand(cmd, false);
    if (!ok) {
      QMessageBox::critical(pParent.data(), tr("Error Launching Program"), tr("Unable to run command: %1").arg(cmd), QMessageBox::Ok);
    }
  });
  dlg->open();
}

void LaunchButton::_openRunDialog()
{
  if (useMenu) {
    QMenu* menu = XdgMenu::mainMenu().menu();
    if (menu && !didInstallFilter) {
      menu->installEventFilter(this);
      QObject::connect(menu, SIGNAL(updated()), this, SLOT(updateSearch()));
      didInstallFilter = true;
    }
    openXdgList(this);
  } else {
    openRunDialog(this);
  }
}

void LaunchButton::loadSettings(QSettings&)
{
  // No settings
}

void LaunchButton::saveSettings(QSettings&)
{
  // No settings
}

void LaunchButton::openXdgList(QWidget* parent)
{
  QMenu* menu = XdgMenu::mainMenu().menu();
  bool deleteAfter = false;
  if (!menu || !menu->actions().count()) {
    // No menu items found. Use a temporary menu.
    menu = new QMenu(parent);
    menu->addAction("Menu loading...")->setEnabled(false);
    menu->addSeparator();
    deleteAfter = true;
  }
  if (menu->title().isEmpty()) {
    menu->setTitle("Launch");
    menu->addSeparator();
    QAction* runCommand = menu->addAction("Run command...");
    runCommand->setIcon(c_PI->icon("system-run", QStyle::SP_DirOpenIcon));
    runCommand->setData("__runCommand__");
    QAction* configCommand = menu->addAction("CRUDE Configurator");
    configCommand->setIcon(c_PI->icon({ "configuration-editor", "preferences-system" }, QStyle::SP_DesktopIcon));
    configCommand->setData("__configCommand__");
    LaunchButton* btn = qobject_cast<LaunchButton*>(parent);
    if (btn) {
      menu->addSeparator();
      QAction* logoutAction = menu->addAction("Log out", [btn] { btn->promptLogout(); });
      logoutAction->setIcon(c_PI->icon("system-log-out"));
      PowerManagement* pm = PowerManagement::instance();
      for (auto action : pm->allActions()) {
        if (pm->canPerform(action)) {
          QAction* pmAction = menu->addAction(pm->actionName(action), [btn, action] { btn->promptPower(action); });
          switch (action) {
          case PowerPlugin::Shutdown: pmAction->setIcon(c_PI->icon("system-shutdown")); break;
          case PowerPlugin::Suspend:
          case PowerPlugin::Hibernate:
          case PowerPlugin::HybridSleep:
          case PowerPlugin::SuspendThenHibernate: pmAction->setIcon(c_PI->icon("weather-few-clouds-night")); break;
          case PowerPlugin::Reboot: pmAction->setIcon(c_PI->icon("system-reboot")); break;
          default:
            // No icon
            break;
          }
        }
      }
    }
  }
  c_PI->setActiveWindow(parent->effectiveWinId());
  menu->setFocus();
  QTimer::singleShot(16, [menu]{
      callXcbSetFocus(menu->winId());
  });
  QAction* action = menu->exec(parent ? parent->mapToGlobal(parent->pos()) : QCursor::pos());
  if (deleteAfter) {
    menu->deleteLater();
  }
  runAction(action, parent);
}

void LaunchButton::runAction(QAction* action, QWidget* parent)
{
  if (!action) {
    return;
  }
  if (action->data().toString() == "__runCommand__") {
    openRunDialog(parent);
  } else if (action->data().toString() == "__configCommand__") {
    c_PI->runCommand(QApplication::applicationDirPath() + "/crude-config");
  } else if (action->data().canConvert<XdgLauncher>()) {
    XdgLauncher launcher = action->data().value<XdgLauncher>();
    bool ok = launcher.exec();
    if (!ok) {
      QMessageBox::critical(
          parent, "Error Launching Program", "Unable to run command: " + launcher.execPath(), QMessageBox::Ok
      );
    }
  }
}

void LaunchButton::promptLogout()
{
  if (promptConfirm("log out")) {
    Panel* panel = qobject_cast<Panel*>(parent());
    if (panel) {
      panel->shutdown("logout");
    }
  }
}

void LaunchButton::promptPower(PowerPlugin::Action action)
{
  Panel* panel = qobject_cast<Panel*>(parent());
  if (action == PowerPlugin::Reboot && promptConfirm("reboot your computer")) {
    if (panel) {
      panel->shutdown("reboot");
      return;
    }
  } else if (action == PowerPlugin::Shutdown && promptConfirm("shut down your computer")) {
    if (panel) {
      panel->shutdown("shutdown");
      return;
    }
  }
  PowerManagement::instance()->perform(action);
}

bool LaunchButton::promptConfirm(const QString& actionText)
{
  // TODO: Give other apps a chance to reject
  int ok = QMessageBox::question(
      nullptr, tr("Log Out?"), tr("Are you sure you want to %1?").arg(actionText), QMessageBox::Ok, QMessageBox::Cancel
  );
  return ok == QMessageBox::Ok;
}

void LaunchButton::showEvent(QShowEvent*)
{
  reloadHotkeys();
}

bool LaunchButton::eventFilter(QObject*, QEvent* event)
{
  if (event->type() == QEvent::KeyPress) {
    QKeyEvent* ke = static_cast<QKeyEvent*>(event);
    QString text = ke->text().trimmed();
    if (text.isEmpty()) {
      return false;
    }
    for (QChar ch : text) {
      if (!ch.isPrint()) {
        return false;
      }
    }
    lastSearch.clear();
    XdgMenu::mainMenu().menu()->close();
    searchMenu->clear();
    searchMenu->addAction(searchAction);
    searchMenu->popup(mapToGlobal(pos()));
    searchBox->setFocus();
    searchBox->setText(text);
    searchBox->end(false);
    return true;
  }
  return false;
}

void LaunchButton::updateSearch()
{
  QString pattern = searchBox->text();
  if (searchMenu->isVisible() && !pattern.isEmpty()) {
    updateSearch(pattern, true);
  }
}

static bool matchAction(QAction* action, const QString& pattern)
{
  if (action->text().startsWith(pattern, Qt::CaseInsensitive)) {
    return true;
  }
  return action->text().contains(" " + pattern, Qt::CaseInsensitive);
}

void LaunchButton::updateSearch(const QString& pattern, bool force)
{
  QList<QAction*> actions = searchMenu->actions();
  if (pattern.isEmpty()) {
    searchMenu->clear();
    searchMenu->addAction(searchAction);
  } else if (actions.count() <= 2 || force || !pattern.startsWith(lastSearch)) {
    recursiveSearch(XdgMenu::mainMenu().menu(), pattern);
  } else {
    for (QAction* action : searchMenu->actions()) {
      if (action == searchAction) {
        continue;
      } else if (!matchAction(action, pattern)) {
        searchMenu->removeAction(action);
      }
    }
  }
  actions = searchMenu->actions();
  if (actions.count() > 1) {
    searchMenu->setDefaultAction(actions[1]);
  }
  lastSearch = pattern;
  searchMenu->popup(mapToGlobal(pos()));
  searchBox->setFocus();
}

void LaunchButton::recursiveSearch(QMenu* menu, const QString& pattern)
{
  QList<QAction*> actions = searchMenu->actions();
  for (QAction* action : menu->actions()) {
    if (matchAction(action, pattern)) {
      if (!actions.contains(action)) {
        searchMenu->addAction(action);
      }
    }
    QMenu* actionMenu = action->menu();
    if (actionMenu) {
      QMetaObject::invokeMethod(actionMenu, "populate", Qt::QueuedConnection);
      recursiveSearch(actionMenu, pattern);
    }
  }
}

void LaunchButton::runSearchAction(QAction* action)
{
  runAction(action, this);
}

void LaunchButton::runFirstSearch()
{
  QList<QAction*> actions = searchMenu->actions();
  if (actions.length() >= 2) {
    // actions[0] is searchAction
    runAction(actions[1]);
  }
  searchMenu->close();
}
