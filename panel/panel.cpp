#include "panel.h"

#include "libcrude/platformintegration.h"
#include "panelwidget.h"
#include "taskswitcher.h"

#include <QApplication>
#include <QBoxLayout>
#include <QScreen>
#include <QSessionManager>
#include <QTimer>

#include <X11/SM/SMlib.h>
#include <cstdlib>

Panel::Panel(QWidget* parent) : ScreenDock(parent), systray(nullptr), shutdownMethod("logout")
{
  PlatformIntegration::instance()->setAsTaskbar(this);

  setAttribute(Qt::WA_AlwaysShowToolTips, true);

  QBoxLayout* layout = new QBoxLayout(QBoxLayout::LeftToRight, this);
  layout->setContentsMargins(1, 1, 1, 1);
  layout->setSpacing(1);

  QObject::connect(this, SIGNAL(orientationChanged(Qt::Orientation)), this, SLOT(updateOrientation()));
  QObject::connect(
      this, &Panel::ready, this, [this] { setScreenEdge(screenEdge()); }, Qt::QueuedConnection
  );

  qApp->setFallbackSessionManagementEnabled(false);
  QObject::connect(qApp, SIGNAL(saveStateRequest(QSessionManager&)), this, SLOT(saveStateRequest(QSessionManager&)));
  QObject::connect(qApp, SIGNAL(commitDataRequest(QSessionManager&)), this, SLOT(commitDataRequest(QSessionManager&)));
}

void Panel::updateOrientation()
{
  QBoxLayout::Direction dir = orientation() == Qt::Vertical ? QBoxLayout::TopToBottom : QBoxLayout::LeftToRight;
  static_cast<QBoxLayout*>(layout())->setDirection(dir);
}

void Panel::addItem(PanelWidget* item, bool stretch)
{
  items << item;
  QWidget* widget = item->asWidget();
  QBoxLayout* box = static_cast<QBoxLayout*>(layout());
  if (!widget) {
    if (stretch) {
      box->addStretch(1);
    }
    return;
  }
  box->addWidget(widget, stretch ? 1 : 0);
  if (item->componentType == PanelWidget::SysTray) {
    systray = item;
    widget->show();
  }
  if (widget->metaObject()->indexOfSlot("setOrientation") >= 0) {
    QObject::connect(this, SIGNAL(orientationChanged(Qt::Orientation)), widget, SLOT(setOrientation(Qt::Orientation)));
  }
  if (widget->metaObject()->indexOfSignal("panelWidgetReady(PanelWidget*)") >= 0) {
    QObject::connect(widget, SIGNAL(panelWidgetReady(PanelWidget*)), this, SLOT(panelWidgetReady(PanelWidget*)));
    notReady << item;
  } else {
    QTimer::singleShot(100, this, SLOT(panelWidgetReady()));
  }
  emit orientationChanged(orientation());
}

void Panel::panelWidgetReady(PanelWidget* item)
{
  if (item == nullptr && notReady.isEmpty()) {
    emit ready();
    return;
  }
  int removed = notReady.removeAll(item);
  if (removed > 0 && notReady.isEmpty()) {
    emit ready();
  }
}

static SmcCallbacks dummyCallbacks;

void Panel::shutdown(const QString& method)
{
  if (method == "logout" && !qApp->arguments().contains("--in-session")) {
    // If not running as part of a CRUDE session, treat the logout command as just a quit command.
    qApp->quit();
    return;
  }
  shutdownMethod = method;
  char* clientId;
  char err[1024];
  SmcConn sm = SmcOpenConnection(nullptr, nullptr, 1, 0, 0, &dummyCallbacks, nullptr, &clientId, sizeof(err), err);
  std::free(clientId);
  if (sm) {
    SmcRequestSaveYourself(sm, SmSaveBoth, true, SmInteractStyleAny, false, true);
    SmcCloseConnection(sm, 0, nullptr);
    // TODO: failsafe?
  } else {
    qApp->quit();
  }
}

void Panel::saveStateRequest(QSessionManager& sm)
{
  // Since we expect the session manager to start us regardless of the saved session,
  // tell it that we don't want to be auto-restarted.
  sm.setManagerProperty("CrudeLogoutMethod", shutdownMethod);
  sm.setRestartHint(QSessionManager::RestartNever);
}

void Panel::commitDataRequest(QSessionManager& sm)
{
  sm.setManagerProperty("CrudeLogoutMethod", shutdownMethod);
}

void Panel::clear()
{
  while (!items.isEmpty()) {
    PanelWidget* item = items.takeFirst();
    if (!item) {
      continue;
    } else if (item == systray) {
      item->asWidget()->hide();
    } else {
      delete item->asWidget();
    }
  }
  // Remove any remaining spacers and take the systray out of the layout
  while (layout()->takeAt(0))
    ;
}
