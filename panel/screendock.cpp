#include "screendock.h"

#include "libcrude/platformintegration.h"

#include <QApplication>
#include <QScreen>
#include <QWindow>

ScreenDock::ScreenDock(QWidget* parent)
: QWidget(parent),
  strutSize(0),
  crossSize(-1),
  lastSize(0),
  edge(Qt::BottomEdge),
  lastEdge(Qt::BottomEdge),
  updateThrottle(Throttle::connectedTo(this, SLOT(updatePosition())))
{
  setWindowFlags(Qt::FramelessWindowHint | Qt::NoDropShadowWindowHint | Qt::WindowStaysOnTopHint);

  updateThrottle.timeout(20);

  updatePosition();
  QScreen* screen = qApp->primaryScreen();
  QObject::connect(qApp, SIGNAL(primaryScreenChanged(QScreen*)), &updateThrottle, SLOT(trigger()));
  QObject::connect(screen, SIGNAL(availableGeometryChanged(QRect)), &updateThrottle, SLOT(trigger()));
  QObject::connect(screen, SIGNAL(geometryChanged(QRect)), &updateThrottle, SLOT(trigger()));
  QObject::connect(screen, SIGNAL(virtualGeometryChanged(QRect)), &updateThrottle, SLOT(trigger()));
  QObject::connect(c_PI, SIGNAL(screenAreaChanged()), &updateThrottle, SLOT(trigger()));
}

Qt::Edge ScreenDock::screenEdge() const
{
  return edge;
}

void ScreenDock::setScreenEdge(Qt::Edge e)
{
  edge = e;
  // don't throttle explicit changes
  updatePosition();
  emit screenEdgeChanged(e);
  emit orientationChanged(orientation());
}

Qt::Orientation ScreenDock::orientation() const
{
  if (edge == Qt::LeftEdge || edge == Qt::RightEdge) {
    return Qt::Vertical;
  } else {
    return Qt::Horizontal;
  }
}

void ScreenDock::showEvent(QShowEvent*)
{
  updateThrottle.trigger();
}

void ScreenDock::resizeEvent(QResizeEvent*)
{
  updateThrottle.trigger();
}

void ScreenDock::moveEvent(QMoveEvent*)
{
  // don't throttle this one
  updatePosition();
}

void ScreenDock::updatePosition()
{
  if (!isVisible()) {
    return;
  }

  // While QScreen::availableGeometry() is available for normal windows, it doesn't work
  // for us because (1) we might be loaded before the window manager, and (2) our own
  // geometry might be included in the result. Therefore we ask the window manager for a
  // list of struts. This could hypothetically be wrong if the WM is reserving part of the
  // desktop itself instead of creating a window for the task, but this isn't likely.

  QScreen* screen = qApp->primaryScreen();
  // TODO: should this be virtualGeometry?
  QRect screenBounds = screen->virtualGeometry();

  bool isVertical = (edge == Qt::LeftEdge || edge == Qt::RightEdge);
  QRect targetGeom = screenBounds;
  int newSize;
  if (isVertical) {
    newSize = crossSize < 1 ? minimumSizeHint().width() : crossSize;
    targetGeom.setWidth(newSize);
    if (edge == Qt::RightEdge) {
      // QRect::right() has an intentional off-by-one that we have to account for.
      int screenRight = screenBounds.right() + 1;
      targetGeom.moveRight(screenRight);
    }
  } else {
    newSize = crossSize < 1 ? minimumSizeHint().height() : crossSize;
    targetGeom.setHeight(newSize);
    if (edge == Qt::BottomEdge) {
      // QRect::bottom() has an intentional off-by-one that we have to account for.
      int screenBottom = screenBounds.bottom() + 1;
      targetGeom.moveBottom(screenBottom);
    }
  }

  auto struts = c_PI->findStruts();
  QList<WId> myWindows;
  for (const QWidget* widget : qApp->topLevelWidgets()) {
    myWindows << widget->effectiveWinId();
  }
  for (const auto& strut : struts) {
    if (myWindows.contains(strut.window)) {
      continue;
    }
    QRect g = strut.geometry;
    if (!g.intersects(targetGeom)) {
      // not colliding with this one
      continue;
    }
    // Heuristic: If the collision can be avoided by shortening our long axis by less than 50%, do that.
    // Otherwise, if the collision can be avoided by moving our short axis away from the screen edge, do that.
    // Otherwise, give up.
    if (isVertical) {
      if (g.bottom() >= targetGeom.top() && g.top() < targetGeom.top()) {
        int heightAdjust = targetGeom.top() - g.bottom();
        if (heightAdjust < targetGeom.height() / 2) {
          g.setTop(g.bottom() + 1);
          continue;
        }
      }
      if (g.bottom() > targetGeom.bottom() && g.top() <= targetGeom.top()) {
        int heightAdjust = g.top() - targetGeom.bottom();
        if (heightAdjust < targetGeom.height() / 2) {
          g.setBottom(g.top());
          continue;
        }
      }
      if (edge == Qt::LeftEdge && g.right() + targetGeom.width() < screenBounds.width()) {
        targetGeom.moveLeft(g.right() + 1);
        continue;
      } else if (edge == Qt::RightEdge && g.left() - targetGeom.width() > 0) {
        targetGeom.moveRight(g.left());
        continue;
      }
    } else { // horizontal
      if (g.right() >= targetGeom.left() && g.left() < targetGeom.left()) {
        int widthAdjust = targetGeom.left() - g.right();
        if (widthAdjust < targetGeom.width() / 2) {
          continue;
        }
      }
      if (g.right() > targetGeom.right() && g.left() <= targetGeom.left()) {
        int widthAdjust = g.left() - targetGeom.right();
        if (widthAdjust < targetGeom.width() / 2) {
          targetGeom.setRight(g.left());
          continue;
        }
      }
      if (edge == Qt::TopEdge && g.top() + targetGeom.height() < screenBounds.height()) {
        targetGeom.moveTop(g.bottom() + 1);
        continue;
      } else if (edge == Qt::BottomEdge && g.bottom() - targetGeom.height() > 0) {
        targetGeom.moveBottom(g.top());
        continue;
      }
    }
  }

  switch (edge) {
  case Qt::TopEdge: strutSize = targetGeom.bottom(); break;
  case Qt::BottomEdge: strutSize = screenBounds.height() - targetGeom.top(); break;
  case Qt::LeftEdge: strutSize = targetGeom.right(); break;
  case Qt::RightEdge: strutSize = screenBounds.width() - targetGeom.left(); break;
  }
  if (strutSize > quint32(isVertical ? screenBounds.width() : screenBounds.height())) {
    qWarning("ScreenDock: an impossible strut was requested");
    strutSize = 0;
    newSize = 0;
  }
  lastEdge = edge;
  lastSize = newSize;
  if (geometry() != targetGeom) {
    setGeometry(targetGeom);
  }
  PlatformIntegration::instance()->setStrut(this, edge, strutSize);
  raise();
}

int ScreenDock::edgeSize() const
{
  return crossSize;
}

void ScreenDock::setEdgeSize(int size)
{
  crossSize = size;
  // don't throttle explicit changes
  updatePosition();
}
