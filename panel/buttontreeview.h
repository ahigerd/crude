#ifndef CRUDE_BUTTONTREEVIEW_H
#define CRUDE_BUTTONTREEVIEW_H

#include <QFrame>
#include <QPersistentModelIndex>
#include <QTimer>
class QTreeView;
class QAbstractItemModel;

class ButtonTreeView : public QFrame
{
  Q_OBJECT

public:
  ButtonTreeView(QWidget* parent = nullptr);

  int minimumButtonWidth() const;
  void setMinimumButtonWidth(int width);

  int maximumButtonWidth() const;
  void setMaximumButtonWidth(int width);

  int iconSize() const;
  void setIconSize(int size);

  bool isFlat() const;
  void setFlat(bool on);

  bool isOverlay() const;
  void setOverlay(bool on);

  int displayRole() const;
  void setDisplayRole(int role);

  QAbstractItemModel* model() const;
  void setModel(QAbstractItemModel* model);

  QModelIndex selectedIndex() const;

  Qt::Orientation orientation() const;

  bool eventFilter(QObject* obj, QEvent* event);

signals:
  void itemClicked(const QModelIndex& index);
  void contextMenuRequested(const QModelIndex& index, const QRect& itemRect);

public slots:
  void reset();
  void setOrientation(Qt::Orientation o);
  void setSelectedIndex(const QModelIndex& index);

protected slots:
  void onDataChanged(const QModelIndex& index);
  void updateButtonWidth();
  void openPopup(const QModelIndex& index);
  void hidePopup();
  void buttonHeld();

protected:
  void paintEvent(QPaintEvent* event);
  void resizeEvent(QResizeEvent* event);
  void mousePressEvent(QMouseEvent* event);
  void mouseMoveEvent(QMouseEvent* event);
  void mouseReleaseEvent(QMouseEvent* event);
  void contextMenuEvent(QContextMenuEvent* event);
  QRect itemRect(int index) const;
  QRect popupRect(int index) const;
  int indexAt(const QPoint& pos) const;

  QAbstractItemModel* _model;
  QTreeView* popup;

private:
  int minWidth;
  int maxWidth;
  bool vertical;
  int buttonMargin;
  int buttonWidth;
  int buttonHeight;
  int spacing;
  int activeButton;
  int _iconSize;
  bool flat;
  bool overlay;
  int _displayRole;
  QPersistentModelIndex popupIndex, currentIndex;
  QTimer longPress;
};

#endif
