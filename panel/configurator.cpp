#include "configurator.h"

#include "panel.h"
#include "panelwidget.h"

#include <QMetaEnum>
#include <QMetaObject>
#include <QtDebug>

PanelConfigurator::PanelConfigurator(const QString& path)
{
  if (path.isEmpty()) {
    _settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, "Alkahest", "crude-panel");
  } else {
    _settings = new QSettings(path, QSettings::IniFormat);
  }
}

PanelConfigurator::~PanelConfigurator()
{
  delete _settings;
}

void PanelConfigurator::apply(Panel* panel)
{
  _settings->sync();
  panel->clear();
  panel->setScreenEdge(_settings->value("edge", Qt::BottomEdge).value<Qt::Edge>());
  panel->setEdgeSize(_settings->value("size", -1).toInt());
  QStringList parts = _settings->value("components").toStringList();
  if (parts.isEmpty()) {
    parts = _settings->value("components").toString().split(",");
  }
  bool hadSystray = panel->systray;
  bool hasSystray = false;
  for (const QString& part : parts) {
    QString name = part.trimmed();
    if (name.isEmpty()) {
      continue;
    }
    if (name == "spacer") {
      panel->addItem(nullptr, true);
      continue;
    }
    _settings->beginGroup(name);
    int type = QMetaEnum::fromType<PanelWidget::Type>().keyToValue(qPrintable(_settings->value("type").toString()));
    if (type == PanelWidget::SysTray) {
      hasSystray = true;
    }
    if (type < 0) {
      qDebug() << "Panel: unknown type" << _settings->value("type").toString() << "for component" << name;
    } else {
      PanelWidget* item;
      if (type == PanelWidget::SysTray && panel->systray) {
        item = panel->systray;
      } else {
        item = PanelWidget::create((PanelWidget::Type)type, panel);
      }
      item->settingsName = name;
      item->loadSettings(*_settings);
      panel->addItem(item, item->shouldStretch());
    }
    _settings->endGroup();
  }

  if (panel->items.isEmpty()) {
    PanelWidget* item = PanelWidget::create(PanelWidget::Launcher, panel);
    item->settingsName = "launcher";
    panel->addItem(item, false);

    item = PanelWidget::create(PanelWidget::Switcher, panel);
    item->settingsName = "taskbar";
    panel->addItem(item, true);

    item = PanelWidget::create(PanelWidget::SysTray, panel);
    item->settingsName = "systray";
    hasSystray = true;
    panel->addItem(item, false);

    item = PanelWidget::create(PanelWidget::Clock, panel);
    item->settingsName = "clock";
    panel->addItem(item, false);
  }

  if (hadSystray && !hasSystray) {
    // The systray was removed from the configuration. Normally we retain it
    // to avoid bouncing all of the tray icons, but since we're explicitly
    // removing it, destroy it.
    panel->systray->asWidget()->deleteLater();
    panel->systray = nullptr;
  } else if (hadSystray && hasSystray) {
    // The systray was recycled, so it needs to signal its readiness again.
    QMetaObject::invokeMethod(panel->systray->asWidget(), "panelWidgetReady", Q_ARG(PanelWidget*, panel->systray));
  }
}

void PanelConfigurator::save(Panel* panel)
{
  _settings->setValue("edge", QMetaEnum::fromType<Qt::Edge>().valueToKey(panel->screenEdge()));
  if (panel->edgeSize() > 0) {
    _settings->setValue("size", panel->edgeSize());
  } else {
    _settings->remove("size");
  }
  QStringList components;
  for (PanelWidget* item : panel->items) {
    if (!item) {
      components << "spacer";
      continue;
    }
    components << item->settingsName;
    _settings->beginGroup(item->settingsName);
    _settings->setValue("type", QMetaEnum::fromType<PanelWidget::Type>().valueToKey(item->componentType));
    item->saveSettings(*_settings);
    _settings->endGroup();
  }
  _settings->setValue("components", QVariant::fromValue(components));

  _settings->sync();
}

QSettings& PanelConfigurator::settings()
{
  return *_settings;
}
