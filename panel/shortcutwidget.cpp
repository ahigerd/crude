#include "shortcutwidget.h"

#include "libcrude/platformintegration.h"
#include "libcrude/xdglauncher.h"

#include <QAction>
#include <QButtonGroup>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPushButton>

REGISTER_PANEL_WIDGET(Shortcuts, ShortcutWidget);

ShortcutWidget::ShortcutWidget(QWidget* parent) : QWidget(parent)
{
  setContentsMargins(0, 0, 0, 0);
  QHBoxLayout* layout = new QHBoxLayout(this);
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, 0, 0);
  setLayout(layout);
  QObject::connect(&buttons, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(onClick(QAbstractButton*)));
}

void ShortcutWidget::loadSettings(QSettings& settings)
{
  QHBoxLayout* hbox = static_cast<QHBoxLayout*>(layout());
  for (QAbstractButton* button : buttons.buttons()) {
    button->deleteLater();
    buttons.removeButton(button);
  }
  commands.clear();

  for (const QString& key : settings.childKeys()) {
    if (key.contains('_') || key == "type") {
      continue;
    }
    QString command = settings.value(key).toString();
    QString iconName = settings.value(key + "_icon").toString();
    QString iconLabel = command;
    if (command.endsWith(".desktop")) {
      XdgLauncher launcher(command);
      if (iconName.isEmpty()) {
        iconName = launcher.iconPath();
      }
      iconLabel = launcher.name();
    }
    QPushButton* button = new QPushButton(this);
    button->setFlat(true);
    if (iconName.isEmpty()) {
      button->setText(iconLabel);
    } else {
      QIcon icon = c_PI->icon(iconName);
      if (icon.isNull()) {
        button->setText(iconLabel);
      } else {
        button->setIcon(icon);
        button->setToolTip(iconLabel);
      }
    }
    commands[button] = command;
    buttons.addButton(button);
    hbox->addWidget(button);
  }
}

void ShortcutWidget::saveSettings(QSettings& settings)
{
  // N/A: The defaults are empty and this isn't configurable outside of the configurator.
  Q_UNUSED(settings);
}

void ShortcutWidget::onClick(QAbstractButton* button) const
{
  QString command = commands[button];
  bool ok = false;
  if (command.endsWith(".desktop")) {
    XdgLauncher launcher(command);
    command = launcher.execPath();
    ok = launcher.exec();
  } else {
    ok = c_PI->runCommand(command, true);
  }
  if (!ok) {
    QMessageBox::critical(window(), "Error Launching Program", "Unable to run command: " + command, QMessageBox::Ok);
  }
}
