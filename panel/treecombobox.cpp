#include "treecombobox.h"

#include <QAbstractItemModel>
#include <QContextMenuEvent>
#include <QTreeView>

TreeComboBox::TreeComboBox(QAbstractItemModel* model, QWidget* parent) : QComboBox(parent)
{
  setModel(model);
  treeView = new QTreeView(this);
  treeView->installEventFilter(this);
  treeView->setHeaderHidden(true);
  treeView->setItemsExpandable(false);
  treeView->setRootIsDecorated(false);
  treeView->setSortingEnabled(false);
  treeView->setUniformRowHeights(true);
  treeView->setContextMenuPolicy(Qt::DefaultContextMenu);
  setContextMenuPolicy(Qt::DefaultContextMenu);
  setView(treeView);
}

QModelIndex TreeComboBox::selectedIndex() const
{
  return selection;
}

void TreeComboBox::setSelectedIndex(const QModelIndex& index)
{
  // Don't change the model around while the popup is open
  if (!treeView->isVisible()) {
    setRootModelIndex(index.parent());
    setCurrentIndex(index.row());
  }
  if (selection != index) {
    selection = index;
    emit currentIndexChanged(index);
  }
}

void TreeComboBox::showPopup()
{
  setRootModelIndex(QModelIndex());
  treeView->expandAll();
  treeView->setCurrentIndex(selection);
  QComboBox::showPopup();
}

void TreeComboBox::hidePopup()
{
  if (treeView->currentIndex().isValid()) {
    setSelectedIndex(treeView->currentIndex());
  } else {
    setSelectedIndex(selection);
  }
  QComboBox::hidePopup();
}

bool TreeComboBox::eventFilter(QObject* obj, QEvent* event)
{
  if (obj == treeView && event->type() == QEvent::Leave) {
    treeView->setCurrentIndex(QModelIndex());
  } else if (obj == treeView && event->type() == QEvent::ContextMenu) {
    QContextMenuEvent* me = static_cast<QContextMenuEvent*>(event);
    QModelIndex index = treeView->indexAt(me->pos());
    if (!index.isValid()) {
      return false;
    }
    QRect rect = treeView->visualRect(index);
    rect.moveTopLeft(treeView->viewport()->mapToGlobal(rect.topLeft()));
    emit contextMenuRequested(index, rect);
  }
  return false;
}

void TreeComboBox::contextMenuEvent(QContextMenuEvent*)
{
  QRect rect = this->rect();
  rect.moveTopLeft(mapToGlobal(rect.topLeft()));
  emit contextMenuRequested(selectedIndex(), rect);
}
