#include "switchoverlay.h"

#include "libcrude/windowlist.h"

#include <QApplication>
#include <QKeyEvent>
#include <QMetaObject>
#include <QMouseEvent>
#include <QScreen>
#include <QStyle>
#include <QSortFilterProxyModel>

SwitchOverlay::SwitchOverlay(WindowList* windowList, bool linear, QWidget* parent)
: ButtonTreeView(parent), windowList(windowList), linear(linear)
{
  proxy = new QSortFilterProxyModel(this);
  if (linear) {
    proxy->setSortRole(WindowList::CreationOrderRole);
    proxy->setSourceModel(windowList);
    proxy->sort(0, Qt::AscendingOrder);
  } else {
    proxy->setSortRole(WindowList::FocusOrderRole);
    proxy->setSourceModel(windowList);
    proxy->sort(0, Qt::DescendingOrder);
  }

  setWindowFlags(Qt::Window | Qt::X11BypassWindowManagerHint | Qt::WindowStaysOnTopHint);
  setOverlay(true);
  setModel(proxy);
  setFlat(true);
  setIconSize(32);
  setDisplayRole(WindowList::WindowTitleRole);
  setContentsMargins(3, 3, 3, 3);
  setFrameStyle(QFrame::Panel | QFrame::Plain);
  setLineWidth(1);

  if (windowList->showIcons()) {
    setFixedHeight(128 + font().pixelSize() * 2 + 64);
  } else {
    setOrientation(Qt::Vertical);
    setFixedWidth(200);
  }

  QObject::connect(windowList, SIGNAL(rowsInserted(QModelIndex, int, int)), this, SLOT(recenter()));
  QObject::connect(windowList, SIGNAL(rowsRemoved(QModelIndex, int, int)), this, SLOT(recenter()));
  QObject::connect(windowList, SIGNAL(modelReset()), this, SLOT(recenter()));
  installEventFilter(this);
  setMouseTracking(true);
}

void SwitchOverlay::present()
{
  recenter();
  show();
  activateWindow();
  setFocus();
  grabKeyboard();
  grabMouse();
}

void SwitchOverlay::recenter()
{
  QRect screenGeom = qApp->primaryScreen()->geometry();
  int rows = model()->rowCount();
  if (windowList->showIcons()) {
    int spacing = style()->pixelMetric(QStyle::PM_ToolBarItemSpacing);
    int width = rows * (iconSize() * 4 + spacing) + 10 - spacing;
    if (width > screenGeom.width()) {
      width = screenGeom.width();
    }
    setFixedWidth(width);
  } else {
    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    int height = rows * itemRect(0).height() + frameWidth * 2;
    if (height > screenGeom.height()) {
      height = screenGeom.height();
    }
    setFixedHeight(height);
  }
  move((screenGeom.width() - width()) / 2, (screenGeom.height() - height()) / 2);
}

bool SwitchOverlay::eventFilter(QObject* obj, QEvent* event)
{
  if (event->type() == QEvent::KeyPress) {
    QKeyEvent* ke = static_cast<QKeyEvent*>(event);
    QModelIndex sel = selectedIndex();
    QModelIndex newIndex = sel;
    if (ke->key() == Qt::Key_Up) {
      newIndex = newIndex.siblingAtRow(newIndex.row() - 1);
    } else if (ke->key() == Qt::Key_Down) {
      newIndex = newIndex.siblingAtRow(newIndex.row() + 1);
    }
    if (newIndex != sel && newIndex.isValid()) {
      setSelectedIndex(newIndex);
    }
  } else if (event->type() == QEvent::KeyRelease) {
    QKeyEvent* ke = static_cast<QKeyEvent*>(event);
    if (ke->modifiers() == Qt::NoModifier) {
      emit itemClicked(selectedIndex());
      hide();
      return true;
    }
  }
  return ButtonTreeView::eventFilter(obj, event);
}

void SwitchOverlay::mouseMoveEvent(QMouseEvent* event)
{
  if (event->buttons()) {
    ButtonTreeView::mouseMoveEvent(event);
    return;
  }
  int hover = indexAt(event->pos());
  if (hover < 0) {
    return;
  }
  QModelIndex sel = selectedIndex();
  if (sel.parent().isValid()) {
    sel = sel.parent();
  }
  if (hover != sel.row()) {
    setSelectedIndex(model()->index(hover, 0));
  }
}

void SwitchOverlay::setSelectedWindow(const QModelIndex& index)
{
  QModelIndex proxyIndex(index);
  if (index.model() == windowList) {
    proxyIndex = proxy->mapFromSource(index);
  }
  setSelectedIndex(proxyIndex);
}

void SwitchOverlay::selectNext(int direction, bool switchApp)
{
  QModelIndex index = selectedIndex();
  QModelIndex parent = index.parent();
  if (switchApp) {
    // Switch between groups
    if (parent.isValid()) {
      index = parent;
    }
    int groupCount = proxy->rowCount();
    if (groupCount == 0) {
      return;
    }
    int row = (index.row() + groupCount + direction) % groupCount;
    QModelIndex sourceIndex = proxy->mapToSource(proxy->index(row, 0));
    setSelectedWindow(windowList->activeChild(sourceIndex));
  } else if (parent.isValid()) {
    // Switch between windows within groups
    int windowCount = proxy->rowCount(parent);
    int row = (index.row() + windowCount + direction) % windowCount;
    setSelectedWindow(proxy->index(row, 0, parent));
  }
}
