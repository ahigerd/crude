#include "grabwindow.h"

#include "libcrude/xcb/xcbextension.h"
#include "libcrude/xcb/xcbrequest_fixes.h"
#include "libcrude/xcb/xcbrequest_prop.h"
#include "libcrude/xcb/xcbrequest_render.h"
#include "libcrude/xcb/xcbrequest_shape.h"
#include "libcrude/xcb/xcbrequest_wm.h"
#include "utility.h"
#include "windowtracker.h"

#include <QScreen>
#include <QX11Info>
#include <QtDebug>

//#define DEBUG(ex) if (c_PI->isDebugEnabled()) { qDebug() << ex; } else { ex; }
#define DEBUG(ex) ex
static quint32 _NET_FRAME_EXTENTS = 0;

struct GrabWindowPrivate
{
  quint32 rootId, windowId, damageId, visualId, renderId, formatId, compositeId, frameWindowId, childWindowId, shapeId;
  bool hasAttr : 1;
  bool hasGeom : 1;
  bool hasPicture : 1;
  bool hasTree : 1;
  bool isMapped : 1;
  QRect geom, frameGeom, extentGeom, childGeom;
  QRegion shape;
};

GrabWindow::GrabWindow(quint32 winId) : d(new GrabWindowPrivate)
{
  if (_NET_FRAME_EXTENTS == 0) {
    _NET_FRAME_EXTENTS = callXcbInternAtom(true, "_NET_FRAME_EXTENTS")->takeResult();
  }

  d->rootId = QX11Info::appRootWindow();
  d->windowId = winId;
  d->damageId = 0;
  d->frameWindowId = winId;
  d->childWindowId = 0;
  d->visualId = 0;
  d->renderId = 0;
  d->formatId = 0;
  d->compositeId = 0;
  d->shapeId = 0;
  d->hasAttr = false;
  d->hasGeom = false;
  d->hasPicture = false;
  d->hasTree = false;
  callXcbChangeAttributes(
      winId,
      XCB_CW_EVENT_MASK,
      { XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_VISIBILITY_CHANGE | XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY |
        XCB_EVENT_MASK_STRUCTURE_NOTIFY }
  );
  fetchProps();
}

GrabWindow::~GrabWindow()
{
  if (d->damageId) {
    callDamageDestroy(d->damageId);
  }
  if (d->shapeId) {
    callDestroyRegion(d->shapeId);
  }
  if (d->renderId) {
    callRenderFreePicture(d->renderId);
  }
  if (d->compositeId) {
    callXcbFreePixmap(d->compositeId);
  }
}

quint32 GrabWindow::windowId() const
{
  return d->windowId;
}

quint32 GrabWindow::frameWindowId() const
{
  return d->frameWindowId;
}

quint32 GrabWindow::childWindowId() const
{
  if (d->childWindowId) {
    return d->childWindowId;
  }
  return d->windowId;
}

bool GrabWindow::isMapped() const
{
  return d->isMapped;
}

quint32 GrabWindow::visualId() const
{
  return d->visualId;
}

quint32 GrabWindow::renderId() const
{
  if (!d->renderId) {
    d->renderId = xcb_generate_id(c_X11);
    xcb_render_create_picture_value_list_t value;
    value.subwindowmode = 1;
    auto cookie =
        callRenderCreatePicture(d->renderId, d->windowId, d->formatId, XCB_RENDER_CP_SUBWINDOW_MODE, value)->uniquePtr();
    if (!cookie->force()) {
      callRenderFreePicture(d->renderId);
      d->renderId = 0;
    }
  }
  return d->renderId;
}

QRect GrabWindow::geometry() const
{
  return d->geom;
}

QRect GrabWindow::extentsGeometry() const
{
  return d->extentGeom;
}

QRect GrabWindow::frameGeometry() const
{
  return d->frameGeom;
}

QRect GrabWindow::childGlobalGeometry() const
{
  if (d->childGeom.isNull()) {
    return d->geom.translated(d->frameGeom.topLeft());
  }
  return d->childGeom;
}

bool GrabWindow::isAlpha() const
{
  return isAlphaVisual(visualId());
}

void GrabWindow::fetchProps()
{
  auto cAttrib = callXcbGetAttributes(d->windowId)->uniquePtr();
  if (!cAttrib->force() || cAttrib->result()->map_state != XCB_MAP_STATE_VIEWABLE) {
    // There was an error fetching the window, or it's unmapped.
    // Either way there's nothing to see.
    d->isMapped = false;
    d->visualId = 0;
    return;
  }
  auto attrib = cAttrib->result();
  d->isMapped = true;
  d->visualId = attrib->visual;
  d->formatId = getRenderFormatForVisual(d->visualId);

  callShapeSelectInput(d->windowId, true);
  auto cShape = callShapeGetRects(d->windowId, XCB_SHAPE_SK_CLIP)->uniquePtr();

  std::unique_ptr<XcbGetGeometry> cGeom;
  std::unique_ptr<XcbGetProperty> cExtent;
  std::unique_ptr<XcbQueryTree> cTree;
  int offsetX = 0;
  int offsetY = 0;
  QRect extentGeom;
  QVector<quint32> extents;
  quint32 parentId = d->windowId;
  int extentDepth = -1;
  do {
    cGeom = callXcbGetGeometry(parentId)->uniquePtr();
    if (extentDepth < 0) {
      cExtent = callXcbGetProperty(parentId, _NET_FRAME_EXTENTS, XCB_ATOM_CARDINAL)->uniquePtr();
    } else {
      cExtent.reset(nullptr);
    }
    cTree = callXcbQueryTree(parentId)->uniquePtr();

    if (!cGeom->force()) {
      d->isMapped = false;
      d->visualId = 0;
      return;
    }
    auto geom = cGeom->result();
    d->frameGeom = QRect(geom->x, geom->y, geom->width, geom->height);
    if (parentId == d->windowId) {
      d->geom = d->frameGeom; // QRect(0, 0, geom->width, geom->height);
    } else if (parentId && parentId != d->rootId) {
      offsetX += geom->x;
      offsetY += geom->y;
    }

    if (cExtent && cExtent->force()) {
      extents = xcbPropertyValue<QVector<quint32>>(cExtent->result());
      extentDepth = 0;
      extentGeom = d->geom;
    } else if (extentDepth >= 0) {
      extentDepth++;
      if (extentDepth > 1) {
        extentDepth = -1;
        extents.clear();
      }
    }

    d->frameWindowId = parentId;
    parentId = cTree->force() ? cTree->result()->parent : d->rootId;
  } while (parentId && parentId != d->rootId);

  // translate the local geometry out of screen space
  d->geom.translate(offsetX - d->frameGeom.x(), offsetY - d->frameGeom.y());
  if (extents.size()) {
    // If we found extents, apply them
    d->extentGeom = d->geom.adjusted(-extents[0], -extents[2], extents[1], extents[3]);
  } else if (d->frameWindowId == d->windowId) {
    // If we didn't find extents, but we're looking at a top-level window, look for children
    // Default to just keeping the frame geometry if no children with extents are found
    d->extentGeom = d->geom;
    for (auto child : QueryTreeChildren(callXcbQueryTree(d->windowId)->result())) {
      auto cExtent = callXcbGetProperty(child, _NET_FRAME_EXTENTS, XCB_ATOM_CARDINAL)->uniquePtr();
      if (!cExtent->force()) {
        continue;
      }
      extents = xcbPropertyValue<QVector<quint32>>(cExtent->result());
      if (extents.isEmpty()) {
        continue;
      }
      auto cGeom = callXcbGetGeometry(child)->uniquePtr();
      if (!cGeom->force()) {
        continue;
      }
      auto rect = cGeom->result();
      d->childWindowId = child;
      d->childGeom = QRect(rect->x + d->frameGeom.x(), rect->y + d->frameGeom.y(), rect->width, rect->height);
      d->extentGeom = QRect(
          rect->x - extents[0],
          rect->y - extents[2],
          rect->width + extents[0] + extents[1],
          rect->height + extents[2] + extents[3]
      );
      break;
    }
  } else {
    // Otherwise assume the extents match the geometry
    d->extentGeom = d->geom;
  }
}

QImage GrabWindow::grab()
{
  return grab(geometry().translated(-geometry().topLeft()));
}

QImage GrabWindow::grab(const QRect& rect)
{
  QImage result;
  int frame = frameWindowId();
  QRect screenGeom = qApp->primaryScreen()->geometry().translated(-frameGeometry().topLeft());
  QRect adjusted = rect.translated(geometry().topLeft()).intersected(d->extentGeom).intersected(screenGeom);
  if (callCompositeRedirectSubwindows(frame, XCB_COMPOSITE_REDIRECT_AUTOMATIC)->force()) {
    quint32 pixmap = xcb_generate_id(c_X11);
    if (callCompositeNameWindowPixmap(frame, pixmap)->force()) {
      result = pixmapToImage(pixmap, adjusted);
      callXcbFreePixmap(pixmap);
    }
    callCompositeUnredirectWindows(frame, XCB_COMPOSITE_REDIRECT_AUTOMATIC)->force();
  }
  if (result.isNull()) {
    auto wt = WindowTracker::instance();
    auto stack = wt->stackingOrder();
    callXcbConfigureWindow(frame, XCB_CONFIG_WINDOW_STACK_MODE, { XCB_STACK_MODE_ABOVE })->force();
    result = pixmapToImage(frame, adjusted);
    wt->restoreStackingOrder(stack);
  }
  return result;
}

void GrabWindow::markClean()
{
  if (d->damageId) {
    callDamageSubtract(d->damageId);
  }
}

QImage GrabWindow::pixmapToImage(quint32 pixmapId, const QRect& rect)
{
  if (rect.isEmpty()) {
    return QImage();
  }
  auto imageCookie = callXcbGetImage(XCB_IMAGE_FORMAT_Z_PIXMAP, pixmapId, rect, 0x00FFFFFF)->uniquePtr();
  if (imageCookie->force()) {
    return QImage(xcb_get_image_data(imageCookie->result()), rect.width(), rect.height(), QImage::Format_RGB32).copy();
  } else {
    return QImage();
  }
}

/*
void GrabWindow::clearShape() {
  d->shape = QRegion();
  if (d->shapeId) {
    callDestroyRegion(d->shapeId);
    d->shapeId = 0;
  }
}

void GrabWindow::setShape(const QRegion& region, bool isClip) {
  if (d->shapeId) {
    clearShape();
  }
  d->shape = region;
  d->shapeId = xcb_generate_id(c_X11);
  bool ok = callGetWindowRegion(d->shapeId, d->windowId, isClip ? XCB_SHAPE_SK_CLIP : XCB_SHAPE_SK_BOUNDING)->force();
  if (!ok) {
    d->shapeId = 0;
  }
}

QRegion GrabWindow::shape() const {
  return d->shape;
}

void GrabWindow::queryShape(bool isClip) {
  QRegion rgn;
  auto cookie = callShapeGetRects(d->windowId, isClip ? XCB_SHAPE_SK_CLIP : XCB_SHAPE_SK_BOUNDING)->uniquePtr();
  if (cookie->force()) {
    for (const auto& rect : ShapeRectList(cookie->result())) {
      rgn += QRect(rect.x, rect.y, rect.width, rect.height);
    }
    setShape(rgn, isClip);
  } else {
    clearShape();
  }
}
*/

void GrabWindow::drawOnto(quint32 picture, const QRect& clipRect, const QPoint& pos)
{
  int offsetX = d->geom.left() - clipRect.left() + pos.x();
  int offsetY = d->geom.top() - clipRect.top() + pos.y();
  if (d->shapeId) {
    auto cookie = callRenderSetClipRegion(picture, d->shapeId, offsetX, offsetY)->uniquePtr();
    if (!cookie->force()) {
      DEBUG(CrudeXcb::translateError(cookie->error()->error_code));
    }
  }
  auto cookie = callRenderComposite(
                    isAlpha() ? XCB_RENDER_PICT_OP_OVER : XCB_RENDER_PICT_OP_SRC,
                    renderId(),
                    picture,
                    0,
                    0,
                    offsetX,
                    offsetY,
                    d->geom.width(),
                    d->geom.height()
  )
                    ->uniquePtr();
  if (!cookie->force()) {
    DEBUG(CrudeXcb::translateError(cookie->error()->error_code));
  }
  if (d->shapeId) {
    auto cookie = callRenderSetClipRect(picture, 0, 0, QRect(0, 0, clipRect.width(), clipRect.height()))->uniquePtr();
    if (!cookie->force()) {
      DEBUG(CrudeXcb::translateError(cookie->error()->error_code));
    }
  }
}

void GrabWindow::setDamageEnabled(bool on)
{
  if (!on && d->damageId) {
    callDamageDestroy(d->damageId);
    d->damageId = 0;
  } else if (on && !d->damageId) {
    d->damageId = xcb_generate_id(c_X11);
    callDamageCreate(d->damageId, d->windowId, XCB_DAMAGE_REPORT_LEVEL_NON_EMPTY);
  }
}
