#include "utility.h"

#include <QMap>
#include <QSet>
#include <QtDebug>

static QMap<xcb_render_pictformat_t, RenderPictFormat> formatsById;
static QMap<xcb_visualid_t, RenderPictFormat> formatsByVisual;
static QSet<xcb_visualid_t> alphaVisuals;
static xcb_render_pictformat_t argb32 = 0;

static void loadFormats()
{
  auto qpcookie = callRenderQueryPictFormats()->uniquePtr();
  if (!qpcookie->force() || qpcookie->error()) {
    qFatal("could not get pict formats");
  }
  auto reply = qpcookie->result();
  for (const auto& iter : RenderPictFormats(reply)) {
    formatsById[iter.id] = iter;
  }
  for (const auto& screen : RenderPictScreens(reply)) {
    // TODO: what do I do with this?
    //xcb_render_pictformat_t fallback = screen->fallback;
    for (const auto& depth : RenderPictDepths(screen)) {
      for (const auto& visual : RenderPictVisuals(depth)) {
        formatsByVisual[visual.visual] = formatsById[visual.format];
        if (depth->depth == 32) {
          alphaVisuals << visual.visual;
        }
        if (depth->depth == 32 && argb32 == 0) {
          auto direct = formatsById[visual.format].direct;
          if (direct.alpha_shift == 24 && direct.red_shift == 16 && direct.green_shift == 8 && direct.blue_shift == 0) {
            argb32 = visual.format;
          }
        }
      }
    }
  }
}

xcb_render_pictformat_t getRenderFormatForVisual(xcb_visualid_t visual)
{
  if (formatsByVisual.isEmpty()) {
    // It's synchronous, but it only happens once.
    loadFormats();
  }
  auto iter = formatsByVisual.find(visual);
  if (iter == formatsByVisual.end()) {
    return XCB_NONE;
  }
  return iter->id;
}

xcb_render_pictformat_t getRenderFormatForARGB32()
{
  if (formatsByVisual.isEmpty()) {
    // It's synchronous, but it only happens once.
    loadFormats();
  }
  if (!argb32) {
    qDebug() << "XXX: could not find ARGB32 format";
  }
  return argb32;
}

bool isAlphaVisual(xcb_visualid_t visual)
{
  return alphaVisuals.contains(visual);
}
