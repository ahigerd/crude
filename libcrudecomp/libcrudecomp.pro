TEMPLATE = lib
TARGET = crudecomp
CONFIG += staticlib create_prl
QT = core widgets x11extras
include(../crude-common.pri)
LIBS += -L.. -lcrude

HEADERS += grabwindow.h   windowtracker.h   utility.h
SOURCES += grabwindow.cpp windowtracker.cpp utility.cpp
