#ifndef CCOMP_GRABWINDOW_H
#define CCOMP_GRABWINDOW_H

#include <QByteArray>
#include <QImage>
#include <QRect>
#include <QRegion>
#include <QString>

#include <memory>

class GrabWindowPrivate;

class GrabWindow
{
public:
  static QImage pixmapToImage(quint32 pixmapId, const QRect& rect);

  GrabWindow(quint32 winId);
  ~GrabWindow();

  quint32 windowId() const;
  quint32 frameWindowId() const;
  quint32 childWindowId() const;
  bool isMapped() const;
  bool isAlpha() const;
  quint32 visualId() const;
  quint32 renderId() const;

  void setDamageEnabled(bool on);

  // The window's geometry in its frame's coordinate system
  QRect geometry() const;
  // The drawable part of the frame in the frame's coordinate system
  QRect extentsGeometry() const;
  // The frame's geometry in the screen coordinate system
  QRect frameGeometry() const;
  // The frame's primary child's geometry in the screen coordinate system
  QRect childGlobalGeometry() const;

  /*
  void queryShape(bool isClip);
  void clearShape();
  void setShape(const QRegion& region, bool isClip);
  QRegion shape() const;
  */

  // The rect is in window coordinates, not global coordinates
  QImage grab(const QRect& rect);
  // Grab the selected window
  QImage grab();
  // Draw part of the window (in screen coordinates) onto an XRender picture
  void drawOnto(quint32 picture, const QRect& clipRect, const QPoint& pos = QPoint());

  void fetchProps();
  void markClean();

  inline QByteArray toHex() const { return QByteArray::number(windowId(), 16); }

private:
  std::unique_ptr<GrabWindowPrivate> d;
  /*
  void ensureAttributes();
  bool ancestorStep();
  bool geometryStep();
  bool pictureStep();
  */
};

#endif
