#ifndef CCOMP_UTILITY_H
#define CCOMP_UTILITY_H

#include "../libcrude/xcb/xcbrequest_render.h"

#include <QRect>

xcb_render_pictformat_t getRenderFormatForVisual(xcb_visualid_t visual);
xcb_render_pictformat_t getRenderFormatForARGB32();
bool isAlphaVisual(xcb_visualid_t visual);

template <typename T>
QRect extractRect(const T& orig)
{
  return QRect(orig.x, orig.y, orig.width, orig.height);
}

#endif
