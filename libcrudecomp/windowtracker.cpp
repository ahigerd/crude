#include "windowtracker.h"

#include "grabwindow.h"
#include "libcrude/xcb/xcbextension.h"
#include "libcrude/xcb/xcbrequest_fixes.h"
#include "libcrude/xcb/xcbrequest_shape.h"
#include "libcrude/xcb/xcbrequest_wm.h"
#include "libcrude/xcb/xcbselectionmanager.h"
#include "utility.h"

#include <QScreen>
#include <QX11Info>

#include <algorithm>

static WindowTracker* WindowTracker_instance = nullptr;
static bool didInit = false;
static bool shapeOK = false;
static std::vector<quint32> lastStack;

void WindowTracker::initExtensions()
{
  if (didInit) {
    return;
  }
  didInit = true;
  bool compositeOK = CrudeXcb::checkExtension(CrudeXcb::composite, 0, 4);
  bool renderOK = CrudeXcb::checkExtension(CrudeXcb::render, 0, 11);
  bool fixesOK = CrudeXcb::checkExtension(CrudeXcb::xfixes, 5, 0);
  bool damageOK = CrudeXcb::checkExtension(CrudeXcb::damage, 1, 1);
  shapeOK = CrudeXcb::checkExtension(CrudeXcb::shape, 1, 1);
  if (!compositeOK || !renderOK || !fixesOK || !damageOK) {
    qFatal(
        "Could not initialize extensions:%s%s%s%s",
        compositeOK ? " composite" : "",
        renderOK ? " render" : "",
        fixesOK ? " fixes" : "",
        damageOK ? " damage" : ""
    );
  }
}

WindowTracker* WindowTracker::instance()
{
  return WindowTracker_instance;
}

bool WindowTracker::hasCompositeManager()
{
  XcbSelectionManager cm("_NET_WM_CM_S0");
  return cm.ownerId();
}

bool WindowTracker::supportsShape()
{
  return shapeOK;
}

WindowTracker::WindowTracker() : trackDamage(false)
{
  initExtensions();

  WindowTracker_instance = this;
  XcbIntegration::instance()->addEventHandler(this);
  root = QX11Info::appRootWindow();
  rootWindow = new GrabWindow(root);
  callXcbChangeAttributes(
      root, XCB_CW_EVENT_MASK, { XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_STRUCTURE_NOTIFY }
  );
  for (quint32 winId : stackingOrder()) {
    trackNewWindow(winId);
  }
  // TODO: capture screen geometry updates at runtime
  // TODO: multihead
  screenGeom = qApp->primaryScreen()->geometry();

  dirtyThrottle.setInterval(5);
  dirtyThrottle.setSingleShot(true);
  QObject::connect(&dirtyThrottle, SIGNAL(timeout()), this, SLOT(emitDirty()));
}

WindowTracker::~WindowTracker()
{
  XcbIntegration::instance()->removeEventHandler(this);
  if (WindowTracker_instance == this) {
    WindowTracker_instance = nullptr;
  }
  for (GrabWindow* win : windows) {
    delete win;
  }
  delete rootWindow;
}

std::vector<quint32> WindowTracker::stackingOrder()
{
  return QueryTreeChildren(callXcbQueryTree(QX11Info::appRootWindow())->result());
}

std::vector<quint32> WindowTracker::visibleStackingOrder()
{
  int capacity = lastStack.size();
  lastStack.clear();
  lastStack.reserve(capacity);
  for (quint32 win : stackingOrder()) {
    if (std::find(ignored.begin(), ignored.end(), win) != ignored.end()) {
      continue;
    }
    if (isWindowVisible(win)) {
      lastStack.push_back(win);
    }
  }
  return lastStack;
}

void WindowTracker::restoreStackingOrder(const std::vector<quint32>& stack)
{
  xcb_window_t previous = 0;
  callGrabServer()->force();
  for (auto iter = stack.begin(); iter != stack.end(); iter++) {
    xcb_window_t other = *iter;
    if (previous) {
      callXcbConfigureWindow(
          other, XCB_CONFIG_WINDOW_SIBLING | XCB_CONFIG_WINDOW_STACK_MODE, { previous, XCB_STACK_MODE_ABOVE }
      );
    }
    previous = other;
  }
  callUngrabServer()->force();
  lastStack.clear();
}

bool WindowTracker::handleEvent(const CrudeXcb::EventType& extEvent)
{
  switch (extEvent.extension) {
  case CrudeXcb::core:
    switch (extEvent.eventType) {
    case XCB_CIRCULATE_NOTIFY: restacked(); return true;
    case XCB_CREATE_NOTIFY: trackNewWindow(extEvent.cast<xcb_create_notify_event_t>()->window); return true;
    case XCB_MAP_NOTIFY: trackNewWindow(extEvent.cast<xcb_map_notify_event_t>()->window); return true;
    case XCB_DESTROY_NOTIFY: windowGone(extEvent.cast<xcb_destroy_notify_event_t>()->window); return true;
    case XCB_UNMAP_NOTIFY: windowGone(extEvent.cast<xcb_unmap_notify_event_t>()->window); return true;
    case XCB_REPARENT_NOTIFY: {
      auto e = extEvent.cast<xcb_reparent_notify_event_t>();
      if (e->parent == root) {
        trackNewWindow(e->window);
      } else {
        windowGone(e->window);
      }
    }
      return true;
    case XCB_VISIBILITY_NOTIFY: {
      uint32_t winId = extEvent.cast<xcb_visibility_notify_event_t>()->window;
      GrabWindow* win = windows.value(winId);
      if (win) {
        markDirty(winId, win->frameGeometry());
      }
    }
      return true;
    case XCB_EXPOSE: markDirty(0, extractRect(*extEvent.cast<xcb_expose_event_t>())); return true;
    case XCB_GRAPHICS_EXPOSURE:
    case XCB_NO_EXPOSURE:
      // Events that we get opted into by asking for exposures, but we don't need
      return true;
    case XCB_CONFIGURE_NOTIFY: {
      auto e = extEvent.cast<xcb_configure_notify_event_t>();
      markDirty(e->window, extractRect(*e));
      updateWindow(e->window);
    }
      return true;
    }
    break;
  case CrudeXcb::damage:
    if (trackDamage) {
      auto dEvent = extEvent.cast<xcb_damage_notify_event_t>();
      auto win = windows.value(dEvent->drawable);
      if (win) {
        QRect dirty(
            dEvent->geometry.x + dEvent->area.x, dEvent->geometry.y + dEvent->area.y, dEvent->area.width, dEvent->area.height
        );
        markDirty(dEvent->drawable, dirty);
      }
    }
    return true;
  case CrudeXcb::shape: {
    auto sEvent = extEvent.cast<xcb_shape_notify_event_t>();
    GrabWindow* win = windows.value(sEvent->affected_window);
    if (!win) {
      return false;
    }
    /*
        if (sEvent->shape_kind == XCB_SHAPE_SK_CLIP || sEvent->shape_kind == XCB_SHAPE_SK_BOUNDING) {
          if (sEvent->shaped) {
            win->queryShape(sEvent->shape_kind == XCB_SHAPE_SK_CLIP);
          } else {
            win->clearShape();
          }
        }
        */
  }
    return true;
  default: break;
  }
  return false;
}

void WindowTracker::trackNewWindow(quint32 winId)
{
  restacked();
  if (ignored.contains(winId)) {
    return;
  }
  GrabWindow* win = windows.value(winId);
  if (!win) {
    windows[winId] = win = new GrabWindow(winId);
    win->setDamageEnabled(trackDamage);
  }
  markDirty(winId, win->geometry());
}

void WindowTracker::windowGone(quint32 winId)
{
  restacked();
  GrabWindow* win = windows.take(winId);
  if (!win) {
    // already untracked
    return;
  }
  dirtyArea += win->geometry();
  delete win;
}

void WindowTracker::updateWindow(quint32 winId)
{
  if (!windows.contains(winId)) {
    // This might happen if we miss an event somehow (e.g. during startup).
    // Handle it gracefully.
    trackNewWindow(winId);
    return;
  }
  GrabWindow* win = windows[winId];
  dirtyArea += win->geometry();
  win->fetchProps();
  dirtyArea += win->geometry();
}

void WindowTracker::markDirty(quint32 dirty, const QRect& rect)
{
  if (!trackDamage) {
    // Damage tracking is disabled, so just ignore it.
    return;
  }
  if (dirty == 0) {
    // force the region to be dirty without querying
    dirtyArea += rect;
    if (!dirtyThrottle.isActive()) {
      dirtyThrottle.start();
    }
    return;
  }
  if (ignored.contains(dirty)) {
    return;
  }
  auto stack = stackingOrder();
  QRegion visible(screenGeom);
  for (auto iter = stack.crbegin(); iter != stack.crend(); iter++) {
    quint32 winId = *iter;
    if (ignored.contains(winId)) {
      continue;
    }
    GrabWindow* win = windows.value(winId);
    if (win && win->isMapped() && visible.intersects(win->frameGeometry())) {
      if (winId == dirty && !visible.intersects(rect)) {
        // The dirty window is exposed, but the dirty region within it is occluded
        return;
      }
      visible -= win->frameGeometry();
    }
    if (winId == dirty) {
      dirtyArea += rect;
      if (!dirtyThrottle.isActive()) {
        dirtyThrottle.start();
      }
      return;
    } else if (!visible.intersects(rect)) {
      // the dirty region is obscured
      return;
    }
  }
}

void WindowTracker::emitDirty()
{
  emit regionDirty(dirtyArea);
  for (GrabWindow* win : windows) {
    win->markClean();
  }
}

void WindowTracker::restacked()
{
  lastStack.clear();
}

QImage WindowTracker::grab(const QRect& rect)
{
  // TODO: optimize for incremental
  // TODO: capture mouse pointer
  quint32 pixmap = xcb_generate_id(c_X11);
  quint32 picture = xcb_generate_id(c_X11);

  bool ok = callXcbCreatePixmap(32, pixmap, root, rect.width(), rect.height())->force();
  if (!ok) {
    return QImage();
  }
  int format = getRenderFormatForARGB32();
  xcb_render_create_picture_value_list_t value;
  value.subwindowmode = 1;
  ok = callRenderCreatePicture(picture, pixmap, format, XCB_RENDER_CP_SUBWINDOW_MODE, value)->force();
  if (!ok) {
    callXcbFreePixmap(pixmap);
    return QImage();
  }
  bool didComposite = callCompositeRedirectSubwindows(root, XCB_COMPOSITE_REDIRECT_AUTOMATIC)->force();

  QRect pixRect(0, 0, rect.width(), rect.height());
  rootWindow->drawOnto(picture, rect);
  if (didComposite) {
    callCompositeUnredirectWindows(root, XCB_COMPOSITE_REDIRECT_AUTOMATIC)->force();
  } else {
    // Not composited: need to grab all windows manually
    /*
    for (quint32 winId : stack) {
      GrabWindow* win = windows.value(winId);
      if (!win || !win->isMapped()) continue;
      if (!rect.intersects(win->extentsGeometry())) {
        continue;
      }
      win->drawOnto(picture, rect);
    }
    */
  }
  QImage result = GrabWindow::pixmapToImage(pixmap, pixRect);
  callRenderFreePicture(picture);
  callXcbFreePixmap(pixmap);
  return result;
}

QImage WindowTracker::grab()
{
  return grab(screenGeom);
}

GrabWindow* WindowTracker::windowAt(const QPoint& point, bool includeFrame) const
{
  if (lastStack.empty()) {
    stackingOrder();
  }
  for (auto iter = lastStack.crbegin(); iter != lastStack.crend(); iter++) {
    quint32 winId = *iter;
    if (ignored.contains(winId)) {
      continue;
    }
    GrabWindow* win = windows.value(winId);
    if (win && win->isMapped() && (includeFrame ? win->frameGeometry() : win->childGlobalGeometry()).contains(point)) {
      return win;
    }
  }
  return nullptr;
}

void WindowTracker::ignoreWindow(quint32 winId)
{
  ignored << winId;
  GrabWindow* win = windows.take(winId);
  if (win) {
    delete win;
  }
}

bool WindowTracker::isWindowVisible(quint32 winId) const
{
  GrabWindow* win = windows.value(winId);
  return win && win->isMapped();
}

QVector<GrabWindow*> WindowTracker::visibleWindows() const
{
  QVector<GrabWindow*> result;
  if (lastStack.empty()) {
    stackingOrder();
  }
  QRegion visible(screenGeom);
  for (auto iter = lastStack.crbegin(); iter != lastStack.crend(); iter++) {
    quint32 winId = *iter;
    if (ignored.contains(winId)) {
      continue;
    }
    GrabWindow* win = windows.value(winId);
    if (!win || !win->isMapped()) {
      continue;
    }
    if (visible.intersects(win->frameGeometry())) {
      result.push_front(win);
      visible -= win->frameGeometry();
    }
  }
  return result;
}

QVector<GrabWindow*> WindowTracker::visibleWindows(const QRect& intersect) const
{
  QVector<GrabWindow*> result;
  if (lastStack.empty()) {
    stackingOrder();
  }
  QRegion visible(screenGeom);
  for (auto iter = lastStack.crbegin(); iter != lastStack.crend(); iter++) {
    quint32 winId = *iter;
    if (ignored.contains(winId)) {
      continue;
    }
    GrabWindow* win = windows.value(winId);
    if (!win || !win->isMapped()) {
      continue;
    }
    if (visible.intersects(win->frameGeometry())) {
      visible -= win->frameGeometry();
      if (win->frameGeometry().intersects(intersect)) {
        result.push_front(win);
      }
    }
  }
  return result;
}

void WindowTracker::setDamageEnabled(bool on)
{
  trackDamage = on;
  rootWindow->setDamageEnabled(on);
  for (GrabWindow* win : windows) {
    win->setDamageEnabled(on);
  }
}
