#ifndef CCOMP_WINDOWTRACKER_H
#define CCOMP_WINDOWTRACKER_H

#include "libcrude/xcb/xcbintegration.h"

#include <QMap>
#include <QObject>
#include <QRegion>
#include <QTimer>

#include <vector>
class GrabWindow;

class WindowTracker : public QObject, public XcbHandlerInterface
{
  Q_OBJECT

public:
  static void initExtensions();
  static WindowTracker* instance();
  static bool hasCompositeManager();
  static bool supportsShape();

  WindowTracker();
  ~WindowTracker();

  static std::vector<quint32> stackingOrder();
  static void restoreStackingOrder(const std::vector<quint32>& stack);
  std::vector<quint32> visibleStackingOrder();

  void setDamageEnabled(bool on);
  bool handleEvent(const CrudeXcb::EventType& event);

  QImage grab(const QRect& rect);
  QImage grab();

  QVector<GrabWindow*> visibleWindows() const;
  QVector<GrabWindow*> visibleWindows(const QRect& intersect) const;
  GrabWindow* windowAt(const QPoint& point, bool includeFrame = true) const;
  bool isWindowVisible(quint32 winId) const;
  void ignoreWindow(quint32 winId);

signals:
  void regionDirty(const QRegion& region);

private slots:
  void emitDirty();

private:
  void trackNewWindow(quint32 winId);
  void windowGone(quint32 winId);
  void updateWindow(quint32 winId);
  void markDirty(quint32 winId, const QRect& rect);
  void restacked();

  quint32 root;
  GrabWindow* rootWindow;
  QMap<quint32, GrabWindow*> windows;
  QVector<quint32> ignored;
  QRegion dirtyArea;
  QRect screenGeom;
  QTimer dirtyThrottle;
  bool trackDamage;
};

#endif
