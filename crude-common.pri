OBJECTS_DIR = .tmp
MOC_DIR = .tmp
RCC_DIR = .tmp
CONFIG += debug link_prl
VER_MAJ = 0
VER_MIN = 0
VER_PAT = 1
VERSION = $${VER_MAJ}.$${VER_MIN}.$${VER_PAT}
!defined(INSTALL_BINS): INSTALL_BINS = /usr/local/bin/
!defined(INSTALL_PLUGINS): INSTALL_PLUGINS = /usr/local/lib/crude/
DEFINES += CRUDE_VERSION=\'\"$${VERSION}\"\' CRUDE_PLUGIN_PATH=\'\"$${INSTALL_PLUGINS}\"\'
DESTDIR = $$PWD
INCLUDEPATH += $$_PRO_FILE_PWD_ $$PWD

contains(TEMPLATE,app) {
  target.path = $${INSTALL_BINS}
  INSTALLS += target
}
plugin {
  target.path = $${INSTALL_PLUGINS}
  INSTALLS += target
}
